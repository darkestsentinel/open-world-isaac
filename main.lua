local oworld = require "scripts.openworld.init"

oworld.FloorsInit()

if include then
	include("modes.emptyworld")
	include("modes.gridtest")
	include("modes.floortest")
	include("modes.multifloortest")
	include("modes.zombie_apocalypse")
	
else
	require "modes.emptyworld"
	require "modes.gridtest"
	require "modes.floortest"
	require "modes.multifloortest"
	require "modes.zombie_apocalypse"
end

-- Yellow Key --

oworld.YELLOW_KEY = Isaac.GetItemIdByName("Yellow Key") 

oworld:AddCallback(ModCallbacks.MC_USE_ITEM, function(_, itemid, rng)
	local player = Isaac.GetPlayer(0)
	local cdoorspot, closestdistance
	local currentroom = oworld.GetCurrentRoom()
	for _,doorspot in ipairs(oworld.PossibleRoomDoorLocations) do
		if currentroom:IsDoorSlotAllowed(doorspot.Slot) then
			local pos = oworld.GetPositionByWorldTile(doorspot.Tile)
			local dist = (player.Position-pos):LengthSquared()
			if not cdoorspot or dist < closestdistance then
				cdoorspot = doorspot
				closestdistance = dist
			end
		end
	end
	
	if cdoorspot and closestdistance <= 20000 then
		local addvectorbasedonrotation = {[0]=Vector(0,-1), [90]=Vector(1,0), [180]=Vector(0,1), [270]=Vector(-1,0)}
		local roomtile = oworld.GetRoomTileByWorldPos(oworld.WorldPlayerPos)
		local room = oworld.AddRoomToFloor(roomtile+addvectorbasedonrotation[cdoorspot.Rotation], RoomShape.ROOMSHAPE_1x1, RoomType.ROOM_DEFAULT, true)
		return true
	end
end, oworld.YELLOW_KEY)

if not oworld.DoorSprite then
	oworld.DoorSprite = Sprite()
	oworld.DoorSprite:Load("gfx/grid/door_01_normaldoor.anm2", true)
	oworld.DoorSprite:SetFrame("Closed", 0)
	oworld.DoorSprite.Color = Color(1,1,1,0.3,0,0,0)
end

oworld:AddCallback(ModCallbacks.MC_POST_PLAYER_RENDER, function(_, player)
	if player.Index ~= Isaac.GetPlayer(0).Index then return end
	if player:GetActiveItem() == oworld.YELLOW_KEY then
		local doorspots_toremove = {}
		for k,doorspot in ipairs(oworld.PossibleRoomDoorLocations) do
			local grid = oworld.GetGridEntity(doorspot.Tile)
			if not grid or grid.Desc.Type ~= GridEntityType.GRID_WALL then
				table.insert(doorspots_toremove, k)
			else
				local pos = oworld.GetPositionByWorldTile(doorspot.Tile)
				if (player.Position-pos):LengthSquared() <= 20000 then
					oworld.DoorSprite.Rotation = doorspot.Rotation
					oworld.DoorSprite:Render(Isaac.WorldToScreen(pos+Vector(0,20):Rotated(doorspot.Rotation)), oworld.utils.VEC_ZERO, oworld.utils.VEC_ZERO)
					break
				end
			end
		end
		
		for i,k in ipairs(doorspots_toremove) do
			table.remove(oworld.PossibleRoomDoorLocations, k-(i-1))
		end
	end
end)