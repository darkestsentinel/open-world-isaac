local oworld = require "scripts.openworld"

oworld.AddGameMode({
	name = "MultiFloorTest",
	isfloormode = true,
	options = {
		{name="LevelStage", var="LevelStage", defaultvalue=LevelStage.STAGE1_1, lowestvalue = LevelStage.STAGE1_1, highestvalue = LevelStage.STAGE7},
		{name="Custom Shapes", var="CustomShapesEnabled", defaultvalue=false},
		{name="Width", var="WorldWidth", defaultvalue=3000, lowestvalue = oworld.TilesOnScreenActive.X*4, step=10},
		{name="Height", var="WorldHeight", defaultvalue=3000, lowestvalue = oworld.TilesOnScreenActive.Y*4, step=10},
		{name="Number of Stages", var="MultiFloorNumExtraStages", defaultvalue=5, lowestvalue=0}
	},
	generateworld = function()
		Isaac.GetPlayer(0):AddCollectible(CollectibleType.COLLECTIBLE_TREASURE_MAP, 0, false)
		Isaac.GetPlayer(0):AddCollectible(CollectibleType.COLLECTIBLE_COMPASS, 0, false)
		
		for numExtraStages=1, oworld.MultiFloorNumExtraStages do
			oworld.utils.DelayFunction(function()
				oworld.LevelStage = oworld.LevelStage + 1
				for i=1, oworld.TotalRoomIds do
					local room = oworld.Rooms[oworld.TotalRoomIds - (i - 1)]
					
					if room and room:GetType() == RoomType.ROOM_BOSS then
						local adj_roomTiles = {Vector(0,1), Vector(1,0), Vector(0,-1), Vector(-1,0)}
						for i2 = 1, 4 do
							local roomtile = room.RoomTile + adj_roomTiles[i2]
							
							if oworld.GetRoomByRoomTile(roomtile) then
								local startingRoomTile = room.RoomTile + adj_roomTiles[(i2+1)%4 + 1]
							
								oworld.SpawnInStage(oworld.GetRandomStageByLevelStage(oworld.LevelStage), oworld.LevelStage, nil, startingRoomTile)
								break
							end
						end
						
						break
					end
				end
			end, numExtraStages)
		end
		oworld.LevelStage = 1
	end
})

oworld:AddCallback(ModCallbacks.MC_POST_CHECK_ROOM_ALIGNMENT, function(_, roomTile, roomShape, roomType, adjRoom, allowed)
	local mode = oworld.GetCurrentMode()
	
	if mode.name == "MultiFloorTest" then
		if adjRoom.LevelStage ~= oworld.LevelStage and adjRoom:GetType() ~= RoomType.ROOM_BOSS then
			return false
		end
	end
end)