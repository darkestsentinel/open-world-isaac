local oworld = require "scripts.openworld"

oworld.AddGameMode({
	name = "Empty World",
	options = {
		{name="Width", var="WorldWidth", defaultvalue=200, lowestvalue = oworld.TilesOnScreenActive.X*4},
		{name="Height", var="WorldHeight", defaultvalue=200, lowestvalue = oworld.TilesOnScreenActive.Y*4}
	}
})