local oworld = require "scripts.openworld"

oworld.AddGameMode({
	name = "GridTest",
	generateworld = function()
		oworld.SetWorldFloorSprite("gfx/backdrop/openworld_basement_backdrop.png")
		local testgrids = {GridEntityType.GRID_DECORATION, GridEntityType.GRID_ROCK, GridEntityType.GRID_ROCKB, GridEntityType.GRID_ROCKT, GridEntityType.GRID_PIT, GridEntityType.GRID_ROCK_ALT, GridEntityType.GRID_ROCK_BOMB}
		local playertile = oworld.GetWorldTileByWorldPos(oworld.WorldPlayerPos)
		for x=1, oworld.WorldWidth do
			for y=1, oworld.WorldHeight do
				if x ~= playertile.X and y ~= playertile.Y then
					oworld.AddGridEntityData(x, y, oworld.World, testgrids[math.random(#testgrids)], 0)
				end
			end
		end
	end
})