local oworld = require "scripts.openworld"

local game = Game()
oworld.ZombieSpawnRate = 150
oworld.ZombieSpawnDelay = 0
oworld.SpawnRampUp = 1.03

oworld.AddGameMode({
	name = "Zombie Apocalypse",
	noworldboundary = true,
	options = {
		{name="Spawn Delay", var="ZombieSpawnRate", defaultvalue=150, lowestvalue=1},
		{name="Spawn Ramp Up", var="SpawnRampUp", defaultvalue=1.03, lowestvalue=1, step=0.005}
	},
	generateworld = function()
		oworld.SetWorldFloorSprite("gfx/backdrop/openworld_basement_backdrop.png")
	end
})

oworld:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	local mode = oworld.GetCurrentMode()
	if mode and mode.name == "Zombie Apocalypse" then
		oworld.ZombieSpawnDelay = math.max(0, oworld.ZombieSpawnDelay-1)
		if oworld.ZombieSpawnDelay == 0 then
			oworld.ZombieSpawnRate = oworld.ZombieSpawnRate/oworld.SpawnRampUp
			oworld.ZombieSpawnDelay = math.ceil(oworld.ZombieSpawnRate)
			
			-- chooses a random position at the screen's borders to spawn the zombie
			local randomvector = Vector(math.random()-0.5,math.random()-0.5)
			local zombiepos = Vector(0,0)
			if randomvector.Y > 0 then
				zombiepos = Vector(randomvector.X*16*40, 8*40)
			else
				zombiepos = Vector(randomvector.X*16*40, -8*40)
			end
			local zombie = Isaac.Spawn(EntityType.ENTITY_GAPER, math.random(0,1), 0, zombiepos+game:GetRoom():GetCenterPos(), oworld.utils.VEC_ZERO, nil)
			zombie:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
		end
	end
end)