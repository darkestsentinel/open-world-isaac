local oworld = require "scripts.openworld"

oworld.AddGameMode({
	name = "FloorTest",
	isfloormode = true,
	options = {
		{name="LevelStage", var="LevelStage", defaultvalue=LevelStage.STAGE1_1, lowestvalue = LevelStage.STAGE1_1, highestvalue = LevelStage.STAGE7},
		{name="Custom Shapes", var="CustomShapesEnabled", defaultvalue=false},
	}
})

--oworld.StartGameModeOnRun("FloorTest")

oworld:AddCallback(ModCallbacks.MC_PRE_GENERATE_WORLD, function()
	local mode = oworld.GetCurrentMode()
	
	if mode and mode.name == "FloorTest" then
		oworld.WorldWidth = 500
		oworld.WorldHeight = 500
	end
end)