if REPENTANCE then
	if not APIOverride then
		APIOverride = {
			OverriddenClasses = {}
		}
	end

	function APIOverride.GetClass(class)
		if type(class) == "function" then
			return getmetatable(class()).__class
		else
			return getmetatable(class).__class
		end
	end

	function APIOverride.OverrideClass(class)
		local class_mt = APIOverride.GetClass(class)

		local classDat = APIOverride.OverriddenClasses[class_mt.__type]
		if not classDat then
			classDat = {Original = class_mt, New = {}}

			local oldIndex = class_mt.__index

			rawset(class_mt, "__index", function(self, k)
				return classDat.New[k] or oldIndex(self, k)
			end)

			APIOverride.OverriddenClasses[class_mt.__type] = classDat
		end

		return classDat
	end

	function APIOverride.GetCurrentClassFunction(class, funcKey)
		local class_mt = APIOverride.GetClass(class)
		return class_mt[funcKey]
	end

	function APIOverride.OverrideClassFunction(class, funcKey, fn)
		local classDat = APIOverride.OverrideClass(class)
		classDat.New[funcKey] = fn
	end

	--[[ Example, changes the Remove function on EntityTear only, inserting a hook

	local EntityTear_Remove_Old = APIOverride.GetCurrentClassFunction(EntityTear, "Remove")

	APIOverride.OverrideClassFunction(EntityTear, "Remove", function(entity)
		print("Hooked EntityTear:Remove!")
		EntityTear_Remove_Old(entity)
	end)

	]]

else

	if not APIOverride then
		APIOverride = {
			OverridenClasses = {}
		}
	end

	local metatable = getmetatable(EntityTear)
	local function recursive__index(tbl, key, initial, noOverride)
		local meta = getmetatable(tbl)
		local val = rawget(meta, key)
		if val then
			if not noOverride then
				local initialType = getmetatable(initial or tbl).__type
				if APIOverride.OverridenClasses[initialType] and APIOverride.OverridenClasses[initialType][key] then
					return APIOverride.OverridenClasses[initialType][key]
				end
			end

			return val
		end

		local propget = rawget(meta, "__propget")
		if propget and propget[key] then
			return propget[key](initial or tbl)
		end

		local parent = rawget(meta, "__parent")
		if parent then
			return recursive__index(parent, key, initial or tbl)
		end
	end

	function APIOverride.OverrideClass(class)
		local class_mt = getmetatable(class).__class
		if not APIOverride.OverridenClasses[class_mt.__type] then
			APIOverride.OverridenClasses[class_mt.__type] = {}
			rawset(getmetatable(class).__class, "__index", recursive__index)
		end
	end

	function APIOverride.GetCurrentClassFunction(class, funcKey)
		local class_mt = getmetatable(class).__class
		if APIOverride.OverridenClasses[class_mt.__type] and APIOverride.OverridenClasses[class_mt.__type][funcKey] then
			return APIOverride.OverridenClasses[class_mt.__type][funcKey]
		else
			return recursive__index(class_mt, funcKey, nil, true)
		end
	end

	function APIOverride.OverrideClassFunction(class, funcKey, fn)
		local class_mt = getmetatable(class).__class
		APIOverride.OverrideClass(class)
		APIOverride.OverridenClasses[class_mt.__type][funcKey] = fn
	end

	--[[ Example, changes the Remove function on EntityTear only.
	APIOverride.OverrideClassFunction(EntityTear, "Remove", function()
		Isaac.DebugString("no removing for you")
	end)]]

end
