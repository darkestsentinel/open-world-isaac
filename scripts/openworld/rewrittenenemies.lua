local oworld = require "scripts.openworld"

local game = Game()
local sfx = SFXManager()

function oworld.IsWorldPosFree(worldpos, entitysize, gridcollisionclass)
	entitysize = entitysize or 0
	gridcollisionclass = gridcollisionclass or GridCollisionClass.COLLISION_SOLID
	tilecheck_distance = 1 + math.floor(entitysize/40)
	local worldtile = oworld.GetWorldTileByWorldPos(worldpos)
	for x=-tilecheck_distance + worldtile.X, tilecheck_distance + worldtile.X do
		for y=-tilecheck_distance + worldtile.Y, tilecheck_distance + worldtile.Y do
			local gridworldpos = Vector(x,y)
			local grid = oworld.GetGridEntity(gridworldpos)
			if grid and (Vector(math.max(math.min(worldpos.X, gridworldpos.X+20),gridworldpos.X-20), math.max(math.min(worldpos.Y, gridworldpos.Y+20),gridworldpos.Y-20)) - worldpos):LengthSquared() <= entitysize ^ 2 then
				if oworld.CanCollideWithGrid(gridcollisionclass, grid.CollisionClass) then
					return false
				end
			end
		end
	end
	return true
end

function oworld.GetDefaultGroundMovement(npc, speed, targetpos)
	local room = game:GetRoom()
	targetpos = targetpos or npc:GetPlayerTarget().Position
	if room:CheckLine(npc.Position, targetpos, 0, 0, false, false) then
		return (targetpos-npc.Position):Resized(speed)
	else
		return npc.Pathfinder:FindGridPath(targetpos, speed/6, 0, true, true)
	end
end

function oworld.GetRandomGroundMovement(npc, speed, frames_til_change_dir)
	local data = npc:GetData()
	data.ChangeDirectionTimer = data.ChangeDirectionTimer or frames_til_change_dir + 5
	
	if data.ChangeDirectionTimer ~= 0 then
		data.ChangeDirectionTimer = data.ChangeDirectionTimer - 1
	end
	
	if not data.TargetWorldPos then
		for i=1, 10 do
			local targetpos = npc.Position + (Vector.FromAngle(math.random()*360) * frames_til_change_dir * speed)
			targetworldpos = oworld.PositionToWorldPos(targetpos)
			if oworld.IsWorldPosFree(targetworldpos, npc.Size, npc:GetData().FakeGridCollisionClass) then
				if oworld.CheckLine(npc.Position, targetpos, 0, 0, false, false) then
					data.TargetWorldPos = targetworldpos
					data.ChangeDirectionTimer = frames_til_change_dir + 5
					break
				end
			end
		end
	end
	
	if data.TargetWorldPos then
		local targetpos = oworld.WorldPosToPosition(data.TargetWorldPos)
		if (targetpos-npc.Position):LengthSquared() <= speed ^ 2 or data.ChangeDirectionTimer == 0  then
			data.TargetWorldPos = nil
		end
		return (targetpos-npc.Position):Resized(speed)
	end
end

function oworld.GetViableMovementDirection(npc, currentDir) -- gives currentDir in case the entity should avoid going the same or opposite direction
	local dirs = {Vector(1,0), Vector(0,1), Vector(-1,0), Vector(0,-1)}
	local viable_dirs = {}
	for i=1, 4 do
		local dir = dirs[i]
		local grid = oworld.GetGridEntity(oworld.GetWorldTileByPosition(npc.Position) + dir)
		if not grid or not oworld.CanCollideWithGrid(npc:GetData().FakeGridCollisionClass, grid.CollisionClass) then
			table.insert(viable_dirs, dir)
		end
	end
	if currentDir then
		local prefered_dirs = {}
		for _,dir in ipairs(viable_dirs) do
			if math.abs(currentDir.X) ~= math.abs(currentDir.X) then
				table.insert(prefered_dirs, dir)
			end
		end
		if #prefered_dirs ~= 0 then
			viable_dirs = prefered_dirs
		end
	end
	local num_viable_dirs = #viable_dirs
	if num_viable_dirs ~= 0 then
		return viable_dirs[math.random(num_viable_dirs)]
	end
end

-- returns (Vector) Velocity, (Boolean) AlignedToGrid
function oworld.GetRandomGridMovement(npc, speed, changeDirChance, dirChangeOnlyOnGrid, tryRotatingOnlyLeftAndRight)
	local data = npc:GetData()
	if not data.MovementDirection then
		data.MovementDirection = oworld.GetViableMovementDirection(npc)
		if data.MovementDirection then
			data.TargetWorldPos = oworld.GetWorldPosByWorldTile(oworld.GetWorldTileByPosition(npc.Position) + data.MovementDirection)
		end
	end
	
	if not dirChangeOnlyOnGrid and changeDirChance and math.random() <= changeDirChance then
		data.MovementDirection = oworld.GetViableMovementDirection(npc, tryRotatingOnlyLeftAndRight and data.MovementDirection)
	end
	
	if data.MovementDirection then
		local AlignedToGrid = false
		if (data.TargetWorldPos-oworld.PositionToWorldPos(npc.Position)):LengthSquared() <= (speed/2)^2 then
			AlignedToGrid = true
			if dirChangeOnlyOnGrid and changeDirChance and math.random() <= changeDirChance then
				data.MovementDirection = oworld.GetViableMovementDirection(npc, tryRotatingOnlyLeftAndRight and data.MovementDirection)
				data.TargetWorldPos = data.MovementDirection and oworld.GetWorldPosByWorldTile(oworld.GetWorldTileByPosition(npc.Position) + data.MovementDirection)
			else
				local grid = oworld.GetGridEntity(oworld.GetWorldTileByPosition(npc.Position) + data.MovementDirection)
				if grid and oworld.CanCollideWithGrid(data.FakeGridCollisionClass, grid.CollisionClass) then
					data.MovementDirection = oworld.GetViableMovementDirection(npc, tryRotatingOnlyLeftAndRight and data.MovementDirection)
					data.TargetWorldPos = data.MovementDirection and oworld.GetWorldPosByWorldTile(oworld.GetWorldTileByPosition(npc.Position) + data.MovementDirection)
				else
					data.TargetWorldPos = oworld.GetWorldPosByWorldTile(oworld.GetWorldTileByPosition(npc.Position) + data.MovementDirection)
				end
			end
		end
		if data.TargetWorldPos then
			return (data.TargetWorldPos - oworld.PositionToWorldPos(npc.Position)):Resized(speed), AlignedToGrid
		else
			return oworld.utils.VEC_ZERO, false
		end
	else
		return oworld.utils.VEC_ZERO, false
	end
end

-- hopTargetTile is optional, it's random hopping if nil. tileRange is infinite when set on 0
function oworld.GetGroundHoppingMovement(npc, hopFrames, hopDelay, tileRange, hopTargetTile, hopAnim)
	local data = npc:GetData()
	hopAnim = hopAnim or "Hop"
	
	data.HopDelay = data.HopDelay or hopDelay
	
	if data.IsHopping then
		if not data.HopTargetTile then
			local npcWorldTile = oworld.GetWorldTileByPosition(npc.Position)
			
			if hopTargetTile then
				local targetTileGrid = oworld.GetGridEntity(hopTargetTile)
				local isInRange = not tileRange or math.abs(hopTargetTile.X - npcWorldTile.X) + math.abs(hopTargetTile.Y - npcWorldTile.Y) <= tileRange
				
				-- if hopTargetTile is in range, hop straight to it
				if isInRange and (not targetTileGrid or targetTileGrid.CollisionClass == GridCollisionClass.COLLISION_NONE) then
					data.HopTargetTile = hopTargetTile
					
				else -- if not, search for the closest tile to hopTargetTile where it can hop to
					local closestViableTile
					local closestDistanceToTargetInTiles
					for x=-tileRange, tileRange do
						for y=-tileRange, tileRange do
							if math.abs(x) + math.abs(y) <= tileRange then
								local worldtile = npcWorldTile + Vector(x,y)
								local grid = oworld.GetGridEntity(worldtile)
								
								if not grid or grid.CollisionClass == GridCollisionClass.COLLISION_NONE then
									local distToTargetInTiles = math.abs(worldtile.X - hopTargetTile.X) + math.abs(worldtile.Y - hopTargetTile.Y)
									
									if not closestViableTile or closestDistanceToTargetInTiles > distToTargetInTiles then
										closestViableTile = worldtile
										closestDistanceToTargetInTiles = distToTargetInTiles
									end
								end
							end
						end
					end
					
					if closestViableTile then
						data.HopTargetTile = closestViableTile
					else
						data.HopTargetTile = npcWorldTile
					end
				end
				
			else -- choose random tile in range to hop to
				local room = game:GetRoom()
				local validHopTile
				for i=1, 10 do -- 10 random iterations at max to find a valid tile in range, otherwise it'll jump to it's own tile
					local rTile
					if tileRange then
						rTile = npcWorldTile + Vector(math.random(-tileRange, tileRange), math.random(-tileRange, tileRange))
					else
						rTile = Vector(math.random(room.TopLeftTile.X, BottomRightTile.X), math.random(room.TopLeftTile.Y, BottomRightTile.Y))
					end
					
					if not tileRange or math.abs(rTile.X - npcWorldTile.X) + math.abs(rTile.Y - npcWorldTile.Y) then
						local grid = oworld.GetGridEntity(rTile)
						
						if not grid or grid.CollisionClass == GridCollisionClass.COLLISION_NONE then
							validHopTile = rTile
							break
						end
					end
				end
				
				if validHopTile then
					data.HopTargetTile = validHopTile
				else
					data.HopTargetTile = npcWorldTile
				end
			end
			
			local targetPos = oworld.GetPositionByWorldTile(data.HopTargetTile)
			data.HopVector = (targetPos - npc.Position) / data.FramesLeftInHop
		end
		
		local targetPos = oworld.GetPositionByWorldTile(data.HopTargetTile)
		npc.Velocity = data.HopVector
		data.FramesLeftInHop = data.FramesLeftInHop - 1
		
		if data.FramesLeftInHop == 0 then
			data.IsHopping = false
			data.HopDelay = hopDelay
			npc.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_GROUND
			data.HopTargetTile = nil
		end
		
	else
		data.HopDelay = data.HopDelay - 1
		
		if data.HopDelay <= 0 then
			data.IsHopping = true
			data.FramesLeftInHop = hopFrames
			npc:GetSprite():Play(hopAnim, true)
			npc.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_WALLS
		end
	end
end

function oworld.GetWallLockedGridMovement(npc, framesPerGrid)
	local data = npc:GetData()
	
	data.CurrentWallLockWorldTile = oworld.GetWorldTileByPosition(npc.Position)
	
	if not data.WallLockDirection then
		local allowed_dirs = {[Direction.LEFT]=true,true,true,true}
		for dir=Direction.LEFT, Direction.DOWN do
			local worldtile = data.CurrentWallLockWorldTile + oworld.utils.DirToVel[dir]
			local grid = oworld.GetGridEntity(worldtile)
			if grid and grid.CollsionClass ~= GridCollisionClass.COLLISION_NONE then
				allowed_dirs[dir] = false
			end
		end
		
		local best_dir
		for dir=Direction.LEFT, Direction.DOWN do
			if allowed_dirs[dir] and (dir == Direction.DOWN or allowed_dirs[(dir+3)%4] == false or allowed_dirs[(dir+1)%4] == false) then
				best_dir = dir
				break
			end
		end
		
		data.WallLockDirection = best_dir or Direction.DOWN
		data.WallLockTargetWorldPos = oworld.GetWorldPosByWorldTile(data.CurrentWallLockWorldTile + oworld.utils.DirToVel[data.WallLockDirection])
		data.WallLockFramesUntilGrid = framesPerGrid
	end
	
	local vel = oworld.utils.VEC_ZERO
	if data.WallLockTargetWorldPos then
		vel = (data.WallLockTargetWorldPos - oworld.PositionToWorldPos(npc.Position)) / data.WallLockFramesUntilGrid
	end
	
	if data.WallLockFramesUntilGrid then
		data.WallLockFramesUntilGrid = data.WallLockFramesUntilGrid - 1
	end
	
	if data.WallLockFramesUntilGrid == 0 then
		data.CurrentWallLockWorldTile = oworld.GetWorldTileByPosition(npc.Position)
		
		local allowed_dirs = {[Direction.LEFT]=true,true,true,true}
		allowed_dirs[(data.WallLockDirection+2)%4] = nil
		for dir=Direction.LEFT, Direction.DOWN do
			local worldtile = data.CurrentWallLockWorldTile + oworld.utils.DirToVel[dir]
			local grid = oworld.GetGridEntity(worldtile)
			if grid and grid.CollsionClass ~= GridCollisionClass.COLLISION_NONE then
				allowed_dirs[dir] = false
			end
		end
		
		local best_dir
		local start_dir = data.WallLockPreferedDirection or Direction.LEFT
		for i=start_dir, start_dir+3 do
			local dir = i%4
			if allowed_dirs[dir] and (allowed_dirs[(dir+3)%4] == false or allowed_dirs[(dir+1)%4] == false) then
				best_dir = dir
				data.WallLockPreferedDirection = allowed_dirs[(dir+3)%4] == false and (dir+3)%4 or (dir+1)%4
				break
			end
		end
		if not best_dir then
			local start_dir = data.WallLockPreferedDirection or Direction.LEFT
			for i=start_dir, start_dir+3 do
				local dir = i%4
				if dir ~= (data.WallLockDirection+2)%4 and allowed_dirs[dir] then
					for i=1, 2 do
						local worldtile = data.CurrentWallLockWorldTile + oworld.utils.DirToVel[dir] + oworld.utils.DirToVel[(dir + 2*i - 1)%4]
						local grid = oworld.GetGridEntity(worldtile)
						if grid and grid.CollsionClass ~= GridCollisionClass.COLLISION_NONE then
							best_dir = dir
							break
						end
					end
				end
				if best_dir then break end
			end
		end
		
		data.WallLockDirection = best_dir or allowed_dirs[data.WallLockDirection] ~= false and data.WallLockDirection or (data.WallLockDirection+2)%4
		data.WallLockTargetWorldPos = oworld.GetWorldPosByWorldTile(data.CurrentWallLockWorldTile + oworld.utils.DirToVel[data.WallLockDirection])
		data.WallLockFramesUntilGrid = framesPerGrid
	end
	
	return vel
end

function oworld.DefaultWalkSpriteHandler(npc, playbackSpeed, walkVertName, walkHoriName)
	local sprite = npc:GetSprite()
	if npc.Velocity.X == 0 and npc.Velocity.Y == 0 then
		sprite.PlaybackSpeed = 0
		sprite:SetFrame(walkVertName or "WalkVert", 0)
	elseif math.abs(npc.Velocity.X) >= math.abs(npc.Velocity.Y) then
		sprite.PlaybackSpeed = playbackSpeed or 1
		if npc.Velocity.X >= 0 and (not sprite:IsPlaying(walkHoriName or "WalkHori") or sprite.FlipX) then
			sprite:Play(walkHoriName or "WalkHori", true)
			sprite.FlipX = false
		elseif npc.Velocity.X < 0 and (not sprite:IsPlaying(walkHoriName or "WalkHori") or not sprite.FlipX) then
			sprite:Play(walkHoriName or "WalkHori", true)
			sprite.FlipX = true
		end
	else
		sprite.PlaybackSpeed = playbackSpeed or 1
		if npc.Velocity.Y >= 0 and (not sprite:IsPlaying(walkVertName or "WalkVert") or sprite.FlipX) then
			sprite:Play(walkVertName or "WalkVert", true)
			sprite.FlipX = false
		elseif npc.Velocity.Y < 0 and (not sprite:IsPlaying(walkVertName or "WalkVert") or not sprite.FlipX) then
			sprite:Play(walkVertName or "WalkVert", true)
			sprite.FlipX = true
		end
	end
end

function oworld.DefaultMoveSpriteHandler(npc, playbackSpeed, moveHoriName, moveDownName, moveUpName)
	local sprite = npc:GetSprite()
	if npc.Velocity.X == 0 and npc.Velocity.Y == 0 then
		sprite.PlaybackSpeed = 0
		sprite:SetFrame(moveDownName or "Move Down", 0)
	elseif math.abs(npc.Velocity.X) >= math.abs(npc.Velocity.Y) then
		sprite.PlaybackSpeed = playbackSpeed or 1
		if npc.Velocity.X >= 0 and (not sprite:IsPlaying(moveHoriName or "Move Hori") or sprite.FlipX) then
			sprite:Play(moveHoriName or "Move Hori", true)
			sprite.FlipX = false
		elseif npc.Velocity.X < 0 and (not sprite:IsPlaying(moveHoriName or "Move Hori") or not sprite.FlipX) then
			sprite:Play(moveHoriName or "Move Hori", true)
			sprite.FlipX = true
		end
	else
		sprite.PlaybackSpeed = playbackSpeed or 1
		if npc.Velocity.Y >= 0 and (not sprite:IsPlaying(moveDownName or "Move Down") or sprite.FlipX) then
			sprite:Play(moveDownName or "Move Down", true)
			sprite.FlipX = false
		elseif npc.Velocity.Y < 0 and (not sprite:IsPlaying(moveUpName or "Move Up") or not sprite.FlipX) then
			sprite:Play(moveUpName or "Move Up", true)
			sprite.FlipX = true
		end
	end
end

oworld.GaperGrowled = false

oworld.RewrittenEnemies = {
	[EntityType.ENTITY_GAPER] = {
		[-1] = {
			OnPreUpdate = function(npc) 
				npc.Velocity = npc.Velocity*0.7 + oworld.GetDefaultGroundMovement(npc, 3.5)*0.3
				oworld.DefaultWalkSpriteHandler(npc)
				npc:GetSprite():PlayOverlay("Head", true)
				
				local framecount = game:GetFrameCount()
				if not oworld.GaperGrowled and framecount%60 == 0 then
					sfx:Play(SoundEffect.SOUND_MONSTER_ROAR_0, 0.6, 0, false, 1)
					oworld.GaperGrowled = true
				elseif oworld.GaperGrowled and framecount%60 == 1 then
					oworld.GaperGrowled = false
				end
				
				if npc.Variant <= 2 then
					return true
				end
			end
		},
		[0] = { -- FROWNING GAPER
			OnPreUpdate = function(npc)
				local data,targetpos = npc:GetData(),npc:GetPlayerTarget().Position
				if not data.FrowningGaperAwake and (npc.Position-targetpos):LengthSquared() <= 160000 then
					data.FrowningGaperAwake = true
					npc:GetSprite():PlayOverlay("Head", true)
				elseif not data.FrowningGaperAwake then
					npc:GetSprite():SetOverlayFrame("Head", 0)
				end
				
				local framecount = game:GetFrameCount()
				if not oworld.GaperGrowled and framecount%30 == 0 then
					sfx:Play(SoundEffect.SOUND_MONSTER_ROAR_0, 0.6, 0, false, 1)
					oworld.GaperGrowled = true
				elseif oworld.GaperGrowled and framecount%30 == 1 then
					oworld.GaperGrowled = false
				end
				
				if data.FrowningGaperAwake then
					if npc:GetSprite():IsOverlayFinished("Head") then
						npc.Velocity = npc.Velocity*0.7 + oworld.GetDefaultGroundMovement(npc, 5.5)*0.3
					else
						npc.Velocity = npc.Velocity*0.7 + oworld.GetDefaultGroundMovement(npc, 3.5)*0.3
					end
				end
				oworld.DefaultWalkSpriteHandler(npc)
				return true
			end
		}
	},
	[EntityType.ENTITY_GUSHER] = {
		[-1] = {
			OnUpdate = function(npc)
				npc.Velocity = oworld.GetRandomGroundMovement(npc, 2, 30)
			end
		}
	},
	[EntityType.ENTITY_HORF] = {
		[-1] = {
			OnPreUpdate = function(npc)
				local targetpos = npc:GetPlayerTarget().Position
				if not oworld.CheckLine(npc.Position, targetpos, 3, 0, false, false) then
					local sprite = npc:GetSprite()
					if sprite:IsFinished("Attack") then
						sprite:Play("Shake", true)
					end
					npc.Velocity = oworld.utils.VEC_ZERO
					return true
				end
			end
		}
	},
	[EntityType.ENTITY_POOTER] = {
		[-1] = {
			OnPreUpdate = function(npc)
				local targetpos = npc:GetPlayerTarget().Position
				if not oworld.CheckLine(npc.Position, targetpos, 3, 0, false, false) then
					local sprite, data = npc:GetSprite(), npc:GetData()
					if sprite:IsFinished("Attack") then
						sprite:Play("Fly", true)
					end
					data.TargetRotation = data.TargetRotation or 0
					data.CurrentRotation = data.TargetRotation + ((data.CurrentRotation or 0)*0.5)
					data.Speed = data.Speed or 1
					if npc.FrameCount%5 == 0 then
						data.TargetRotation = math.random(-70,70)
						data.Speed = math.random()+0.5
					end
					npc.Velocity = (targetpos-npc.Position):Resized(data.Speed):Rotated(data.CurrentRotation)
					return true
				end
			end
		}
	},
	[EntityType.ENTITY_CLOTTY] = {
		[-1] = {
			OnPreUpdate = function(npc)
				local data, sprite = npc:GetData(), npc:GetSprite()
				if sprite:IsFinished("Appear") then
					sprite:Play("Hop", true)
				end
				
				if sprite:IsPlaying("Attack") then
					local frame = sprite:GetFrame()
					if frame == 10 then
						if npc.Variant == 0 then -- CLOTTY
							for i=1, 4 do
								Isaac.Spawn(EntityType.ENTITY_PROJECTILE, 0, 0, npc.Position, Vector.FromAngle(i*90)*8, npc)
							end
						elseif npc.Variant == 1 then -- CLOT
							for i=1, 4 do
								local proj = Isaac.Spawn(EntityType.ENTITY_PROJECTILE, 0, 0, npc.Position, Vector.FromAngle(i*90+45)*8, npc)
								proj:GetSprite().Color = Color(0.3,0.3,0.3,1,0,0,0)
							end
						elseif npc.Variant == 2 then -- I.BLOB
							for i=1, 8 do
								Isaac.Spawn(EntityType.ENTITY_PROJECTILE, ProjectileVariant.PROJECTILE_TEAR, 0, npc.Position, Vector.FromAngle(i*45)*6, npc)
							end
						end
					end
				end
				if sprite:IsFinished("Attack") then
					sprite:Play("Hop", true)
				end
				
				local oldvelocity = npc.Velocity
				if sprite:IsPlaying("Hop") then
					npc.Velocity = oworld.GetRandomGroundMovement(npc, 3, 20)
					if not data.TargetWorldPos then
						data.TargetWorldPos = nil
						sprite:Play("Attack", true)
						npc.Velocity = oworld.utils.VEC_ZERO
					end
				else
					npc.Velocity = oworld.utils.VEC_ZERO
				end
				npc.Velocity = (oldvelocity*0.93) + (npc.Velocity*0.12)
				sprite.FlipX = npc.Velocity.X < 0
				if npc.Variant <= 2 then
					return true
				end
			end
		}
	},
	[EntityType.ENTITY_MULLIGAN] = {
		[2] = {
			OnUpdate = function(npc)
				local data = npc:GetData()
				npc.Velocity = (data.OWPrevVelocity or npc.Velocity)*0.7 + oworld.GetDefaultGroundMovement(npc, 7)*0.3
				data.OWPrevVelocity = npc.Velocity
			end
		}
	},
	[EntityType.ENTITY_LARRYJR] = {
		[-1] = { -- THE HOLLOW
			OnPreUpdate = function(npc)
				if npc:IsDead() then return end
				local sprite, data = npc:GetSprite(), npc:GetData()
				if not data.OWInit then
					local hollows = Isaac.FindByType(EntityType.ENTITY_LARRYJR, npc.Variant, -1, false, false)
					local hollow_world_tiles = {}
					for i,hollow in ipairs(hollows) do -- checks all hollows and maps them on world tiles
						local worldtile = oworld.GetWorldTileByWorldPos(oworld.PositionToWorldPos(hollow.Position))
						hollow_world_tiles[worldtile.X] = hollow_world_tiles[worldtile.X] or {}
						hollow_world_tiles[worldtile.X][worldtile.Y] = hollow
					end
					
					local dirs = {Vector(-1,0), Vector(0,-1), Vector(1,0), Vector(0,1)}
					while true do -- checks all hollows and decides whether they should be head or body segments
						local hollow_head
						local head_worldtile
						local connection_num
						for _,hollow in ipairs(hollows) do -- checks for a hollow with the least connecting other hollows to make him head
							if not hollow:GetData().IsHeadSegment and not hollow:GetData().IsBodySegment then
								local worldtile = oworld.GetWorldTileByWorldPos(oworld.PositionToWorldPos(hollow.Position))
								local c_num = 0
								for _,dir in ipairs(dirs) do
									hollow_world_tiles[worldtile.X + dir.X] = hollow_world_tiles[worldtile.X + dir.X] or {}
									local adj_hollow = hollow_world_tiles[worldtile.X + dir.X][worldtile.Y + dir.Y]
									if adj_hollow and not adj_hollow:GetData().IsHeadSegment and not adj_hollow:GetData().IsBodySegment then
										c_num = c_num + 1
									end
								end
								
								if not hollow_head or c_num < connection_num then
									hollow_head = hollow
									head_worldtile = worldtile
									connection_num = c_num
								end
							end
						end
						
						if not hollow_head then -- no more unassigned hollows
							break
						else -- sets up a hollow head
							local head_data = hollow_head:GetData()
							head_data.IsHeadSegment = true
							head_data.BodySegments = {}
							head_data.Target_Worldpos = {oworld.PositionToWorldPos(hollow_head.Position)}
							head_data.ConstantVelocity = Vector.FromAngle(22.5 + math.random(0,7)*45)*10
							local num_segments = 0
							local check_tile = head_worldtile
							while true do -- checks which hollows should become it's body segments
								local posible_body_segments = {}
								for _,dir in ipairs(dirs) do
									hollow_world_tiles[check_tile.X + dir.X] = hollow_world_tiles[check_tile.X + dir.X] or {}
									local adj_hollow = hollow_world_tiles[check_tile.X + dir.X][check_tile.Y + dir.Y]
									if adj_hollow and not adj_hollow:GetData().IsHeadSegment and not adj_hollow:GetData().IsBodySegment then
										table.insert(posible_body_segments, adj_hollow)
									end
								end
								if #posible_body_segments ~= 0 then -- sets up a hollow body segment
									local adj_hollow = posible_body_segments[math.random(#posible_body_segments)]
									local adj_hollowdata = adj_hollow:GetData()
									adj_hollowdata.IsBodySegment = true
									adj_hollowdata.ParentHead = hollow_head
									adj_hollow.Parent = hollow_head
									
									num_segments = num_segments + 1
									adj_hollowdata.SegmentId = num_segments
									adj_hollow:GetSprite().PlaybackSpeed = num_segments*4 + 1
									adj_hollowdata.SkipPlaybackSpeedReset = true
									table.insert(head_data.BodySegments, adj_hollow)
									check_tile = oworld.GetWorldTileByWorldPos(oworld.PositionToWorldPos(adj_hollow.Position))
								else
									break
								end
							end
							head_data.NumSegments = num_segments
							if head_data.NumSegments == 0 then
								hollow_head:Die()
								return
							end
						end
					end
					
					data.OWInit = true
				end
				
				if data.IsBodySegment then -- check if segment died, assigns new head
					if data.SegmentId == 1 and data.ParentHead:IsDead() or data.SegmentId ~= 1 and not data.ParentHead:IsDead() and data.ParentHead:GetData().BodySegments[data.SegmentId - 1]:IsDead() then
						local parent_data = data.ParentHead:GetData()
						data.IsHeadSegment = true
						data.BodySegments = {}
						for i,segment in ipairs(parent_data.BodySegments) do
							if i > data.SegmentId then
								table.insert(data.BodySegments, segment)
							end
						end
						data.Target_Worldpos = {}
						for i,worldpos in ipairs(parent_data.Target_Worldpos) do
							if i > data.SegmentId then
								table.insert(data.Target_Worldpos, worldpos)
							end
						end
						data.NumSegments = parent_data.NumSegments - data.SegmentId
						
						if not data.ParentHead:IsDead() then
							for i,segment in ipairs(parent_data.BodySegments) do
								if i >= data.SegmentId then
									parent_data.BodySegments[i] = nil
								end
							end
							parent_data.NumSegments = data.SegmentId - 1
						end
						
						data.SegmentId = nil
						data.IsBodySegment = nil
						data.ParentHead = nil
						
						if data.NumSegments == 0 then
							npc:Die()
							return
						end
						
						for i,segment in ipairs(data.BodySegments) do
							segment:GetData().SegmentId = i
							segment:GetData().ParentHead = npc
						end
					end
				end
				
				if data.IsHeadSegment then -- head update code
					data.FakeGridCollisionClass = EntityGridCollisionClass.GRIDCOLL_WALLS
					
					if data.BodySegments[data.NumSegments]:IsDead() then
						table.remove(data.BodySegments, data.NumSegments)
						data.NumSegments = data.NumSegments - 1
						if data.NumSegments == 0 then
							npc:Die()
							return
						end
					end
					
					if npc:CollidesWithGrid() then
						local grid_up = oworld.GetGridEntity(oworld.GetWorldTileByPosition(Vector(npc.Position.X, npc.Position.Y-2-npc.Size)))
						local grid_down = oworld.GetGridEntity(oworld.GetWorldTileByPosition(Vector(npc.Position.X, npc.Position.Y+2+npc.Size)))
						if grid_up and (grid_up.CollisionClass == GridCollisionClass.COLLISION_WALL or grid_up.CollisionClass == GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER) or grid_down and (grid_down.CollisionClass == GridCollisionClass.COLLISION_WALL or grid_down.CollisionClass == GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER) then
							data.ConstantVelocity = Vector(data.ConstantVelocity.X, -data.ConstantVelocity.Y)
							local grid_right = oworld.GetGridEntity(oworld.GetWorldTileByPosition(Vector(npc.Position.X+2+npc.Size, npc.Position.Y)))
							local grid_left = oworld.GetGridEntity(oworld.GetWorldTileByPosition(Vector(npc.Position.X-2-npc.Size, npc.Position.Y)))
							if grid_right and (grid_right.CollisionClass == GridCollisionClass.COLLISION_WALL or grid_right.CollisionClass == GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER) or grid_left and (grid_left.CollisionClass == GridCollisionClass.COLLISION_WALL or grid_left.CollisionClass == GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER) then
								data.ConstantVelocity = Vector(-data.ConstantVelocity.X, data.ConstantVelocity.Y)
							end
						else
							data.ConstantVelocity = Vector(-data.ConstantVelocity.X, data.ConstantVelocity.Y)
						end
					end
					
					table.insert(data.Target_Worldpos, 1, data.Target_Worldpos[1] + data.ConstantVelocity)
					data.Target_Worldpos[data.NumSegments*4 + 2] = nil
					
					npc.Velocity = (oworld.WorldPosToPosition(data.Target_Worldpos[1]) -  npc.Position)
					
					-- sprite handling
					sprite.PlaybackSpeed = 1
					if math.abs(npc.Velocity.Y) > math.abs(npc.Velocity.X) then
						if npc.Velocity.Y < 0 then
							if not sprite:IsPlaying("WalkHeadUp") then
								local frame = sprite:GetFrame()
								sprite:Play("WalkHeadUp", true)
								sprite.PlaybackSpeed = 1 + frame
							end
						else
							if not sprite:IsPlaying("WalkHeadDown") then
								local frame = sprite:GetFrame()
								sprite:Play("WalkHeadDown", true)
								sprite.PlaybackSpeed = 1 + frame
							end
						end
					else
						sprite.FlipX = npc.Velocity.X < 0
						if not sprite:IsPlaying("WalkHeadHori") then
							local frame = sprite:GetFrame()
							sprite:Play("WalkHeadHori", true)
							sprite.PlaybackSpeed = 1 + frame
						end
					end
					
					for i,segment in ipairs(data.BodySegments) do -- body segments update code
						segment:GetData().FakeGridCollisionClass = GridCollisionClass.COLLISION_NONE
						if data.Target_Worldpos[i*4 + 1] then
							segment.Velocity = (oworld.WorldPosToPosition(data.Target_Worldpos[i*4 + 1]) -  segment.Position)
							
							-- the very last segment has a chance to spawn poops
							if i == data.NumSegments and math.random(1,150) == 1 then
								local target_tile = oworld.GetWorldTileByPosition(segment.Position)
								local grid = oworld.GridSpawn(GridEntityType.GRID_POOP, 0,  target_tile, false)
								if grid then
									grid.NoReward = true
								end
							end
						end
							
						local segment_sprite = segment:GetSprite()
						segment_sprite.PlaybackSpeed = 1
						if math.abs(segment.Velocity.Y) > math.abs(segment.Velocity.X) then
							if segment.Velocity.Y > 0 then
								if not segment_sprite:IsPlaying("WalkBodyUp") then
									local frame = segment_sprite:GetFrame()
									segment_sprite:Play("WalkBodyUp", true)
									segment_sprite.PlaybackSpeed = 1 + frame
								end
							else
								if not segment_sprite:IsPlaying("WalkBodyDown") or not segment_sprite:IsPlaying("Butt") then
									local frame = segment_sprite:GetFrame()
									if i == data.NumSegments then
										segment_sprite:Play("Butt", true)
									else
										segment_sprite:Play("WalkBodyDown", true)
									end
									segment_sprite.PlaybackSpeed = 1 + frame
								end
							end
						else
							segment_sprite.FlipX = segment.Velocity.X < 0
							if not segment_sprite:IsPlaying("WalkBodyHori") then
								local frame = segment_sprite:GetFrame()
								segment_sprite:Play("WalkBodyHori", true)
								segment_sprite.PlaybackSpeed = 1 + frame
							end
						end
					end
				end
				
				return true
			end
		},
		[0] = { -- LARRY JR.
			OnPreUpdate = function(npc)
				if npc:IsDead() then return end
				local sprite, data = npc:GetSprite(), npc:GetData()
				if not data.OWInit then
					local larrys = Isaac.FindByType(EntityType.ENTITY_LARRYJR, 0, -1, false, false)
					local larry_world_tiles = {}
					for i,larry in ipairs(larrys) do -- checks all larry's and maps them on world tiles
						local worldtile = oworld.GetWorldTileByWorldPos(oworld.PositionToWorldPos(larry.Position))
						larry_world_tiles[worldtile.X] = larry_world_tiles[worldtile.X] or {}
						larry_world_tiles[worldtile.X][worldtile.Y] = larry
					end
					
					local dirs = {Vector(-1,0), Vector(0,-1), Vector(1,0), Vector(0,1)}
					while true do -- checks all larry's and decides whether they should be head or body segments
						local larry_head
						local head_worldtile
						local connection_num
						for _,larry in ipairs(larrys) do -- checks for a larry with the least connecting other larry's to make him head
							if not larry:GetData().IsHeadSegment and not larry:GetData().IsBodySegment then
								local worldtile = oworld.GetWorldTileByWorldPos(oworld.PositionToWorldPos(larry.Position))
								local c_num = 0
								for _,dir in ipairs(dirs) do
									larry_world_tiles[worldtile.X + dir.X] = larry_world_tiles[worldtile.X + dir.X] or {}
									local adj_larry = larry_world_tiles[worldtile.X + dir.X][worldtile.Y + dir.Y]
									if adj_larry and not adj_larry:GetData().IsHeadSegment and not adj_larry:GetData().IsBodySegment then
										c_num = c_num + 1
									end
								end
								
								if not larry_head or c_num < connection_num then
									larry_head = larry
									head_worldtile = worldtile
									connection_num = c_num
								end
							end
						end
						
						if not larry_head then -- no more unassigned larry's
							break
						else -- sets up a larry head
							local head_data = larry_head:GetData()
							head_data.IsHeadSegment = true
							head_data.BodySegments = {}
							head_data.Target_Worldtiles = {head_worldtile}
							local num_segments = 0
							local check_tile = head_worldtile
							while true do -- checks which larry's should become it's body segments
								local posible_body_segments = {}
								for _,dir in ipairs(dirs) do
									larry_world_tiles[check_tile.X + dir.X] = larry_world_tiles[check_tile.X + dir.X] or {}
									local adj_larry = larry_world_tiles[check_tile.X + dir.X][check_tile.Y + dir.Y]
									if adj_larry and not adj_larry:GetData().IsHeadSegment and not adj_larry:GetData().IsBodySegment then
										table.insert(posible_body_segments, adj_larry)
									end
								end
								if #posible_body_segments ~= 0 then -- sets up a larry body segment
									local adj_larry = posible_body_segments[math.random(#posible_body_segments)]
									local adj_larrydata = adj_larry:GetData()
									adj_larrydata.IsBodySegment = true
									adj_larrydata.ParentHead = larry_head
									adj_larry.Parent = larry_head
									
									num_segments = num_segments + 1
									adj_larrydata.SegmentId = num_segments
									adj_larry:GetSprite().PlaybackSpeed = num_segments*4 + 1
									adj_larrydata.SkipPlaybackSpeedReset = true
									table.insert(head_data.BodySegments, adj_larry)
									table.insert(head_data.Target_Worldtiles, oworld.GetWorldTileByWorldPos(oworld.PositionToWorldPos(adj_larry.Position)))
									check_tile = oworld.GetWorldTileByWorldPos(oworld.PositionToWorldPos(adj_larry.Position))
								else
									break
								end
							end
							head_data.NumSegments = num_segments
							if head_data.NumSegments == 0 then
								larry_head:Die()
								return
							end
						end
					end
					
					data.OWInit = true
				end
				
				if data.IsBodySegment then -- check if segment died, assigns new head
					if data.SegmentId == 1 and data.ParentHead:IsDead() or data.SegmentId ~= 1 and not data.ParentHead:IsDead() and data.ParentHead:GetData().BodySegments[data.SegmentId - 1]:IsDead() then
						local parent_data = data.ParentHead:GetData()
						data.IsHeadSegment = true
						data.BodySegments = {}
						for i,segment in ipairs(parent_data.BodySegments) do
							if i > data.SegmentId then
								table.insert(data.BodySegments, segment)
							end
						end
						data.Target_Worldtiles = {}
						for i,tile in ipairs(parent_data.Target_Worldtiles) do
							if i > data.SegmentId then
								table.insert(data.Target_Worldtiles, tile)
							end
						end
						data.NumSegments = parent_data.NumSegments - data.SegmentId
						
						if not data.ParentHead:IsDead() then
							for i,segment in ipairs(parent_data.BodySegments) do
								if i >= data.SegmentId then
									parent_data.BodySegments[i] = nil
								end
							end
							parent_data.NumSegments = data.SegmentId - 1
						end
						
						data.SegmentId = nil
						data.IsBodySegment = nil
						data.ParentHead = nil
						
						if data.NumSegments == 0 then
							npc:Die()
							return
						end
						
						for i,segment in ipairs(data.BodySegments) do
							segment:GetData().SegmentId = i
							segment:GetData().ParentHead = npc
						end
					end
				end
				
				if data.IsHeadSegment then -- head update code
					data.FakeGridCollisionClass = EntityGridCollisionClass.GRIDCOLL_GROUND
					data.Speed = data.Speed or 7
					data.FramesPerGrid = data.FramesPerGrid or math.ceil(40/data.Speed)
					data.FramesPerGridTimer = data.FramesPerGridTimer or 0
					
					if data.BodySegments[data.NumSegments]:IsDead() then
						table.remove(data.BodySegments, data.NumSegments)
						data.NumSegments = data.NumSegments - 1
						if data.NumSegments == 0 then
							npc:Die()
							return
						end
					end
					
					if data.FramesPerGridTimer ~= 0 then
						data.FramesPerGridTimer = data.FramesPerGridTimer - 1
					else
						npc.Position = oworld.GetPositionByWorldTile(data.Target_Worldtiles[1])
						for i,segment in ipairs(data.BodySegments) do
							segment.Position = oworld.GetPositionByWorldTile(data.Target_Worldtiles[i + 1])
						end
						local dirs = {Vector(-1,0), Vector(0,-1), Vector(1,0), Vector(0,1)}
						local will_collide = false
						if data.MoveDirection then -- check for incoming collision
							local check_tile = data.Target_Worldtiles[1] + dirs[data.MoveDirection]
							local grid = oworld.GetGridEntity(check_tile)
							if grid and oworld.CanCollideWithGrid(data.FakeGridCollisionClass, grid.CollisionClass) then
								will_collide = true
								oworld.DamageGridEntity(grid, 1)
							end
						end
						-- change direction upon incoming collision, or at random
						if not data.MoveDirection or math.random(1,5) == 1 or will_collide then
							local viable_dirs = {}
							for i=1, 4 do
								local check_tile = data.Target_Worldtiles[1] + dirs[i]
								local grid = oworld.GetGridEntity(check_tile)
								if not grid or not oworld.CanCollideWithGrid(data.FakeGridCollisionClass, grid.CollisionClass) then
									table.insert(viable_dirs, i)
								end
							end
							
							if #viable_dirs ~= 0 then
								data.MoveDirection = viable_dirs[math.random(#viable_dirs)]
							else
								data.MoveDirection = math.random(1,4)
							end
						end
						table.insert(data.Target_Worldtiles, 1, data.Target_Worldtiles[1] + dirs[data.MoveDirection])
						data.Target_Worldtiles[data.NumSegments + 3] = nil
						data.FramesPerGridTimer = data.FramesPerGrid
					end
					
					npc.Velocity = (oworld.GetPositionByWorldTile(data.Target_Worldtiles[1]) -  npc.Position) / (data.FramesPerGridTimer + 1)
					
					-- sprite handling
					sprite.PlaybackSpeed = 1
					if math.abs(npc.Velocity.Y) > math.abs(npc.Velocity.X) then
						if npc.Velocity.Y < 0 then
							if not sprite:IsPlaying("WalkHeadUp") then
								local frame = sprite:GetFrame()
								sprite:Play("WalkHeadUp", true)
								sprite.PlaybackSpeed = 1 + frame
							end
						else
							if not sprite:IsPlaying("WalkHeadDown") then
								local frame = sprite:GetFrame()
								sprite:Play("WalkHeadDown", true)
								sprite.PlaybackSpeed = 1 + frame
							end
						end
					else
						sprite.FlipX = npc.Velocity.X < 0
						if not sprite:IsPlaying("WalkHeadHori") then
							local frame = sprite:GetFrame()
							sprite:Play("WalkHeadHori", true)
							sprite.PlaybackSpeed = 1 + frame
						end
					end
					
					for i,segment in ipairs(data.BodySegments) do -- body segments update code
						segment:GetData().FakeGridCollisionClass = GridCollisionClass.COLLISION_NONE
						segment.Velocity = (oworld.GetPositionByWorldTile(data.Target_Worldtiles[i + 1]) -  segment.Position) / (data.FramesPerGridTimer + 1)
						
						local segment_sprite = segment:GetSprite()
						segment_sprite.PlaybackSpeed = 1
						if math.abs(segment.Velocity.Y) > math.abs(segment.Velocity.X) then
							if segment.Velocity.Y > 0 then
								if not segment_sprite:IsPlaying("WalkBodyUp") then
									local frame = segment_sprite:GetFrame()
									segment_sprite:Play("WalkBodyUp", true)
									segment_sprite.PlaybackSpeed = 1 + frame
								end
							else
								if not segment_sprite:IsPlaying("WalkBodyDown") or not segment_sprite:IsPlaying("Butt") then
									local frame = segment_sprite:GetFrame()
									if i == data.NumSegments then
										segment_sprite:Play("Butt", true)
									else
										segment_sprite:Play("WalkBodyDown", true)
									end
									segment_sprite.PlaybackSpeed = 1 + frame
								end
							end
						else
							segment_sprite.FlipX = segment.Velocity.X < 0
							if not segment_sprite:IsPlaying("WalkBodyHori") then
								local frame = segment_sprite:GetFrame()
								segment_sprite:Play("WalkBodyHori", true)
								segment_sprite.PlaybackSpeed = 1 + frame
							end
						end
						
						-- the very last segment has a chance to spawn poops
						if i == data.NumSegments and math.random(1,75) == 1 then
							local target_tile = data.Target_Worldtiles[i + 2]
							local room_ents = Isaac.GetRoomEntities()
							local tile_free = true
							for _,e in ipairs(room_ents) do
								local tile = oworld.GetWorldTileByPosition(e.Position)
								if tile.X == target_tile.X and tile.Y == target_tile.Y then
									tile_free = false
									break
								end
							end
							
							if tile_free then
								local grid = oworld.GridSpawn(GridEntityType.GRID_POOP, 0,  target_tile, false)
								grid.NoReward = true
							end
						end
					end
				end
				
				return true
			end
		}
	},
	[EntityType.ENTITY_MAGGOT] = {
		[-1] = { -- MAGGOT
			OnUpdate = function(npc)
				local vel, aligned_to_grid = oworld.GetRandomGridMovement(npc, 4.25, 0.03, false, true)
				npc.Velocity = vel
			end
		}
	},
	[EntityType.ENTITY_CHARGER] = {
		[-1] = { -- CHARGER
			OnPreUpdate = function(npc)
				local data = npc:GetData()
				local target = npc:GetPlayerTarget()
				if data.IsCharging then
					if npc:CollidesWithGrid() then
						data.IsCharging = false
						data.MovementDirection = nil
					else
						npc.Velocity = data.MovementDirection*7
						oworld.DefaultMoveSpriteHandler(npc, 1, "Attack Hori", "Attack Down", "Attack Up")
					end
				end
				
				if not data.IsCharging then
					local vel, aligned_to_grid = oworld.GetRandomGridMovement(npc, 4.25, 0.03, false, true)
					local tile, target_tile = oworld.GetWorldTileByPosition(npc.Position), oworld.GetWorldTileByPosition(target.Position)
					
					if aligned_to_grid and (tile.X == target_tile.X or tile.Y == target_tile.Y)
					and (npc.Position - target.Position):LengthSquared() <= 160 ^ 2
					and oworld.CheckLine(npc.Position, target.Position, 0, 0, false, false) then
						data.MovementDirection = (target_tile - tile):Normalized()
						data.IsCharging = true
						npc.Velocity = data.MovementDirection*7
						oworld.DefaultMoveSpriteHandler(npc, 1, "Attack Hori", "Attack Down", "Attack Up")
						sfx:Play(SoundEffect.SOUND_CHILD_ANGRY_ROAR, 0.7, 0, false, 1)
					else
						npc.Velocity = vel
						oworld.DefaultMoveSpriteHandler(npc)
					end
				end
				if not npc:IsDead() then return true end
			end
		},
		[2] = { -- DANK CHARGER
			OnPreUpdate = function(npc)
				local data, sprite = npc:GetData(), npc:GetSprite()
				local target = npc:GetPlayerTarget()
				if data.IsCharging then
					if data.IsPreparingToCharge and (sprite:IsFinished("Alert Hori") or sprite:IsFinished("Alert Down") or sprite:IsFinished("Alert Up")) then
						data.IsPreparingToCharge = false
						oworld.DefaultMoveSpriteHandler(npc, 1, "Attack Hori", "Attack Down", "Attack Up")
						sfx:Play(SoundEffect.SOUND_CHILD_ANGRY_ROAR, 0.7, 0, false, 1)
					end
					if not data.IsPreparingToCharge and not data.IsRecovering then
						if npc:CollidesWithGrid() then
							data.MovementDirection = data.MovementDirection*-1
							npc.Position = npc.Position + data.MovementDirection
						end
						data.ChargeSpeed = data.ChargeSpeed - 0.25
						if data.ChargeSpeed <= 1 then
							data.ChargeSpeed = 0
							data.IsRecovering = true
							npc.Velocity = data.MovementDirection
							oworld.DefaultMoveSpriteHandler(npc, 1, "Recover Hori", "Recover Down", "Recover Up")
							npc.Velocity = oworld.utils.VEC_ZERO
						else
							npc.Velocity = data.MovementDirection*data.ChargeSpeed
							oworld.DefaultMoveSpriteHandler(npc, 1, "Attack Hori", "Attack Down", "Attack Up")
							if npc.FrameCount%2 == 0 then
								local creep = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.CREEP_BLACK, 0, npc.Position, oworld.utils.VEC_ZERO, npc)
								creep:Update()
							end
						end
					end
					if data.IsRecovering then
						npc.Velocity = oworld.utils.VEC_ZERO
						if sprite:IsFinished("Recover Hori") or sprite:IsFinished("Recover Down") or sprite:IsFinished("Recover Up") then
							data.IsCharging = false
							data.IsRecovering = false
							data.MovementDirection = nil
						end
					end
				end
				
				if not data.IsCharging then
					local vel, aligned_to_grid = oworld.GetRandomGridMovement(npc, 4.25, 0.03, false, true)
					local tile, target_tile = oworld.GetWorldTileByPosition(npc.Position), oworld.GetWorldTileByPosition(target.Position)
					
					if aligned_to_grid and (tile.X == target_tile.X or tile.Y == target_tile.Y)
					and (npc.Position - target.Position):LengthSquared() <= 160 ^ 2
					and oworld.CheckLine(npc.Position, target.Position, 0, 0, false, false) then
						data.MovementDirection = (target_tile - tile):Normalized()
						data.IsCharging = true
						data.IsPreparingToCharge = true
						data.ChargeSpeed = 20
						npc.Velocity = data.MovementDirection
						oworld.DefaultMoveSpriteHandler(npc, 1, "Alert Hori", "Alert Down", "Alert Up")
						npc.Velocity = oworld.utils.VEC_ZERO
					else
						npc.Velocity = vel
						oworld.DefaultMoveSpriteHandler(npc)
					end
				end
			end
		}
	},
	[EntityType.ENTITY_GLOBIN] = {
		[-1] = {
			OnUpdate = function(npc)
				if npc.State == NpcState.STATE_MOVE then
					local data = npc:GetData()
					if npc.Variant == 1 then -- GLAZING GLOBIN
						npc.Velocity = (data.OWLastVelocity or npc.Velocity)*0.7 + oworld.GetDefaultGroundMovement(npc, 7.5)*0.3
					else
						npc.Velocity = (data.OWLastVelocity or npc.Velocity)*0.7 + oworld.GetDefaultGroundMovement(npc, 5)*0.3
					end
					data.OWLastVelocity = npc.Velocity
				else
					npc.Velocity = oworld.utils.VEC_ZERO
				end
			end
		}
	},
	[EntityType.ENTITY_BOOMFLY] = {
		[-1] = {
			OnUpdate = function(npc)
				local data = npc:GetData()
				if not data.MovementDirection then
					data.MovementDirection = Vector.FromAngle(math.random(0,3)*90-45)
				end
				if npc:CollidesWithGrid() then
					local grid_up = oworld.GetGridEntity(oworld.GetWorldTileByPosition(Vector(npc.Position.X, npc.Position.Y-2-npc.Size)))
					local grid_down = oworld.GetGridEntity(oworld.GetWorldTileByPosition(Vector(npc.Position.X, npc.Position.Y+2+npc.Size)))
					if grid_up and (grid_up.CollisionClass == GridCollisionClass.COLLISION_WALL or grid_up.CollisionClass == GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER) or grid_down and (grid_down.CollisionClass == GridCollisionClass.COLLISION_WALL or grid_down.CollisionClass == GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER) then
						data.MovementDirection = Vector(data.MovementDirection.X, -data.MovementDirection.Y)
						data.OWLastVelocity = Vector(data.OWLastVelocity.X, -data.OWLastVelocity.Y)
						local grid_right = oworld.GetGridEntity(oworld.GetWorldTileByPosition(Vector(npc.Position.X+2+npc.Size, npc.Position.Y)))
						local grid_left = oworld.GetGridEntity(oworld.GetWorldTileByPosition(Vector(npc.Position.X-2-npc.Size, npc.Position.Y)))
						if grid_right and (grid_right.CollisionClass == GridCollisionClass.COLLISION_WALL or grid_right.CollisionClass == GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER) or grid_left and (grid_left.CollisionClass == GridCollisionClass.COLLISION_WALL or grid_left.CollisionClass == GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER) then
							data.MovementDirection = Vector(-data.MovementDirection.X, data.MovementDirection.Y)
							data.OWLastVelocity = Vector(-data.OWLastVelocity.X, data.OWLastVelocity.Y)
						end
					else
						data.MovementDirection = Vector(-data.MovementDirection.X, data.MovementDirection.Y)
						data.OWLastVelocity = Vector(-data.OWLastVelocity.X, data.OWLastVelocity.Y)
					end
				end
				
				npc.Velocity = (data.OWLastVelocity or npc.Velocity)*0.7 + data.MovementDirection*7 * 0.3
				data.OWLastVelocity = npc.Velocity
			end
		}
	},
	[EntityType.ENTITY_MAW] = {
		[-1] = {
			OnPreUpdate = function(npc)
				local targetpos = npc:GetPlayerTarget().Position
				if not oworld.CheckLine(npc.Position, targetpos, 3, 0, false, false) then
					local sprite = npc:GetSprite()
					if sprite:IsFinished("Shoot") then
						sprite:Play("Idle", true)
					end
					npc.Velocity = npc.Velocity*0.88 + (targetpos - npc.Position):Resized(0.2)
					return true
				end
			end
		},
		[1] = {} -- RED MAW
	},
	[EntityType.ENTITY_HOST] = {
		[-1] = {
			OnPreUpdate = function(npc)
				local sprite = npc:GetSprite()
				if not oworld.CheckLine(npc:GetPlayerTarget().Position, npc.Position, 3, 0, false, false) then
					npc.State = NpcState.STATE_IDLE
					if sprite:IsFinished("Shoot") then
						sprite:Play("Idle")
					end
					npc.Velocity = oworld.utils.VEC_ZERO
					return true
				end
			end
		}
	},
	[EntityType.ENTITY_CHUB] = {
		[-1] = {
			OnPreUpdate = function(npc)
				local data, sprite = npc:GetData(), npc:GetSprite()
				
				if npc:HasMortalDamage() then
					if data.IsHead then
						sprite:Play("DeathHead", true)
						
					else
						sprite:Play("DeathBody0" .. tostring(data.BodyPartId), true)
					end
					npc.Velocity = oworld.utils.VEC_ZERO
					npc.State = NpcState.STATE_DEATH
					
					return true
				end
				
				if data.EatsBomb then
					if data.BombExplosionDelay ~= 0 then
						data.BombExplosionDelay = data.BombExplosionDelay - 1
						oworld.DefaultMoveSpriteHandler(npc, 1, "WalkSadHori", "WalkSadDown", "WalkSadDown")
						npc.Velocity = oworld.utils.VEC_ZERO
						
						return true
						
					else
						data.EatsBomb = false
						Isaac.Explode(npc.Position, npc, 60)
					end
				end
				
				if not data.IsBodyPart then
					if not data.Init then
						data.IsHead = true
						data.NumStoredPositions = 14
						data.StoredPositions = {}
						data.Speed = 5
						npc.MaxHitPoints = npc.HitPoints/3
						npc.HitPoints = npc.MaxHitPoints
						
						if npc.Variant == 1 then -- C.H.A.D.
							data.SummonLimit = 5
							data.SummonType = EntityType.ENTITY_SUCKER
							data.SummonAmount = 2
						
						elseif npc.Variant == 2 then -- Carrion Queen
							data.SummonType = "Red Poop"
							
						else -- default
							data.SummonLimit = 3
							data.SummonType = EntityType.ENTITY_CHARGER
							data.SummonAmount = 1
						end
						
						for i=1, data.NumStoredPositions do
							data.StoredPositions[i] = oworld.PositionToWorldPos(npc.Position)
						end
						
						data.BodyParts = {}
						
						for i=1, 2 do
							local bodypart = Isaac.Spawn(EntityType.ENTITY_CHUB, npc.Variant, 0, npc.Position, oworld.utils.VEC_ZERO, nil)
							bodypart:GetData().IsBodyPart = true
							bodypart:GetData().Head = npc
							bodypart:GetData().BodyPartId = i
							bodypart:GetSprite():Play("WalkBody0" .. tostring(i))
							bodypart.MaxHitPoints = npc.MaxHitPoints
							bodypart.HitPoints = bodypart.MaxHitPoints
							
							data.BodyParts[i] = bodypart
						end
						
						data.Init = true
					end
					
					if npc.Variant == 1 then -- C.H.A.D.
						data.Speed = 5 * (1 + (npc.MaxHitPoints-npc.HitPoints) / (npc.MaxHitPoints*5))
					
					elseif npc.Variant == 2 then -- Carrion Queen
						if not data.IsDiagonallyMoving and npc.HitPoints < npc.MaxHitPoints*0.5 then
							data.IsDiagonallyMoving = true
							data.Speed = 10
							data.ConstantVelocity = Vector.FromAngle(45):Resized(data.Speed)
							npc.Velocity = data.ConstantVelocity
						end
					end
					
					if not data.IsSpawningSummon and not data.IsDiagonallyMoving and math.random(1, 450) == 1 then
						if type(data.SummonLimit) == "number" and Isaac.CountEntities(nil, data.SummonType, -1, -1) < data.SummonLimit then
							data.IsSpawningSummon = true
							data.SpawnedSummon = false
							data.SpawningSummonDelay = 30
							
							oworld.DefaultMoveSpriteHandler(data.BodyParts[2], 1, "PoopSide", "PoopDown", "PoopUp")
						end
					end
					
					if data.IsSpawningSummon then
						local lastbodypart_sprite = data.BodyParts[2]:GetSprite()
						
						if data.SpawningSummonDelay == 0 then
							data.IsSpawningSummon = false
							lastbodypart_sprite:Play("WalkBody0" .. tostring(i))
						else
							data.SpawningSummonDelay = data.SpawningSummonDelay - 1
							npc.Velocity = oworld.utils.VEC_ZERO
							
							if lastbodypart_sprite:IsFinished("PoopSide") or lastbodypart_sprite:IsFinished("PoopDown")
							or lastbodypart_sprite:IsFinished("PoopUp") then
								
								if not data.SpawnedSummon then
									if data.SummonType == "Red Poop" then
										oworld.GridSpawn(GridEntityType.GRID_POOP, oworld.PoopVariant.Red, oworld.GetWorldTileByPosition(data.BodyParts[2].Position))
									
									else
										for i=1, data.SummonAmount do
											Isaac.Spawn(data.SummonType, 0, 0, data.BodyParts[2].Position, oworld.utils.VEC_ZERO, npc)
										end
									end
									data.SpawnedSummon = true
								end
								
								oworld.DefaultMoveSpriteHandler(npc, 1, "WalkAttack01Hori", "WalkAttack01Down", "WalkAttack01Down")
							else
							
								oworld.DefaultMoveSpriteHandler(npc, 1, "WalkChubbyHori", "WalkChubbyDown", "WalkChubbyDown")
							end
							
							return true
						end
					end
					
					if data.IsDiagonallyMoving then -- Carrion Queen
						if npc:CollidesWithGrid() then
							local grid_up = oworld.GetGridEntity(oworld.GetWorldTileByPosition(Vector(npc.Position.X, npc.Position.Y-2-npc.Size)))
							local grid_down = oworld.GetGridEntity(oworld.GetWorldTileByPosition(Vector(npc.Position.X, npc.Position.Y+2+npc.Size)))
							if grid_up and (grid_up.CollisionClass == GridCollisionClass.COLLISION_WALL or grid_up.CollisionClass == GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER) or grid_down and (grid_down.CollisionClass == GridCollisionClass.COLLISION_WALL or grid_down.CollisionClass == GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER) then
								data.ConstantVelocity = Vector(data.ConstantVelocity.X, -data.ConstantVelocity.Y)
								local grid_right = oworld.GetGridEntity(oworld.GetWorldTileByPosition(Vector(npc.Position.X+2+npc.Size, npc.Position.Y)))
								local grid_left = oworld.GetGridEntity(oworld.GetWorldTileByPosition(Vector(npc.Position.X-2-npc.Size, npc.Position.Y)))
								if grid_right and (grid_right.CollisionClass == GridCollisionClass.COLLISION_WALL or grid_right.CollisionClass == GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER) or grid_left and (grid_left.CollisionClass == GridCollisionClass.COLLISION_WALL or grid_left.CollisionClass == GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER) then
									data.ConstantVelocity = Vector(-data.ConstantVelocity.X, data.ConstantVelocity.Y)
								end
							else
								data.ConstantVelocity = Vector(-data.ConstantVelocity.X, data.ConstantVelocity.Y)
							end
						end
						
						table.insert(data.StoredPositions, 1, oworld.PositionToWorldPos(npc.Position))
						data.StoredPositions[data.NumStoredPositions + 1] = nil
						
						npc.Velocity = data.ConstantVelocity
						
						oworld.DefaultMoveSpriteHandler(npc, 1, "WalkNormalHori", "WalkNormalDown", "WalkNormalUp")
						
						for i,bodypart in ipairs(data.BodyParts) do
							if i == 1 then
								bodypart.Velocity = (data.StoredPositions[math.ceil(data.NumStoredPositions * 0.25)] + Vector(0,-1)) - oworld.PositionToWorldPos(bodypart.Position)
							
							elseif i == 2 then
								bodypart.Velocity = (data.StoredPositions[data.NumStoredPositions * 0.5] + Vector(0,-2)) - oworld.PositionToWorldPos(bodypart.Position)
							end
						end
						
						return true
					end
					
					local target = npc:GetPlayerTarget()
					if data.IsCharging then
						if npc:CollidesWithGrid() then
							data.IsCharging = false
							data.MovementDirection = nil
						else
							npc.Velocity = data.MovementDirection*data.Speed*1.5
							oworld.DefaultMoveSpriteHandler(npc, 1, "WalkAttack01Hori", "WalkAttack01Down", "WalkAttackUp")
						
							table.insert(data.StoredPositions, 1, oworld.PositionToWorldPos(npc.Position))
							data.StoredPositions[data.NumStoredPositions + 1] = nil
						end
					end
					
					if not data.IsCharging then
						local vel, aligned_to_grid = oworld.GetRandomGridMovement(npc, 5, 0.03, false, true)
						local tile, target_tile = oworld.GetWorldTileByPosition(npc.Position), oworld.GetWorldTileByPosition(target.Position)
						
						if aligned_to_grid and (tile.X == target_tile.X or tile.Y == target_tile.Y)
						and (npc.Position - target.Position):LengthSquared() <= 280 ^ 2
						and oworld.CheckLine(npc.Position, target.Position, 0, 0, false, false) then
							data.MovementDirection = (target_tile - tile):Normalized()
							data.IsCharging = true
							npc.Velocity = data.MovementDirection*data.Speed
							oworld.DefaultMoveSpriteHandler(npc, 1, "WalkAttack01Hori", "WalkAttack01Down", "WalkAttackUp")
							sfx:Play(SoundEffect.SOUND_MONSTER_ROAR_1, 0.7, 0, false, 1)
						else
							npc.Velocity = vel
							oworld.DefaultMoveSpriteHandler(npc, 1, "WalkNormalHori", "WalkNormalDown", "WalkNormalUp")
						end
						
						table.insert(data.StoredPositions, 1, oworld.PositionToWorldPos(npc.Position))
						data.StoredPositions[data.NumStoredPositions + 1] = nil
					end
					
					for i,bodypart in ipairs(data.BodyParts) do
						if i == 1 then
							bodypart.Velocity = (data.StoredPositions[data.NumStoredPositions * 0.5] + Vector(0,-1)) - oworld.PositionToWorldPos(bodypart.Position)
						
						elseif i == 2 then
							bodypart.Velocity = (data.StoredPositions[data.NumStoredPositions] + Vector(0,-2)) - oworld.PositionToWorldPos(bodypart.Position)
						end
					end
					
				else -- body parts
				
					if not data.Head or not data.Head:Exists() then
						npc:Remove()
					end
					
					if data.Head:GetData().EatsBomb or data.Head:GetData().IsSpawningSummon or data.Head:IsDead() then
						npc.Velocity = oworld.utils.VEC_ZERO
					end
				end
				
				return true
			end
		}
	},
	[EntityType.ENTITY_HOPPER] = {
		[-1] = {
			OnPreUpdate = function(npc)
				local target = npc:GetPlayerTarget()
				
				local targetTile
				local hoppingDistance
				if npc.Variant == 1 then -- Trite
					hoppingDistance = math.random(5,7)
					targetTile = oworld.GetWorldTileByPosition(target.Position)
				else
					hoppingDistance = 3
					targetTile = target.Position:DistanceSquared(npc.Position) <= 120 ^ 2 and oworld.GetWorldTileByPosition(target.Position)
				end
				
				npc.Velocity = npc.Velocity*0.85
				oworld.GetGroundHoppingMovement(npc, 20, 30, hoppingDistance, targetTile, "Hop")
				
				return true
			end
		}
	},
	[EntityType.ENTITY_SPITY] = {
		[-1] = {
			OnUpdate = function(npc)
				if npc:GetSprite():IsPlaying("Move Hori") or npc:GetSprite():IsPlaying("Move Up")
				or npc:GetSprite():IsPlaying("Move Down") then
					npc.Velocity = oworld.GetRandomGridMovement(npc, 4.25, 0.01, true, true)
				
				else
					npc.Velocity = oworld.utils.VEC_ZERO
				end
			end
		}
	},
	[EntityType.ENTITY_BRAIN] = {
		[-1] = {
			OnUpdate = function(npc)
				npc.Velocity = oworld.GetRandomGridMovement(npc, 2.75, 0.01, true, true)
			end
		}
	},
	[EntityType.ENTITY_LEAPER] = {
		[-1] = {
			OnPreUpdate = function(npc)
				local data = npc:GetData()
				local target = npc:GetPlayerTarget()
				
				local hoppingDistance = 3
				local targetTile = target.Position:DistanceSquared(npc.Position) <= 120 ^ 2 and oworld.GetWorldTileByPosition(target.Position)
				
				if data.HopDelay == 1 then
					if math.random(1,7) == 1 then
						data.IsLeaping = true
						npc.Velocity = oworld.utils.VEC_ZERO
						data.HopDelay = 30
						npc:GetSprite():Play("BigJumpUp", true)
						npc.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
					end
				end
				
				if data.IsLeaping then
					local sprite = npc:GetSprite()
					
					if sprite:IsFinished("BigJumpUp") then
						sprite:Play("BigJumpDown", true)
						npc.Position = target.Position
						npc.Velocity = oworld.utils.VEC_ZERO
					end
					
					if sprite:IsPlaying("BigJumpDown") then
						if sprite:GetFrame() == 22 then -- landing
							for i=1, 4 do
								Isaac.Spawn(EntityType.ENTITY_PROJECTILE, 0, 0, npc.Position, Vector.FromAngle(i*90) * 7, npc)
							end
							npc.EntityCollisionClass = EntityCollisionClass.ENTCOLL_ALL
						end
					end
					
					if sprite:IsFinished("BigJumpDown") then
						sprite:Play("Idle", true)
						data.IsLeaping = false
					end
					
				else
					npc.Velocity = npc.Velocity*0.85
					oworld.GetGroundHoppingMovement(npc, 20, 30, hoppingDistance, targetTile, "Hop")
				end
				
				return true
			end
		}
	},
	[EntityType.ENTITY_MRMAW] = {
		[0] = { -- Mr. Maw body
			OnUpdate = function(npc)
				npc.Velocity = oworld.GetDefaultGroundMovement(npc, npc.Velocity:Length())
			end
		},
		[2] = { -- Mr. Red Maw body
			OnUpdate = function(npc)
				npc.Velocity = oworld.GetDefaultGroundMovement(npc, npc.Velocity:Length())
			end
		}
	},
	[EntityType.ENTITY_GURDY] = {
		[-1] = {
			OnPreUpdate = function(npc)
				local sprite = npc:GetSprite()
				
				local attack2 = sprite:IsPlaying("Attack2")
				local attack4 = sprite:IsPlaying("Attack4")
				local attack5 = sprite:IsPlaying("Attack5")
				
				-- prevents Gurdy from shooting bugged projectiles and rewrite that part instead
				if attack2 or attack4 or attack5 then
					if sprite:GetFrame() == 17 then
						local baseRotation = attack5 and 0
											or attack2 and 90
											or attack4 and 180
						
						local angleToTarget = (npc:GetPlayerTarget().Position - npc.Position):GetAngleDegrees()
						
						baseRotation = math.max(math.min(angleToTarget, baseRotation + 5), baseRotation - 5)
						
						for i=1, 5 do
							local proj = Isaac.Spawn(EntityType.ENTITY_PROJECTILE, 0, 0, npc.Position, Vector.FromAngle(baseRotation + (i - 3)*12) * 10, npc):ToProjectile()
							proj.Height = -35
						end
						
						sfx:Play(SoundEffect.SOUND_BOSS_GURGLE_ROAR, 0.7, 0, false, 1)
					end
					
					return true
				end
			end
		}
	},
	[EntityType.ENTITY_BABY] = {
		[-1] = {
			OnUpdate = function(npc)
				local sprite = npc:GetSprite()
				
				if sprite:IsPlaying("Vanish") and sprite:GetFrame() == 5 then
					npc:GetData().PreVanishWorldPos = oworld.PositionToWorldPos(npc.Position)
				end
				
				if sprite:IsPlaying("Vanish2") and sprite:GetFrame() == 0 then
					local target = npc:GetPlayerTarget()
					local room = game:GetRoom()
					local topLeftPos, bottomRightPos = room:GetTopLeftPos(), room:GetBottomRightPos()
					
					local telePos
					for i=1, 10 do -- 10 iterations to find a fitting spot to teleport to
						local rPos = room:GetRandomPosition(0)
						
						if rPos:DistanceSquared(target.Position) > 10000 then
							telePos = rPos
							break
						end
					end
					
					if telePos then
						npc.Position = telePos
					else
						npc.Position = oworld.WorldPosToPosition(npc:GetData().PreVanishWorldPos)
					end
				end
			end
		}
	},
	[EntityType.ENTITY_VIS] = {
		[-1] = {
			OnUpdate = function(npc)
				local sprite = npc:GetSprite()
				
				if sprite:IsPlaying("WalkHori") or sprite:IsPlaying("WalkVert") then
					npc.Velocity = oworld.GetRandomGridMovement(npc, 3.5, 0.05)
				end
			end
		}
	},
	[EntityType.ENTITY_GUTS] = {
		[-1] = {
			OnUpdate = function(npc)
				npc.Velocity = oworld.GetWallLockedGridMovement(npc, 6)
				
				npc:GetSprite().FlipX = npc.Velocity.X > 0
			end
		}
	},
	--[[[EntityType.ENTITY_KNIGHT] = {
		[-1] = {
			OnUpdate = function(npc)
				npc.Velocity = oworld.GetRandomGridMovement(npc, 4, 0.03, true, true)
			end
		}
	},]]
	
}

for rwenemytype, rwenemy in pairs(oworld.RewrittenEnemies) do
	oworld:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, npc)
		if rwenemy[npc.Variant] and rwenemy[npc.Variant].OnUpdate then
			rwenemy[npc.Variant].OnUpdate(npc)
		elseif rwenemy[-1] and rwenemy[-1].OnUpdate then
			rwenemy[-1].OnUpdate(npc)
		end
	end, rwenemytype)
end

oworld:AddCallback(ModCallbacks.MC_PRE_NPC_UPDATE, function(_, npc)
	if oworld.RewrittenEnemies[npc.Type] then
		if oworld.RewrittenEnemies[npc.Type][npc.Variant] and oworld.RewrittenEnemies[npc.Type][npc.Variant].OnPreUpdate then
			return oworld.RewrittenEnemies[npc.Type][npc.Variant].OnPreUpdate(npc)
		elseif oworld.RewrittenEnemies[npc.Type][-1] and oworld.RewrittenEnemies[npc.Type][-1].OnPreUpdate then
			return oworld.RewrittenEnemies[npc.Type][-1].OnPreUpdate(npc)
		end
	end
end)

-- to prevent multiple chubs from spawning next to eachother
oworld:AddCallback(ModCallbacks.MC_PRE_ADD_ENTITY_DATA, function(_, edata, worldtile, world)
	if edata.id == EntityType.ENTITY_CHUB then
		local dirs = {Vector(0,1), Vector(1,0), Vector(0,-1), Vector(-1,0)}
		for _,dir in ipairs(dirs) do
			for i=1, 2 do
				local adj_edata = oworld.GetEntityData(worldtile.X + dir.X*i, worldtile.Y + dir.Y*i, world)
				
				if adj_edata.id == EntityType.ENTITY_CHUB then
					return true
				end
			end
		end
	end
end)

-- letting all chub's parts share health
oworld:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, function(_, ent, dmg, flags, src, invuln)
	local data = ent:GetData()
	
	if data.SkipDmgChecks then
		data.SkipDmgChecks = false
		return
	end
	
	if ent.Variant == 2 and data.BodyPartId ~= 2 then -- Carrion Queen
		return false
	end
	
	if data.IsHead then
		data.SkipDmgChecks = true
		ent:TakeDamage(dmg/3, flags, src, invuln)
		
		for _,bodypart in ipairs(data.BodyParts) do
			bodypart:GetData().SkipDmgChecks = true
			bodypart:TakeDamage(dmg/3, flags, src, invuln)
		end
		
		return false
		
	elseif data.IsBodyPart then
		data.Head:GetData().SkipDmgChecks = true
		data.Head:TakeDamage(dmg/3, flags, src, invuln)
		
		for _,bodypart in ipairs(data.Head:GetData().BodyParts) do
			bodypart:GetData().SkipDmgChecks = true
			bodypart:TakeDamage(dmg/3, flags, src, invuln)
		end
		
		return false
	end
end, EntityType.ENTITY_CHUB)

-- bomb interaction with chub
oworld:AddCallback(ModCallbacks.MC_PRE_BOMB_COLLISION, function(_, bomb, coll, low)
	if coll and coll.Type == EntityType.ENTITY_CHUB and coll:GetData().IsHead then
		coll:GetData().EatsBomb = true
		coll:GetData().BombExplosionDelay = 60
		bomb:Remove()
	end
end)