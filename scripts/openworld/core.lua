local oworld = require "scripts.openworld"

local game = Game()

oworld.TilesOnScreenActive = {X=15,Y=13} -- the number of tiles away entities get loaded at
oworld.TilesOnScreenRendered = {X=12, Y=7}

oworld.CheapEffectVariant = Isaac.GetEntityVariantByName("Cheap Effect Ent")

oworld.CustomHeavyOpenDoorSoundId = Isaac.GetSoundIdByName("Custom Heavy Door Open")

-- RNG --

oworld.RNG = RNG()
oworld:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, function(_, continue)
	if not continue then
		oworld.RNG:SetSeed(game:GetSeeds():GetStartSeed(), 0)
	end
end)

function oworld.GetRNG()
	local rng = RNG()
	oworld.RNG:Next()
	rng:SetSeed(oworld.RNG:GetSeed(), 0)
	return rng
end

-- Callbacks --

local modcallbacks = {
	NPCUpdateCallbacks = "MC_NPC_UPDATE", 
	EffectUpdateCallbacks = "MC_POST_EFFECT_UPDATE", 
	PickupUpdateCallbacks = "MC_POST_PICKUP_UPDATE", 
	PlayerUpdateCallbacks = "MC_POST_PLAYER_UPDATE", 
	BombUpdateCallbacks = "MC_POST_BOMB_UPDATE"
}

for varname,callbackname in pairs(modcallbacks) do
	oworld[varname] = {}
	oworld:AddCallback(ModCallbacks[callbackname], function(_, ent)
		for _,callback in ipairs(oworld[varname]) do
			if (not callback.Id or callback.Id == -1 or ent.Type == callback.Id) and (not callback.Variant or callback.Variant == -1 or ent.Variant == callback.Variant) then
				callback.Fn(ent, ent)
			end
		end
	end)
end

oworld:AddCallback(ModCallbacks.MC_PRE_NPC_UPDATE, function(_, npc)
	for _,callback in ipairs(oworld.PreNPCUpdateCallbacks) do
		if not callback.Type or callback.Type == -1 or callback.Type == npc.Type then
			local fnreturn = callback.Fn(npc, npc)
			if fnreturn ~= nil then
				oworld:PreUpdateEntities(npc)
				if fnreturn == true then
					oworld:PostUpdateEntities(npc)
				end
				return fnreturn
			end
		end
	end
end)

oworld:AddCallback(ModCallbacks.MC_PRE_NPC_COLLISION, function(_, npc, collider, low)
	for _,callback in ipairs(oworld.PreNPCCollisionCallbacks) do
		if not callback.Type or callback.Type == -1 or callback.Type == npc.Type then
			local fnreturn = callback.Fn(npc, npc, collider, low)
			return fnreturn
		end
	end
end)

local newCallbacks = {
	{Name = "MC_PRE_GENERATE_WORLD", Table = "PreGenerateWorldCallbacks"},
	{Name = "MC_POST_GENERATE_WORLD", Table = "PostGenerateWorldCallbacks"},
	{Name = "MC_PRE_LOAD_ENTITY", Table = "PreLoadEntityCallbacks"}, -- args(edata), optional callback arg: EntityType
	{Name = "MC_POST_LOAD_ENTITY", Table = "PostLoadEntityCallbacks"}, -- args(Entity), optional callback arg: EntityType
	{Name = "MC_PRE_BLOW_UP_DOOR", Table = "PreBlowUpDoorCallbacks"}, -- args(DoorGridEntity), optional callback arg: DoorType (DoorGridEntity.CanBlowUp can be used to stop/allow blowing up)
	{Name = "MC_POST_BLOW_UP_DOOR", Table = "PostBlowUpDoorCallbacks"}, -- args(DoorGridEntity), optional callback arg: DoorType
	{Name = "MC_PRE_CLEAR_ROOM", Table = "PreClearRoomCallbacks"}, -- args(room), return true to prevent the room from clearing
	{Name = "MC_POST_CLEAR_ROOM", Table = "PostClearRoomCallbacks"}, -- args(room)
	{Name = "MC_PRE_ADD_ENTITY_DATA", Table = "PreAddEntityDataCallbacks"}, -- args(edata, worldtile, world), return true to prevent the entity from being added, return edata with changes to add those changes to the entity
	{Name = "MC_POST_SLOT_UPDATE", Table = "PostSlotUpdateCallbacks"}, -- args(slot), optional callback arg: slot variant
	{Name = "MC_POST_CHECK_ROOM_ALIGNMENT", Table = "PostCheckRoomAlignmentCallbacks"}, -- args(roomTile, roomShape, roomType, adjRoom, allowed), return true to allow the room placement, return false to disallow room placement, return nil to let normal rules dictate room placement
	{Name = "MC_POST_CHECK_DOOR_ALIGNMENT", Table = "PostCheckDoorAlignmentCallbacks"} -- args(roomTile, roomShape, roomType, adjRoom, allowed), return true to allow the door placement, return false to disallow door placement, return nil to let normal rules dictate door placement
}

oworld.OWNewCallbackStart = 518

oworld.PostNewRoomCallbacks = {}
oworld.PostNewLevelCallbacks = {}
oworld.PreNPCUpdateCallbacks = {}
oworld.PreNPCCollisionCallbacks = {}
oworld.PreSpawnCleanAward = {}

oworld.LastCallbackFn = nil
oworld.LastExtraCallbackParam = nil

local callbacks = {
	[ModCallbacks.MC_NPC_UPDATE] = {"NPCUpdateCallbacks", function() return {Fn = oworld.LastCallbackFn, Id = oworld.LastExtraCallbackParam} end},
	[ModCallbacks.MC_POST_EFFECT_UPDATE] = {"EffectUpdateCallbacks", function() return {Fn = oworld.LastCallbackFn, Variant = oworld.LastExtraCallbackParam} end},
	[ModCallbacks.MC_POST_PICKUP_UPDATE] = {"PickupUpdateCallbacks", function() return {Fn = oworld.LastCallbackFn, Variant = oworld.LastExtraCallbackParam} end},
	[ModCallbacks.MC_POST_PLAYER_UPDATE] = {"PlayerUpdateCallbacks", function() return {Fn = oworld.LastCallbackFn, Variant = oworld.LastExtraCallbackParam} end},
	[ModCallbacks.MC_POST_BOMB_UPDATE] = {"BombUpdateCallbacks", function() return {Fn = oworld.LastCallbackFn, Variant = oworld.LastExtraCallbackParam} end},
	[ModCallbacks.MC_POST_NEW_ROOM] = {"PostNewRoomCallbacks", function() return oworld.LastCallbackFn end},
	[ModCallbacks.MC_POST_NEW_LEVEL] = {"PostNewLevelCallbacks", function() return oworld.LastCallbackFn end},
	[ModCallbacks.MC_PRE_NPC_UPDATE] = {"PreNPCUpdateCallbacks", function() return {Fn = oworld.LastCallbackFn, Type = oworld.LastExtraCallbackParam} end},
	[ModCallbacks.MC_PRE_NPC_COLLISION] = {"PreNPCCollisionCallbacks", function() return {Fn = oworld.LastCallbackFn, Type = oworld.LastExtraCallbackParam} end},
	[ModCallbacks.MC_PRE_SPAWN_CLEAN_AWARD] = {"PreSpawnCleanAward", function() return oworld.LastCallbackFn end},
}

for i,callback in ipairs(newCallbacks) do
	oworld[callback.Table] = {}
	ModCallbacks[callback.Name] = oworld.OWNewCallbackStart + i
	callbacks[oworld.OWNewCallbackStart + i] = {callback.Table, function() return {Fn = oworld.LastCallbackFn, Param = oworld.LastExtraCallbackParam} end}
end

if not oworld.OldAddCallback then
	oworld.OldAddCallback = Isaac.AddCallback
	function Isaac.AddCallback(ref, callbackId, callbackFn, extraParam)
		oworld.LastCallbackFn = callbackFn
		oworld.LastExtraCallbackParam = extraParam
		if callbacks[callbackId] then
			if type(callbacks[callbackId]) == "function" then
				callbacks[callbackId]()
			else
				table.insert(oworld[callbacks[callbackId][1]], callbacks[callbackId][2]())
			end
		else
			oworld.OldAddCallback(ref, callbackId, callbackFn, extraParam or -1)
		end
	end
end

-- Coordinates --

function oworld.PositionToWorldPos(position) -- worldpos is the position relative to the open world
	local cpos = oworld.RealRoom:GetCenterPos()
	return position-cpos+oworld.WorldPlayerPos
end

function oworld.WorldPosToPosition(worldpos)
	local cpos = oworld.RealRoom:GetCenterPos()
	return worldpos+cpos-oworld.WorldPlayerPos
end

function oworld.GetWorldTileByWorldPos(worldpos)
	return Vector(math.ceil(worldpos.X/40),math.ceil(worldpos.Y/40))
end

function oworld.GetWorldTileByPosition(position)
	local worldpos = oworld.PositionToWorldPos(position)
	return Vector(math.ceil(worldpos.X/40),math.ceil(worldpos.Y/40))
end

function oworld.GetWorldPosByWorldTile(worldtile)
	return Vector(worldtile.X*40-20,worldtile.Y*40-20)
end

function oworld.GetPositionByWorldTile(worldtile)
	return oworld.WorldPosToPosition(Vector(worldtile.X*40-20,worldtile.Y*40-20))
end

-- GameModes --

oworld.GameModeEnabledOnRun = nil
oworld.GameModes = {}

function oworld.AddGameMode(gamemode)
	for _,mode in ipairs(oworld.GameModes) do
		if gamemode.name == mode.name then
			return
		end
	end
	table.insert(oworld.GameModes, gamemode)
end

function oworld.GetCurrentMode()
	if oworld.CurrentGameMode then
		return oworld.GameModes[oworld.CurrentGameMode]
	end
end

function oworld.StartGameModeOnRun(name)
	for i,gamemode in ipairs(oworld.GameModes) do
		if gamemode.name == name then
			oworld.GameModeEnabledOnRun = i
			
			break
		end
	end
end

function oworld.ClearGameModeOnRun()
	oworld.GameModeEnabledOnRun = nil
end

-- Worlds --

if not oworld.World then
	oworld.World = {}
	oworld.WorldWidth = 200
	oworld.WorldHeight = 200
	oworld.WorldPlayerPos = oworld.utils.VEC_ZERO
	oworld.CenterPosOffset = oworld.utils.VEC_ZERO
	oworld.GenerateWorld = nil
end

function oworld.LoadWorldTile(tile) -- spawns in the entities of that world tile and empties that world tile, returns all entities spawned in
	local spawned_in_entities = {}
	for i,edata in ipairs(oworld.World[tile.X][tile.Y]) do
		if edata.isgrid then
			oworld.GridSpawn(edata.id, edata.variant, tile, true)
		else
			for _,callback in ipairs(oworld.PreLoadEntityCallbacks) do
				if not callback.Param or callback.Param == -1 or edata.id == callback.Param then
					callback.Fn(nil, edata)
				end
			end
			
			if edata then
				local ent = Isaac.Spawn(edata.id, edata.variant or 0, edata.subtype or 0, edata.worldpos and oworld.WorldPosToPosition(edata.worldpos) or oworld.GetPositionByWorldTile(tile), edata.velocity or oworld.utils.VEC_ZERO, nil)
				
				local npc = ent:ToNPC()
				if npc and npc:IsChampion() then
					npc:Morph(edata.id, edata.variant, edata.subtype, 0)
				end
				
				local sprite = ent:GetSprite()
				if edata.data then
					for k,v in pairs(edata.data) do
						ent:GetData()[k] = v
						if k == "OWRefId" then
							for _,ref_id in ipairs(v) do
								oworld.EntityReferences[ref_id] = ent
							end
						end
					end
				end
				local data = ent:GetData()
				data.WorldPosLastLoaded = edata.worldpos or oworld.GetWorldPosByWorldTile(tile)
				data.WorldPosInit = false
				ent.HitPoints = edata.hitpoints or ent.HitPoints
				ent.MaxHitPoints = edata.MaxHitPoints or ent.MaxHitPoints
				if edata.entityflags then
					ent:AddEntityFlags(edata.entityflags)
				end
				ent:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
				ent.SpawnerType = edata.spawnertype or ent.SpawnerType
				ent.SpawnerVariant = edata.spawnervariant or ent.SpawnerVariant
				ent.SpawnerEntity = edata.spawnerentity or ent.SpawnerEntity
				ent.Mass = edata.mass or ent.Mass
				data.FakeGridCollisionClass = edata.gridcolclass or edata.GridCollisionClass
				ent.EntityCollisionClass = edata.entcolclass or ent.EntityCollisionClass
				ent.CollisionDamage = edata.coldmg or ent.CollisionDamage
				ent.Parent = edata.parent or ent.Parent
				ent.Child = edata.child or ent.Child
				ent.Target = edata.target or ent.Target
				ent.DepthOffset = edata.depthoffset or ent.DepthOffset
				if edata.visible ~= nil then
					ent.Visible = edata.visible
				end
				
				if edata.sprite then
					if edata.sprite.anm2 then
						sprite:Load(edata.sprite.anm2, true) 
					end
					edata.sprite.replacedpngs = edata.sprite.replacedpngs or {}
					for i,png in pairs(edata.sprite.replacedpngs) do
						sprite:ReplaceSpritesheet(i, png)
					end
					if edata.sprite.animname then
						if edata.sprite.on == "playing" then
							sprite:Play(edata.sprite.animname, true)
						elseif edata.sprite.on == "frame" and edata.sprite.frame then
							sprite:SetFrame(edata.sprite.animname, edata.sprite.frame)
						end
					end
					if edata.sprite.overlayanimname then
						if edata.sprite.overlayon == "playing" then
							sprite:PlayOverlay(edata.sprite.overlayanimname, true)
						elseif edata.sprite.overlayon == "frame" and edata.sprite.overlayframe then
							sprite:SetOverlayFrame(edata.sprite.overlayanimname, edata.sprite.overlayframe)
						end
					end
					if edata.sprite.overlayrenderpriority ~= nil then
						sprite:SetOverlayRenderPriority(edata.sprite.overlayrenderpriority)
					end
					sprite.Offset = edata.sprite.offset or sprite.Offset
					sprite.Scale = edata.sprite.scale or sprite.Scale
					sprite.Rotation = edata.sprite.rotation or sprite.Rotation
					if edata.sprite.flipx ~= nil then sprite.FlipX = edata.sprite.flipx end
					if edata.sprite.flipy ~= nil then sprite.FlipY = edata.sprite.flipy end
					sprite.PlaybackSpeed = edata.sprite.playbackspeed or sprite.PlaybackSpeed
				end
				
				if ent.State and edata.state then ent.State = edata.state end
				
				local pickup = ent:ToPickup()
				if pickup then
					pickup.Charge = edata.charge or pickup.Charge
					pickup.Price = edata.price or pickup.Price
					if edata.touched ~= nil then pickup.Touched = edata.touched end
					pickup.ShopItemId = edata.shopitemid or pickup.ShopItemId
					if edata.theresoptions ~= nil then pickup.TheresOptionsPickup = edata.theresoptions end
					pickup.Wait = edata.wait or pickup.Wait
					if edata.autoupdateprice ~= nil then pickup.AutoUpdatePrice = edata.autoupdateprice end
					sprite:SetLastFrame()
				end
				
				if ent.Type == EntityType.ENTITY_SLOT then
					table.insert(oworld.SlotMachines, ent)
				end
				
				for _,callback in ipairs(oworld.PostLoadEntityCallbacks) do
					if not callback.Param or callback.Param == -1 or ent.Type == callback.Param then
						callback.Fn(nil, ent)
					end
				end
				
				table.insert(spawned_in_entities, ent)
			end
		end
	end
	oworld.World[tile.X][tile.Y] = {}
	return spawned_in_entities
end

function oworld.AddEntityData(x, y, world, edata, returnReference) -- adds the EntityData to a worldtile
	world = world or oworld.World
	if edata.Sprite then
		for k,v in pairs(edata.Sprite) do
			edata.data.oworldStoredSprite[k] = v
		end
	end
	
	for _,callback in ipairs(oworld.PreAddEntityDataCallbacks) do
		local ret = callback.Fn(nil, edata, Vector(x,y), world)
		
		if ret == true then
			return
			
		elseif type(ret) == "table" then
			edata = ret
		end
	end
	
	table.insert(world[x][y], edata)
	function edata:Remove()
		for i,ed in ipairs(world[x][y]) do
			if ed == edata then
				table.remove(world[x][world[y]], i)
			end
		end
	end
	
	local ref_id
	if returnReference then
		oworld.NumEntityReferences = oworld.NumEntityReferences + 1
		oworld.EntityReferences[oworld.NumEntityReferences] = edata
		edata.data.OWRefId = edata.data.OWRefId or {}
		ref_id = oworld.NumEntityReferences
		table.insert(edata.data.OWRefId, ref_id)
	end
	
	local tile = Vector(x,y)
	if oworld.IsWorldTileInLoadingDistance(tile) then
		oworld.LoadWorldTile(tile)
	end
	
	if returnReference then
		return {
			GetEntity = function()
				return oworld.EntityReferences[ref_id]
			end,
			IsLoaded = function()
				return type(oworld.EntityReferences[ref_id]) == "userdata"
			end,
			Exists = function()
				return not not oworld.EntityReferences[ref_id]
			end
		}
	end
end

function oworld.AddGridEntityData(x, y, world, gridtype, variant)
	return oworld.GridSpawn(gridtype, variant, Vector(x,y), true)
end

function oworld.GetEntityData(x, y, world) -- returns list of all EntityData on that world tile
	world = world or oworld.World
	return world[x][y]
end

function oworld.GetWorld() -- for saving a world
	return oworld.World
end

function oworld.SetWorld(world) -- loading a world
	oworld.World = world
end

function oworld.GetGridEntities() -- for saving gridentities
	return oworld.FakeGridEntities
end

function oworld.SetGridEntities(gridentities) -- loading gridentities
	oworld.FakeGridEntities = gridentities
end

function oworld.UnloadEntity(ent) -- removes the entity and puts it in the world tile, returns the coordinates of the world tile
	ent = ent:ToPickup() or ent
	local sprite,data = ent:GetSprite(), ent:GetData()
	local tile = oworld.GetWorldTileByPosition(ent.Position)
	
	data.oworldStoredSprite = data.oworldStoredSprite or {}
	data.oworldStoredSprite.frame = sprite:GetFrame()
	data.oworldStoredSprite.overlayframe = sprite:GetOverlayFrame()
	
	local edata = {
		id = ent.Type,
		variant = ent.Variant,
		subtype = ent.SubType,
		velocity = ent.Velocity,
		worldpos = oworld.PositionToWorldPos(ent.Position),
		data = data,
		hitpoints = ent.HitPoints,
		maxhitpoints = ent.MaxHitPoints,
		entityflags = ent:GetEntityFlags(),
		spawnertype = ent.SpawnerType,
		spawnervariant = ent.SpawnerVariant,
		spawnerentity = ent.SpawnerEntity,
		mass = ent.Mass,
		gridcolclass = ent.GridCollisionClass,
		entcolclass = ent.EntityCollisionClass,
		coldmg = ent.CollisionDamage,
		parent = ent.Parent,
		child = ent.Child,
		target = ent.Target,
		depthoffset = ent.DepthOffset,
		visible = ent.Visible,
		sprite = {
			anm2 = data.oworldStoredSprite.anm2,
			animname = data.oworldStoredSprite.animname,
			on = data.oworldStoredSprite.on or "playing",
			replacedpngs = data.oworldStoredSprite.replacedpngs or {},
			overlayanimname = data.oworldStoredSprite.overlayanimname,
			overlayon = data.oworldStoredSprite.overlayon,
			overlayrenderpriority = data.oworldStoredSprite.overlayrenderpriority,
			offset = sprite.Offset,
			scale = sprite.Scale,
			rotation = sprite.Rotation,
			flipx = sprite.FlipX,
			flipy = sprite.FlipY,
			playbackspeed = sprite.PlaybackSpeed
		},
		state = ent.State,
		charge = ent.Charge,
		price = ent.Price,
		touched = ent.Touched,
		shopitemid = ent.ShopItemId,
		theresoptions = ent.TheresOptionsPickup,
		wait = ent.Wait,
		autoupdateprice = ent.AutoUpdatePrice
	}
	oworld.AddEntityData(tile.X, tile.Y, oworld.World, edata)
	data.OWRemoveByUnload = true
	ent:Remove()
	if data.OWRefId then
		for _,ref_id in ipairs(data.OWRefId) do
			oworld.EntityReferences[ref_id] = edata
		end
	end
	return tile
end

oworld:AddCallback(ModCallbacks.MC_POST_ENTITY_REMOVE, function(_, ent)
	local data = ent:GetData()
	if not data.OWRemoveByUnload and data.OWRefId then
		for _,ref_id in ipairs(data.OWRefId) do
			oworld.EntityReferences[data.OWRefId] = nil
		end
	end
	
	local room, is_custom_room = game:GetRoom()
	if is_custom_room and not room:IsClear() then
		local room_is_clear = true
		local ptr_hash = GetPtrHash(ent)
		for _,e in ipairs(Isaac.GetRoomEntities()) do
			if GetPtrHash(e) ~= ptr_hash and e:CanShutDoors() then
				room_is_clear = false
				break
			end
		end
		room:SetClear(room_is_clear)
	end
	
	data.OWRemoveByUnload = false
end)

function oworld.LoadGridEntity(grid)
	if grid and not grid.Loaded then
		grid.Loaded = true
		if not grid.Desc.Initialized then
			oworld.GridInit(grid)
		end
		return true
	end
end

function oworld.UnloadGridEntity(grid)
	if grid and grid.Loaded then
		grid.Loaded = false
		return true
	end
end

function oworld.ResetWorldBoundaries(width, height)
	oworld.World = {}
	oworld.WorldWidth = width
	oworld.WorldHeight = height
	for x=1, width do
		oworld.World[x] = {}
		for y=1, height do
			oworld.World[x][y] = {}
		end
	end
	
	oworld.GridHandlerEntities = {}
	oworld.FakeGridEntities = {}
	for x=1, width do
		oworld.FakeGridEntities[x] = {}
	end
	
	oworld.TileInRoom = {}
	for x=1, width do
		oworld.TileInRoom[x] = {}
	end
	oworld.Rooms = {}
	oworld.TotalRoomIds = 0
	oworld.Floors = {}
	oworld.TotalFloorIds = 0
	oworld.WorldFloorEnts = {}
	oworld.Overlays = {}
	oworld.TotalOverlayIds = 0
end

function oworld.SetupOpenWorld(width, height) -- changes the current room into an open world
	local room,player = game:GetRoom(),Isaac.GetPlayer(0)
	local mode = oworld.GetCurrentMode()
	
	oworld.ResetWorldBoundaries(width, height)
	
	player.Position = room:GetCenterPos()
	oworld.WorldPlayerPos = Vector(width*20,height*20)
	oworld.CenterPosOffset = Vector(20,20)
	
	oworld.LastDevilRoomStage = LevelStage.STAGE_NULL
	oworld.TookDevilDealInRun = false
	
	for _,callback in ipairs(oworld.PreGenerateWorldCallbacks) do
		callback.Fn()
	end
	
	oworld.SetWorldFloorSprite("gfx/backdrop/openworld_black_backdrop.png")
	
	if mode and mode.isfloormode then
		oworld.WorldAlreadyGenerated = true
		
		oworld.LoadInStage(oworld.GetRandomStageByLevelStage(oworld.LevelStage), oworld.LevelStage)
	end
	
	-- runs the floor generation function
	if oworld.GenerateWorld then
		oworld.GenerateWorld()
	end
	
	for _,callback in ipairs(oworld.PostGenerateWorldCallbacks) do
		callback.Fn()
	end
	
	oworld.UpdateScreen()
	oworld.UpdateGetRoom()
	for _,fn in ipairs(oworld.PostNewLevelCallbacks) do
		fn()
	end
end

oworld:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, function()
	oworld.World = {}
	oworld.WorldPlayerPos = oworld.utils.VEC_ZERO
	oworld.CenterPosOffset = oworld.utils.VEC_ZERO
	
	oworld.Floors = {}
	oworld.TotalFloorIds = 0
	oworld.WorldFloorEnt = nil
	oworld.WorldFloorEnts = {}
	
	oworld.FakeGridEntities = {}
	oworld.GridHandlerEntities = {}
	
	oworld.TileInRoom = {}
	oworld.Rooms = {}
	oworld.TotalRoomIds = 0
	
	oworld.Overlays = {}
	oworld.TotalOverlayIds = 0
	
	oworld.WorldSetupActive = true
	oworld.FirstStartingRoomIndex  = game:GetLevel():GetCurrentRoomIndex()
	
	oworld.InModeMenu = true
	game:GetSeeds():AddSeedEffect(SeedEffect.SEED_NO_HUD)
	
	if oworld.GameModeEnabledOnRun then
		oworld.CurrentGameMode = oworld.GameModeEnabledOnRun
		
		oworld.ModeIsSelected = true
		oworld.GenerateWorld = oworld.GameModes[oworld.CurrentGameMode].generateworld
		game:GetLevel():SetStage(LevelStage.STAGE1_1, StageType.STAGETYPE_ORIGINAL)
		Isaac.ExecuteCommand("goto d.0")
		
	else
		oworld.GenerateWorld = nil
		oworld.CurrentGameMode = nil
		oworld.InOptionsMenu = false
		oworld.SelectedOption = 1
		oworld.MenuScroll = 0
		oworld.ModeIsSelected = false
		oworld.TextAlpha = 1
	end
end)

oworld.OldAddCallback(oworld, ModCallbacks.MC_POST_NEW_ROOM, function()
	oworld.RealRoom = oworld.RealRoom or game:GetRoom()
	for slot=DoorSlot.LEFT0, DoorSlot.DOWN1 do
		oworld.RealRoom:RemoveDoor(slot)
	end
	if oworld.WorldSetupActive then
		oworld.SetupOpenWorld(oworld.WorldWidth, oworld.WorldHeight)
		oworld.WorldSetupActive = false
	end
end, -1)

-- Slot Machine Update -- (as somehow the api didn't see a need for it?!)

oworld.SlotMachines = {}

oworld:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	local numSlots = #oworld.SlotMachines
	local numSlotsRemoved = 0
	for i=1, numSlots do
		local slot = oworld.SlotMachines[i - numSlotsRemoved]
		if slot and not slot:Exists() then
			table.remove(oworld.SlotMachines, i - numSlotsRemoved)
		end
	end
	
	for _,slot in ipairs(oworld.SlotMachines) do
		for _,callback in ipairs(oworld.PostSlotUpdateCallbacks) do
			if not callback.Param or callback.Param == -1 or slot.Variant == callback.Param then
				callback.Fn(nil, slot)
			end
		end
	end
end)

-- STAGEAPI COMPAT (should only be relevant for ab+) --

function oworld.LoadStageAPICompat()
	StageAPI.AddCallback("Open World Isaac", "PRE_SHADING_RENDER", 1, function(shadingEntity)
		shadingEntity:Remove()
	end)
end

if StageAPI and StageAPI.Loaded then
    oworld.LoadStageAPICompat()
else
    if not StageAPI then
        StageAPI = {Loaded = false, ToCall = {}}
    end

    StageAPI.ToCall[#StageAPI.ToCall + 1] = oworld.LoadStageAPICompat
end

------------------------------
-- OPEN WORLD FUNCTIONALITY --
------------------------------

function oworld.IsWorldTileInLoadingDistance(worldtile)
	local ctile = oworld.GetWorldTileByWorldPos(oworld.WorldPlayerPos)
	local startx, endx = ctile.X-oworld.TilesOnScreenRendered.X, ctile.X+oworld.TilesOnScreenRendered.X
	local starty, endy = ctile.Y-oworld.TilesOnScreenRendered.Y, ctile.Y+oworld.TilesOnScreenRendered.Y
	return worldtile.X >= startx and worldtile.X <= endx and worldtile.Y >= starty and worldtile.Y <= endy
end

function oworld.UpdateScreen()
	local ctile = oworld.GetWorldTileByWorldPos(oworld.WorldPlayerPos)
	local startx, endx = ctile.X-oworld.TilesOnScreenRendered.X-1, ctile.X+oworld.TilesOnScreenRendered.X+1
	local starty, endy = ctile.Y-oworld.TilesOnScreenRendered.Y-1, ctile.Y+oworld.TilesOnScreenRendered.Y+1
	for x=startx-1, endx+1 do
		for y=starty-1, endy+1 do
			local grid = oworld.GetGridEntity(Vector(x,y))
			if x <= startx or x >= endx or y <= starty or y >= endy then
				oworld.UnloadGridEntity(grid)
			else
				oworld.LoadGridEntity(grid)
			end
		end
	end 
	startx, endx = ctile.X-oworld.TilesOnScreenActive.X, ctile.X+oworld.TilesOnScreenActive.X
	starty, endy = ctile.Y-oworld.TilesOnScreenActive.Y, ctile.Y+oworld.TilesOnScreenActive.Y
	for x=startx, endx do
		for y=starty, endy do
			oworld.LoadWorldTile(Vector(x,y))
		end
	end
end

function oworld.TeleportMainPlayer(worldpos)
	for _,ent in ipairs(oworld.roomEntities) do
		local data = ent:GetData()
		if not data.OWorldStaticEntity and not data.IsGridRenderer and ent.Type ~= EntityType.ENTITY_PLAYER then
			local cpos = oworld.WorldPosToPosition(worldpos)
			if ent.Position.X-cpos.X <= -oworld.TilesOnScreenActive.X*40-40 or ent.Position.X-cpos.X >= oworld.TilesOnScreenActive.X*40+40
			or ent.Position.Y-cpos.Y <= -oworld.TilesOnScreenActive.Y*40-40 or ent.Position.Y-cpos.Y >= oworld.TilesOnScreenActive.Y*40+40 then
				oworld.UnloadEntity(ent)
			end
		end
	end
	
	local ctile = oworld.GetWorldTileByWorldPos(oworld.WorldPlayerPos)
	local startx, endx = ctile.X-oworld.TilesOnScreenRendered.X-1, ctile.X+oworld.TilesOnScreenRendered.X+1
	local starty, endy = ctile.Y-oworld.TilesOnScreenRendered.Y-1, ctile.Y+oworld.TilesOnScreenRendered.Y+1
	for x=startx-1, endx+1 do
		for y=starty-1, endy+1 do
			local grid = oworld.GetGridEntity(Vector(x,y))
			if x <= startx or x >= endx or y <= starty or y >= endy then
				oworld.UnloadGridEntity(grid)
			end
		end
	end
	
	oworld.WorldPlayerPos = worldpos
	oworld.UpdateScreen()
end

oworld.EntityReferences = {}
oworld.NumEntityReferences = 0

function oworld.EntityRef(ent)
	oworld.NumEntityReferences = oworld.NumEntityReferences + 1
	oworld.EntityReferences[oworld.NumEntityReferences] = ent
	local ref_id = oworld.NumEntityReferences
	ent:GetData().OWRefId = ent:GetData().OWRefId or {}
	table.insert(ent:GetData().OWRefId, ref_id)
	ent = nil
	
	return {
		GetEntity = function()
			return oworld.EntityReferences[ref_id]
		end,
		IsLoaded = function()
			return type(oworld.EntityReferences[ref_id]) == "userdata"
		end,
		Exists = function()
			return not not oworld.EntityReferences[ref_id]
		end
	}
end

function oworld.FindClosestOccupiedWorldTile(worldpos, gridcollisionclass, isplayer, maxtilecheckrange)
	maxtilecheckrange = maxtilecheckrange or 10
	local worldtile = oworld.GetWorldTileByWorldPos(worldpos)
	local grid = oworld.GetGridEntity(worldtile)
	if grid and not grid.NoCollision and oworld.CanCollideWithGrid(gridcollisionclass, grid.CollisionClass, isplayer) then
		return worldtile, worldpos
	end
	local closestworldtiles = {}
	local foundclosestworldtiles = false
	local dirs = {Vector(1,1),Vector(-1,1),Vector(1,-1),Vector(-1,-1)}
	local checkrange = 1
	while checkrange ~= maxtilecheckrange do
		for i=0, checkrange do
			for diri=1, 4 do
				local tile = worldtile + Vector(i*dirs[diri].X,(i-checkrange)*dirs[diri].Y)
				local grid = oworld.GetGridEntity(tile)
				if grid and not grid.NoCollision and oworld.CanCollideWithGrid(gridcollisionclass, grid.CollisionClass, isplayer) then
					table.insert(closestworldtiles, tile)
					foundclosestworldtiles = true
				end
			end
		end
		if foundclosestworldtiles then
			break
		end
		checkrange = checkrange + 1
	end
	if checkrange == maxtilecheckrange then
		return nil
	end
	local closesttile = nil
	local closestworldposintile = nil
	local closestdistance = nil
	for _,tile in ipairs(closestworldtiles) do
		local tileworldpos = oworld.GetWorldPosByWorldTile(tile)
		local tileclosestworldpos = Vector(math.max(math.min(worldpos.X, tileworldpos.X+20), tileworldpos.X-20), math.max(math.min(worldpos.Y, tileworldpos.Y+20), tileworldpos.Y-20))
		local distance = (tileclosestworldpos-worldpos):LengthSquared()
		if not closestdistance or distance < closestdistance then
			closestdistance = distance
			closestworldposintile = tileclosestworldpos
			closesttile = tile
		end
	end
	return closesttile, closestworldposintile
end

function oworld.FindClosestFreeWorldTile(worldpos, gridcollisionclass, isplayer, maxtilecheckrange)
	maxtilecheckrange = maxtilecheckrange or 10
	local worldtile = oworld.GetWorldTileByWorldPos(worldpos)
	local grid = oworld.GetGridEntity(worldtile)
	if not grid or grid.NoCollision or not oworld.CanCollideWithGrid(gridcollisionclass, grid.CollisionClass, isplayer) then
		return worldtile
	end
	local closestworldtiles = {}
	local foundclosestworldtiles = false
	local dirs = {Vector(1,1),Vector(-1,1),Vector(1,-1),Vector(-1,-1)}
	local checkrange = 1
	while checkrange ~= maxtilecheckrange do
		for i=0, checkrange do
			for diri=1, 4 do
				local tile = worldtile + Vector(i*dirs[diri].X,(i-checkrange)*dirs[diri].Y)
				local grid = oworld.GetGridEntity(tile)
				if not grid or grid.NoCollision or not oworld.CanCollideWithGrid(gridcollisionclass, grid.CollisionClass, isplayer) then
					table.insert(closestworldtiles, tile)
					foundclosestworldtiles = true
				end
			end
		end
		if foundclosestworldtiles then
			break
		end
		checkrange = checkrange + 1
	end
	if checkrange == maxtilecheckrange then
		return nil
	end
	local closesttile = nil
	local closestworldposintile = nil
	local closestdistance = nil
	for _,tile in ipairs(closestworldtiles) do
		local tileworldpos = oworld.GetWorldPosByWorldTile(tile)
		local tileclosestworldpos = Vector(math.max(math.min(worldpos.X, tileworldpos.X+20), tileworldpos.X-20), math.max(math.min(worldpos.Y, tileworldpos.Y+20), tileworldpos.Y-20))
		local distance = (tileclosestworldpos-worldpos):LengthSquared()
		if not closestdistance or distance < closestdistance then
			closestdistance = distance
			closestworldposintile = tileclosestworldpos
			closesttile = tile
		end
	end
	return closesttile, closestworldposintile
end

function oworld.FindClosestFreeWorldPos(worldpos, gridcollisionclass, isplayer, size, maxtilecheckrange)
	local worldtile = oworld.GetWorldTileByWorldPos(worldpos)
	local grid = oworld.GetGridEntity(worldtile)
	if not grid or grid.NoCollision or not oworld.CanCollideWithGrid(gridcollisionclass, grid.CollisionClass, isplayer) then
		if size then
			local tile, closestoccupiedworldpos = oworld.FindClosestOccupiedWorldTile(worldpos, gridcollisionclass, isplayer, 3 + math.floor(size/40))
			if not closestoccupiedworldpos or (closestoccupiedworldpos-worldpos):LengthSquared() > size ^ 2 then 
				return worldpos 
			end
			return closestoccupiedworldpos + (worldpos-closestoccupiedworldpos):Resized(size), closestoccupiedworldpos
		else
			return worldpos
		end
	else
		local tile, closestfreeworldpos = oworld.FindClosestFreeWorldTile(worldpos, gridcollisionclass, isplayer, 3)
		if not closestfreeworldpos then return worldpos end
		if size then
			return closestfreeworldpos + (closestfreeworldpos-worldpos):Resized(size), closestfreeworldpos
		else
			return closestfreeworldpos, closestfreeworldpos
		end
	end
end

table.insert(oworld.PlayerUpdateCallbacks, 1, {Fn = function(player)
	local data,room,mode = player:GetData(),game:GetRoom(),oworld.GetCurrentMode()
	if oworld.InModeMenu then
		player.ControlsEnabled = false
	end
	if not oworld.World[1] then return end
	
	-- keeps the player between the world boundaries
	if mode and not mode.noworldboundary then
		local nextplayerpos = oworld.WorldPlayerPos+player.Velocity
		if nextplayerpos.X > (oworld.WorldWidth*40)-(oworld.TilesOnScreenActive.X*40+20) then
			player.Velocity = Vector((oworld.WorldWidth*40)-(oworld.TilesOnScreenActive.X*40+20)-oworld.WorldPlayerPos.X, player.Velocity.Y)
		elseif nextplayerpos.X < oworld.TilesOnScreenActive.X*40+20 then
			player.Velocity = Vector((oworld.TilesOnScreenActive.X*40+20)-oworld.WorldPlayerPos.X, player.Velocity.Y)
		end
		if nextplayerpos.Y > (oworld.WorldHeight*40)-(oworld.TilesOnScreenActive.Y*40+20) then
			player.Velocity = Vector(player.Velocity.X, (oworld.WorldHeight*40)-(oworld.TilesOnScreenActive.Y*40+20)-oworld.WorldPlayerPos.Y)
		elseif nextplayerpos.Y < oworld.TilesOnScreenActive.Y*40+20 then
			player.Velocity = Vector(player.Velocity.X, (oworld.TilesOnScreenActive.Y*40+20)-oworld.WorldPlayerPos.Y)
		end
	end
	
	local mainplayer = Isaac.GetPlayer(0)
	if player.Index ~= mainplayer.Index then return end
	
	-- update entity positions
	local mainplayerposdifference = oworld.PositionToWorldPos(mainplayer.Position)-oworld.WorldPlayerPos
	oworld.WorldPlayerPos = oworld.WorldPlayerPos+mainplayerposdifference
	oworld.CenterPosOffset = oworld.CenterPosOffset+mainplayerposdifference
	local cpos = oworld.RealRoom:GetCenterPos()
	local offsetfromcenter = cpos-mainplayer.Position
	for _,e in ipairs(oworld.roomEntities) do
		if not e:GetData().OWorldStaticEntity then
			e.Position = e.Position+offsetfromcenter
		end
	end
	if oworld.GridRenderer then
		oworld.GridRenderer.Position = mainplayer.Position+Vector(0,-500)
	end
	
	-- loading entities when they come onscreen, loading and unloading grid entities
	if mode and not mode.noworldboundary then
		if oworld.CenterPosOffset.X >= 40 then
			oworld.CenterPosOffset.X = oworld.CenterPosOffset.X-40
			oworld.UpdateScreen()
		elseif oworld.CenterPosOffset.X <= -40 then
			oworld.CenterPosOffset.X = oworld.CenterPosOffset.X+40
			oworld.UpdateScreen()
		end
		if oworld.CenterPosOffset.Y >= 40 then
			oworld.CenterPosOffset.Y = oworld.CenterPosOffset.Y-40
			oworld.UpdateScreen()
		elseif oworld.CenterPosOffset.Y <= -40 then
			oworld.CenterPosOffset.Y = oworld.CenterPosOffset.Y+40
			oworld.UpdateScreen()
		end
	end
end})

oworld.NoGridCollEnts = {
	{id=EntityType.ENTITY_BOIL},
	{id=EntityType.ENTITY_FIREPLACE},
	{id=EntityType.ENTITY_GURDY}
}

oworld.StationaryEnts = {
	{id=EntityType.ENTITY_PICKUP, variant=PickupVariant.PICKUP_SHOPITEM},
	{id=EntityType.ENTITY_PICKUP, variant=PickupVariant.PICKUP_BIGCHEST},
	{id=EntityType.ENTITY_PICKUP, variant=PickupVariant.PICKUP_TROPHY},
	{id=EntityType.ENTITY_PICKUP, variant=PickupVariant.PICKUP_BED},
	{id=EntityType.ENTITY_SLOT},
	{id=EntityType.ENTITY_SHOPKEEPER},
	{id=EntityType.ENTITY_BOIL},
	{id=EntityType.ENTITY_FIREPLACE},
	{id=EntityType.ENTITY_GURDY},
	{id=EntityType.ENTITY_EFFECT, variant=EffectVariant.DEVIL},
	{id=EntityType.ENTITY_EFFECT, variant=EffectVariant.ANGEL}
}

function oworld:PreUpdateEntities(ent)
	local data = ent:GetData()
	
	for _,edata in ipairs(oworld.NoGridCollEnts) do
		if ent.Type == edata.id and (not edata.variant or ent.Variant == edata.variant) then
			ent.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_NONE
			data.FakeGridCollisionClass = EntityGridCollisionClass.GRIDCOLL_NONE
		end
	end
	
	if data.FakeGridCollisionClass ~= ent.GridCollisionClass and ent.GridCollisionClass ~= EntityGridCollisionClass.GRIDCOLL_NONE then
		data.FakeGridCollisionClass = ent.GridCollisionClass
	end
	data.FakeGridCollisionClass = data.FakeGridCollisionClass or EntityGridCollisionClass.GRIDCOLL_NONE
	ent.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_NONE
	
	if not data.WorldPosInit and data.WorldPosLastLoaded then
		ent.Position = oworld.WorldPosToPosition(data.WorldPosLastLoaded)
		if ent.FrameCount >= 10 then
			data.WorldPosInit = true
		end
	end
end

function oworld:PostUpdateEntities(ent)
	local mode,data = oworld.GetCurrentMode(),ent:GetData()
	
	for _,edata in ipairs(oworld.NoGridCollEnts) do
		if ent.Type == edata.id and (not edata.variant or ent.Variant == edata.variant) then
			ent.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_NONE
			data.FakeGridCollisionClass = EntityGridCollisionClass.GRIDCOLL_NONE
		end
	end
	
	if data.FakeGridCollisionClass ~= ent.GridCollisionClass and ent.GridCollisionClass ~= EntityGridCollisionClass.GRIDCOLL_NONE then
		data.FakeGridCollisionClass = ent.GridCollisionClass
	end
	data.FakeGridCollisionClass = data.FakeGridCollisionClass or EntityGridCollisionClass.GRIDCOLL_NONE
	ent.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_NONE
	
	if not oworld.RealRoom then return end
	
	if not data.WorldPosInit and data.WorldPosLastLoaded then
		ent.Position = oworld.WorldPosToPosition(data.WorldPosLastLoaded)
	end
	
	if ent.Type == EntityType.ENTITY_PICKUP then
		if ent.Variant == PickupVariant.PICKUP_COLLECTIBLE then
			ent.Friction = 0
			if data.WorldPosLastLoaded then
				ent.Position = (oworld.WorldPosToPosition(data.WorldPosLastLoaded)+ent.Position)/2
			end
		end
		if ent:ToPickup().Price ~= 0 then
			ent.Friction = 0
			if data.WorldPosLastLoaded then
				ent.Position = oworld.WorldPosToPosition(data.WorldPosLastLoaded)
			end
		end
	end
	
	for _,edata in ipairs(oworld.StationaryEnts) do
		if ent.Type == edata.id and (not edata.variant or ent.Variant == edata.variant) then
			ent.Friction = 0
			if data.WorldPosLastLoaded then
				ent.Position = oworld.WorldPosToPosition(data.WorldPosLastLoaded)
			end
		end
	end
	
	local player = ent:ToPlayer()
	if player then
		if not oworld.SecretRoomsVisibleOnMap and player:HasCollectible(CollectibleType.COLLECTIBLE_BLUE_MAP) then
			for _,room in ipairs(oworld.Rooms) do
				if room:GetType() == RoomType.ROOM_SECRET or room:GetType() == RoomType.ROOM_SUPERSECRET then
					oworld.AddRoomToMap(room)
				end
			end
			oworld.SecretRoomsVisibleOnMap = true
		end
	end
	
	-- collision to grids
	ent:GetData().OWCollidesWithGrid = false
	if mode then
		local old_targetpos
		if ent.Velocity:LengthSquared() > 19 ^ 2 then
			old_targetpos = ent.Position + ent.Velocity:Resized(19)
			local grid = oworld.GetGridEntity(oworld.GetWorldTileByPosition(old_targetpos))
			if not grid or grid.NoCollision or not oworld.CanCollideWithGrid(data.FakeGridCollisionClass, grid.CollisionClass, ent.Type == EntityType.ENTITY_PLAYER) then
				old_targetpos = ent.Position + ent.Velocity
			end
		else
			old_targetpos = ent.Position + ent.Velocity
		end
		local old_targetworldpos = oworld.PositionToWorldPos(old_targetpos)
		local worldpos, closestoccupiedworldpos = oworld.FindClosestFreeWorldPos(old_targetworldpos, data.FakeGridCollisionClass, ent.Type == EntityType.ENTITY_PLAYER, ent.Size)
		if closestoccupiedworldpos and (old_targetworldpos.X ~= worldpos.X or old_targetworldpos.Y ~= worldpos.Y) then
			ent:GetData().OWCollidesWithGrid = true
			local new_targetpos = oworld.WorldPosToPosition(worldpos)
			new_targetpos = oworld.FindClosestFreeWorldPos(oworld.PositionToWorldPos(new_targetpos), data.FakeGridCollisionClass, ent.Type == EntityType.ENTITY_PLAYER, ent.Size)
			new_targetpos = oworld.WorldPosToPosition(new_targetpos)
			local grid = oworld.GetGridEntity(oworld.GetWorldTileByWorldPos(old_targetworldpos))
			if grid and not grid.NoCollision and oworld.CanCollideWithGrid(data.FakeGridCollisionClass, grid.CollisionClass, ent.Type == EntityType.ENTITY_PLAYER) then
				ent.Position = new_targetpos
			else
				local to_new_target = new_targetpos - ent.Position
				if to_new_target:LengthSquared() > ent.Velocity:LengthSquared() then
					ent.Velocity = (new_targetpos - ent.Position):Resized(ent.Velocity:Length())
				else
					ent.Velocity = (new_targetpos - ent.Position)
				end
			end
		end
	end
	
	-- curse doors
	local grid = oworld.GetGridEntity(oworld.GetWorldTileByPosition(ent.Position))
	if grid and grid.Desc.Type == GridEntityType.GRID_DOOR and grid.DoorType == RoomType.ROOM_CURSE then
		ent:TakeDamage(1, 0, EntityRef(ent), 0)
	end
	
	-- spikes
	if grid and grid.Desc.Type == GridEntityType.GRID_SPIKES then
		if (player or ent:ToNPC()) and data.FakeGridCollisionClass >= EntityGridCollisionClass.GRIDCOLL_GROUND then
			ent:TakeDamage(1, 0, EntityRef(ent), 0)
		end
	end
	
	-- unloading entities when they get offscreen
	if mode and not mode.noworldboundary and not data.OWorldStaticEntity and not data.IsGridRenderer and ent.Type ~= EntityType.ENTITY_PLAYER then
		local cpos = oworld.RealRoom:GetCenterPos()
		if ent.Position.X-cpos.X <= -oworld.TilesOnScreenActive.X*40-40 or ent.Position.X-cpos.X >= oworld.TilesOnScreenActive.X*40+40
		or ent.Position.Y-cpos.Y <= -oworld.TilesOnScreenActive.Y*40-40 or ent.Position.Y-cpos.Y >= oworld.TilesOnScreenActive.Y*40+40 then
			oworld.UnloadEntity(ent)
		end
	end
end

local updatecallbacks = {"MC_NPC_UPDATE", "MC_POST_EFFECT_UPDATE", "MC_POST_PICKUP_UPDATE", "MC_POST_PLAYER_UPDATE", "MC_POST_BOMB_UPDATE"}
for _,callback in ipairs(updatecallbacks) do
	oworld:AddCallback(ModCallbacks[callback], oworld.PreUpdateEntities)
	oworld.OldAddCallback(oworld, ModCallbacks[callback], oworld.PostUpdateEntities, -1)
end

oworld:AddCallback(ModCallbacks.MC_POST_SLOT_UPDATE, oworld.PostUpdateEntities)

for _,entityClass in ipairs(oworld.EntityClasses) do
	APIOverride.OverrideClassFunction(entityClass, "CollidesWithGrid", function(ent)
		return ent:GetData().OWCollidesWithGrid
	end)
end

--[[oworld:AddCallback(ModCallbacks.MC_POST_RENDER, function()
	local entities = oworld.roomEntities
	for _,ent in ipairs(entities) do
		if ent.Type ~= EntityType.ENTITY_EFFECT then
			local data = ent:GetData()
			if not data.RedTestCircle then
				data.RedTestCircle = Sprite()
				data.RedTestCircle:Load("gfx/effects/red_circle_test.anm2", true)
				data.RedTestCircle:SetFrame("Idle", 0)
				local scale = ent.Size/49
				data.RedTestCircle.Scale = Vector(scale, scale)
			end
			data.RedTestCircle:Render(Isaac.WorldToScreen(ent.Position), oworld.utils.VEC_ZERO, oworld.utils.VEC_ZERO)
			
			if data.ClosestOccupiedWorldPos then
				local occupiedpos = oworld.WorldPosToPosition(data.ClosestOccupiedWorldPos)
				if not data.RedTestLine then
					data.RedTestLine = Sprite()
					data.RedTestLine:Load("gfx/effects/red_line_test.anm2", true)
					data.RedTestLine:SetFrame("Idle", 0)
				end
				local scale = ((occupiedpos-ent.Position):Length() - ent.Size)/49
				data.RedTestLine.Scale = Vector(scale, scale)
				data.RedTestLine.Rotation = (occupiedpos-ent.Position):GetAngleDegrees()
				data.RedTestLine:Render(Isaac.WorldToScreen(ent.Position+(occupiedpos-ent.Position):Resized(ent.Size)), oworld.utils.VEC_ZERO, oworld.utils.VEC_ZERO)
			end
		end
	end
end)]]

oworld:AddCallback(ModCallbacks.MC_PRE_SPAWN_CLEAN_AWARD, function()
	if not oworld.GetCurrentMode() then
		return true
	end
end)

function oworld:EntitiesInit(ent)
	local data,sprite = ent:GetData(),ent:GetSprite()
	data.FakeGridCollisionClass = ent.GridCollisionClass
	data.oworldStoredSprite = {
		anm2 = nil,
		animname = nil,
		on = "playing",
		replacedpngs = {},
		overlayanimname = nil,
		overlayon = nil,
		overlayrenderpriority = nil
	}
	
	if ent.Type == EntityType.ENTITY_PICKUP then
		if ent.Variant == PickupVariant.PICKUP_COLLECTIBLE or ent.Variant == PickupVariant.PICKUP_SHOPITEM
		or ent.Variant == PickupVariant.PICKUP_BIGCHEST or ent.Variant == PickupVariant.PICKUP_TROPHY
		or ent.Variant == PickupVariant.PICKUP_BED then
			ent.Friction = 0
		end
	end
	if ent.Type == EntityType.ENTITY_SLOT or ent.Type == EntityType.ENTITY_SHOPKEEPER then
		ent.Friction = 0
	end
	
	--[[local data = ent:GetData()
	if data.FakeGridCollisionClass ~= ent.GridCollisionClass and ent.GridCollisionClass ~= EntityGridCollisionClass.GRIDCOLL_NONE then
		data.FakeGridCollisionClass = ent.GridCollisionClass
	end
	ent.GridCollisionClass = EntityGridCollisionClass.GRIDCOLL_NONE]]
end

local initcallbacks = {"MC_POST_PLAYER_INIT", "MC_POST_NPC_INIT", "MC_POST_PICKUP_INIT", "MC_POST_EFFECT_INIT", "MC_POST_BOMB_INIT"}
for _,callback in ipairs(initcallbacks) do
	oworld:AddCallback(ModCallbacks[callback], oworld.EntitiesInit)
end

-- tear collision to grids
function oworld:UpdateTears(tear)
	if not oworld.World[1] then return end
	local tile = oworld.GetWorldTileByPosition(tear.Position)
	local grid = oworld.FakeGridEntities[tile.X][tile.Y]
	if grid and grid.CollisionClass >= GridCollisionClass.COLLISION_OBJECT and not grid.NoCollision and tear.TearFlags & TearFlags.TEAR_SPECTRAL ~= TearFlags.TEAR_SPECTRAL then
		tear:Die()
	end
end
oworld:AddCallback(ModCallbacks.MC_POST_TEAR_UPDATE, oworld.UpdateTears)

-- projectile collision to grids
function oworld:UpdateProjectiles(proj)
	if not oworld.World[1] then return end
	local tile = oworld.GetWorldTileByPosition(proj.Position)
	local grid = oworld.FakeGridEntities[tile.X][tile.Y]
	if grid and grid.CollisionClass >= GridCollisionClass.COLLISION_OBJECT and not grid.NoCollision then
		proj:Die()
	end
end
oworld:AddCallback(ModCallbacks.MC_POST_PROJECTILE_UPDATE, oworld.UpdateProjectiles)

oworld:AddCallback(ModCallbacks.MC_POST_RENDER, function()
	math.randomseed(Isaac.GetTime())
end)

Isaac.DebugString("Loaded Open World core")