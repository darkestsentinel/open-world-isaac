local oworld = require "scripts.openworld"

local game = Game()

if not oworld.CurrentGameMode then
	oworld.CurrentGameMode = nil
	oworld.InModeMenu = false
	oworld.InOptionsMenu = false
	oworld.SelectedOption = 1
	oworld.MenuScroll = 0
	oworld.ModeIsSelected = false
	oworld.TextAlpha = 1
	oworld.BlackBackground = Sprite()
	oworld.BlackBackground:Load("gfx/ui/black_background.anm2", true)
	oworld.BlackBackground:SetFrame("Background", 0)
end

function oworld.SetActiveButton(buttonaction, func) -- keeps activating the button when the button is held
	oworld.ActivateButton = func
	oworld.ActivateButton()
	oworld.ActiveButton = buttonaction
	oworld.MaxActivateButtonDelay = 15
	oworld.ActivateButtonDelay = 15
end

oworld:AddCallback(ModCallbacks.MC_POST_UPDATE, function() -- stops rendering the menu when starting the mode
	local level = game:GetLevel()
	if oworld.ModeIsSelected and oworld.InModeMenu then
		oworld.InModeMenu = false
		game:GetSeeds():RemoveSeedEffect(SeedEffect.SEED_NO_HUD)
		for i=0, game:GetNumPlayers()-1 do
			local player = Isaac.GetPlayer(i)
			player.ControlsEnabled = true
		end
	end
end)

oworld:AddCallback(ModCallbacks.MC_POST_RENDER, function()
	if oworld.InModeMenu then
		local player = Isaac.GetPlayer(0)
		if oworld.ActiveButton then -- functionality of the oworld.SetActiveButton function
			if Input.IsActionPressed(oworld.ActiveButton, player.ControllerIndex) then
				oworld.ActivateButtonDelay = oworld.ActivateButtonDelay-1
				if oworld.ActivateButtonDelay == 0 then
					oworld.ActivateButton()
					oworld.MaxActivateButtonDelay = oworld.MaxActivateButtonDelay*0.8
					oworld.ActivateButtonDelay = math.ceil(oworld.MaxActivateButtonDelay)
				end
			else
				oworld.ActiveButton = nil
			end
		end
		
		if not oworld.ModeIsSelected and not game:IsPaused() then
			-- going up in the menu
			if Input.IsActionTriggered(ButtonAction.ACTION_MENUUP, player.ControllerIndex) then
				oworld.SetActiveButton(ButtonAction.ACTION_MENUUP, function()
					oworld.SelectedOption = oworld.SelectedOption-1
					if oworld.SelectedOption == 0 then
						if oworld.InOptionsMenu then
							oworld.SelectedOption = #oworld.GameModes[oworld.CurrentGameMode].options+1
						else
							oworld.SelectedOption = #oworld.GameModes
						end
					end
				end)
			end
			-- going down in the menu
			if Input.IsActionTriggered(ButtonAction.ACTION_MENUDOWN, player.ControllerIndex) then
				oworld.SetActiveButton(ButtonAction.ACTION_MENUDOWN, function()
					if oworld.InOptionsMenu then
						oworld.SelectedOption = oworld.SelectedOption%(#oworld.GameModes[oworld.CurrentGameMode].options+1)+1
					else
						oworld.SelectedOption = oworld.SelectedOption%(#oworld.GameModes)+1
					end
				end)
			end
			
			-- decreasing a value by pressing left
			if Input.IsActionTriggered(ButtonAction.ACTION_MENULEFT, player.ControllerIndex) then
				oworld.SetActiveButton(ButtonAction.ACTION_MENULEFT, function()
					if oworld.InOptionsMenu and oworld.SelectedOption <= #oworld.GameModes[oworld.CurrentGameMode].options then
						local option = oworld.GameModes[oworld.CurrentGameMode].options[oworld.SelectedOption]
						if type(option.defaultvalue) == "number" then
							oworld[option.var] = math.max(option.lowestvalue or oworld[option.var]-(option.step or 1), oworld[option.var]-(option.step or 1))
						
						elseif type(option.defaultvalue) == "boolean" then
							oworld[option.var] = not oworld[option.var]
						
						elseif type(option.defaultvalue) == "table" then
							option.index = option.index-1
							if option.index == 0 then option.index = #option.defaultvalue end
							oworld[option.var] = option.defaultvalue[option.index]
						end
					end
				end)
			end
			-- increasing a value by pressing right
			if Input.IsActionTriggered(ButtonAction.ACTION_MENURIGHT, player.ControllerIndex) then
				oworld.SetActiveButton(ButtonAction.ACTION_MENURIGHT, function()
					if oworld.InOptionsMenu and oworld.SelectedOption <= #oworld.GameModes[oworld.CurrentGameMode].options then
						local option = oworld.GameModes[oworld.CurrentGameMode].options[oworld.SelectedOption]
						if type(option.defaultvalue) == "number" then
							oworld[option.var] = math.min(option.highestvalue or oworld[option.var]+(option.step or 1), oworld[option.var]+(option.step or 1))
						elseif type(option.defaultvalue) == "boolean" then
							oworld[option.var] = not oworld[option.var]
						elseif type(option.defaultvalue) == "table" then
							option.index = option.index%(#option.defaultvalue)+1
							oworld[option.var] = option.defaultvalue[option.index]
						end
					end
				end)
			end
			
			if Input.IsActionTriggered(ButtonAction.ACTION_MENUCONFIRM, player.ControllerIndex) then
				if oworld.InOptionsMenu then
					-- clicking on the start button in the options menu starts the mode
					if oworld.SelectedOption == #oworld.GameModes[oworld.CurrentGameMode].options+1 then
						oworld.ModeIsSelected = true
						oworld.GenerateWorld = oworld.GameModes[oworld.CurrentGameMode].generateworld
						game:GetLevel():SetStage(LevelStage.STAGE1_1, StageType.STAGETYPE_ORIGINAL)
						Isaac.ExecuteCommand("goto d.0")
						oworld.InModeMenu = false
						game:GetSeeds():RemoveSeedEffect(SeedEffect.SEED_NO_HUD)
						for i=0, game:GetNumPlayers()-1 do
							local player = Isaac.GetPlayer(i)
							player.ControlsEnabled = true
						end
					end
				else
					oworld.CurrentGameMode = oworld.SelectedOption
					-- clicking on a mode with options opens the options menu
					if oworld.GameModes[oworld.CurrentGameMode].options then
						oworld.InOptionsMenu = true
						oworld.SelectedOption = 1
						oworld.MenuScroll = 0
						for _,option in ipairs(oworld.GameModes[oworld.CurrentGameMode].options) do
							if type(option.defaultvalue) == "table" then
								option.index = option.index or 1
								oworld[option.var] = option.defaultvalue[option.index]
							else
								oworld[option.var] = option.defaultvalue
							end
						end
					else -- clicking on a mode without options starts the mode
						oworld.ModeIsSelected = true
						oworld.GenerateWorld = oworld.GameModes[oworld.CurrentGameMode].generateworld
						game:GetLevel():SetStage(LevelStage.STAGE1_1, StageType.STAGETYPE_ORIGINAL)
						Isaac.ExecuteCommand("goto d.0")
						oworld.InModeMenu = false
						game:GetSeeds():RemoveSeedEffect(SeedEffect.SEED_NO_HUD)
						for i=0, game:GetNumPlayers()-1 do
							local player = Isaac.GetPlayer(i)
							player.ControlsEnabled = true
						end
					end
				end
			end
			-- pressing back while in the option menu sends you back to the modes menu
			if Input.IsActionTriggered(ButtonAction.ACTION_MENUBACK, player.ControllerIndex) then
				if oworld.InOptionsMenu then
					oworld.InOptionsMenu = false
					oworld.SelectedOption = oworld.CurrentGameMode
				end
			end
		elseif not game:IsPaused() then -- fades away text whenever the mode is selected
			oworld.TextAlpha = math.max(0,oworld.TextAlpha-0.15)
		end
	
		local crenderpos = oworld.utils.getScreenCenterPosition()
		oworld.BlackBackground:Render(crenderpos, oworld.utils.VEC_ZERO, oworld.utils.VEC_ZERO)
		
		if oworld.InOptionsMenu then -- rendering the options menu
			for i,option in ipairs(oworld.GameModes[oworld.CurrentGameMode].options) do
				local value = oworld[option.var]
				if type(option.defaultvalue) == "number" then -- add the right amount of decimals to a numbered value
					value = tostring(value)
					local max_decimals = math.max(string.len(string.match(tostring(option.defaultvalue), "%.(%d+)") or ""), string.len(string.match(tostring(option.step or 1), "%.(%d+)") or ""))
					local current_decimals = string.len(string.match(value, "%.(%d+)") or "")
					for i=1, max_decimals-current_decimals do
						if i == 1 and current_decimals == 0 then
							value = value..".0"
						else
							value = value.."0"
						end
					end
				elseif type(option.defaultvalue) == "table" then
					value = option.defaultvalue[option.index]
				end
				local str = option.name..": "..tostring(value)
				if oworld.SelectedOption == i then
					oworld.MenuScroll = (oworld.MenuScroll*0.95)+(((i-1)*12)*0.05)
					Isaac.RenderText(str, crenderpos.X-(string.len(str)*3), crenderpos.Y+((i-1)*12)-oworld.MenuScroll, 1, 1, 1, oworld.TextAlpha)
				else
					Isaac.RenderText(str, crenderpos.X-(string.len(str)*3), crenderpos.Y+((i-1)*12)-oworld.MenuScroll, 0.5, 0.5, 0.5, oworld.TextAlpha)
				end
			end
			
			local i = #oworld.GameModes[oworld.CurrentGameMode].options+1
			local str = "Start"
			if oworld.SelectedOption == i then
				oworld.MenuScroll = (oworld.MenuScroll*0.95)+(((i-1)*12)*0.05)
				Isaac.RenderText(str, crenderpos.X-(string.len(str)*3), crenderpos.Y+((i-1)*12)-oworld.MenuScroll, 1, 1, 1, oworld.TextAlpha)
			else
				Isaac.RenderText(str, crenderpos.X-(string.len(str)*3), crenderpos.Y+((i-1)*12)-oworld.MenuScroll, 0.5, 0.5, 0.5, oworld.TextAlpha)
			end
		else -- rendering the modes menu
			for i,mode in ipairs(oworld.GameModes) do
				if oworld.SelectedOption == i then
					oworld.MenuScroll = (oworld.MenuScroll*0.95)+(((i-1)*12)*0.05)
					Isaac.RenderText(mode.name, crenderpos.X-(string.len(mode.name)*3), crenderpos.Y+((i-1)*12)-oworld.MenuScroll, 1, 1, 1, oworld.TextAlpha)
				else
					Isaac.RenderText(mode.name, crenderpos.X-(string.len(mode.name)*3), crenderpos.Y+((i-1)*12)-oworld.MenuScroll, 0.5, 0.5, 0.5, oworld.TextAlpha)
				end
			end
		end
	end
end)

Isaac.DebugString("Loaded Open World menu")