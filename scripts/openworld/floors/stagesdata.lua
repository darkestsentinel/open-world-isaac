local oworld = require "scripts.openworld"

return {
	{
		Name = "Basement",
		LevelStage = {LevelStage.STAGE1_1, LevelStage.STAGE1_2},
		StageType = StageType.STAGETYPE_ORIGINAL,
		Backdrop = oworld.BACKDROP.BASEMENT,
		Floor = oworld.FLOOR.BASEMENT,
		LuaRoomId = 1,
		Bosses = {
			{id = EntityType.ENTITY_DINGLE, variant = 0}, -- Dingle
			{id = EntityType.ENTITY_DUKE, variant = 0}, -- Duke of Flies
			{id = EntityType.ENTITY_GEMINI, variant = 0}, -- Gemini
			{id = EntityType.ENTITY_GURGLING, variant = 1}, -- Gurglings
			{id = EntityType.ENTITY_LARRYJR, variant = 0}, -- Larry jr.
			{id = EntityType.ENTITY_MONSTRO, variant = 0}, -- Monstro
			{id = EntityType.ENTITY_FALLEN, variant = 0}, -- The Fallen
			{id = EntityType.ENTITY_FAMINE, variant = 0}, -- Famine
			{id = EntityType.ENTITY_HEADLESS_HORSEMAN, variant = 0}, -- Headless Horseman
			{id = EntityType.ENTITY_GEMINI, variant = 1}, -- Steven
			{id = EntityType.ENTITY_DINGLE, variant = 1}, -- Dangle
			{id = EntityType.ENTITY_LITTLE_HORN, variant = 0}, -- Little Horn
			{id = EntityType.ENTITY_RAG_MAN, variant = 0}, -- Rag Man
			{id = EntityType.ENTITY_GURGLING, variant = 2} -- Turdlings
		}
	},
	{
		Name = "Cellar",
		LevelStage = {LevelStage.STAGE1_1, LevelStage.STAGE1_2},
		StageType = StageType.STAGETYPE_WOTL,
		Backdrop = oworld.BACKDROP.CELLAR,
		Floor = oworld.FLOOR.SHOP,
		LuaRoomId = 6,
		Bosses = {
			{id = EntityType.ENTITY_GEMINI, variant = 2}, -- Blighted Ovum
			{id = EntityType.ENTITY_DUKE, variant = 0}, -- Duke of Flies
			{id = EntityType.ENTITY_FISTULA_BIG	, variant = 0}, -- Fistula
			{id = EntityType.ENTITY_THE_HAUNT, variant = 0}, -- Haunt
			{id = EntityType.ENTITY_PIN, variant = 0}, -- Pin
			{id = EntityType.ENTITY_WIDOW, variant = 0}, -- Widow
			{id = EntityType.ENTITY_FALLEN, variant = 0}, -- The Fallen
			{id = EntityType.ENTITY_FAMINE, variant = 0}, -- Famine
			{id = EntityType.ENTITY_HEADLESS_HORSEMAN, variant = 0}, -- Headless Horseman
			{id = EntityType.ENTITY_LITTLE_HORN, variant = 0}, -- Little Horn
			{id = EntityType.ENTITY_RAG_MAN, variant = 0}, -- Rag Man
		}
	},
	{
		Name = "The Caves",
		LevelStage = {LevelStage.STAGE2_1, LevelStage.STAGE2_2},
		StageType = StageType.STAGETYPE_ORIGINAL,
		Backdrop = oworld.BACKDROP.CAVES,
		Floor = oworld.FLOOR.CAVES,
		LuaRoomId = 5,
		Bosses = {
			{id = EntityType.ENTITY_CHUB, variant = 0}, -- Chub
			{id = EntityType.ENTITY_DARK_ONE, variant = 0}, -- Dark One
			{id = EntityType.ENTITY_GURDY, variant = 0}, -- Gurdy
			{id = EntityType.ENTITY_GURDY_JR, variant = 0}, -- Gurdy Jr.
			{id = EntityType.ENTITY_MEGA_FATTY, variant = 0}, -- Mega Fatty
			{id = EntityType.ENTITY_MEGA_MAW, variant = 0}, -- Mega Maw
			{id = EntityType.ENTITY_PEEP, variant = 0}, -- Peep
			{id = EntityType.ENTITY_FALLEN, variant = 0}, -- The Fallen
			{id = EntityType.ENTITY_CHUB, variant = 1}, -- Chad
			{id = EntityType.ENTITY_HEADLESS_HORSEMAN, variant = 0}, -- Headless Horseman
			{id = EntityType.ENTITY_PESTILENCE, variant = 0}, -- Pestilence
			{id = EntityType.ENTITY_FORSAKEN, variant = 0}, -- Forsaken
			{id = EntityType.ENTITY_PIN, variant = 2}, -- The Frail
			{id = EntityType.ENTITY_STAIN, variant = 0}, -- Stain
			{id = EntityType.ENTITY_BIG_HORN, variant = 0}, -- Big Horn
			{id = EntityType.ENTITY_RAG_MEGA, variant = 0}, -- Rag Mega
		}
	},
	{
		Name = "Catacombs",
		LevelStage = {LevelStage.STAGE2_1, LevelStage.STAGE2_2},
		StageType = StageType.STAGETYPE_WOTL,
		Backdrop = oworld.BACKDROP.CATACOMBS,
		Floor = oworld.FLOOR.CATACOMBS,
		LuaRoomId = 3,
		Bosses = {
			{id = EntityType.ENTITY_CHUB, variant = 2}, -- Carrion Queen
			{id = EntityType.ENTITY_DARK_ONE, variant = 0}, -- Dark One
			{id = EntityType.ENTITY_GURDY_JR, variant = 0}, -- Gurdy Jr.
			{id = EntityType.ENTITY_LARRYJR, variant = 1}, -- The Hollow
			{id = EntityType.ENTITY_DUKE, variant = 1}, -- Husk
			{id = EntityType.ENTITY_PEEP, variant = 0}, -- Peep
			{id = EntityType.ENTITY_POLYCEPHALUS, variant = 0}, -- Polycephalus
			{id = EntityType.ENTITY_WIDOW, variant = 1}, -- The Wretched
			{id = EntityType.ENTITY_FALLEN, variant = 0}, -- The Fallen
			{id = EntityType.ENTITY_HEADLESS_HORSEMAN, variant = 0}, -- Headless Horseman
			{id = EntityType.ENTITY_PESTILENCE, variant = 0}, -- Pestilence
			{id = EntityType.ENTITY_FORSAKEN, variant = 0}, -- Forsaken
			{id = EntityType.ENTITY_PIN, variant = 2}, -- The Frail
			{id = EntityType.ENTITY_STAIN, variant = 0}, -- Stain
			{id = EntityType.ENTITY_BIG_HORN, variant = 0}, -- Big Horn
			{id = EntityType.ENTITY_RAG_MEGA, variant = 0}, -- Rag Mega
		}
	},
	{
		Name = "The Depths",
		LevelStage = {LevelStage.STAGE3_1, LevelStage.STAGE3_2},
		StageType = StageType.STAGETYPE_ORIGINAL,
		Backdrop = oworld.BACKDROP.DEPTHS,
		Floor = oworld.FLOOR.DEPTHS,
		LuaRoomId = 10,
		Bosses = {
			{id = EntityType.ENTITY_ADVERSARY, variant = 0}, -- The Adversary
			{id = EntityType.ENTITY_CAGE, variant = 0}, -- The Cage
			{id = EntityType.ENTITY_GATE, variant = 0}, -- The Gate
			{id = EntityType.ENTITY_LOKI, variant = 0}, -- Loki
			{id = EntityType.ENTITY_MONSTRO2, variant = 0}, -- Monstro II
			{id = EntityType.ENTITY_FALLEN, variant = 0}, -- The Fallen
			{id = EntityType.ENTITY_MONSTRO2, variant = 1}, -- Gish
			{id = EntityType.ENTITY_HEADLESS_HORSEMAN, variant = 0}, -- Headless Horseman
			{id = EntityType.ENTITY_WAR, variant = 0}, -- War
			{id = EntityType.ENTITY_BROWNIE, variant = 0}, -- Brownie
			{id = EntityType.ENTITY_SISTERS_VIS, variant = 0}, -- Sisters Vis
		}
	},
	{
		Name = "Necropolis",
		LevelStage = {LevelStage.STAGE3_1, LevelStage.STAGE3_2},
		StageType = StageType.STAGETYPE_WOTL,
		Backdrop = oworld.BACKDROP.NECROPOLIS,
		Floor = oworld.FLOOR.NECROPOLIS,
		LuaRoomId = 12,
		Bosses = {
			{id = EntityType.ENTITY_ADVERSARY, variant = 0}, -- The Adversary
			{id = EntityType.ENTITY_CAGE, variant = 0}, -- The Cage
			{id = EntityType.ENTITY_GATE, variant = 0}, -- The Gate
			{id = EntityType.ENTITY_LOKI, variant = 0}, -- Loki
			{id = EntityType.ENTITY_MASK_OF_INFAMY, variant = 0}, -- Mask of Infamy
			{id = EntityType.ENTITY_FALLEN, variant = 0}, -- The Fallen
			{id = EntityType.ENTITY_HEADLESS_HORSEMAN, variant = 0}, -- Headless Horseman
			{id = EntityType.ENTITY_WAR, variant = 0}, -- War
			{id = EntityType.ENTITY_BROWNIE, variant = 0}, -- Brownie
			{id = EntityType.ENTITY_SISTERS_VIS, variant = 0}, -- Sisters Vis
		}
	},
	{
		Name = "Womb",
		LevelStage = {LevelStage.STAGE4_1, LevelStage.STAGE4_2},
		StageType = StageType.STAGETYPE_ORIGINAL,
		Backdrop = oworld.BACKDROP.WOMB,
		Floor = oworld.FLOOR.WOMB,
		LuaRoomId = 16,
		Bosses = {
			{id = EntityType.ENTITY_BLASTOCYST_BIG, variant = 0}, -- Blastocyst
			{id = EntityType.ENTITY_PEEP, variant = 1}, -- Bloat
			{id = EntityType.ENTITY_DADDYLONGLEGS, variant = 0}, -- Daddy Long Legs
			{id = EntityType.ENTITY_LOKI, variant = 1}, -- Lokii
			{id = EntityType.ENTITY_MAMA_GURDY, variant = 0}, -- Mama Gurdy
			{id = EntityType.ENTITY_MR_FRED, variant = 0}, -- Mr. Fred
			{id = EntityType.ENTITY_PIN, variant = 1}, -- Scolex
			{id = EntityType.ENTITY_FISTULA_BIG, variant = 1}, -- Teratoma
			{id = EntityType.ENTITY_FALLEN, variant = 0}, -- The Fallen
			{id = EntityType.ENTITY_WAR, variant = 1}, -- Conquest
			{id = EntityType.ENTITY_DEATH, variant = 0}, -- Death
			{id = EntityType.ENTITY_HEADLESS_HORSEMAN, variant = 0}, -- Headless Horseman
			{id = EntityType.ENTITY_DADDYLONGLEGS, variant = 1}, -- Triachnid
			{id = EntityType.ENTITY_MATRIARCH, variant = 0}, -- The Matriarch
			{id = EntityType.ENTITY_SISTERS_VIS, variant = 0}, -- Sisters Vis
		}
	},
	{
		Name = "Utero",
		LevelStage = {LevelStage.STAGE4_1, LevelStage.STAGE4_2},
		StageType = StageType.STAGETYPE_WOTL,
		Backdrop = oworld.BACKDROP.UTERO,
		Floor = oworld.FLOOR.WOMB,
		LuaRoomId = 15,
		Bosses = {
			{id = EntityType.ENTITY_BLASTOCYST_BIG, variant = 0}, -- Blastocyst
			{id = EntityType.ENTITY_PEEP, variant = 1}, -- Bloat
			{id = EntityType.ENTITY_DADDYLONGLEGS, variant = 0}, -- Daddy Long Legs
			{id = EntityType.ENTITY_LOKI, variant = 1}, -- Lokii
			{id = EntityType.ENTITY_MAMA_GURDY, variant = 0}, -- Mama Gurdy
			{id = EntityType.ENTITY_MR_FRED, variant = 0}, -- Mr. Fred
			{id = EntityType.ENTITY_FISTULA_BIG, variant = 1}, -- Teratoma
			{id = EntityType.ENTITY_FALLEN, variant = 0}, -- The Fallen
			{id = EntityType.ENTITY_WAR, variant = 1}, -- Conquest
			{id = EntityType.ENTITY_DEATH, variant = 0}, -- Death
			{id = EntityType.ENTITY_HEADLESS_HORSEMAN, variant = 0}, -- Headless Horseman
			{id = EntityType.ENTITY_DADDYLONGLEGS, variant = 1}, -- Triachnid
			{id = EntityType.ENTITY_MATRIARCH, variant = 0}, -- The Matriarch
			{id = EntityType.ENTITY_SISTERS_VIS, variant = 0}, -- Sisters Vis
		}
	},
	{
		Name = "Blue Womb",
		LevelStage = {LevelStage.STAGE4_3},
		StageType = StageType.STAGETYPE_AFTERBIRTH,
		Backdrop = oworld.BACKDROP.BLUE_WOMB,
		Floor = oworld.FLOOR.BLUE_WOMB,
		LuaRoomId = 1,
		Bosses = {
			{id = EntityType.ENTITY_HUSH, variant = 0}, -- Hush
		}
	},
	{
		Name = "Cathedral",
		LevelStage = {LevelStage.STAGE5},
		StageType = StageType.STAGETYPE_ORIGINAL,
		Backdrop = oworld.BACKDROP.CATHEDRAL,
		Floor = oworld.FLOOR.CATHEDRAL,
		LuaRoomId = 4,
		Bosses = {
			{id = EntityType.ENTITY_ISAAC, variant = 0}, -- Isaac
		}
	},
	{
		Name = "Sheol",
		LevelStage = {LevelStage.STAGE5},
		StageType = StageType.STAGETYPE_ORIGINAL,
		Backdrop = oworld.BACKDROP.SHEOL,
		Floor = oworld.FLOOR.SHEOL,
		LuaRoomId = 14,
		Bosses = {
			{id = EntityType.ENTITY_SATAN, variant = 0}, -- Satan
		}
	},
	{
		Name = "The Chest",
		LevelStage = {LevelStage.STAGE6},
		StageType = StageType.STAGETYPE_WOTL,
		Backdrop = oworld.BACKDROP.CHEST,
		Floor = oworld.FLOOR.SHOP,
		LuaRoomId = 7
	},
	{
		Name = "Dark Room",
		LevelStage = {LevelStage.STAGE6},
		StageType = StageType.STAGETYPE_WOTL,
		Backdrop = oworld.BACKDROP.DARK_ROOM,
		Floor = oworld.FLOOR.SHEOL,
		LuaRoomId = 9
	},
	{
		Name = "The Void",
		LevelStage = {LevelStage.STAGE6},
		StageType = StageType.STAGETYPE_AFTERBIRTH,
		IsBackdropRandom = true,
		LuaRoomId = 1
	}
}