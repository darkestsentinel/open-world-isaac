local oworld = require "scripts.openworld"
oworld.FloorsInitCalled = true

local game = Game()

local MinimapAPI = require "scripts.minimapapi"

oworld.LevelStage = LevelStage.STAGE1_1

if include then
	oworld.ROOMSHAPE = include("scripts.openworld.floors.roomshapedata")
	oworld.STAGES = include("scripts.openworld.floors.stagesdata")
	include("scripts.openworld.floors.luarooms")

else
	oworld.ROOMSHAPE = require "scripts.openworld.floors.roomshapedata"
	oworld.STAGES = require "scripts.openworld.floors.stagesdata"
	require "scripts.openworld.floors.luarooms"
end

function oworld.AddNewRoomShape(RoomShapeInfo)
	local id = #oworld.ROOMSHAPE + 1
	oworld.ROOMSHAPE[id] = RoomShapeInfo
	
	return id
end

function oworld.AddNewStage(StageInfo)
	table.insert(oworld.STAGES, StageInfo)
end

function oworld.GetWeightedRoomShape()
	if oworld.CustomShapesEnabled then
		return math.random(1, #oworld.ROOMSHAPE)
		
	else
		if math.random() <= 0.85 then
			return RoomShape.ROOMSHAPE_1x1
		else
			return math.random(RoomShape.ROOMSHAPE_IH, RoomShape.ROOMSHAPE_LBR)
		end
	end
end

oworld.OldAddCallback(oworld, ModCallbacks.MC_POST_NEW_ROOM, function()
	local mode = oworld.GetCurrentMode()
	if mode and mode.isfloormode and not oworld.WorldAlreadyGenerated then
		oworld.LoadInStage(oworld.GetRandomStageByLevelStage(oworld.LevelStage), oworld.LevelStage)
	end
	oworld.WorldAlreadyGenerated = nil
end, -1)


oworld:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, function()
	oworld.RoomsLayout = {}
	oworld.PossibleRoomDoorLocations = {}
	oworld.OpenRoomSpots = {}
end)

function oworld.GetRandomStageByLevelStage(levelStage)
	local possibleStages = {}
	for _,stage in ipairs(oworld.STAGES) do
		for _,levelS in ipairs(stage.LevelStage) do
			if levelS == levelStage then
				table.insert(possibleStages, stage)
				break
			end
		end
	end
	
	return possibleStages[math.random(#possibleStages)]
end

function oworld.SetLevelStage(levelStage)
	oworld.LevelStage = levelStage
end

-- numRooms is optional, otherwise gets based on levelStage
function oworld.LoadInStage(stage, levelStage, numRooms)
	oworld.RoomsLayout = {}
	oworld.PossibleRoomDoorLocations = {}
	oworld.OpenRoomSpots = {}
	
	oworld.TookRedHeartDamageThisStage = false
	oworld.TookRedHeartDamageAgainstBossThisStage = false
	oworld.BlewUpShopkeeperThisFloor = false
	oworld.BlewUpBeggarThisFloor = false
	oworld.BlewUpDevilBeggarThisFloor = false
	
	for _,room in pairs(oworld.Rooms) do
		MinimapAPI:RemoveRoomByID(room.ID)
	end

	if oworld.WorldAlreadyGenerated then
		oworld.ResetWorldBoundaries(oworld.WorldWidth, oworld.WorldHeight)
	else
		oworld.SetupOpenWorld(oworld.WorldWidth, oworld.WorldHeight)
		return
	end
	oworld.PostNewLevel = false
	
	oworld.SpawnInStage(stage, levelStage, numRooms, oworld.GetRoomTileByWorldTile(Vector(math.floor(oworld.WorldWidth*0.5),math.floor(oworld.WorldHeight*0.5))))
	
	oworld.TeleportMainPlayer(oworld.PositionToWorldPos(oworld.Rooms[1]:GetGridPosition(67)))
	MinimapAPI:SetPlayerPosition(oworld.Rooms[1].RoomTile)
	
	oworld.RenderFloors()
end

function oworld.SpawnInStage(stage, levelStage, numRooms, startingRoomTile)
	-- generating the starting room
	local backdrop = stage.Backdrop
	local floor = stage.Floor
	local starting_room = oworld.AddRoomToFloor(startingRoomTile, RoomShape.ROOMSHAPE_1x1, RoomType.ROOM_DEFAULT, nil, nil, nil, backdrop, floor, nil, true)

	--oworld.TeleportMainPlayer(oworld.PositionToWorldPos(starting_room:GetGridPosition(67)))
	
	-- getting the number of rooms for the floor
	local level = game:GetLevel()
	local stageid = levelStage
	local number_of_rooms = numRooms or math.random(0,1) + math.floor(stageid * 10/3)
	
	if level:GetCurses() & LevelCurse.CURSE_OF_LABYRINTH == 0 then
		number_of_rooms = math.min(45, math.floor(number_of_rooms * 1.8))
	elseif level:GetCurses() & LevelCurse.CURSE_OF_THE_LOST == 0 then
		number_of_rooms = number_of_rooms + 4
	end
	if stageid == 12 then -- the void
		number_of_rooms = 50 + math.floor(math.random()*10)
	end
	if game.Difficulty == Difficulty.DIFFICULTY_HARD then
		number_of_rooms = number_of_rooms + 2 + math.random(0,1)
	end
	number_of_rooms = number_of_rooms - 1 
	
	-- generating all other default rooms
	local luarooms = oworld.AdjustLuaRooms(oworld.LUA_ROOM[stage.LuaRoomId]())
	local num_generated_rooms = 0
	while num_generated_rooms ~= number_of_rooms do
		local roomtile = oworld.OpenRoomSpots[math.random(#oworld.OpenRoomSpots)]
		local shape = oworld.GetWeightedRoomShape()
		local room = oworld.AddRoomToFloor(roomtile, shape, RoomType.ROOM_DEFAULT, true, shape == RoomShape.ROOMSHAPE_1x1 and 1, luarooms, backdrop, floor)
		if room then
			num_generated_rooms = num_generated_rooms+1
		end
	end
	
	-- generating all special rooms
	local specialluarooms = oworld.AdjustLuaRooms(oworld.LUA_ROOM[0]())
	local mainplayer = Isaac.GetPlayer(0)
	local silver_dollar, bloody_crown, fragmented_card
	for i=0, game:GetNumPlayers()-1 do
		local player = Isaac.GetPlayer(i)
		if player:HasTrinket(TrinketType.TRINKET_SILVER_DOLLAR) then silver_dollar = true end
		if player:HasTrinket(TrinketType.TRINKET_BLOODY_CROWN) then bloody_crown = true end
		if player:HasTrinket(TrinketType.TRINKET_FRAGMENTED_CARD) then fragmented_card = true end
	end
	
	-- Boss
	local bossluarooms
	if stage.Bosses then
		local boss = stage.Bosses[math.random(#stage.Bosses)]
		bossluarooms = oworld.GetLuaRoomsByBoss(specialluarooms, boss.id, boss.variant)
	end
	
	local bossRoom = oworld.TryPlaceRoomInFloor(RoomShape.ROOMSHAPE_1x1, RoomType.ROOM_BOSS, true, {1}, bossluarooms or specialluarooms, backdrop, floor)
	bossRoom.SpawnTrapdoorOnClear = true
	bossRoom.SpawnAngelOrDevilRoomOnClear = true
	-- Super Secret
	oworld.TryPlaceRoomInFloor(RoomShape.ROOMSHAPE_1x1, RoomType.ROOM_SUPERSECRET, true, {1}, specialluarooms, backdrop, floor)
	
	-- Shop
	if (stageid < 7 or stageid < 9 and silver_dollar) and game:GetVictoryLap() < 3 then
		oworld.TryPlaceRoomInFloor(RoomShape.ROOMSHAPE_1x1, RoomType.ROOM_SHOP, true, {1}, specialluarooms, backdrop, floor)
	end
	-- Treasure
	if stageid < 7 or stageid < 9 and bloody_crown then
		oworld.TryPlaceRoomInFloor(RoomShape.ROOMSHAPE_1x1, RoomType.ROOM_TREASURE, true, {1}, specialluarooms, backdrop, floor)
	end
	
	if stageid < 12 then -- Dice and Sacrifice
		local roomtype
		if math.random(1,5) == 1 and mainplayer:GetNumKeys() > 1 or math.random(1,50) == 1 then
			roomtype = RoomType.ROOM_DICE
		else
			roomtype = RoomType.ROOM_SACRIFICE
		end
		
		if math.random(1,4) == 1 and mainplayer:GetHearts()+mainplayer:GetSoulHearts() >= mainplayer:GetMaxHearts() or math.random(1,7) == 1 then
			oworld.TryPlaceRoomInFloor(RoomShape.ROOMSHAPE_1x1, roomtype, true, {1}, specialluarooms, backdrop, floor)
		end
	end
	
	-- Library
	if math.random(1,3) == 1 and game:GetStateFlag(GameStateFlag.STATE_BOOK_PICKED_UP) or math.random(1,20) == 1 then
		oworld.TryPlaceRoomInFloor(RoomShape.ROOMSHAPE_1x1, RoomType.ROOM_LIBRARY, true, {1}, specialluarooms, backdrop, floor)
	end
	
	-- Curse
	if math.random(1,2) == 1 and game:GetStateFlag(GameStateFlag.STATE_DEVILROOM_VISITED) or math.random(1,20) == 1 then
		oworld.TryPlaceRoomInFloor(RoomShape.ROOMSHAPE_1x1, RoomType.ROOM_CURSE, true, {1}, specialluarooms, backdrop, floor)
	end
	
	-- Miniboss
	if math.random(1,4) == 1 and stageid ~= 1 or math.random(1,4) == 1 then
		oworld.TryPlaceRoomInFloor(RoomShape.ROOMSHAPE_1x1, RoomType.ROOM_MINIBOSS, true, {1}, specialluarooms, backdrop, floor)
	end
	
	-- Challenge
	if (math.random(1,2) == 1 or stageid >= 2) and mainplayer:GetHearts() + mainplayer:GetSoulHearts() >= mainplayer:GetMaxHearts() and stageid > 1 then
		oworld.TryPlaceRoomInFloor(RoomShape.ROOMSHAPE_1x1, RoomType.ROOM_CHALLENGE, true, {1}, specialluarooms, backdrop, floor)
	end
	
	-- Chest and Arcade
	do
		local roomtype
		if math.random(1,3) == 1 and mainplayer:GetNumKeys() > 1 or math.random(1,10) == 1 then
			roomtype = RoomType.ROOM_CHEST
		else
			roomtype = RoomType.ROOM_ARCADE
		end
		
		if mainplayer:GetNumCoins() >= 5 and (stageid == 2 or stageid == 4 or stageid == 6 or stageid == 8) then
			oworld.TryPlaceRoomInFloor(RoomShape.ROOMSHAPE_1x1, roomtype, true, {1}, specialluarooms, backdrop, floor)
		end
	end
	
	-- Bedroom
	if stageid < 7 then
		local roomtype
		if math.random(1,2) == 1 then
			roomtype = RoomType.ROOM_ISAACS
		else
			roomtype = RoomType.ROOM_BARREN
		end
		
		local maxhearts
		if mainplayer:GetPlayerType() == PlayerType.PLAYER_THELOST or mainplayer:GetPlayerType() == PlayerType.PLAYER_XXX or mainplayer:GetPlayerType() == PlayerType.PLAYER_THESOUL then
			maxhearts = mainplayer:GetMaxHearts()
		else
			maxhearts = mainplayer:GetMaxHearts() + mainplayer:GetBoneHearts()*2
		end
		
		local second = false
		if (mainplayer:GetHearts() < 2 and mainplayer:GetSoulHearts() <= 0) or (maxhearts <= 0 and mainplayer:GetSoulHearts() <= 2) then
			second = true
		end
		
		if math.random(1,50) == 1 or (math.random(1,5) == 1 and second) then
			oworld.TryPlaceRoomInFloor(RoomShape.ROOMSHAPE_1x1, roomtype, true, {1}, specialluarooms, backdrop, floor)
		end
	end
	
	-- Secret
	local numsecretrooms = 1
	if fragmented_card then numsecretrooms = numsecretrooms+1 end
	for i=1, numsecretrooms do
		if not oworld.TryPlaceRoomInFloor(RoomShape.ROOMSHAPE_1x1, RoomType.ROOM_SECRET, true, {3}, specialluarooms, backdrop, floor) then
			if not oworld.TryPlaceRoomInFloor(RoomShape.ROOMSHAPE_1x1, RoomType.ROOM_SECRET, true, {2}, specialluarooms, backdrop, floor) then
				oworld.TryPlaceRoomInFloor(RoomShape.ROOMSHAPE_1x1, RoomType.ROOM_SECRET, true, {1}, specialluarooms, backdrop, floor)
			end
		end
	end
end

function oworld.GetRoomTileByWorldPos(worldpos)
	local tile = oworld.GetWorldTileByWorldPos(worldpos)
	return Vector(math.floor(tile.X/17),math.floor(tile.Y/11))
end

function oworld.GetRoomTileByWorldTile(worldtile)
	return Vector(math.floor(worldtile.X/17), math.floor(worldtile.Y/11))
end

function oworld.GetRoomByRoomTile(roomtile)
	return oworld.GetRoomByTile(Vector(roomtile.X*17+9, roomtile.Y*11+6))
end

function oworld.GetTopLeftWorldTileByRoomTile(roomtile)
	return Vector(roomtile.X*17, roomtile.Y*11)
end

function oworld.DoesRoomFitInFloor(roomtile, shape, Type, allowednumconnectedrooms)
	shape = shape or RoomShape.ROOMSHAPE_1x1
	Type = Type or RoomType.ROOM_DEFAULT
	if type(allowednumconnectedrooms) == "number" then
		allowednumconnectedrooms = {allowednumconnectedrooms}
	end
	
	local adjacentroomoffsets = {Vector(0,1), Vector(1,0), Vector(0,-1), Vector(-1,0)}
	for _,roomtileoffset in ipairs(oworld.ROOMSHAPE[shape].RoomTiles) do
		croomtile = roomtile+roomtileoffset
		oworld.RoomsLayout[croomtile.X] = oworld.RoomsLayout[croomtile.X] or {}
		if oworld.RoomsLayout[croomtile.X][croomtile.Y] then
			return false
		end
		
		local connectedrooms = {}
		local secret_room_has_connected_default_room = false
		for _,adjacentroomoffset in ipairs(adjacentroomoffsets) do
			local aroom = oworld.GetRoomByRoomTile(Vector(croomtile.X+adjacentroomoffset.X, croomtile.Y+adjacentroomoffset.Y))
			if aroom then
				local roomPlacementAllowed = true
				
				if shape == RoomShape.ROOMSHAPE_IH or shape == RoomShape.ROOMSHAPE_IIH then
					if adjacentroomoffset.Y ~= 0 then
						roomPlacementAllowed = false
					end
				elseif shape == RoomShape.ROOMSHAPE_IV or shape == RoomShape.ROOMSHAPE_IIV then
					if adjacentroomoffset.X ~= 0 then
						roomPlacementAllowed = false
					end
				end
				
				if Type == RoomType.ROOM_SECRET then
					if aroom:GetType() == RoomType.ROOM_BOSS or aroom:GetType() == RoomType.ROOM_SUPERSECRET then
						roomPlacementAllowed = false
					end
					if aroom:GetType() == RoomType.ROOM_DEFAULT then
						secret_room_has_connected_default_room = true
					end
				elseif aroom:GetType() ~= RoomType.ROOM_DEFAULT then
					roomPlacementAllowed = false
				end
				
				for slot=DoorSlot.LEFT0, DoorSlot.DOWN1 do
					local worldtile = aroom:GetDoorSlotWorldTile(slot)
					if worldtile then
						local toplefttile = Vector(croomtile.X*17, croomtile.Y*11)
						local bottomrighttile = Vector(croomtile.X*17+13, croomtile.Y*11+7)
						
						if math.max(math.min(worldtile.X, bottomrighttile.X+8), toplefttile.X-8) == worldtile.X
						and math.max(math.min(worldtile.Y, bottomrighttile.Y+6), toplefttile.Y-6) == worldtile.Y then
							if not aroom:IsDoorSlotAllowed(slot) then
								roomPlacementAllowed = false
							end
						end
					end
				end
				
				local callbackRoomPlacementAllowed = nil
				for _,callback in ipairs(oworld.PostCheckRoomAlignmentCallbacks) do
					local ret = callback.Fn(nil, croomtile, shape, Type, aroom, roomPlacementAllowed)
					if ret ~= nil then
						callbackRoomPlacementAllowed = ret
						break
					end
				end
				
				if callbackRoomPlacementAllowed == false or callbackRoomPlacementAllowed ~= true and not roomPlacementAllowed then
					return false
				end
				
				table.insert(connectedrooms, aroom)
			end
		end
		
		if Type == RoomType.ROOM_SECRET and not secret_room_has_connected_default_room then
			return false
		end
		
		if allowednumconnectedrooms then
			local allowednum = false
			local totalconnectedrooms = #connectedrooms
			for _,numrooms in ipairs(allowednumconnectedrooms) do
				if totalconnectedrooms == numrooms then
					allowednum = true
					break
				end
			end
			
			if not allowednum then
				return false
			end
		end
	end
	
	return true, connectedrooms
end

function oworld.AddRoomToFloor(roomtile, shape, Type, randomizeroomtile, allowednumconnectedrooms, luarooms, backdrop, floor, dontgeneratedoors, ignoreroomchecks)
	Type = Type or RoomType.ROOM_DEFAULT
	if randomizeroomtile then
		roomtile = roomtile-oworld.ROOMSHAPE[shape].RoomTiles[math.random(#oworld.ROOMSHAPE[shape].RoomTiles)]
	end
	local roomallowed, connectedrooms = ignoreroomchecks or oworld.DoesRoomFitInFloor(roomtile, shape, Type, allowednumconnectedrooms)
	if not roomallowed then return end
	
	for _,roomtileoffset in ipairs(oworld.ROOMSHAPE[shape].RoomTiles) do
		local croomtile = roomtile+roomtileoffset
		oworld.RoomsLayout[croomtile.X] = oworld.RoomsLayout[croomtile.X] or {}
		oworld.RoomsLayout[croomtile.X][croomtile.Y] = true
		local total_openspots_removed = 0
		for i=1, #oworld.OpenRoomSpots do
			if croomtile.X == oworld.OpenRoomSpots[i].X and croomtile.Y == oworld.OpenRoomSpots[i].Y then
				table.remove(oworld,OpenRoomSpots, i-total_openspots_removed)
			end
		end
	end
	
	local customroomshape = oworld.utils.copy_table(oworld.ROOMSHAPE[shape].RoomShape)
	
	for _,square in ipairs(customroomshape) do
		square.Tile = square.Tile + Vector(roomtile.X*17,roomtile.Y*11)
	end
	
	--[[local customroomshape = {
		{Tile=Vector(roomtile.X*17,roomtile.Y*11)+oworld.ROOMSHAPE[shape].TileOffset, Size=oworld.ROOMSHAPE[shape].Size}
	}
	if oworld.ROOMSHAPE[shape].TileOffset2 then
		customroomshape[2] = {Tile=Vector(roomtile.X*17,roomtile.Y*11)+oworld.ROOMSHAPE[shape].TileOffset2, Size=oworld.ROOMSHAPE[shape].Size2}
	end]]
	
	local room = oworld.AddRoom(
		customroomshape, 
		backdrop, 
		{1}, 
		floor,
		shape,
		Type
	)

	if oworld.ROOMSHAPE[shape].Overlay then
		room:AddOverlay(oworld.ROOMSHAPE[shape].Overlay)
	end
	if oworld.ROOMSHAPE[shape].CenterTile then
		room.CenterWorldTile = oworld.ROOMSHAPE[shape].CenterTile + room.TopLeftTile - Vector(1,1)
	end
	room.RoomTile = roomtile
	room.LevelStage = oworld.LevelStage
	room.DoorSlotWorldTile = {}
	room:UpdateDoorSlotWorldTiles()
	
	if Type == RoomType.ROOM_SECRET or Type == RoomType.ROOM_SUPERSECRET then
		room:Hide()
	end
	
	if oworld.PostNewLevel and (Type ~= RoomType.ROOM_SECRET and Type ~= RoomType.ROOM_SUPERSECRET or oworld.SecretRoomsVisibleOnMap) then
		oworld.AddRoomToMap(room)
	end
	
	if room.RoomId == 1 then
		room.StartingRoom = true
	else
		room.StartingRoom = false
	end
	
	local roomsinwidth = math.floor(oworld.WorldWidth/17)
	room.GridIndex = roomtile.Y * 12 + roomtile.X + roomtile.Y
	if room.RoomShapeId == RoomShape.ROOMSHAPE_LTL then
		room.SafeGridIndex = room.GridIndex + 1
	else
		room.SafeGridIndex = room.GridIndex
	end
	
	if not dontgeneratedoors then
		for doorslot=DoorSlot.LEFT0, DoorSlot.DOWN1 do
			local tile = room:GetDoorSlotWorldTile(doorslot)
			if tile and room:IsDoorSlotAllowed(doorslot) then
				local placed_door = false
				local doorstructure = oworld.GenerateDoorStructure(oworld.GetGridEntity(tile), 8)
				if doorstructure then
					local connectedroom = oworld.GetRoomByTile(doorstructure.Doors[2].Tile)
					local grid = oworld.GetGridEntity(doorstructure.Doors[2].Tile)
					if grid.Slot and connectedroom:IsDoorSlotAllowed(grid.Slot) then
						room:AddDoor(doorstructure)
					end
				end
				
				if not placed_door then
					table.insert(oworld.PossibleRoomDoorLocations, {Rotation=((doorslot-1)*90)%360, Tile=tile, Slot=doorslot})
				end
			end
		end
	end
	
	local adjacentroomoffsets = {Vector(0,1), Vector(1,0), Vector(0,-1), Vector(-1,0)}
	for _,roomtileoffset in ipairs(oworld.ROOMSHAPE[shape].RoomTiles) do
		for _,adjacentroomoffset in ipairs(adjacentroomoffsets) do
			local x,y = roomtile.X+roomtileoffset.X+adjacentroomoffset.X, roomtile.Y+roomtileoffset.Y+adjacentroomoffset.Y
			local aroom = oworld.GetRoomByRoomTile(Vector(x,y))
			if not aroom then
				table.insert(oworld.OpenRoomSpots, Vector(x,y))
			end
		end
	end
	
	--[[room.MapSprite = Sprite()
	room.MapSprite:Load("gfx/ui/minimap1.anm2", true)
	room.MapSprite:SetFrame("RoomVisited", shape-1)
	room.MapSprite.Color = Color(1,1,1,0.5,0,0,0)
	if oworld.RoomTypeIconName[Type] then
		room.MapRoomTypeIconSprite = Sprite()
		room.MapRoomTypeIconSprite:Load("gfx/ui/minimap1.anm2", true)
		room.MapRoomTypeIconSprite:SetFrame(oworld.RoomTypeIconName[Type], 0)
		room.MapRoomTypeIconSprite.Color = Color(1,1,1,0.5,0,0,0)
	end]]
	
	if luarooms then
		room:SetLayout(room:ChooseLayoutFromLuaRooms(luarooms))
		room:SpawnLayout()
	end
	
	return room, connectedrooms
end

function oworld.TryPlaceRoomInFloor(shape, Type, randomizeroomtile, allowednumconnectedrooms, luarooms, backdrop, floor)
	local validroomspots = {}
	for _,roomtile in ipairs(oworld.OpenRoomSpots) do
		if randomizeroomtile then
			for _,roomtileoffset in ipairs(oworld.ROOMSHAPE[shape].RoomTiles) do
				roomtile = roomtile-roomtileoffset
				if oworld.DoesRoomFitInFloor(roomtile, shape, Type, allowednumconnectedrooms) then
					table.insert(validroomspots, roomtile)
				end
			end
		else
			if oworld.DoesRoomFitInFloor(roomtile, shape, Type, allowednumconnectedrooms) then
				table.insert(validroomspots, roomtile)
			end
		end
	end
	
	local totalvalidroomspots = #validroomspots
	if totalvalidroomspots ~= 0 then
		if Type == RoomType.ROOM_SUPERSECRET then -- puts the supersecret room to the nearest possible location of the boss room
			local bossroom
			for _,room in ipairs(oworld.GetRooms()) do
				if room:GetType() == RoomType.ROOM_BOSS then
					bossroom = room
					break
				end
			end
			
			local closestvalidroomtile, distance
			for _,roomtile in ipairs(validroomspots) do
				local dist = (bossroom.RoomTile-roomtile):LengthSquared()
				if not closestvalidroomtile or dist < distance then
					closestvalidroomtile = roomtile
					distance = dist
				end
			end
			
			return oworld.AddRoomToFloor(closestvalidroomtile, shape, Type, false, allowednumconnectedrooms, luarooms, backdrop, floor)
		else
			return oworld.AddRoomToFloor(validroomspots[math.random(totalvalidroomspots)], shape, Type, false, allowednumconnectedrooms, luarooms, backdrop, floor)
		end
	end
	
	return nil
end

oworld.RoomTypeIconName = {
	[RoomType.ROOM_SHOP] = "Shop",
	[RoomType.ROOM_LIBRARY] = "Shop",
	[RoomType.ROOM_TREASURE] = "TreasureRoom",
	[RoomType.ROOM_BOSS] = "Boss",
	[RoomType.ROOM_MINIBOSS] = "Miniboss",
	[RoomType.ROOM_SECRET] = "SecretRoom",
	[RoomType.ROOM_SUPERSECRET] = "SuperSecretRoom",
	[RoomType.ROOM_ARCADE] = "Arcade",
	[RoomType.ROOM_CURSE] = "CurseRoom",
	[RoomType.ROOM_CHALLENGE] = "AmbushRoom",
	[RoomType.ROOM_LIBRARY] = "Library",
	[RoomType.ROOM_SACRIFICE] = "SacrificeRoom",
	[RoomType.ROOM_ANGEL] = "AngelRoom",
	[RoomType.ROOM_ISAACS] = "IsaacsRoom",
	[RoomType.ROOM_BARREN] = "BarrenRoom",
	[RoomType.ROOM_DICE] = "DiceRoom"
}

MinimapAPI:AddPlayerPositionCallback(oworld, function()
	local mode = oworld.GetCurrentMode()
	if mode and mode.isfloormode then
		local room = game:GetRoom()
		if room.RoomTile then
			return room.RoomTile
		end
	end
end)

oworld:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function(_, room)
	local mode = oworld.GetCurrentMode()
	if mode and mode.isfloormode then
		if oworld.CurrentRoomIndex then
			oworld.PrevRoomIndex = oworld.CurrentRoomIndex
		else
			oworld.PrevRoomIndex = room.GridIndex
		end
		oworld.CurrentRoomIndex = room.GridIndex
	end
end)

function oworld.AddRoomToMap(room)
	local pos = room.RoomTile
	local displayoffset
	if room.RoomShapeId == RoomShape.ROOMSHAPE_LTL then
		displayoffset = Vector(1,0)
	end
	local icon = oworld.RoomTypeIconName[room.Type]
	room.Minimap = MinimapAPI:AddRoom({
		ID = room.RoomId,
		Position = pos,
		Shape = oworld.ROOMSHAPE[room.RoomShapeId].MinimapId or room.RoomShapeId,
		Type = room.Type,
		PermanentIcons = icon and {icon} or nil
	})
	if displayoffset then
		room.Minimap.DisplayPosition = pos + displayoffset
	end
	return room.Minimap
end

-- adding all rooms to the map at the start of the level
oworld:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, function()
	local mode = oworld.GetCurrentMode()
	if mode and mode.isfloormode then
		MinimapAPI.Level = {}
		for _,room in pairs(oworld.Rooms) do
			if (room:GetType() ~= RoomType.ROOM_SECRET and room:GetType() ~= RoomType.ROOM_SUPERSECRET or oworld.SecretRoomsVisibleOnMap) then
				oworld.AddRoomToMap(room)
			end
		end
		oworld.PostNewLevel = true
	end
end)

-- unhide secret rooms when uncovered
oworld:AddCallback(ModCallbacks.MC_POST_BLOW_UP_DOOR, function(_, door)
	if door.TargetRoomType == RoomType.ROOM_SECRET or door.TargetRoomType == RoomType.ROOM_SUPERSECRET then
		local worldtile = door:GetTile()
		local secretRoom
		
		for _,otherDoorData in ipairs(door.DoorStructure.Doors) do
			if otherDoorData.Tile.X ~= worldtile.X or otherDoorData.Tile.Y ~= worldtile.Y then
				secretRoom = oworld.GetRoomByTile(otherDoorData.Tile)
			end
		end
		
		if secretRoom and secretRoom:IsHidden() then
			secretRoom:Reveal()
		end
	
	elseif door.CurrentRoomType == RoomType.ROOM_SECRET or door.CurrentRoomType == RoomType.ROOM_SUPERSECRET then
		local secretRoom = oworld.GetRoomByTile(door:GetTile())
		
		if secretRoom and secretRoom:IsHidden() then
			secretRoom:Reveal()
		end
	end
end)

-- confusing enemies in rooms other than the player's one
oworld:AddCallback(ModCallbacks.MC_PRE_NPC_UPDATE, function(_, npc)
	local mode = oworld.GetCurrentMode()
	if mode and mode.isfloormode then
		local room = game:GetRoom()
		if not room:IsPositionInRoom(npc.Position, 0) then
			npc:AddEntityFlags(EntityFlag.FLAG_CONFUSION)
		end
	end
end)

-- adjusting the world's collectibles to the right pools
do
	local OWCalledGetCollectible = false
	
	oworld:AddCallback(ModCallbacks.MC_PRE_ADD_ENTITY_DATA, function(_, edata, worldtile, world)
		-- in case of collectible, adjusts it to the right pool
		if edata.id == EntityType.ENTITY_PICKUP and (not edata.subtype or edata.subtype == 0) then
			if edata.variant == PickupVariant.PICKUP_COLLECTIBLE then
				local room = oworld.GetRoomByTile(worldtile)
				if room then
					local pool = game:GetItemPool()
					local roomType = room:GetType()

					OWCalledGetCollectible = true
					edata.subtype = pool:GetCollectible(pool:GetPoolForRoom(roomType, room.Seed), true, room.Seed)
				end
			
			elseif edata.variant == PickupVariant.PICKUP_SHOPITEM then
				local room = oworld.GetRoomByTile(worldtile)
				if room then
					local pool = game:GetItemPool()
					local roomType = room:GetType()
					
					if roomType == RoomType.ROOM_DEVIL then
						OWCalledGetCollectible = true
						edata.subtype = pool:GetCollectible(ItemPoolType.POOL_DEVIL, true, room.Seed)
						edata.price = -1
						edata.autoupdateprice = false
						edata.variant = PickupVariant.PICKUP_COLLECTIBLE
					end
				end
			end
		end
		
		return edata
	end)
	
	oworld:AddCallback(ModCallbacks.MC_POST_PICKUP_INIT, function(_, pickup)
		if pickup.Price and not pickup.AutoUpdatePrice then
			local room = oworld.GetRoomByTile(oworld.GetWorldTileByPosition(pickup.Position))
			local pool = game:GetItemPool()
			
			pickup.SubType = pool:GetCollectible(ItemPoolType.POOL_SHOP, true, room.Seed)
		end
	end, PickupVariant.PICKUP_COLLECTIBLE)
end

-- checking if a player has taken a devil deal in this run
oworld:AddCallback(ModCallbacks.MC_PRE_PICKUP_COLLISION, function(_, pickup, coll, low)
	if pickup.Price < 0 then
		oworld.TookDevilDealInRun = true
	end
end, PickupVariant.PICKUP_COLLECTIBLE)

-- checking if a shopkeeper is blown up this floor
oworld:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, function(_, ent, dmg, flags, src, invuln)
	if not oworld.BlewUpShopkeeperThisFloor and flags & DamageFlag.DAMAGE_EXPLOSION ~= 0 then
		oworld.BlewUpShopkeeperThisFloor = true
	end
end, EntityType.ENTITY_SHOPKEEPER)

-- checking if a beggar is blown up this floor
oworld:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, function(_, ent, dmg, flags, src, invuln)
	if not oworld.BlewUpBeggarThisFloor and ent.Variant >= 4 and ent.Variant <= 7 then -- beggar variants
		
		if flags & DamageFlag.DAMAGE_EXPLOSION ~= 0 then
			oworld.BlewUpBeggarThisFloor = true
			
			if ent.Variant == 5 then
				oworld.BlewUpDevilBeggarThisFloor = true
			end
		end
	end
end, EntityType.ENTITY_SLOT)

-- checking if player took red heart damage this floor
oworld:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, function(_, ent, dmg, flags, src, invuln)
	local player = ent:ToPlayer()
	
	if player:GetSoulHearts() + player:GetEternalHearts() == 0 then
		oworld.TookRedHeartDamageThisStage = true
		
		if src and src.Entity and src.Entity:ToNPC() and src.Entity:ToNPC():IsBoss() then
			oworld.TookRedHeartDamageAgainstBossThisStage = true
		end
	end
end, EntityType.ENTITY_PLAYER)

-- calculating chance for devil room to spawn
function oworld.GetDevilRoomChance()
	if oworld.LevelStage == LevelStage.STAGE1_1 then
		return 0
	end
	
	local player = Isaac.GetPlayer(0) -- just the main player for ease
	
	if player:HasCollectible(CollectibleType.COLLECTIBLE_GOAT_HEAD) then
		return 1 -- guarantee
	end
	
	local dchance = 0.01 -- basechance of 1%
	
	-- additions
	if oworld.BlewUpShopkeeperThisFloor then
		dchance = dchance + 0.1
	end
	if player:HasCollectible(CollectibleType.COLLECTIBLE_PENTAGRAM) then
		dchance = dchance + 0.1
		
		if player:GetCollectibleNum() > 1 then
			dchacne = dchance + 0.05
		end
	end
	if player:HasCollectible(CollectibleType.COLLECTIBLE_BLACK_CANDLE) then
		dchance = dchance + 0.15
	end
	if player:HasCollectible(CollectibleType.COLLECTIBLE_BOOK_REVELATIONS) then
		dchance = dchance + 0.175
	end
	if oworld.BlewUpBeggarThisFloor then
		dchance = dchance + 0.35
	end
	if not oworld.TookRedHeartDamageAgainstBossThisStage then
		dchance = dchance + 0.35
	end
	if not oworld.TookRedHeartDamageThisStage then
		dchance = dchance + 0.99
	end
	if player:HasCollectible(CollectibleType.COLLECTIBLE_BOOK_OF_BELIAL) then
		dchance = dchance + 0.125
	end
	
	-- subtractions
	if oworld.LastDevilRoomStage ~= LevelStage.STAGE_NULL then
		if oworld.LastDevilRoomStage - oworld.LevelStage > -2 then
			dchance = dchance * 0.25
		
		elseif oworld.LastDevilRoomStage - oworld.LevelStage == -2 then
			dchance = dchance * 0.5
		end
	end
	
	return dchance
end

-- calculating chance for angel room to spawn
function oworld.GetAngelRoomChance()
	if oworld.LastDevilRoomStage == LevelStage.STAGE_NULL or oworld.TookDevilDealInRun then
		return 0
	end
	
	local player = Isaac.GetPlayer(0) -- just the main player for ease
	
	if player:HasCollectible(CollectibleType.COLLECTIBLE_EUCHARIST) then
		return 1 -- guarantee
	end
	
	local inversechance = 0.5 -- basechance of 50%, inverse for ease of coin toss calculations
	
	if player:HasCollectible(CollectibleType.COLLECTIBLE_ROSARY) then
		inversechance = inversechance * 0.5
	end
	
	-- WIP, Donating 10 coins to the Donation Machine on the current floor
	
	if player:HasCollectible(CollectibleType.COLLECTIBLE_KEY_PIECE_1) then
		inversechance = inversechance * 0.75
	end
	if player:HasCollectible(CollectibleType.COLLECTIBLE_KEY_PIECE_2) then
		inversechance = inversechance * 0.75
	end
	if oworld.BlewUpDevilBeggarThisFloor then
		inversechance = inversechance * 0.75
	end
	
	-- WIP, Donating to a Beggar until it pays out with an item on the current floor
	-- WIP, "You feel blessed!" fortune in the Sacrifice Room of the current floor
	
	return 1 - inversechance
end

-- Clearing Rooms --

oworld:AddCallback(ModCallbacks.MC_POST_CLEAR_ROOM, function(_, room)
	if room.SpawnTrapdoorOnClear then
		local trapdoor = oworld.GridSpawn(GridEntityType.GRID_TRAPDOOR, 0, oworld.GetWorldTileByPosition(Vector(room:GetCenterPos().X, room:GetTopLeftPos().Y + 60)), true)
		trapdoor.OnEnter = function()
			oworld.SetLevelStage(oworld.LevelStage + 1)
			Isaac.ExecuteCommand("goto d.0")
		end
		
		room.SpawnTrapdoorOnClear = false
	end
	
	if room.SpawnAngelOrDevilRoomOnClear then
		for doorslot=DoorSlot.LEFT0, DoorSlot.DOWN1 do
			local tile = room:GetDoorSlotWorldTile(doorslot)
			if tile and room:IsDoorSlotAllowed(doorslot) then
				if math.random() < oworld.GetDevilRoomChance() then
					local targetroomtile = oworld.GetRoomTileByWorldTile(room:GetDoorSlotWorldTile(doorslot) + Vector.FromAngle(doorslot * 90 - 180) * 6)
					local Type
					
					if math.random() < oworld.GetAngelRoomChance() then
						Type = RoomType.ROOM_ANGEL
						
					else
						Type = RoomType.ROOM_DEVIL
						oworld.LastDevilRoomStage = oworld.LevelStage
					end
					
					local specialluarooms = oworld.AdjustLuaRooms(oworld.LUA_ROOM[0]())
					specialluarooms = oworld.AddDevilAndAngelStatues(specialluarooms)
					local backdrop = oworld.SPECIAL_BACKDROP[Type]
					local Floor = oworld.SPECIAL_FLOOR[Type]
					
					local specialroom = oworld.AddRoomToFloor(targetroomtile, RoomShape.ROOMSHAPE_1x1, Type, false, nil, specialluarooms, backdrop, Floor, true, true)
					
					if specialroom then
						local doorstructure = oworld.GenerateDoorStructure(oworld.GetGridEntity(tile), 4)
						if doorstructure then
							local connectedroom = oworld.GetRoomByTile(doorstructure.Doors[2].Tile)
							local grid = oworld.GetGridEntity(doorstructure.Doors[2].Tile)
							if grid.Slot and connectedroom:IsDoorSlotAllowed(grid.Slot) then
								room:AddDoor(doorstructure)
								
								local door = oworld.GetGridEntity(tile)
								if door.Desc.Type == GridEntityType.GRID_DOOR then -- making sure the door actually got added
									door:Open()
								end
							end
						end
					end
				end
				
				break
			end
		end
		
		room.SpawnAngelOrDevilRoomOnClear = false
	end
end)

Isaac.DebugString("Open World Floors are succesfully loaded!")