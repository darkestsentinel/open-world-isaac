local oworld = require "scripts.openworld"

function oworld.AdjustLuaRooms(luarooms) -- stretches big rooms layouts out so that they fit
	for luaroomid,luaroom in ipairs(luarooms) do
		local duplicatedgrids = {}
		for entsdataid,entsdata in ipairs(luaroom) do
			if not entsdata.ISDOOR then
				
				local layoutstretching = oworld.ROOMSHAPE[luaroom.SHAPE].LayoutStretching
				if layoutstretching then
					if layoutstretching.X then
						if layoutstretching.ReverseX then
							local totalx = #layoutstretching.X
							entsdata.GRIDX = entsdata.GRIDX + totalx
							for i,x in ipairs(layoutstretching.X) do
								local addduplicate = false
								if entsdata.GRIDX == x and entsdata[1].TYPE >= 1000 then
									addduplicate = true
								end
								
								local correctxcategory = false
								if entsdata.GRIDX <= x then
									entsdata.GRIDX = entsdata.GRIDX-(totalx-i+1)
									correctxcategory = true
								end
								
								if addduplicate then
									local dupentsdata = oworld.utils.copy_table(entsdata)
									dupentsdata.GRIDX = dupentsdata.GRIDX+1
									if layoutstretching.ReverseY then
										dupentsdata.GRIDY = dupentsdata.GRIDY+4
									end
									table.insert(duplicatedgrids, dupentsdata)
								end
								if correctxcategory then
									break
								end
							end
						else
							local totalx = #layoutstretching.X
							for i,x in ipairs(layoutstretching.X) do
								local addduplicate = false
								if entsdata.GRIDX == x and entsdata[1].TYPE >= 1000 then
									addduplicate = true
								end
								
								local correctxcategory = false
								if entsdata.GRIDX >= x then
									entsdata.GRIDX = entsdata.GRIDX+(totalx-i+1)
									correctxcategory = true
								end
								
								if addduplicate then
									local dupentsdata = oworld.utils.copy_table(entsdata)
									dupentsdata.GRIDX = dupentsdata.GRIDX-1
									if layoutstretching.ReverseY then
										dupentsdata.GRIDY = dupentsdata.GRIDY+4
									end
									table.insert(duplicatedgrids, dupentsdata)
								end
								if correctxcategory then
									break
								end
							end
						end
					end
					
					if layoutstretching.Y then
						if layoutstretching.ReverseY then
							local totaly = #layoutstretching.Y
							entsdata.GRIDY = entsdata.GRIDY + totaly
							for i,y in ipairs(layoutstretching.Y) do
								local addduplicate = false
								if entsdata.GRIDY == y and entsdata[1].TYPE >= 1000 then
									addduplicate = true
								end
								
								local correctycategory = false
								if entsdata.GRIDY <= y then
									entsdata.GRIDY = entsdata.GRIDY-(totaly-i+1)
									correctycategory = true
								end
								
								if addduplicate then
									local dupentsdata = oworld.utils.copy_table(entsdata)
									dupentsdata.GRIDY = dupentsdata.GRIDY+1
									table.insert(duplicatedgrids, dupentsdata)
								end
								if correctycategory then
									break
								end
							end
						else
							local totaly = #layoutstretching.Y
							for i,y in ipairs(layoutstretching.Y) do
								local addduplicate = false
								if entsdata.GRIDY == y and entsdata[1].TYPE >= 1000 then
									addduplicate = true
								end
								
								local correctycategory = false
								if entsdata.GRIDY >= y then
									entsdata.GRIDY = entsdata.GRIDY+(totaly-i+1)
									correctycategory = true
								end
								
								if addduplicate then
									local dupentsdata = oworld.utils.copy_table(entsdata)
									dupentsdata.GRIDY = dupentsdata.GRIDY-1
									table.insert(duplicatedgrids, dupentsdata)
								end
								if correctycategory then
									break
								end
							end
						end
					end
				end
				
			end
		end
		
		for _,entsdata in ipairs(duplicatedgrids) do
			table.insert(luaroom, entsdata)
		end
	end
	
	return luarooms
end

function oworld.AddDevilAndAngelStatues(luarooms)
	for luaroomid,luaroom in ipairs(luarooms) do
		if luaroom.TYPE == RoomType.ROOM_DEVIL then
			table.insert(luaroom, {ISDOOR=false, GRIDX=6, GRIDY=2, 
				{TYPE=999, VARIANT=EffectVariant.DEVIL, SUBTYPE=0, WEIGHT=1}
			})
			
		elseif luaroom.TYPE == RoomType.ROOM_ANGEL then
			table.insert(luaroom, {ISDOOR=false, GRIDX=6, GRIDY=2, 
				{TYPE=999, VARIANT=EffectVariant.ANGEL, SUBTYPE=0, WEIGHT=1}
			})
		end
	end
	
	return luarooms
end

function oworld.GetLuaRoomsByBoss(luarooms, bossId, bossVariant)
	local newluarooms = {}
	
	for luaroomid,luaroom in ipairs(luarooms) do
		if luaroom.TYPE == RoomType.ROOM_BOSS then
			local bossIsFound = false
			for entsdataid, entsdata in ipairs(luaroom) do
			
				if not entsdata.ISDOOR then
					for entdataid, entdata in ipairs(entsdata) do
						
						if entdata.TYPE == bossId and (bossVariant == -1 or entdata.VARIANT == bossVariant) then
							bossIsFound = true
							table.insert(newluarooms, luaroom)
							break
						end
					end
					
					if bossIsFound then
						break
					end
				end
			end
			
			if bossIsFound then
				break
			end
		end
	end
	
	return newluarooms
end

oworld.LUA_ROOM = oworld.LUA_ROOM or {
	[0] = require("resources.luarooms.specialrooms"),
	require("resources.luarooms.basement"),
	require("resources.luarooms.burning basement"),
	require("resources.luarooms.catacombs"),
	require("resources.luarooms.cathedral"),
	require("resources.luarooms.caves"),
	require("resources.luarooms.cellar"),
	require("resources.luarooms.chest"),
	require("resources.luarooms.dank depths"),
	require("resources.luarooms.dark room"),
	require("resources.luarooms.depths"),
	require("resources.luarooms.flooded caves"),
	require("resources.luarooms.necropolis"),
	require("resources.luarooms.scarred womb"),
	require("resources.luarooms.sheol"),
	require("resources.luarooms.utero"),
	require("resources.luarooms.womb")
}