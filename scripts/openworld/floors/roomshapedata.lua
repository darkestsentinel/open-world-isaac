local oworld = require "scripts.openworld"

return {
	[RoomShape.ROOMSHAPE_1x1] = {
		RoomTiles = {oworld.utils.VEC_ZERO},
		TileOffset = oworld.utils.VEC_ZERO,
		RoomShape = {{Tile=oworld.utils.VEC_ZERO, Size=Vector(13,7)}},
		Size = Vector(13,7),
		Overlay = oworld.ROOM_OVERLAY.SHADING_1x1
	},
	[RoomShape.ROOMSHAPE_IH] = {
		RoomTiles = {oworld.utils.VEC_ZERO},
		TileOffset = Vector(0,2),
		RoomShape = {{Tile=Vector(0,2), Size=Vector(13,3)}},
		Size = Vector(13,3),
		Overlay = oworld.ROOM_OVERLAY.SHADING_IH
	},
	[RoomShape.ROOMSHAPE_IV] = {
		RoomTiles = {oworld.utils.VEC_ZERO},
		TileOffset = Vector(4,0),
		RoomShape = {{Tile=Vector(4,0), Size=Vector(5,7)}},
		Size = Vector(5,7),
		Overlay = oworld.ROOM_OVERLAY.SHADING_IV
	},
	[RoomShape.ROOMSHAPE_1x2] = {
		RoomTiles = {oworld.utils.VEC_ZERO, Vector(0,1)},
		TileOffset = oworld.utils.VEC_ZERO,
		RoomShape = {{Tile=oworld.utils.VEC_ZERO, Size=Vector(13,18)}},
		Size = Vector(13,18),
		Overlay = oworld.ROOM_OVERLAY.SHADING_1x2,
		LayoutStretching = {
			Y = {8,7,5,4}
		}
	},
	[RoomShape.ROOMSHAPE_IIV] = {
		RoomTiles = {oworld.utils.VEC_ZERO, Vector(0,1)},
		TileOffset = Vector(4,0),
		Size = Vector(5,18),
		RoomShape = {{Tile=Vector(4,0), Size=Vector(5,18)}},
		Overlay = oworld.ROOM_OVERLAY.SHADING_IIV,
		LayoutStretching = {
			Y = {8,7,5,4}
		}
	},
	[RoomShape.ROOMSHAPE_2x1] = {
		RoomTiles = {oworld.utils.VEC_ZERO, Vector(1,0)},
		TileOffset = oworld.utils.VEC_ZERO,
		RoomShape = {{Tile=oworld.utils.VEC_ZERO, Size=Vector(30,7)}},
		Size = Vector(30,7),
		Overlay = oworld.ROOM_OVERLAY.SHADING_2x1,
		LayoutStretching = {
			X = {17,14,10,7}
		}
	},
	[RoomShape.ROOMSHAPE_IIH] = {
		RoomTiles = {oworld.utils.VEC_ZERO, Vector(1,0)},
		TileOffset = Vector(0,2),
		RoomShape = {{Tile=Vector(0,2), Size=Vector(30,3)}},
		Size = Vector(30,3),
		Overlay = oworld.ROOM_OVERLAY.SHADING_IIH,
		LayoutStretching = {
			X = {17,14,10,7}
		}
	},
	[RoomShape.ROOMSHAPE_2x2] = {
		RoomTiles = {oworld.utils.VEC_ZERO, Vector(1,0), Vector(0,1), Vector(1,1)},
		TileOffset = oworld.utils.VEC_ZERO,
		RoomShape = {{Tile=oworld.utils.VEC_ZERO, Size=Vector(30,18)}},
		Size = Vector(30,18),
		Overlay = oworld.ROOM_OVERLAY.SHADING_2x2,
		LayoutStretching = {
			X = {17,14,10,7},
			Y = {8,7,5,4}
		}
	},
	[RoomShape.ROOMSHAPE_LTL] = {
		RoomTiles = {Vector(1,0), Vector(0,1), Vector(1,1)},
		TileOffset = Vector(17,0),
		TileOffset2 = Vector(0,11),
		Size = Vector(13,11),
		Size2 = Vector(30,7),
		RoomShape = {{Tile=Vector(17,0), Size=Vector(13,11)}, {Tile=Vector(0,11), Size=Vector(30,7)}},
		Overlay = oworld.ROOM_OVERLAY.SHADING_LTL,
		CenterTile = Vector(20, 11),
		LayoutStretching = {
			ReverseX = true,
			X = {8,9,10,11},
			ReverseY = true,
			Y = {7,8,9,10}
		}
	},
	[RoomShape.ROOMSHAPE_LTR] = {
		RoomTiles = {oworld.utils.VEC_ZERO, Vector(0,1), Vector(1,1)},
		TileOffset = oworld.utils.VEC_ZERO,
		TileOffset2 = Vector(0,11),
		Size = Vector(13,11),
		Size2 = Vector(30,7),
		RoomShape = {{Tile=oworld.utils.VEC_ZERO, Size=Vector(13,11)}, {Tile=Vector(0,11), Size=Vector(30,7)}},
		Overlay = oworld.ROOM_OVERLAY.SHADING_LTR,
		CenterTile = Vector(7, 11),
		LayoutStretching = {
			X = {18,17,16,15},
			ReverseY = true,
			Y = {7,8,9,10}
		}
	},
	[RoomShape.ROOMSHAPE_LBL] = {
		RoomTiles = {Vector(1,0), oworld.utils.VEC_ZERO, Vector(1,1)},
		TileOffset = oworld.utils.VEC_ZERO,
		TileOffset2 = Vector(17,7),
		Size = Vector(30,7),
		Size2 = Vector(13,11),
		RoomShape = {{Tile=oworld.utils.VEC_ZERO, Size=Vector(30,7)}, {Tile=Vector(17,7), Size=Vector(13,11)}},
		Overlay = oworld.ROOM_OVERLAY.SHADING_LBL,
		CenterTile = Vector(20, 4),
		LayoutStretching = {
			ReverseX = true,
			X = {8,9,10,11},
			Y = {11,10,9,8}
		}
	},
	[RoomShape.ROOMSHAPE_LBR] = {
		RoomTiles = {Vector(1,0), Vector(0,1), oworld.utils.VEC_ZERO},
		TileOffset = oworld.utils.VEC_ZERO,
		TileOffset2 = Vector(0,7),
		Size = Vector(30,7),
		Size2 = Vector(13,11),
		RoomShape = {{Tile=oworld.utils.VEC_ZERO, Size=Vector(30,7)}, {Tile=Vector(0,7), Size=Vector(13,11)}},
		Overlay = oworld.ROOM_OVERLAY.SHADING_LBR,
		CenterTile = Vector(7, 4),
		LayoutStretching = {
			X = {18,17,16,15},
			Y = {11,10,9,8}
		}
	}
}