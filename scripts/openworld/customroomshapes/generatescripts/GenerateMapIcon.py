from PIL import Image
import math

def generateMapIcon(squaresString, fileName, largeFileName):
	roomPixXSize = 7
	roomPixYSize = 6
	roomXSize = 13
	roomYSize = 7

	roomMap = {}

	squares = squaresString.split("|")
	for square in squares:
		coors = square.split(",")
		x1,y1,x2,y2 = int(coors[0]),int(coors[1]),int(coors[2]),int(coors[3])
		
		if x1+x2 > 13:
			roomPixXSize = 15
			roomXSize = 30
		
		if y1+y2 > 7:
			roomPixYSize = 13
			roomYSize = 18
		
		for x in range(x2):
			for y in range(y2):
				roomMap[str(x1 + x) + "." + str(y1 + y)] = True

	imageXSize = roomPixXSize*2 + 4
	imageYSize = roomPixYSize*2 + 4

	img = Image.new('RGBA', (34, 30), (0,0,0,0))
	largeimg = Image.new('RGBA', (64, 56), (0,0,0,0))
	pix = img.load()

	pixToRoomXCoor = roomXSize/(roomPixXSize + 1)
	pixToRoomYCoor = roomYSize/(roomPixYSize + 1)

	def inRoom(pixX, pixY):
		x,y = math.floor(pixX*pixToRoomXCoor), math.floor(pixY*pixToRoomYCoor)
		
		checkStr = str(x) + "." + str(y)
		return checkStr in roomMap

	for x in range(0, roomPixXSize + 2):
		for y in range(0, roomPixYSize + 2):
			if inRoom(x,y):
				r,d,l,u = inRoom(x+1,y), inRoom(x,y+1), inRoom(x-1,y), inRoom(x,y-1)
				
				if (not r and not d or not d and not l or not l and not u or not u and not r): #in corner of room
					pix[x,y] = (0,0,0,255)
					pix[x + 17,y] = (0,0,0,255)
					pix[x,y + 15] = (0,0,0,255)
				
				elif (x != 0 and y != 0 and pix[x-1,y-1][0] == 0): #if top-left pixel of current pixel is black, apply shadow
					pix[x,y] = (64, 64, 64, 255)
					pix[x + 17,y] = (128, 128, 128, 255)
					pix[x,y + 15] = (29, 29, 28, 255)
					
				elif (x != 0 and y != 0 and pix[x-1,y-1][0] != 0 and (pix[x,y-1][0] == 0 and inRoom(x+1,y) or pix[x-1,y][0] == 0 and inRoom(x,y+1))): #more complex cases where shadow needs to be applied
					pix[x,y] = (64, 64, 64, 255)
					pix[x + 17,y] = (128, 128, 128, 255)
					pix[x,y + 15] = (29, 29, 28, 255)
				
				else:
					pix[x,y] = (100, 100, 100, 255)
					pix[x + 17,y] = (212, 212, 212, 255)
					pix[x,y + 15] = (42, 41, 35, 255)
				
			else: #outside room
				pix[x,y] = (0,0,0,255)
				pix[x + 17,y] = (0,0,0,255)
				pix[x,y + 15] = (0,0,0,255)
				
	pix = largeimg.load()
	
	pixToRoomXCoor = roomXSize/(roomPixXSize*2 + 1)
	pixToRoomYCoor = roomYSize/(roomPixYSize*2 + 1)

	def inRoom(pixX, pixY):
		x = None
		if (pixX > 16):
			x = math.floor(pixX*pixToRoomXCoor)
		else:
			x = math.ceil(pixX*pixToRoomXCoor)
		
		y = None
		if (pixY > 16):
			y = math.floor(pixY*pixToRoomYCoor)
		else:
			y = math.ceil(pixY*pixToRoomYCoor)
		
		checkStr = str(x) + "." + str(y)
		return checkStr in roomMap

	for x in range(0, roomPixXSize*2 + 2):
		for y in range(0, roomPixYSize*2 + 2):
			if inRoom(x,y):
				r,d,l,u = inRoom(x+1,y), inRoom(x,y+1), inRoom(x-1,y), inRoom(x,y-1)
				
				if (not r and not d or not d and not l or not l and not u or not u and not r): #in corner of room
					pix[x,y] = (0,0,0,255)
					pix[x + 32,y] = (0,0,0,255)
					pix[x,y + 28] = (0,0,0,255)
				
				elif (x != 0 and y != 0 and pix[x-1,y-1][0] == 0): #if top-left pixel of current pixel is black, apply shadow
					pix[x,y] = (64, 64, 64, 255)
					pix[x + 32,y] = (128, 128, 128, 255)
					pix[x,y + 28] = (29, 29, 28, 255)
					
				elif (x != 0 and y != 0 and pix[x-1,y-1][0] != 0 and (pix[x,y-1][0] == 0 or pix[x-1,y][0] == 0)): #more complex cases where shadow needs to be applied
					pix[x,y] = (64, 64, 64, 255)
					pix[x + 32,y] = (128, 128, 128, 255)
					pix[x,y + 28] = (29, 29, 28, 255)
				
				else:
					pix[x,y] = (100, 100, 100, 255)
					pix[x + 32,y] = (212, 212, 212, 255)
					pix[x,y + 28] = (42, 41, 35, 255)
				
			else: #outside room
				pix[x,y] = (0,0,0,255)
				pix[x + 32,y] = (0,0,0,255)
				pix[x,y + 28] = (0,0,0,255)

	img.save(fileName)
	largeimg.save(largeFileName)

print('Put in a RoomShape to generate a fitting map icon.')
print('RoomShape exists out of squares: "topLeftXValue,topLeftYValue,squareWidth,squareHeight"')
print('Squares can be differentiated with the "|" symbol.')
print('Example of RoomShape existing of 3 different squares: "1,1,10,5|1,5,5,5|11,3,5,7"')

user_input = input(" > ")
	
generateMapIcon(user_input, "custom_roomshape.png", "custom_large_roomshape.png")

input("Done!")