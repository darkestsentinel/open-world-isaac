from PIL import Image
import math
import random

def generateMapIcon(squaresString, fileName, largeFileName):
	roomPixXSize = 7
	roomPixYSize = 6
	roomXSize = 13
	roomYSize = 7

	roomMap = {}

	squares = squaresString.split("|")
	for square in squares:
		coors = square.split(",")
		x1,y1,x2,y2 = int(coors[0]),int(coors[1]),int(coors[2]),int(coors[3])
		
		if x1+x2 > 13:
			roomPixXSize = 15
			roomXSize = 30
		
		if y1+y2 > 7:
			roomPixYSize = 13
			roomYSize = 18
		
		for x in range(x2):
			for y in range(y2):
				roomMap[str(x1 + x) + "." + str(y1 + y)] = True

	imageXSize = roomPixXSize*2 + 4
	imageYSize = roomPixYSize*2 + 4

	img = Image.new('RGBA', (34, 30), (0,0,0,0))
	largeimg = Image.new('RGBA', (64, 56), (0,0,0,0))
	pix = img.load()

	pixToRoomXCoor = roomXSize/(roomPixXSize + 1)
	pixToRoomYCoor = roomYSize/(roomPixYSize + 1)

	def inRoom(pixX, pixY):
		x,y = math.floor(pixX*pixToRoomXCoor), math.floor(pixY*pixToRoomYCoor)
		
		checkStr = str(x) + "." + str(y)
		return checkStr in roomMap

	for x in range(0, roomPixXSize + 2):
		for y in range(0, roomPixYSize + 2):
			if inRoom(x,y):
				r,d,l,u = inRoom(x+1,y), inRoom(x,y+1), inRoom(x-1,y), inRoom(x,y-1)
				
				if (not r and not d or not d and not l or not l and not u or not u and not r): #in corner of room
					pix[x,y] = (0,0,0,255)
					pix[x + 17,y] = (0,0,0,255)
					pix[x,y + 15] = (0,0,0,255)
				
				elif (x != 0 and y != 0 and pix[x-1,y-1][0] == 0): #if top-left pixel of current pixel is black, apply shadow
					pix[x,y] = (64, 64, 64, 255)
					pix[x + 17,y] = (128, 128, 128, 255)
					pix[x,y + 15] = (29, 29, 28, 255)
					
				elif (x != 0 and y != 0 and pix[x-1,y-1][0] != 0 and (pix[x,y-1][0] == 0 and inRoom(x+1,y) or pix[x-1,y][0] == 0 and inRoom(x,y+1))): #more complex cases where shadow needs to be applied
					pix[x,y] = (64, 64, 64, 255)
					pix[x + 17,y] = (128, 128, 128, 255)
					pix[x,y + 15] = (29, 29, 28, 255)
				
				else:
					pix[x,y] = (100, 100, 100, 255)
					pix[x + 17,y] = (212, 212, 212, 255)
					pix[x,y + 15] = (42, 41, 35, 255)
				
			else: #outside room
				pix[x,y] = (0,0,0,255)
				pix[x + 17,y] = (0,0,0,255)
				pix[x,y + 15] = (0,0,0,255)
				
	pix = largeimg.load()
	
	pixToRoomXCoor = roomXSize/(roomPixXSize*2 + 1)
	pixToRoomYCoor = roomYSize/(roomPixYSize*2 + 1)

	def inRoom(pixX, pixY):
		x = None
		if (pixX > 16):
			x = math.floor(pixX*pixToRoomXCoor)
		else:
			x = math.ceil(pixX*pixToRoomXCoor)
		
		y = None
		if (pixY > 16):
			y = math.floor(pixY*pixToRoomYCoor)
		else:
			y = math.ceil(pixY*pixToRoomYCoor)
		
		checkStr = str(x) + "." + str(y)
		return checkStr in roomMap

	for x in range(0, roomPixXSize*2 + 2):
		for y in range(0, roomPixYSize*2 + 2):
			if inRoom(x,y):
				r,d,l,u = inRoom(x+1,y), inRoom(x,y+1), inRoom(x-1,y), inRoom(x,y-1)
				
				if (not r and not d or not d and not l or not l and not u or not u and not r): #in corner of room
					pix[x,y] = (0,0,0,255)
					pix[x + 32,y] = (0,0,0,255)
					pix[x,y + 28] = (0,0,0,255)
				
				elif (x != 0 and y != 0 and pix[x-1,y-1][0] == 0): #if top-left pixel of current pixel is black, apply shadow
					pix[x,y] = (64, 64, 64, 255)
					pix[x + 32,y] = (128, 128, 128, 255)
					pix[x,y + 28] = (29, 29, 28, 255)
					
				elif (x != 0 and y != 0 and pix[x-1,y-1][0] != 0 and (pix[x,y-1][0] == 0 or pix[x-1,y][0] == 0)): #more complex cases where shadow needs to be applied
					pix[x,y] = (64, 64, 64, 255)
					pix[x + 32,y] = (128, 128, 128, 255)
					pix[x,y + 28] = (29, 29, 28, 255)
				
				else:
					pix[x,y] = (100, 100, 100, 255)
					pix[x + 32,y] = (212, 212, 212, 255)
					pix[x,y + 28] = (42, 41, 35, 255)
				
			else: #outside room
				pix[x,y] = (0,0,0,255)
				pix[x + 32,y] = (0,0,0,255)
				pix[x,y + 28] = (0,0,0,255)

	img.save(fileName)
	largeimg.save(largeFileName)

def generateRoomShading(squaresString, fileName):
	minX = 1000
	maxX = -1000
	minY = 1000
	maxY = -1000

	roomMap = {}
	wallMap = {}
	cornerMap = {}

	squares = squaresString.split("|")
	for square in squares:
		coors = square.split(",")
		x1,y1,x2,y2 = int(coors[0]),int(coors[1]),int(coors[2]),int(coors[3])
		
		if x1 - 2 < minX:
			minX = x1 - 2
		
		if y1 - 2 < minY:
			minY = y1 - 2
		
		if x1+x2 + 2 > maxX:
			maxX = x1+x2 + 2
		
		if y1+y2 + 2 > maxY:
			maxY = y1+y2 + 2
		
		for x in range(x2):
			for y in range(y2):
				roomMap[str(x1 + x) + "." + str(y1 + y)] = True
	
	imageXOffset = (minX + 1)*26
	imageYOffset = (minY + 1)*26

	imageXSize = (maxX - minX)*26
	imageYSize = (maxY - minY)*26

	defaultAlpha = 89
	img = Image.new('RGBA', (imageXSize, imageYSize), (0,0,0,0))
	pix = img.load()
	
	def inRoom(x, y):
		checkStr = str(x) + "." + str(y)
		return checkStr in roomMap
	
	coordinalAdjTiles = [[1,0],[0,1],[-1,0],[0,-1]]
	diagonalAdjTiles = [[1,1],[-1,1],[-1,-1],[1,-1]]
	
	for x in range(minX, maxX):
		for y in range(minY, maxY):
			if inRoom(x,y):
				checkrange = 1
				
				nearestWalls = []
				foundWall = False
				wallDistance = 0
				
				while (True):
					for i in range(0, checkrange + 1):
						checkX = x + i
						checkY = y + (checkrange - i)
						
						if (not inRoom(checkX, checkY)):
							foundWall = True
							nearestWalls.append([checkX, checkY])
						
						checkX = x + -i
						checkY = y + (checkrange - i)
						
						if (not inRoom(checkX, checkY)):
							foundWall = True
							nearestWalls.append([checkX, checkY])
						
						checkX = x + i
						checkY = y + -(checkrange - i)
						
						if (not inRoom(checkX, checkY)):
							foundWall = True
							nearestWalls.append([checkX, checkY])
						
						checkX = x + -i
						checkY = y + -(checkrange - i)
						
						if (not inRoom(checkX, checkY)):
							foundWall = True
							nearestWalls.append([checkX, checkY])
					
					if (foundWall):
						wallDistance = checkrange
						break
					
					checkrange += 1
				
				for pixX in range((x + 1)*26 - imageXOffset, (x + 2)*26 - imageXOffset):
					for pixY in range((y + 1)*26 - imageYOffset, (y + 2)*26 - imageYOffset):
						if foundWall:
							alpha = 0
							for wall in nearestWalls:
								closestXWallPos = min(max(pixX, wall[0]*26 + 26 - imageXOffset), wall[0]*26 + 52 - imageXOffset)
								closestYWallPos = min(max(pixY, wall[1]*26 + 26 - imageYOffset), wall[1]*26 + 52 - imageYOffset)
								xDiff = abs(closestXWallPos - pixX)
								yDiff = abs(closestYWallPos - pixY)
								
								newAlpha = max(math.floor(120 / (1 + ((xDiff + yDiff)*0.5 + max(xDiff, yDiff)*0.5)*0.1) - (xDiff + yDiff)*0.1), 1)
								if newAlpha > alpha:
									alpha = newAlpha
							
							pix[pixX, pixY] = (0,0,0,alpha)
							
						else:
							pix[pixX, pixY] = (0,0,0,0)
							
			else:
				numInRoomCoorAdjTiles = 0 #number of in-room tiles next to this tile, max counts to 2
				numLoops = 0 
				lastSuccesfullLoop = 0
				
				for tile in coordinalAdjTiles:
					numLoops += 1
					if inRoom(x + tile[0], y + tile[1]):
						numInRoomCoorAdjTiles += 1
						lastSuccesfullLoop = numLoops
						
						if numInRoomCoorAdjTiles == 2:
							break
				
				if numInRoomCoorAdjTiles == 2:
					rotationValue = numLoops*90 - 135 #inner-corner orientation
					
					if numLoops == 4 and inRoom(x + 1, y):
						rotationValue += 90
					
					cornerMap[str(x) + "." + str(y)] = [rotationValue, False]
				
				elif numInRoomCoorAdjTiles == 1:
					wallMap[str(x) + "." + str(y)] = (lastSuccesfullLoop - 1)*90 #wall orientation
					
				else:
					i = 0
					for tile in diagonalAdjTiles:
						i += 1
						if inRoom(x + tile[0], y + tile[1]):
							rotationValue = i*90 - 45 #outer-corner orientation
							cornerMap[str(x) + "." + str(y)] = [rotationValue, True]
	
	checkedOutWallMap = {}
	
	for x in range(minX, maxX):
		for y in range(minY, maxY):
			xyStr = str(x) + "." + str(y)
			if xyStr in cornerMap:
				corner = cornerMap[xyStr]
				
				cornerMinX = (x + 1)*26 - imageXOffset
				cornerMaxX = (x + 2)*26 - imageXOffset
				cornerMinY = (y + 1)*26 - imageYOffset
				cornerMaxY = (y + 2)*26 - imageYOffset
				
				if corner[0] == 45:
					cornerMinX -= 26
					cornerMinY -= 26
				
				elif corner[0] == 135:
					cornerMaxX += 26
					cornerMinY -= 26
				
				elif corner[0] == 225:
					cornerMaxX += 26
					cornerMaxY += 26
				
				elif corner[0] == 315:
					cornerMinX -= 26
					cornerMaxY += 26
				
				rangeMinX = cornerMinX
				rangeMaxX = cornerMaxX
				rangeMinY = cornerMinY
				rangeMaxY = cornerMaxY
				
				innerCornerAtY = None
				innerCornerAtX = None
				
				if corner[1]: #is outer-corner
					for i in range(0,2):
						dir = math.floor((corner[0] - 45)/90 + i)%4
						tile = coordinalAdjTiles[dir]
						checkX = x
						checkY = y
						
						while (True):
							checkX += tile[0]
							checkY += tile[1]
							
							checkxyStr = str(checkX) + "." + str(checkY)
							
							if checkxyStr in wallMap:
								checkedOutWallMap[checkxyStr] = True
								if dir == 0:
									rangeMaxX += 26
								
								elif dir == 1:
									rangeMaxY += 26
								
								elif dir == 2:
									rangeMinX -= 26
								
								else:
									rangeMinY -= 26
							
							elif checkxyStr in cornerMap:
								innerCorner = cornerMap[checkxyStr]
								if (not innerCorner[1]): #is inner-corner
									if dir == 0:
										rangeMaxX += 26
										innerCornerAtX = (checkX + 2)*26 - imageXOffset
								
									elif dir == 1:
										rangeMaxY += 26
										innerCornerAtY = (checkY + 2)*26 - imageYOffset
									
									elif dir == 2:
										rangeMinX -= 26
										innerCornerAtX = (checkX + 1)*26 - imageXOffset
									
									else:
										rangeMinY -= 26
										innerCornerAtY = (checkY + 1)*26 - imageYOffset
								
								break
								
							else:
								break
				
					if corner[0] == 45:
						for pixX in range(rangeMinX, rangeMaxX):
							for pixY in range(rangeMinY, rangeMaxY):
								if pixX >= cornerMinX and pixX < cornerMaxX or pixY >= cornerMinY and pixY < cornerMaxY:
									distanceFromRoom = max(cornerMaxX - pixX, cornerMaxY - pixY)
									
									if innerCornerAtX != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtX - pixX))
									
									if innerCornerAtY != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtY - pixY))
									
									if distanceFromRoom >= 32:
										if pix[pixX, pixY][3] == 0:
											pix[pixX, pixY] = (0,0,0,min(max(1, math.floor((distanceFromRoom - 34)*defaultAlpha*0.2)), defaultAlpha))
									
									else:
										xDiff = pixX - ((x + 1)*26 - imageXOffset) - (pixY - ((y + 1)*26 - imageYOffset))
										yDiff = abs(pixY - ((y + 1)*26 - imageYOffset) - (pixX - ((x + 1)*26 - imageXOffset)))
										alphaMult = 1
										if xDiff > 0:
											alphaMult = 1 / ((rangeMaxX - rangeMinX - 26)*0.01)
										
										else:
											alphaMult = 1 / ((rangeMaxY - rangeMinY - 26)*0.01)
										
										xDiff = abs(xDiff)
										
										alpha = max(math.floor(140 / (1 + ((xDiff + yDiff)*0.5 + max(xDiff, yDiff)*0.5)*0.02*alphaMult) - (xDiff + yDiff)*0.1*alphaMult), 1)
										
										if distanceFromRoom >= 27:
											alpha = max(math.floor(alpha * (1 - (distanceFromRoom - 26)*0.2)), 1)
										
										elif distanceFromRoom <= 2:
											alpha = max(alpha, (3 - distanceFromRoom)*40)
										
										if alpha > pix[pixX, pixY][3]:
											pix[pixX, pixY] = (0,0,0,alpha)
					
					elif corner[0] == 135:
						for pixX in range(rangeMinX, rangeMaxX):
							for pixY in range(rangeMinY, rangeMaxY):
								if pixX >= cornerMinX and pixX < cornerMaxX or pixY >= cornerMinY and pixY < cornerMaxY:
									distanceFromRoom = max(52 - (cornerMaxX - pixX), cornerMaxY - pixY)
									
									if innerCornerAtX != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtX - pixX))
									
									if innerCornerAtY != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtY - pixY))
									
									if distanceFromRoom >= 32:
										if pix[pixX, pixY][3] == 0:
											pix[pixX, pixY] = (0,0,0,min(max(1, math.floor((distanceFromRoom - 34)*defaultAlpha*0.2)), defaultAlpha))
									
									else:
										xDiff = pixX - ((x + 2)*26 - imageXOffset) - (((y + 1)*26 - imageYOffset) - pixY)
										yDiff = abs(pixY - ((y + 2)*26 - imageYOffset) - (((x + 1)*26 - imageXOffset) - pixX))
										
										alphaMult = 1
										if xDiff < 0:
											alphaMult = 1 / ((rangeMaxX - rangeMinX - 26)*0.01)
										
										else:
											alphaMult = 1 / ((rangeMaxY - rangeMinY - 26)*0.01)
										
										xDiff = abs(xDiff)
										
										alpha = max(math.floor(140 / (1 + ((xDiff + yDiff)*0.5 + max(xDiff, yDiff)*0.5)*0.02*alphaMult) - (xDiff + yDiff)*0.1*alphaMult), 1)
										
										if distanceFromRoom >= 27:
											alpha = max(math.floor(alpha * (1 - (distanceFromRoom - 26)*0.2)), 1)
										
										elif distanceFromRoom <= 2:
											alpha = max(alpha, (3 - distanceFromRoom)*40)
										
										if alpha > pix[pixX, pixY][3]:
											pix[pixX, pixY] = (0,0,0,alpha)
					
					elif corner[0] == 225:
						for pixX in range(rangeMinX, rangeMaxX):
							for pixY in range(rangeMinY, rangeMaxY):
								if pixX >= cornerMinX and pixX < cornerMaxX or pixY >= cornerMinY and pixY < cornerMaxY:
									distanceFromRoom = max(52 - (cornerMaxX - pixX), 52 - (cornerMaxY - pixY))
									
									if innerCornerAtX != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtX - pixX))
									
									if innerCornerAtY != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtY - pixY))
									
									if distanceFromRoom >= 32:
										if pix[pixX, pixY][3] == 0:
											pix[pixX, pixY] = (0,0,0,min(max(1, math.floor((distanceFromRoom - 34)*defaultAlpha*0.2)), defaultAlpha))
									
									else:
										xDiff = pixX - ((x + 1)*26 - imageXOffset) - (pixY - ((y + 1)*26 - imageYOffset))
										yDiff = abs(pixY - ((y + 1)*26 - imageYOffset) - (pixX - ((x + 1)*26 - imageXOffset)))
										
										alphaMult = 1
										if xDiff < 0:
											alphaMult = 1 / ((rangeMaxX - rangeMinX - 26)*0.01)
										
										else:
											alphaMult = 1 / ((rangeMaxY - rangeMinY - 26)*0.01)
										
										xDiff = abs(xDiff)
										
										alpha = max(math.floor(140 / (1 + ((xDiff + yDiff)*0.5 + max(xDiff, yDiff)*0.5)*0.02*alphaMult) - (xDiff + yDiff)*0.1*alphaMult), 1)
										
										if distanceFromRoom >= 27:
											alpha = max(math.floor(alpha * (1 - (distanceFromRoom - 26)*0.2)), 1)
										
										elif distanceFromRoom <= 2:
											alpha = max(alpha, (3 - distanceFromRoom)*40)
										
										if alpha > pix[pixX, pixY][3]:
											pix[pixX, pixY] = (0,0,0,alpha)
					
					else:
						for pixX in range(rangeMinX, rangeMaxX):
							for pixY in range(rangeMinY, rangeMaxY):
								if pixX >= cornerMinX and pixX < cornerMaxX or pixY >= cornerMinY and pixY < cornerMaxY:
									distanceFromRoom = max(cornerMaxX - pixX, 52 - (cornerMaxY - pixY))
									
									if innerCornerAtX != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtX - pixX))
									
									if innerCornerAtY != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtY - pixY))
									
									if distanceFromRoom >= 32:
										if pix[pixX, pixY][3] == 0:
											pix[pixX, pixY] = (0,0,0,min(max(1, math.floor((distanceFromRoom - 34)*defaultAlpha*0.2)), defaultAlpha))
									
									else:
										xDiff = pixX - ((x + 2)*26 - imageXOffset) - (((y + 1)*26 - imageYOffset) - pixY)
										yDiff = abs(pixY - ((y + 2)*26 - imageYOffset) - (((x + 1)*26 - imageXOffset) - pixX))
										
										alphaMult = 1
										if xDiff > 0:
											alphaMult = 1 / ((rangeMaxX - rangeMinX - 26)*0.01)
										
										else:
											alphaMult = 1 / ((rangeMaxY - rangeMinY - 26)*0.01)
										
										xDiff = abs(xDiff)
										
										alpha = max(math.floor(140 / (1 + ((xDiff + yDiff)*0.5 + max(xDiff, yDiff)*0.5)*0.02*alphaMult) - (xDiff + yDiff)*0.1*alphaMult), 1)
										
										if distanceFromRoom >= 27:
											alpha = max(math.floor(alpha * (1 - (distanceFromRoom - 26)*0.2)), 1)
										
										elif distanceFromRoom <= 2:
											alpha = max(alpha, (3 - distanceFromRoom)*40)
										
										if alpha > pix[pixX, pixY][3]:
											pix[pixX, pixY] = (0,0,0,alpha)
	
	for x in range(minX, maxX):
		for y in range(minY, maxY):
			xyStr = str(x) + "." + str(y)
			
			if xyStr in wallMap:
				if not (xyStr in checkedOutWallMap):
					angle = wallMap[xyStr]
				
					rangeMinX = (x + 1)*26 - imageXOffset
					rangeMaxX = (x + 2)*26 - imageXOffset
					rangeMinY = (y + 1)*26 - imageYOffset
					rangeMaxY = (y + 2)*26 - imageYOffset
					
					if angle == 0:
						rangeMinX -= 26
					
					elif angle == 90:
						rangeMinY -= 26
						
					elif angle == 180:
						rangeMaxX += 26
					
					else:
						rangeMaxY += 26
					
					for pixX in range(rangeMinX, rangeMaxX):
						for pixY in range(rangeMinY, rangeMaxY):
							distanceFromRoom = 0
							if angle == 0:
								distanceFromRoom = rangeMaxX - pixX 
							
							elif angle == 90:
								distanceFromRoom = rangeMaxY - pixY 
								
							elif angle == 180:
								distanceFromRoom = 52 - (rangeMaxX - pixX)
							
							else:
								distanceFromRoom = 52 - (rangeMaxY - pixY)
							
							if distanceFromRoom >= 32:
								if pix[pixX, pixY][3] == 0:
									pix[pixX, pixY] = (0,0,0,min(max(1, math.floor((distanceFromRoom - 34)*defaultAlpha*0.2)), defaultAlpha))
								
							else:
									
								alpha = 1
								
								if distanceFromRoom >= 27:
									alpha = max(math.floor(alpha * (1 - (distanceFromRoom - 26)*0.2)), 1)
								
								elif distanceFromRoom <= 2:
									alpha = max(alpha, (3 - distanceFromRoom)*40)
								
								if alpha > pix[pixX, pixY][3]:
									pix[pixX, pixY] = (0,0,0,alpha)
							
	#for pixX in range(0, imageXSize):
	#	for pixY in range(0, imageYSize):
	#		if pix[pixX, pixY][3] == 0:
	#			pix[pixX, pixY] = (0,0,0,defaultAlpha)
	
	img.save(fileName)

allRoomTiles = [[1,1], [1,2], [2,1], [2,2]]
	
def getRoomShapeTileSet(roomTiles):
	isGoodRoom = False
	wideRoom = False
	tallRoom = False
	
	for roomTile in roomTiles:
		if roomTile[0] == 2:
			wideRoom = True
		
		if roomTile[1] == 2:
			tallRoom = True
	
	inverseRoomTiles = []
	for roomTile1 in allRoomTiles:
		roomContainsRoomTile = False
		
		for roomTile2 in roomTiles:
			if roomTile1[0] == roomTile2[0] and roomTile1[1] == roomTile2[1]:
				roomContainsRoomTile = True
				break
		
		if not roomContainsRoomTile:
			inverseRoomTiles.append(roomTile1)
	
	while not isGoodRoom:
		squares = []
		numSquares = random.randint(2,6)
		
		while numSquares != 0:
			minX = random.randint(1, 10)*2
			minY = random.randint(1, 8)*2
			maxX = random.randint(2, 15 - minX*0.5)*2
			maxY = random.randint(2, 10 - minY*0.5)*2
			
			squareCorners = [[minX, minY], [minX, minY+maxY], [minX+maxX, minY], [minX+maxX, minY+maxY]]
			
			invalidSquare = False
			
			for roomTile in inverseRoomTiles:
				for squareCorner in squareCorners:
					if squareCorner[0] >= (roomTile[0] - 1)*15 - 1 and squareCorner[0] <= roomTile[0]*15 + 1 and squareCorner[1] >= (roomTile[1] - 1)*9 - 1 and squareCorner[1] <= roomTile[1]*9 + 1:
						invalidSquare = True
			
			if invalidSquare:
				continue
			
			squares.append([minX, minY, maxX, maxY])
			numSquares -= 1
		
		xDoorPositions = [7]
		if wideRoom:
			xDoorPositions.append(23)
		
		yDoorPositions = [4]
		if tallRoom:
			yDoorPositions.append(14)
		
		interveresWithDoors = False
		for x in xDoorPositions:
			fittingSquareFound = False
			for square in squares:
				if x >= square[0] and x <= square[0] + square[2]:
					fittingSquareFound = True
					break
			
			if not fittingSquareFound:
				interveresWithDoors = True
				break
		
		if not interveresWithDoors:
			for y in yDoorPositions:
				fittingSquareFound = False
				for square in squares:
					if y >= square[1] and y <= square[1] + square[3]:
						fittingSquareFound = True
						break
				
				if not fittingSquareFound:
					interveresWithDoors = True
					break
		
		
		if not interveresWithDoors:
			isGoodRoom = True
	
	return squares

def generateRoomShapes(numRoomShapes):
	luafile = open("../customroomshapes.lua", "w")
	
	content += 'local oworld = require "scripts.openworld"\n\n'
	content += 'local MinimapAPI = require "scripts.minimapapi"\n\n'
	
	for i in range(numRoomShapes):
		roomTiles = [[1,1]]
		availableExtraRoomTiles = [[1,2], [2,1], [2,2]]
		numLoops = 0
		
		for roomTile in availableExtraRoomTiles:
			numLoops += 1
			
			if numLoops != 3 or len(roomTiles) > 1:
				if random.randint(1,2) == 1:
					roomTiles.append(roomTile)
		
		squares = getRoomShapeTileSet(roomTiles)
		
		roomTileVectorsStr = ""
		numLoops = 0
		
		for roomTile in roomTiles:
			numLoops += 1
			
			roomTileVectorsStr += "Vector(" + str(roomTile[0] - 1) + "," + str(roomTile[1] - 1) + ")"
			
			if numLoops != len(roomTiles):
				roomTileVectorsStr += ","
		
		content += "do\n"
		content += "oworld.AddNewRoomShape({\n"
		content += '	MinimapId = "OpenWorld' + str(i) + '",\n'
		content += '	RoomTiles = {' + roomTileVectorsStr + '},\n'
		content += '	Overlay = "gfx/backdrop/customroomshadows/custom_roomshadow' + str(i) + '.png",'
		content += "	RoomShape = {\n"
		
		for square in squares:
			content += "		{Tile=Vector(" + str(square[0]) + "," + str(square[1]) + "), Size=Vector(" + str(square[2]) + "," + str(square[3]) + ")},\n"
		
		content += "	}\n"
		content += "})\n\n"
		
		content += "local sprite = Sprite()\n"
		content += 'sprite:Load("gfx/ui/customroomshapes/custom_roomshape.anm2", true)\n'
		content += 'sprite:ReplaceSpritesheet(0, "gfx/ui/customroomshapes/custom_roomshape' + str(i) + '.png")\n'
		content += 'sprite:ReplaceSpritesheet(1, "gfx/ui/customroomshapes/custom_large_roomshape' + str(i) + '.png")\n'
		content += "sprite:LoadGraphics()\n\n"
		
		content += 'MinimapAPI:AddRoomShape("OpenWorld' + str(i) + '",\n'
		content += '	{RoomUnvisited={sprite=sprite, anim="unvisited", frame=0}, RoomVisited={sprite=sprite, anim="visited", frame=0}, RoomCurrent={sprite=sprite, anim="current", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited", frame=0}},\n'
		content += '	{RoomUnvisited={sprite=sprite, anim="unvisited_large", frame=0}, RoomVisited={sprite=sprite, anim="visited_large", frame=0}, RoomCurrent={sprite=sprite, anim="current_large", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited_large", frame=0}},\n'
		content += '	oworld.utils.VEC_ZERO,\n'
		content += '	Vector(2,2),\n'
		content += '	{' + roomTileVectorsStr + '},\n'
		content += '	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},\n'
		content += '	Vector(1,1),\n'
		content += '	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},\n'
		content += '	Vector(1,1),\n'
		content += '	{Vector(-1,0), Vector(-1,1), Vector(0,2), Vector(1,2), Vector(2,1), Vector(2,0), Vector(1,-1), Vector(0,-1)}\n'
		content += ')\n'
		content += 'end\n\n'
		
		squaresString = ""
		numSquares = len(squares)
		for i2 in range(0,numSquares):
			square = squares[i2]
			i3 = 0
			for value in square:
				i3 += 1
				squaresString += str(value)
				if (i3 != 4):
					squaresString += ","
			
			if (i2 != numSquares - 1):
				squaresString += "|"
		
		generateMapIcon(squaresString, "../../../../resources/gfx/ui/customroomshapes/custom_roomshape" + str(i) + ".png", "../../resources/gfx/ui/customroomshapes/custom_large_roomshape" + str(i) + ".png")
		generateRoomShading(squaresString, "../../../../resources/gfx/backdrop/customroomshadows/custom_roomshadow" + str(i) + ".png")
		
		print("roomshape " + str(i) + " loaded - " + str(math.floor((i + 1)/numRoomShapes*100)) + "%")
	
	luafile.write(content)
	luafile.close()

user_input = input("Number of roomshapes > ")

generateRoomShapes(int(user_input))

input("Done!")