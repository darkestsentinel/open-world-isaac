from PIL import Image
import math

def generateRoomShading(squaresString, fileName):
	minX = 1000
	maxX = -1000
	minY = 1000
	maxY = -1000

	roomMap = {}
	wallMap = {}
	cornerMap = {}

	squares = squaresString.split("|")
	for square in squares:
		coors = square.split(",")
		x1,y1,x2,y2 = int(coors[0]),int(coors[1]),int(coors[2]),int(coors[3])
		
		if x1 - 2 < minX:
			minX = x1 - 2
		
		if y1 - 2 < minY:
			minY = y1 - 2
		
		if x1+x2 + 2 > maxX:
			maxX = x1+x2 + 2
		
		if y1+y2 + 2 > maxY:
			maxY = y1+y2 + 2
		
		for x in range(x2):
			for y in range(y2):
				roomMap[str(x1 + x) + "." + str(y1 + y)] = True
	
	imageXOffset = (minX + 1)*26
	imageYOffset = (minY + 1)*26

	imageXSize = (maxX - minX)*26
	imageYSize = (maxY - minY)*26

	defaultAlpha = 89
	img = Image.new('RGBA', (imageXSize, imageYSize), (0,0,0,0))
	pix = img.load()
	
	def inRoom(x, y):
		checkStr = str(x) + "." + str(y)
		return checkStr in roomMap
	
	coordinalAdjTiles = [[1,0],[0,1],[-1,0],[0,-1]]
	diagonalAdjTiles = [[1,1],[-1,1],[-1,-1],[1,-1]]
	
	for x in range(minX, maxX):
		for y in range(minY, maxY):
			if inRoom(x,y):
				checkrange = 1
				
				nearestWalls = []
				foundWall = False
				wallDistance = 0
				
				while (True):
					for i in range(0, checkrange + 1):
						checkX = x + i
						checkY = y + (checkrange - i)
						
						if (not inRoom(checkX, checkY)):
							foundWall = True
							nearestWalls.append([checkX, checkY])
						
						checkX = x + -i
						checkY = y + (checkrange - i)
						
						if (not inRoom(checkX, checkY)):
							foundWall = True
							nearestWalls.append([checkX, checkY])
						
						checkX = x + i
						checkY = y + -(checkrange - i)
						
						if (not inRoom(checkX, checkY)):
							foundWall = True
							nearestWalls.append([checkX, checkY])
						
						checkX = x + -i
						checkY = y + -(checkrange - i)
						
						if (not inRoom(checkX, checkY)):
							foundWall = True
							nearestWalls.append([checkX, checkY])
					
					if (foundWall):
						wallDistance = checkrange
						break
					
					checkrange += 1
				
				for pixX in range((x + 1)*26 - imageXOffset, (x + 2)*26 - imageXOffset):
					for pixY in range((y + 1)*26 - imageYOffset, (y + 2)*26 - imageYOffset):
						if foundWall:
							alpha = 0
							for wall in nearestWalls:
								closestXWallPos = min(max(pixX, wall[0]*26 + 26 - imageXOffset), wall[0]*26 + 52 - imageXOffset)
								closestYWallPos = min(max(pixY, wall[1]*26 + 26 - imageYOffset), wall[1]*26 + 52 - imageYOffset)
								xDiff = abs(closestXWallPos - pixX)
								yDiff = abs(closestYWallPos - pixY)
								
								newAlpha = max(math.floor(120 / (1 + ((xDiff + yDiff)*0.5 + max(xDiff, yDiff)*0.5)*0.1) - (xDiff + yDiff)*0.1), 1)
								if newAlpha > alpha:
									alpha = newAlpha
							
							pix[pixX, pixY] = (0,0,0,alpha)
							
						else:
							pix[pixX, pixY] = (0,0,0,0)
							
			else:
				numInRoomCoorAdjTiles = 0 #number of in-room tiles next to this tile, max counts to 2
				numLoops = 0 
				lastSuccesfullLoop = 0
				
				for tile in coordinalAdjTiles:
					numLoops += 1
					if inRoom(x + tile[0], y + tile[1]):
						numInRoomCoorAdjTiles += 1
						lastSuccesfullLoop = numLoops
						
						if numInRoomCoorAdjTiles == 2:
							break
				
				if numInRoomCoorAdjTiles == 2:
					rotationValue = numLoops*90 - 135 #inner-corner orientation
					
					if numLoops == 4 and inRoom(x + 1, y):
						rotationValue += 90
					
					cornerMap[str(x) + "." + str(y)] = [rotationValue, False]
				
				elif numInRoomCoorAdjTiles == 1:
					wallMap[str(x) + "." + str(y)] = (lastSuccesfullLoop - 1)*90 #wall orientation
					
				else:
					i = 0
					for tile in diagonalAdjTiles:
						i += 1
						if inRoom(x + tile[0], y + tile[1]):
							rotationValue = i*90 - 45 #outer-corner orientation
							cornerMap[str(x) + "." + str(y)] = [rotationValue, True]
	
	checkedOutWallMap = {}
	
	for x in range(minX, maxX):
		for y in range(minY, maxY):
			xyStr = str(x) + "." + str(y)
			if xyStr in cornerMap:
				corner = cornerMap[xyStr]
				
				cornerMinX = (x + 1)*26 - imageXOffset
				cornerMaxX = (x + 2)*26 - imageXOffset
				cornerMinY = (y + 1)*26 - imageYOffset
				cornerMaxY = (y + 2)*26 - imageYOffset
				
				if corner[0] == 45:
					cornerMinX -= 26
					cornerMinY -= 26
				
				elif corner[0] == 135:
					cornerMaxX += 26
					cornerMinY -= 26
				
				elif corner[0] == 225:
					cornerMaxX += 26
					cornerMaxY += 26
				
				elif corner[0] == 315:
					cornerMinX -= 26
					cornerMaxY += 26
				
				rangeMinX = cornerMinX
				rangeMaxX = cornerMaxX
				rangeMinY = cornerMinY
				rangeMaxY = cornerMaxY
				
				innerCornerAtY = None
				innerCornerAtX = None
				
				if corner[1]: #is outer-corner
					for i in range(0,2):
						dir = math.floor((corner[0] - 45)/90 + i)%4
						tile = coordinalAdjTiles[dir]
						checkX = x
						checkY = y
						
						while (True):
							checkX += tile[0]
							checkY += tile[1]
							
							checkxyStr = str(checkX) + "." + str(checkY)
							
							if checkxyStr in wallMap:
								checkedOutWallMap[checkxyStr] = True
								if dir == 0:
									rangeMaxX += 26
								
								elif dir == 1:
									rangeMaxY += 26
								
								elif dir == 2:
									rangeMinX -= 26
								
								else:
									rangeMinY -= 26
							
							elif checkxyStr in cornerMap:
								innerCorner = cornerMap[checkxyStr]
								if (not innerCorner[1]): #is inner-corner
									if dir == 0:
										rangeMaxX += 26
										innerCornerAtX = (checkX + 2)*26 - imageXOffset
								
									elif dir == 1:
										rangeMaxY += 26
										innerCornerAtY = (checkY + 2)*26 - imageYOffset
									
									elif dir == 2:
										rangeMinX -= 26
										innerCornerAtX = (checkX + 1)*26 - imageXOffset
									
									else:
										rangeMinY -= 26
										innerCornerAtY = (checkY + 1)*26 - imageYOffset
								
								break
								
							else:
								break
				
					if corner[0] == 45:
						for pixX in range(rangeMinX, rangeMaxX):
							for pixY in range(rangeMinY, rangeMaxY):
								if pixX >= cornerMinX and pixX < cornerMaxX or pixY >= cornerMinY and pixY < cornerMaxY:
									distanceFromRoom = max(cornerMaxX - pixX, cornerMaxY - pixY)
									
									if innerCornerAtX != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtX - pixX))
									
									if innerCornerAtY != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtY - pixY))
									
									if distanceFromRoom >= 32:
										if pix[pixX, pixY][3] == 0:
											pix[pixX, pixY] = (0,0,0,min(max(1, math.floor((distanceFromRoom - 34)*defaultAlpha*0.2)), defaultAlpha))
									
									else:
										xDiff = pixX - ((x + 1)*26 - imageXOffset) - (pixY - ((y + 1)*26 - imageYOffset))
										yDiff = abs(pixY - ((y + 1)*26 - imageYOffset) - (pixX - ((x + 1)*26 - imageXOffset)))
										alphaMult = 1
										if xDiff > 0:
											alphaMult = 1 / ((rangeMaxX - rangeMinX - 26)*0.01)
										
										else:
											alphaMult = 1 / ((rangeMaxY - rangeMinY - 26)*0.01)
										
										xDiff = abs(xDiff)
										
										alpha = max(math.floor(140 / (1 + ((xDiff + yDiff)*0.5 + max(xDiff, yDiff)*0.5)*0.02*alphaMult) - (xDiff + yDiff)*0.1*alphaMult), 1)
										
										if distanceFromRoom >= 27:
											alpha = max(math.floor(alpha * (1 - (distanceFromRoom - 26)*0.2)), 1)
										
										elif distanceFromRoom <= 2:
											alpha = max(alpha, (3 - distanceFromRoom)*40)
										
										if alpha > pix[pixX, pixY][3]:
											pix[pixX, pixY] = (0,0,0,alpha)
					
					elif corner[0] == 135:
						for pixX in range(rangeMinX, rangeMaxX):
							for pixY in range(rangeMinY, rangeMaxY):
								if pixX >= cornerMinX and pixX < cornerMaxX or pixY >= cornerMinY and pixY < cornerMaxY:
									distanceFromRoom = max(52 - (cornerMaxX - pixX), cornerMaxY - pixY)
									
									if innerCornerAtX != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtX - pixX))
									
									if innerCornerAtY != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtY - pixY))
									
									if distanceFromRoom >= 32:
										if pix[pixX, pixY][3] == 0:
											pix[pixX, pixY] = (0,0,0,min(max(1, math.floor((distanceFromRoom - 34)*defaultAlpha*0.2)), defaultAlpha))
									
									else:
										xDiff = pixX - ((x + 2)*26 - imageXOffset) - (((y + 1)*26 - imageYOffset) - pixY)
										yDiff = abs(pixY - ((y + 2)*26 - imageYOffset) - (((x + 1)*26 - imageXOffset) - pixX))
										
										alphaMult = 1
										if xDiff < 0:
											alphaMult = 1 / ((rangeMaxX - rangeMinX - 26)*0.01)
										
										else:
											alphaMult = 1 / ((rangeMaxY - rangeMinY - 26)*0.01)
										
										xDiff = abs(xDiff)
										
										alpha = max(math.floor(140 / (1 + ((xDiff + yDiff)*0.5 + max(xDiff, yDiff)*0.5)*0.02*alphaMult) - (xDiff + yDiff)*0.1*alphaMult), 1)
										
										if distanceFromRoom >= 27:
											alpha = max(math.floor(alpha * (1 - (distanceFromRoom - 26)*0.2)), 1)
										
										elif distanceFromRoom <= 2:
											alpha = max(alpha, (3 - distanceFromRoom)*40)
										
										if alpha > pix[pixX, pixY][3]:
											pix[pixX, pixY] = (0,0,0,alpha)
					
					elif corner[0] == 225:
						for pixX in range(rangeMinX, rangeMaxX):
							for pixY in range(rangeMinY, rangeMaxY):
								if pixX >= cornerMinX and pixX < cornerMaxX or pixY >= cornerMinY and pixY < cornerMaxY:
									distanceFromRoom = max(52 - (cornerMaxX - pixX), 52 - (cornerMaxY - pixY))
									
									if innerCornerAtX != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtX - pixX))
									
									if innerCornerAtY != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtY - pixY))
									
									if distanceFromRoom >= 32:
										if pix[pixX, pixY][3] == 0:
											pix[pixX, pixY] = (0,0,0,min(max(1, math.floor((distanceFromRoom - 34)*defaultAlpha*0.2)), defaultAlpha))
									
									else:
										xDiff = pixX - ((x + 1)*26 - imageXOffset) - (pixY - ((y + 1)*26 - imageYOffset))
										yDiff = abs(pixY - ((y + 1)*26 - imageYOffset) - (pixX - ((x + 1)*26 - imageXOffset)))
										
										alphaMult = 1
										if xDiff < 0:
											alphaMult = 1 / ((rangeMaxX - rangeMinX - 26)*0.01)
										
										else:
											alphaMult = 1 / ((rangeMaxY - rangeMinY - 26)*0.01)
										
										xDiff = abs(xDiff)
										
										alpha = max(math.floor(140 / (1 + ((xDiff + yDiff)*0.5 + max(xDiff, yDiff)*0.5)*0.02*alphaMult) - (xDiff + yDiff)*0.1*alphaMult), 1)
										
										if distanceFromRoom >= 27:
											alpha = max(math.floor(alpha * (1 - (distanceFromRoom - 26)*0.2)), 1)
										
										elif distanceFromRoom <= 2:
											alpha = max(alpha, (3 - distanceFromRoom)*40)
										
										if alpha > pix[pixX, pixY][3]:
											pix[pixX, pixY] = (0,0,0,alpha)
					
					else:
						for pixX in range(rangeMinX, rangeMaxX):
							for pixY in range(rangeMinY, rangeMaxY):
								if pixX >= cornerMinX and pixX < cornerMaxX or pixY >= cornerMinY and pixY < cornerMaxY:
									distanceFromRoom = max(cornerMaxX - pixX, 52 - (cornerMaxY - pixY))
									
									if innerCornerAtX != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtX - pixX))
									
									if innerCornerAtY != None:
										distanceFromRoom = min(distanceFromRoom, abs(innerCornerAtY - pixY))
									
									if distanceFromRoom >= 32:
										if pix[pixX, pixY][3] == 0:
											pix[pixX, pixY] = (0,0,0,min(max(1, math.floor((distanceFromRoom - 34)*defaultAlpha*0.2)), defaultAlpha))
									
									else:
										xDiff = pixX - ((x + 2)*26 - imageXOffset) - (((y + 1)*26 - imageYOffset) - pixY)
										yDiff = abs(pixY - ((y + 2)*26 - imageYOffset) - (((x + 1)*26 - imageXOffset) - pixX))
										
										alphaMult = 1
										if xDiff > 0:
											alphaMult = 1 / ((rangeMaxX - rangeMinX - 26)*0.01)
										
										else:
											alphaMult = 1 / ((rangeMaxY - rangeMinY - 26)*0.01)
										
										xDiff = abs(xDiff)
										
										alpha = max(math.floor(140 / (1 + ((xDiff + yDiff)*0.5 + max(xDiff, yDiff)*0.5)*0.02*alphaMult) - (xDiff + yDiff)*0.1*alphaMult), 1)
										
										if distanceFromRoom >= 27:
											alpha = max(math.floor(alpha * (1 - (distanceFromRoom - 26)*0.2)), 1)
										
										elif distanceFromRoom <= 2:
											alpha = max(alpha, (3 - distanceFromRoom)*40)
										
										if alpha > pix[pixX, pixY][3]:
											pix[pixX, pixY] = (0,0,0,alpha)
	
	for x in range(minX, maxX):
		for y in range(minY, maxY):
			xyStr = str(x) + "." + str(y)
			
			if xyStr in wallMap:
				if not (xyStr in checkedOutWallMap):
					angle = wallMap[xyStr]
				
					rangeMinX = (x + 1)*26 - imageXOffset
					rangeMaxX = (x + 2)*26 - imageXOffset
					rangeMinY = (y + 1)*26 - imageYOffset
					rangeMaxY = (y + 2)*26 - imageYOffset
					
					if angle == 0:
						rangeMinX -= 26
					
					elif angle == 90:
						rangeMinY -= 26
						
					elif angle == 180:
						rangeMaxX += 26
					
					else:
						rangeMaxY += 26
					
					print(angle)
					
					for pixX in range(rangeMinX, rangeMaxX):
						for pixY in range(rangeMinY, rangeMaxY):
							distanceFromRoom = 0
							if angle == 0:
								distanceFromRoom = rangeMaxX - pixX 
							
							elif angle == 90:
								distanceFromRoom = rangeMaxY - pixY 
								
							elif angle == 180:
								distanceFromRoom = 52 - (rangeMaxX - pixX)
							
							else:
								distanceFromRoom = 52 - (rangeMaxY - pixY)
							
							if distanceFromRoom >= 32:
								if pix[pixX, pixY][3] == 0:
									pix[pixX, pixY] = (0,0,0,min(max(1, math.floor((distanceFromRoom - 34)*defaultAlpha*0.2)), defaultAlpha))
								
							else:
									
								alpha = 1
								
								if distanceFromRoom >= 27:
									alpha = max(math.floor(alpha * (1 - (distanceFromRoom - 26)*0.2)), 1)
								
								elif distanceFromRoom <= 2:
									alpha = max(alpha, (3 - distanceFromRoom)*40)
								
								if alpha > pix[pixX, pixY][3]:
									pix[pixX, pixY] = (0,0,0,alpha)
							
	#for pixX in range(0, imageXSize):
	#	for pixY in range(0, imageYSize):
	#		if pix[pixX, pixY][3] == 0:
	#			pix[pixX, pixY] = (0,0,0,defaultAlpha)
	
	img.save(fileName)

print('Put in a RoomShape to generate a fitting shadow layer.')
print('RoomShape exists out of squares: "topLeftXValue,topLeftYValue,squareWidth,squareHeight"')
print('Squares can be differentiated with the "|" symbol.')
print('Example of RoomShape existing of 3 different squares: "1,1,10,5|1,5,5,5|11,3,5,7"')

user_input = input(" > ")
	
generateRoomShading(user_input, "custom_room_shading.png")

input("Done!")