local MinimapAPI = require "scripts.minimapapi"

do
oworld.AddNewRoomShape({
	MinimapId = "OpenWorld0",
	RoomTiles = {Vector(0,0),Vector(1,0),Vector(1,1)},
	Overlay = "gfx/backdrop/customroomshadows/custom_roomshadow0.png",	RoomShape = {
		{Tile=Vector(18,12), Size=Vector(12,6)},
		{Tile=Vector(4,2), Size=Vector(10,4)},
		{Tile=Vector(18,16), Size=Vector(6,4)},
		{Tile=Vector(18,12), Size=Vector(8,6)},
		{Tile=Vector(8,2), Size=Vector(16,4)},
		{Tile=Vector(18,12), Size=Vector(10,4)},
	}
})

local sprite = Sprite()
sprite:Load("gfx/ui/customroomshapes/custom_roomshape.anm2", true)
sprite:ReplaceSpritesheet(0, "gfx/ui/customroomshapes/custom_roomshape0.png")
sprite:ReplaceSpritesheet(1, "gfx/ui/customroomshapes/custom_large_roomshape0.png")
sprite:LoadGraphics()

MinimapAPI:AddRoomShape("OpenWorld0",
	{RoomUnvisited={sprite=sprite, anim="unvisited", frame=0}, RoomVisited={sprite=sprite, anim="visited", frame=0}, RoomCurrent={sprite=sprite, anim="current", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited", frame=0}},
	{RoomUnvisited={sprite=sprite, anim="unvisited_large", frame=0}, RoomVisited={sprite=sprite, anim="visited_large", frame=0}, RoomCurrent={sprite=sprite, anim="current_large", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited_large", frame=0}},
	oworld.utils.VEC_ZERO,
	Vector(2,2),
	{Vector(0,0),Vector(1,0),Vector(1,1)},
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(-1,0), Vector(-1,1), Vector(0,2), Vector(1,2), Vector(2,1), Vector(2,0), Vector(1,-1), Vector(0,-1)}
)
end

do
oworld.AddNewRoomShape({
	MinimapId = "OpenWorld1",
	RoomTiles = {Vector(0,0),Vector(1,0)},
	Overlay = "gfx/backdrop/customroomshadows/custom_roomshadow1.png",	RoomShape = {
		{Tile=Vector(20,6), Size=Vector(10,14)},
		{Tile=Vector(18,2), Size=Vector(4,4)},
		{Tile=Vector(18,2), Size=Vector(12,18)},
		{Tile=Vector(2,2), Size=Vector(6,18)},
	}
})

local sprite = Sprite()
sprite:Load("gfx/ui/customroomshapes/custom_roomshape.anm2", true)
sprite:ReplaceSpritesheet(0, "gfx/ui/customroomshapes/custom_roomshape1.png")
sprite:ReplaceSpritesheet(1, "gfx/ui/customroomshapes/custom_large_roomshape1.png")
sprite:LoadGraphics()

MinimapAPI:AddRoomShape("OpenWorld1",
	{RoomUnvisited={sprite=sprite, anim="unvisited", frame=0}, RoomVisited={sprite=sprite, anim="visited", frame=0}, RoomCurrent={sprite=sprite, anim="current", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited", frame=0}},
	{RoomUnvisited={sprite=sprite, anim="unvisited_large", frame=0}, RoomVisited={sprite=sprite, anim="visited_large", frame=0}, RoomCurrent={sprite=sprite, anim="current_large", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited_large", frame=0}},
	oworld.utils.VEC_ZERO,
	Vector(2,2),
	{Vector(0,0),Vector(1,0)},
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(-1,0), Vector(-1,1), Vector(0,2), Vector(1,2), Vector(2,1), Vector(2,0), Vector(1,-1), Vector(0,-1)}
)
end

do
oworld.AddNewRoomShape({
	MinimapId = "OpenWorld2",
	RoomTiles = {Vector(0,0),Vector(0,1),Vector(1,0),Vector(1,1)},
	Overlay = "gfx/backdrop/customroomshadows/custom_roomshadow2.png",	RoomShape = {
		{Tile=Vector(18,6), Size=Vector(10,4)},
		{Tile=Vector(14,14), Size=Vector(8,6)},
		{Tile=Vector(6,4), Size=Vector(8,8)},
		{Tile=Vector(20,6), Size=Vector(6,6)},
		{Tile=Vector(18,16), Size=Vector(10,4)},
	}
})

local sprite = Sprite()
sprite:Load("gfx/ui/customroomshapes/custom_roomshape.anm2", true)
sprite:ReplaceSpritesheet(0, "gfx/ui/customroomshapes/custom_roomshape2.png")
sprite:ReplaceSpritesheet(1, "gfx/ui/customroomshapes/custom_large_roomshape2.png")
sprite:LoadGraphics()

MinimapAPI:AddRoomShape("OpenWorld2",
	{RoomUnvisited={sprite=sprite, anim="unvisited", frame=0}, RoomVisited={sprite=sprite, anim="visited", frame=0}, RoomCurrent={sprite=sprite, anim="current", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited", frame=0}},
	{RoomUnvisited={sprite=sprite, anim="unvisited_large", frame=0}, RoomVisited={sprite=sprite, anim="visited_large", frame=0}, RoomCurrent={sprite=sprite, anim="current_large", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited_large", frame=0}},
	oworld.utils.VEC_ZERO,
	Vector(2,2),
	{Vector(0,0),Vector(0,1),Vector(1,0),Vector(1,1)},
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(-1,0), Vector(-1,1), Vector(0,2), Vector(1,2), Vector(2,1), Vector(2,0), Vector(1,-1), Vector(0,-1)}
)
end

do
oworld.AddNewRoomShape({
	MinimapId = "OpenWorld3",
	RoomTiles = {Vector(0,0),Vector(0,1),Vector(1,0),Vector(1,1)},
	Overlay = "gfx/backdrop/customroomshadows/custom_roomshadow3.png",	RoomShape = {
		{Tile=Vector(16,10), Size=Vector(6,8)},
		{Tile=Vector(4,8), Size=Vector(16,12)},
		{Tile=Vector(10,12), Size=Vector(4,8)},
		{Tile=Vector(14,4), Size=Vector(10,12)},
	}
})

local sprite = Sprite()
sprite:Load("gfx/ui/customroomshapes/custom_roomshape.anm2", true)
sprite:ReplaceSpritesheet(0, "gfx/ui/customroomshapes/custom_roomshape3.png")
sprite:ReplaceSpritesheet(1, "gfx/ui/customroomshapes/custom_large_roomshape3.png")
sprite:LoadGraphics()

MinimapAPI:AddRoomShape("OpenWorld3",
	{RoomUnvisited={sprite=sprite, anim="unvisited", frame=0}, RoomVisited={sprite=sprite, anim="visited", frame=0}, RoomCurrent={sprite=sprite, anim="current", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited", frame=0}},
	{RoomUnvisited={sprite=sprite, anim="unvisited_large", frame=0}, RoomVisited={sprite=sprite, anim="visited_large", frame=0}, RoomCurrent={sprite=sprite, anim="current_large", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited_large", frame=0}},
	oworld.utils.VEC_ZERO,
	Vector(2,2),
	{Vector(0,0),Vector(0,1),Vector(1,0),Vector(1,1)},
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(-1,0), Vector(-1,1), Vector(0,2), Vector(1,2), Vector(2,1), Vector(2,0), Vector(1,-1), Vector(0,-1)}
)
end

do
oworld.AddNewRoomShape({
	MinimapId = "OpenWorld4",
	RoomTiles = {Vector(0,0),Vector(0,1)},
	Overlay = "gfx/backdrop/customroomshadows/custom_roomshadow4.png",	RoomShape = {
		{Tile=Vector(2,2), Size=Vector(10,4)},
		{Tile=Vector(2,4), Size=Vector(4,4)},
		{Tile=Vector(2,12), Size=Vector(8,8)},
		{Tile=Vector(2,6), Size=Vector(4,10)},
		{Tile=Vector(4,10), Size=Vector(6,8)},
		{Tile=Vector(2,2), Size=Vector(10,10)},
	}
})

local sprite = Sprite()
sprite:Load("gfx/ui/customroomshapes/custom_roomshape.anm2", true)
sprite:ReplaceSpritesheet(0, "gfx/ui/customroomshapes/custom_roomshape4.png")
sprite:ReplaceSpritesheet(1, "gfx/ui/customroomshapes/custom_large_roomshape4.png")
sprite:LoadGraphics()

MinimapAPI:AddRoomShape("OpenWorld4",
	{RoomUnvisited={sprite=sprite, anim="unvisited", frame=0}, RoomVisited={sprite=sprite, anim="visited", frame=0}, RoomCurrent={sprite=sprite, anim="current", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited", frame=0}},
	{RoomUnvisited={sprite=sprite, anim="unvisited_large", frame=0}, RoomVisited={sprite=sprite, anim="visited_large", frame=0}, RoomCurrent={sprite=sprite, anim="current_large", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited_large", frame=0}},
	oworld.utils.VEC_ZERO,
	Vector(2,2),
	{Vector(0,0),Vector(0,1)},
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(-1,0), Vector(-1,1), Vector(0,2), Vector(1,2), Vector(2,1), Vector(2,0), Vector(1,-1), Vector(0,-1)}
)
end

do
oworld.AddNewRoomShape({
	MinimapId = "OpenWorld5",
	RoomTiles = {Vector(0,0),Vector(0,1),Vector(1,0),Vector(1,1)},
	Overlay = "gfx/backdrop/customroomshadows/custom_roomshadow5.png",	RoomShape = {
		{Tile=Vector(4,6), Size=Vector(18,14)},
		{Tile=Vector(8,2), Size=Vector(4,6)},
		{Tile=Vector(6,12), Size=Vector(20,8)},
		{Tile=Vector(16,12), Size=Vector(4,8)},
	}
})

local sprite = Sprite()
sprite:Load("gfx/ui/customroomshapes/custom_roomshape.anm2", true)
sprite:ReplaceSpritesheet(0, "gfx/ui/customroomshapes/custom_roomshape5.png")
sprite:ReplaceSpritesheet(1, "gfx/ui/customroomshapes/custom_large_roomshape5.png")
sprite:LoadGraphics()

MinimapAPI:AddRoomShape("OpenWorld5",
	{RoomUnvisited={sprite=sprite, anim="unvisited", frame=0}, RoomVisited={sprite=sprite, anim="visited", frame=0}, RoomCurrent={sprite=sprite, anim="current", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited", frame=0}},
	{RoomUnvisited={sprite=sprite, anim="unvisited_large", frame=0}, RoomVisited={sprite=sprite, anim="visited_large", frame=0}, RoomCurrent={sprite=sprite, anim="current_large", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited_large", frame=0}},
	oworld.utils.VEC_ZERO,
	Vector(2,2),
	{Vector(0,0),Vector(0,1),Vector(1,0),Vector(1,1)},
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(-1,0), Vector(-1,1), Vector(0,2), Vector(1,2), Vector(2,1), Vector(2,0), Vector(1,-1), Vector(0,-1)}
)
end

do
oworld.AddNewRoomShape({
	MinimapId = "OpenWorld6",
	RoomTiles = {Vector(0,0),Vector(0,1),Vector(1,0),Vector(1,1)},
	Overlay = "gfx/backdrop/customroomshadows/custom_roomshadow6.png",	RoomShape = {
		{Tile=Vector(8,2), Size=Vector(14,16)},
		{Tile=Vector(4,2), Size=Vector(12,4)},
		{Tile=Vector(20,14), Size=Vector(4,6)},
		{Tile=Vector(12,12), Size=Vector(12,6)},
		{Tile=Vector(16,10), Size=Vector(12,4)},
	}
})

local sprite = Sprite()
sprite:Load("gfx/ui/customroomshapes/custom_roomshape.anm2", true)
sprite:ReplaceSpritesheet(0, "gfx/ui/customroomshapes/custom_roomshape6.png")
sprite:ReplaceSpritesheet(1, "gfx/ui/customroomshapes/custom_large_roomshape6.png")
sprite:LoadGraphics()

MinimapAPI:AddRoomShape("OpenWorld6",
	{RoomUnvisited={sprite=sprite, anim="unvisited", frame=0}, RoomVisited={sprite=sprite, anim="visited", frame=0}, RoomCurrent={sprite=sprite, anim="current", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited", frame=0}},
	{RoomUnvisited={sprite=sprite, anim="unvisited_large", frame=0}, RoomVisited={sprite=sprite, anim="visited_large", frame=0}, RoomCurrent={sprite=sprite, anim="current_large", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited_large", frame=0}},
	oworld.utils.VEC_ZERO,
	Vector(2,2),
	{Vector(0,0),Vector(0,1),Vector(1,0),Vector(1,1)},
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(-1,0), Vector(-1,1), Vector(0,2), Vector(1,2), Vector(2,1), Vector(2,0), Vector(1,-1), Vector(0,-1)}
)
end

do
oworld.AddNewRoomShape({
	MinimapId = "OpenWorld7",
	RoomTiles = {Vector(0,0),Vector(1,0)},
	Overlay = "gfx/backdrop/customroomshadows/custom_roomshadow7.png",	RoomShape = {
		{Tile=Vector(2,2), Size=Vector(4,4)},
		{Tile=Vector(10,2), Size=Vector(12,4)},
		{Tile=Vector(16,2), Size=Vector(6,18)},
		{Tile=Vector(12,6), Size=Vector(18,14)},
		{Tile=Vector(14,4), Size=Vector(16,16)},
		{Tile=Vector(2,2), Size=Vector(12,4)},
	}
})

local sprite = Sprite()
sprite:Load("gfx/ui/customroomshapes/custom_roomshape.anm2", true)
sprite:ReplaceSpritesheet(0, "gfx/ui/customroomshapes/custom_roomshape7.png")
sprite:ReplaceSpritesheet(1, "gfx/ui/customroomshapes/custom_large_roomshape7.png")
sprite:LoadGraphics()

MinimapAPI:AddRoomShape("OpenWorld7",
	{RoomUnvisited={sprite=sprite, anim="unvisited", frame=0}, RoomVisited={sprite=sprite, anim="visited", frame=0}, RoomCurrent={sprite=sprite, anim="current", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited", frame=0}},
	{RoomUnvisited={sprite=sprite, anim="unvisited_large", frame=0}, RoomVisited={sprite=sprite, anim="visited_large", frame=0}, RoomCurrent={sprite=sprite, anim="current_large", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited_large", frame=0}},
	oworld.utils.VEC_ZERO,
	Vector(2,2),
	{Vector(0,0),Vector(1,0)},
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(-1,0), Vector(-1,1), Vector(0,2), Vector(1,2), Vector(2,1), Vector(2,0), Vector(1,-1), Vector(0,-1)}
)
end

do
oworld.AddNewRoomShape({
	MinimapId = "OpenWorld8",
	RoomTiles = {Vector(0,0),Vector(0,1)},
	Overlay = "gfx/backdrop/customroomshadows/custom_roomshadow8.png",	RoomShape = {
		{Tile=Vector(4,2), Size=Vector(6,4)},
		{Tile=Vector(2,6), Size=Vector(10,12)},
		{Tile=Vector(4,16), Size=Vector(6,4)},
	}
})

local sprite = Sprite()
sprite:Load("gfx/ui/customroomshapes/custom_roomshape.anm2", true)
sprite:ReplaceSpritesheet(0, "gfx/ui/customroomshapes/custom_roomshape8.png")
sprite:ReplaceSpritesheet(1, "gfx/ui/customroomshapes/custom_large_roomshape8.png")
sprite:LoadGraphics()

MinimapAPI:AddRoomShape("OpenWorld8",
	{RoomUnvisited={sprite=sprite, anim="unvisited", frame=0}, RoomVisited={sprite=sprite, anim="visited", frame=0}, RoomCurrent={sprite=sprite, anim="current", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited", frame=0}},
	{RoomUnvisited={sprite=sprite, anim="unvisited_large", frame=0}, RoomVisited={sprite=sprite, anim="visited_large", frame=0}, RoomCurrent={sprite=sprite, anim="current_large", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited_large", frame=0}},
	oworld.utils.VEC_ZERO,
	Vector(2,2),
	{Vector(0,0),Vector(0,1)},
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(-1,0), Vector(-1,1), Vector(0,2), Vector(1,2), Vector(2,1), Vector(2,0), Vector(1,-1), Vector(0,-1)}
)
end

do
oworld.AddNewRoomShape({
	MinimapId = "OpenWorld9",
	RoomTiles = {Vector(0,0)},
	Overlay = "gfx/backdrop/customroomshadows/custom_roomshadow9.png",	RoomShape = {
		{Tile=Vector(6,2), Size=Vector(4,4)},
		{Tile=Vector(2,6), Size=Vector(6,14)},
		{Tile=Vector(8,6), Size=Vector(4,14)},
		{Tile=Vector(6,2), Size=Vector(4,4)},
		{Tile=Vector(2,4), Size=Vector(8,16)},
		{Tile=Vector(4,2), Size=Vector(6,18)},
	}
})

local sprite = Sprite()
sprite:Load("gfx/ui/customroomshapes/custom_roomshape.anm2", true)
sprite:ReplaceSpritesheet(0, "gfx/ui/customroomshapes/custom_roomshape9.png")
sprite:ReplaceSpritesheet(1, "gfx/ui/customroomshapes/custom_large_roomshape9.png")
sprite:LoadGraphics()

MinimapAPI:AddRoomShape("OpenWorld9",
	{RoomUnvisited={sprite=sprite, anim="unvisited", frame=0}, RoomVisited={sprite=sprite, anim="visited", frame=0}, RoomCurrent={sprite=sprite, anim="current", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited", frame=0}},
	{RoomUnvisited={sprite=sprite, anim="unvisited_large", frame=0}, RoomVisited={sprite=sprite, anim="visited_large", frame=0}, RoomCurrent={sprite=sprite, anim="current_large", frame=0}, RoomSemivisited={sprite=sprite, anim="semivisited_large", frame=0}},
	oworld.utils.VEC_ZERO,
	Vector(2,2),
	{Vector(0,0)},
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(0.5,0.5), Vector(1.5,0.5), Vector(0.5,1.5), Vector(1.5,1.5)},
	Vector(1,1),
	{Vector(-1,0), Vector(-1,1), Vector(0,2), Vector(1,2), Vector(2,1), Vector(2,0), Vector(1,-1), Vector(0,-1)}
)
end