local oworld = require "scripts.openworld"

oworld.utils = {}

function oworld.utils.Sign(n)
	return n>0 and 1 or n<0 and -1 or 0
end

function oworld.utils.RandomVector(length)
	return Vector(math.random()-0.5,math.random()-0.5):Resized(length)
end

function oworld.utils.copy_table(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[oworld.utils.copy_table(orig_key)] = oworld.utils.copy_table(orig_value)
        end
        setmetatable(copy, oworld.utils.copy_table(getmetatable(orig)))
    else
        copy = orig
    end
    return copy
end

oworld.utils.VEC_ZERO = Vector(0,0)

oworld.utils.dirToVel = {
	[Direction.NO_DIRECTION] = oworld.utils.VEC_ZERO,
	[Direction.LEFT] = Vector(-1,0),
	[Direction.UP] = Vector(0,-1),
	[Direction.RIGHT] = Vector(1,0),
	[Direction.DOWN] = Vector(0,1)
}


-- DELAYED FUNCTION --

-- Copied from rev, took out resetOnNewRoom part

--call function after x updates or renders
oworld.utils.delayedFuncs = {}

function oworld.utils.DelayFunction(func, delay, args, useRender)
	if type(func) == 'number' then
		local temp = func
		func = delay
		delay = temp
	end
	if type(args) ~= "table" then args = {args} end
	table.insert(oworld.utils.delayedFuncs, {func, delay, args, useRender})
end

oworld:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function()
	for i, func in ipairs(oworld.utils.delayedFuncs) do
		if func[4] then
			table.remove(oworld.utils.delayedFuncs, i)
		end
	end
end)

do
	local function delayFunctionHandling(onRender)
		if #oworld.utils.delayedFuncs ~= 0 then
			for i,v in ipairs(oworld.utils.delayedFuncs) do
				if (v[4] and onRender) or (not v[4] and not onRender) then
					if v[2] == 0 then
						if v[3] then
							v[1](table.unpack(v[3]))
						else
							v[1]()
						end
						table.remove(oworld.utils.delayedFuncs, i)
					else
						v[2] = v[2] - 1
					end
				end
			end
		end
	end

	oworld:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
		delayFunctionHandling(false)
	end)

	oworld:AddCallback(ModCallbacks.MC_POST_RENDER, function()
		delayFunctionHandling(true)
	end)

end

-- Screen --

function oworld.utils.getScreenBottomRight()
    return Game():GetRoom():GetRenderSurfaceTopLeft() * 2 + Vector(442,286)
end

function oworld.utils.getScreenCenterPosition()
    return oworld.utils.getScreenBottomRight() / 2
end

Isaac.DebugString("Loaded Open World utilities")

return oworld.utils