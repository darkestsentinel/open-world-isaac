local oworld = require "scripts.openworld"

local game = Game()
local sfx = SFXManager()

oworld.CustomPoopId = Isaac.GetEntityTypeByName("Custom Poop")
oworld.CustomPoopVariant = Isaac.GetEntityVariantByName("Custom Poop")

oworld.FakeGridTypes = {
	[GridEntityType.GRID_DECORATION] = {
		[0] = {
			anm2 = "props_01_basement",
			animation = "Prop01",
			animated = true,
			replacedpngs = {[0] = "gfx/grid/custom_props_01_basement.png"},
			collisionclass = GridCollisionClass.COLLISION_NONE,
			OnInit = function(grid)
				local r = grid.rng:RandomInt(43) + 1
				if r < 10 then r = "0"..tostring(r) end
				grid.Sprite:Play("Prop"..tostring(r), true)
			end
		}
	},
	[GridEntityType.GRID_ROCK] = {
		[0] = {
			anm2 = "grid_rock",
			animation = "normal",
			variations = 3,
			collisionclass = GridCollisionClass.COLLISION_SOLID,
			destructable = true,
			OnInit = function(grid)
				if not grid.Desc.IsBigRock then
					local n = grid.rng:RandomInt(8)
					local fge,t = oworld.FakeGridEntities, grid:GetTile()
					local r,d,l,u,dr,dl,ul,ur = fge[t.X+1][t.Y],fge[t.X][t.Y+1],fge[t.X-1][t.Y],fge[t.X][t.Y-1],fge[t.X+1][t.Y+1],fge[t.X-1][t.Y+1],fge[t.X-1][t.Y-1],fge[t.X+1][t.Y-1]
					local isnonbigrock = function(grid)
						if grid and not grid.Desc.IsBigRock and grid.Desc.Type == GridEntityType.GRID_ROCK then
							return true
						end
						return false
					end
					if n==0 and isnonbigrock(r) then r.Sprite:SetFrame("big", 1)
					elseif n==1 and isnonbigrock(l) then l.Sprite:SetFrame("big", 0)
					elseif n==2 and isnonbigrock(d) then d.Sprite:SetFrame("big", 3)
					elseif n==3 and isnonbigrock(u) then u.Sprite:SetFrame("big", 2)
					elseif n==4 and isnonbigrock(r) and isnonbigrock(d) and isnonbigrock(dr) then
						r.Sprite:SetFrame("big", 5)
						d.Sprite:SetFrame("big", 6)
						dr.Sprite:SetFrame("big", 7)
					elseif n==5 and isnonbigrock(l) and isnonbigrock(d) and isnonbigrock(dl) then
						l.Sprite:SetFrame("big", 4)
						d.Sprite:SetFrame("big", 7)
						dl.Sprite:SetFrame("big", 6)
					elseif n==6 and isnonbigrock(r) and isnonbigrock(u) and isnonbigrock(ur) then
						r.Sprite:SetFrame("big", 7)
						u.Sprite:SetFrame("big", 4)
						ur.Sprite:SetFrame("big", 5)
					elseif n==7 and isnonbigrock(l) and isnonbigrock(u) and isnonbigrock(ul) then
						l.Sprite:SetFrame("big", 6)
						u.Sprite:SetFrame("big", 5)
						ul.Sprite:SetFrame("big", 4)
					else return end
					grid.Sprite:SetFrame("big", n)
					grid.Desc.IsBigRock = true
				end
				if grid.Desc.IsBigRock then
					return true
				end
			end,
			OnDestroy = function(grid)
				for i=1, grid.rng:RandomInt(2) + 1 do
					Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.ROCK_PARTICLE, 0, grid.Position+oworld.utils.RandomVector(math.random()*4), oworld.utils.VEC_ZERO, nil)
				end
				sfx:Play(SoundEffect.SOUND_ROCK_CRUMBLE, 0.5, 0, false, 1)
			end
		}
	},
	[GridEntityType.GRID_ROCKB] = {
		[0] = {
			anm2 = "grid_rock",
			animation = "black",
			collisionclass = GridCollisionClass.COLLISION_SOLID
		}
	},
	[GridEntityType.GRID_ROCKT] = {
		[0] = {
			anm2 = "grid_rock",
			animation = "tinted",
			collisionclass = GridCollisionClass.COLLISION_SOLID,
			destructable = true,
			OnDestroy = function(grid)
				for i=1, grid.rng:RandomInt(2) + 1 do
					Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.ROCK_PARTICLE, 0, grid.Position+oworld.utils.RandomVector(math.random()*4), oworld.utils.VEC_ZERO, nil)
				end
				sfx:Play(SoundEffect.SOUND_ROCK_CRUMBLE, 0.5, 0, false, 1)
				oworld.SpawnTintedRockReward(grid.rng, grid.Position)
			end
		}
	},
	[GridEntityType.GRID_ROCK_BOMB] = {
		[0] = {
			anm2 = "grid_rock",
			animation = "bombrock",
			collisionclass = GridCollisionClass.COLLISION_SOLID,
			destructable = true,
			OnDestroy = function(grid)
				for i=1, grid.rng:RandomInt(2) + 1 do
					Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.ROCK_PARTICLE, 0, grid.Position+oworld.utils.RandomVector(math.random()*4), oworld.utils.VEC_ZERO, nil)
				end
				sfx:Play(SoundEffect.SOUND_ROCK_CRUMBLE, 0.5, 0, false, 1)
				Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.BOMB_EXPLOSION, 0, grid.Position, oworld.utils.VEC_ZERO, nil)
			end
		}
	},
	[GridEntityType.GRID_ROCK_ALT] = {
		[0] = {
			anm2 = "grid_rock",
			animation = "alt",
			variations = 3,
			collisionclass = GridCollisionClass.COLLISION_SOLID,
			destructable = true,
			OnDestroy = function(grid)
				for i=1, grid.rng:RandomInt(2) + 1 do
					local eff = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.ROCK_PARTICLE, 0, grid.Position+oworld.utils.RandomVector(math.random()*4), oworld.utils.VEC_ZERO, nil)
					eff:GetSprite():SetFrame("rubble_alt", grid.rng:RandomInt(4))
				end
				sfx:Play(SoundEffect.SOUND_POT_BREAK, 0.5, 0, false, 1)
			end
		}
	},
	[GridEntityType.GRID_PIT] = {
		[0] = {
			anm2 = "grid_pit",
			animation = "pit",
			collisionclass = GridCollisionClass.COLLISION_PIT,
			OnUpdate = function(grid)
				if not grid.PitInit then
					local fge,t = oworld.FakeGridEntities, grid:GetTile()
					local ip = function(grid)
						if grid then
							return grid.Desc.Type == GridEntityType.GRID_PIT
						end
					end
					local r,d,l,u,dr,dl,ul,ur = ip(fge[t.X+1][t.Y]),ip(fge[t.X][t.Y+1]),ip(fge[t.X-1][t.Y]),ip(fge[t.X][t.Y-1]),ip(fge[t.X+1][t.Y+1]),ip(fge[t.X-1][t.Y+1]),ip(fge[t.X-1][t.Y-1]),ip(fge[t.X+1][t.Y-1])
					if not r and not d and not l and not u then
						grid.Sprite:SetFrame("pit", grid.rng:RandomInt(2)*16)
					elseif not r and not d and l and not u then
						grid.Sprite:SetFrame("pit", 1)
					elseif not r and not d and not l and u then
						grid.Sprite:SetFrame("pit", 2)
					elseif not r and not d and l and u and ul then
						grid.Sprite:SetFrame("pit", 3)
					elseif r and not d and not l and not u then
						grid.Sprite:SetFrame("pit", 4)
					elseif r and not d and l and not u then
						grid.Sprite:SetFrame("pit", 5)
					elseif r and not d and not l and u and ur then
						grid.Sprite:SetFrame("pit", 6)
					elseif r and not d and l and u and ul and ur then
						grid.Sprite:SetFrame("pit", 7)
					elseif not r and d and not l and not u then
						grid.Sprite:SetFrame("pit", 8)
					elseif not r and d and l and not u and dl then
						grid.Sprite:SetFrame("pit", 9)
					elseif not r and d and not l and u then
						grid.Sprite:SetFrame("pit", 10)
					elseif not r and d and l and u and ul then
						grid.Sprite:SetFrame("pit", 11)
					elseif r and d and not l and not u and dr then
						grid.Sprite:SetFrame("pit", 12)
					elseif r and d and l and not u and dl and dr then
						grid.Sprite:SetFrame("pit", 13)
					elseif r and d and not l and u and ur then
						grid.Sprite:SetFrame("pit", 14)
					elseif r and d and l and u and ur and ul then
						grid.Sprite:SetFrame("pit", 15)
					elseif not r and not d and l and u then
						grid.Sprite:SetFrame("pit", 17)
					elseif r and not d and not l and u then
						grid.Sprite:SetFrame("pit", 18)
					elseif not r and d and l and not u then
						grid.Sprite:SetFrame("pit", 19)
					elseif r and d and not l and not u then
						grid.Sprite:SetFrame("pit", 20)
					elseif r and d and l and u and ur then
						grid.Sprite:SetFrame("pit", 21)
					elseif r and d and l and u and ul then
						grid.Sprite:SetFrame("pit", 22)
					elseif r and d and l and u then
						grid.Sprite:SetFrame("pit", 23)
					elseif r and d and not l and u then
						grid.Sprite:SetFrame("pit", 25)
					elseif not r and d and l and u then
						grid.Sprite:SetFrame("pit", 26)
					elseif r and not d and l and u and ul then
						grid.Sprite:SetFrame("pit", 27)
					elseif r and not d and l and u and ur then
						grid.Sprite:SetFrame("pit", 28)
					elseif r and not d and l and u then
						grid.Sprite:SetFrame("pit", 29)
					elseif r and d and l and not u and dl then
						grid.Sprite:SetFrame("pit", 30)
					elseif r and d and l and not u and dr then
						grid.Sprite:SetFrame("pit", 31)
					elseif r and d and l and not u then
						grid.Sprite:SetFrame("pit", 32)
					else
						grid.Sprite:SetFrame("pit", grid.rng:RandomInt(2)*16)
					end
					
					grid.Sprite:ReplaceSpritesheet(1, "gfx/grid/none.png")
					grid.Sprite:LoadGraphics()
					
					grid.PitInit = true
				end
			end
		}
	},
	[GridEntityType.GRID_SPIKES] = {
		[0] = {
			anm2 = "grid_spikes",
			animation = "Spikes01",
			collisionclass = GridCollisionClass.COLLISION_NONE,
			pathfinderavoid = true,
			OnInit = function(grid)
				grid.Sprite:SetFrame("Spikes0" .. tostring(grid.rng:RandomInt(4) + 1), 0)
			end
		}
	},
	[GridEntityType.GRID_POOP] = {
		[0] = {
			collisionclass = GridCollisionClass.COLLISION_OBJECT,
			nocollision = true,
			destructable = true,
			crushable = true,
			OnInit = function(grid)
				local tile = grid:GetTile()
				local poop = Isaac.Spawn(oworld.CustomPoopId, oworld.CustomPoopVariant, 0, oworld.GetPositionByWorldTile(tile), oworld.utils.VEC_ZERO, nil):ToNPC()
				poop:AddEntityFlags(EntityFlag.FLAG_NO_KNOCKBACK | EntityFlag.FLAG_NO_PHYSICS_KNOCKBACK | EntityFlag.FLAG_NO_STATUS_EFFECTS | EntityFlag.FLAG_NO_TARGET | EntityFlag.FLAG_NO_BLOOD_SPLASH | EntityFlag.FLAG_DONT_OVERWRITE | EntityFlag.FLAG_NO_FLASH_ON_DAMAGE)
				poop:ClearEntityFlags(EntityFlag.FLAG_APPEAR)
				poop:GetData().GridEntity = grid
				poop:GetSprite():Play("State1", true)
				grid.PoopReference = oworld.EntityRef(poop)
			end,
			OnUpdate = function(grid)
				local poop = grid.PoopReference:GetEntity()
				if grid.PoopReference:IsLoaded() then
					local sprite = poop:GetSprite()
				elseif not grid.PoopReference:Exists() then
					grid.CollisionClass = GridCollisionClass.COLLISION_NONE
					grid.State = 2
					grid.Desc.State = 2
				end
			end,
			OnRemove = function(grid)
				local poop = grid.PoopReference:GetEntity()
				if grid.PoopReference:IsLoaded() then
					poop:Remove()
				end
			end,
			OnDamage = function(grid, dmg)
				local poop = grid.PoopReference:GetEntity()
				if grid.PoopReference:IsLoaded() then
					poop:TakeDamage(dmg, 0, EntityRef(poop), 0)
				elseif grid.PoopReference:Exists() then
					poop.hitpoints = poop.hitpoints - dmg
				end
			end
		}
	},
	[GridEntityType.GRID_WALL] = {
		[0] = {
			anm2 = "grid_wall",
			animation = "invisible",
			collisionclass = GridCollisionClass.COLLISION_WALL
		}
	},
	[GridEntityType.GRID_DOOR] = {
		[0] = {
			anm2 = "grid_wall",
			animation = "invisible",
			collisionclass = GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER,
			OnUpdate = function(grid)
				local doorEnt = grid.DoorReference:GetEntity()
				local barPosition
				if grid.DoorReference:IsLoaded() then
					local sprite = doorEnt:GetSprite()
					if sprite:GetFilename() == "gfx/grid/door_08_holeinwall.anm2" then
						doorEnt.Position = oworld.GetPositionByWorldTile(grid:GetTile())
					else
						doorEnt.Position = oworld.GetPositionByWorldTile(grid:GetTile())+Vector(0,20):Rotated(sprite.Rotation)
						barPosition = doorEnt.Position
						
						if sprite:IsFinished("KeyClose") then
							sprite:Play(grid.LockedAnimation, true)
						end
					end
					
					if grid.DoorState == "Locked" or grid.DoorState == "Chained" then
						for i=0, game:GetNumPlayers()-1 do
							local player = Isaac.GetPlayer(i)
							if (grid.Position - player.Position):LengthSquared() <= (30 + player.Size) ^ 2 then
								if player:HasGoldenKey() then
									grid:TryUnlock(false, true, true)
									
								elseif not grid.HasCoinLock and player:GetNumKeys() > 0 then
									local isUnlocked = grid:TryUnlock(false, true, false)
									if isUnlocked then
										player:AddKeys(-1)
									end
									
								elseif grid.HasCoinLock and player:GetNumCoins() > 0 then
									local isUnlocked = grid:TryUnlock(false, true, false)
									if isUnlocked then
										player:AddCoins(-1)
									end
								end
							end
						end
					end
				end
				
				if grid.DoorBarReference then
					local doorBarEnt = grid.DoorBarReference:GetEntity()
					if grid.DoorBarReference:IsLoaded() then
						local sprite = doorBarEnt:GetSprite()
						doorBarEnt.Position = barPosition
						if sprite:IsFinished("Vanish") then
							doorBarEnt:Remove()
							grid.DoorBarReference = nil
						end
					end
				end
				
				if grid.DoorChainReference then
					local doorChainEnt = grid.DoorChainReference:GetEntity()
					if grid.DoorChainReference:IsLoaded() then
						local sprite = doorChainEnt:GetSprite()
						if sprite:IsFinished("Appear") then
							sprite:Play("TwoChains", true)
						elseif sprite:IsFinished("KeyOpenChain1") or sprite:IsFinished("GoldenKeyOpenChain1") then
							sprite:Play("OneChain", true)
						elseif sprite:IsFinished("KeyOpenChain2") or sprite:IsFinished("GoldenKeyOpenChain2") or sprite:IsFinished("NoChains") then
							local is_removed = grid:TryRemoveChainLock()
							if not is_removed then
								doorChainEnt:Remove()
								grid.DoorChainReference = nil
							end
						end
					end
				end
				
				if grid.DoorPlanksReference then
					local doorPlanksEnt = grid.DoorPlanksReference:GetEntity()
					if grid.DoorPlanksReference:IsLoaded() then
						if doorPlanksEnt:GetSprite():IsFinished("Appear") then
							doorPlanksEnt:GetSprite():Play("Idle", true)
						end
					end
				end
			end,
			OnRemove = function(grid)
				local doorEnt = grid.DoorReference:GetEntity()
				if grid.DoorReference:IsLoaded() then
					local room = oworld.Rooms[doorEnt:GetData().RoomId]
					for i,door in ipairs(room.Doors) do
						if door.Tile == grid:GetTile() then
							table.remove(room.Doors, i)
							break
						end
					end
					doorEnt:Remove()
				elseif grid.DoorReference:Exists() then
					local room = oworld.Rooms[doorEnt.data.RoomId]
					for i,door in ipairs(room.Doors) do
						if door.Tile == grid:GetTile() then
							table.remove(room.Doors, i)
							break
						end
					end
					doorEnt:Remove()
				end
				local references = {grid.DoorBarReference, grid.DoorChainReference, grid.DoorPlanksReference}
				for _,ref in pairs(references) do
					if ref and ref:Exists() then
						ref:GetEntity():Remove()
						ref = nil
					end
				end
			end,
			OnSpawn = function(grid)
				local tile = grid:GetTile()
				local room = oworld.GetRoomByTile(tile)
				grid.doorFunctions = {}
				grid.doorFunctions.SetRoomTypes = function(grid, CurrentRoomType, TargetRoomType)
					grid.CurrentRoomType, grid.TargetRoomType = CurrentRoomType, TargetRoomType
					local dooranm2
					if TargetRoomType == RoomType.ROOM_SECRET then
						dooranm2 = oworld.DOOR[RoomType.ROOM_SECRET]
						grid.DoorType = RoomType.ROOM_SECRET
					
					elseif TargetRoomType == RoomType.ROOM_DEVIL then
						dooranm2 = oworld.DOOR[RoomType.ROOM_DEVIL]
						grid.DoorType = RoomType.ROOM_DEVIL
						
					elseif TargetRoomType == RoomType.ROOM_ANGEL then
						dooranm2 = oworld.DOOR[RoomType.ROOM_ANGEL]
						grid.DoorType = RoomType.ROOM_ANGEL
						
					else
						dooranm2 = oworld.DOOR[doorstruc.CurrentRoomType] or oworld.DOOR[doorstruc.TargetRoomType] or oworld.DOOR[-1]
						if oworld.DOOR[doorstruc.CurrentRoomType] then
							grid.DoorType = doorstruc.CurrentRoomType
						elseif oworld.DOOR[doorstruc.TargetRoomType] then
							grid.DoorType = doorstruc.TargetRoomType
						else
							grid.DoorType = RoomType.ROOM_DEFAULT
						end
					end
					local door = grid.DoorReference:GetEntity()
					if grid.DoorReference:IsLoaded() then
						door:GetSprite():Load(dooranm2, true)
					elseif grid.DoorReference:Exists() then
						door.sprite.anm2 = dooranm2
					end
				end
				grid.doorFunctions.Open = function(grid)
					if grid.DoorState == "Closed" then
						grid.DoorState = "Open"
						grid.PreviousState = grid.State
						grid.State = DoorState.STATE_OPEN
						grid.CollisionClass = GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER
						local door = grid.DoorReference:GetEntity()
						if grid.DoorReference:IsLoaded() then
							door:GetSprite():Play(grid.OpenAnimation, true)
							if not sfx:IsPlaying(oworld.CustomHeavyOpenDoorSoundId) then
								sfx:Play(oworld.CustomHeavyOpenDoorSoundId, 1, 0, false, 1)
							end
						elseif grid.DoorReference:Exists() then
							door.sprite.animname = "Opened"
						end
					end
				end
				grid.doorFunctions.Close = function(grid, Force)
					if grid.DoorState == "Open" then
						grid.DoorState = "Closed"
						grid.PreviousState = grid.State
						grid.State = DoorState.STATE_CLOSED
						grid.CollisionClass = GridCollisionClass.COLLISION_WALL
						local door = grid.DoorReference:GetEntity()
						if grid.DoorReference:IsLoaded() then
							door:GetSprite():Play(grid.CloseAnimation, true)
							if not sfx:IsPlaying(SoundEffect.SOUND_DOOR_HEAVY_CLOSE) then
								sfx:Play(SoundEffect.SOUND_DOOR_HEAVY_CLOSE, 1, 0, false, 1)
							end
						elseif grid.DoorReference:Exists() then
							if grid.DoorType == RoomType.ROOM_SECRET then  
								door.sprite.animname = "Close"
							else
								door.sprite.animname = "Closed"
							end
						end
					end
				end
				grid.doorFunctions.Bar = function(grid)
					if grid.DoorState ~= "Barred" then
						grid.DoorState = "Barred"
						grid.CollisionClass = GridCollisionClass.COLLISION_WALL
						grid.PreviousState = grid.State
						grid.State = DoorState.STATE_CLOSED
						local door = grid.DoorReference:GetEntity()
						if grid.DoorReference:IsLoaded() then
							if grid.DoorState == "Open" then
								door:GetSprite():Play(grid.CloseAnimation, true)
								if not sfx:IsPlaying(SoundEffect.SOUND_DOOR_HEAVY_CLOSE) then
									sfx:Play(SoundEffect.SOUND_DOOR_HEAVY_CLOSE, 1, 0, false, 1)
								end
							else
								if grid.DoorType == RoomType.ROOM_SECRET then  
									door:GetSprite():Play(grid.CloseAnimation, true)
								else
									door:GetSprite():Play("Closed", true)
								end
							end
							local bar = Isaac.Spawn(EntityType.ENTITY_EFFECT, oworld.CheapEffectVariant, 0, door.Position, oworld.utils.VEC_ZERO, nil)
							bar:GetSprite():Load("gfx/grid/door_17_bardoor.anm2", true)
							bar:GetSprite():Play("Appear", true)
							bar:GetSprite().Rotation = door:GetSprite().Rotation
							bar:GetData().OWIsDoorBar = true
							grid.DoorBarReference = oworld.EntityRef(bar)
							
						elseif grid.DoorReference:Exists() then
							if grid.DoorType == RoomType.ROOM_SECRET then  
								door.sprite.animname = "Close"
							else
								door.sprite.animname = "Closed"
							end
							local tile = grid:GetTile()
							grid.DoorBarReference = oworld.AddEntityData(tile.X, tile.Y, {
								id = EntityType.ENTITY_EFFECT,
								variant = oworld.CheapEffectVariant,
								worldpos = door.worldpos,
								sprite = {
									anm2 = "gfx/grid/door_17_bardoor.anm2",
									animname = "Idle",
									rotation = door.sprite.rotation
								},
								data = {
									OWIsDoorBar = true
								}
							}, true)
						end
					end
				end
				grid.doorFunctions.TryRemoveBar = function(grid)
					if grid.DoorState == "Barred" then
						local bar = grid.DoorBarReference:GetEntity()
						if grid.DoorBarReference:Exists() then
							bar:GetSprite():Play("Vanish", true)
						end
						grid.DoorBarReference = nil
						
						grid.DoorState = "Closed"
						grid.PreviousState = grid.State
						grid.State = DoorState.STATE_CLOSED
						grid.CollisionClass = GridCollisionClass.COLLISION_WALL
						local room = game:GetRoom()
						if room:IsClear() then
							grid:Open()
						end
					end
				end
				grid.doorFunctions.IsBarred = function(grid)
					return grid.DoorState == "Barred"
				end
				grid.doorFunctions.TryAddChainLock = function(grid, onlyOne)
					if grid.DoorState == "Open" then
						grid:Close()
					end
					if grid.DoorState == "Closed" then
						grid.DoorState = "Chained"
						local door = grid.DoorReference:GetEntity()
						if grid.DoorReference:IsLoaded() then
							local chain = Isaac.Spawn(EntityType.ENTITY_EFFECT, oworld.CheapEffectVariant, 0, door.Position, oworld.utils.VEC_ZERO, nil)
							chain:GetSprite().Rotation = door:GetSprite().Rotation
							chain:GetSprite():Load("gfx/grid/door_16_doublelock.anm2", true)
							if onlyOne then
								chain:GetSprite():Play("OneChain", true)
							else
								chain:GetSprite():Play("Appear", true)
							end
							chain:GetData().OWIsDoorChainLock = true
							grid.DoorChainReference = oworld.EntityRef(chain)
							
						elseif grid.DoorReference:Exists() then
							local tile = grid:GetTile()
							grid.DoorChainReference = oworld.AddGridEntityData(tile.X, tile.Y, {
								id = EntityType.ENTITY_EFFECT,
								variant = oworld.CheapEffectVariant,
								worldpos = door.worldpos,
								sprite = {
									anm2 = "gfx/grid/door_16_doublelock.anm2",
									animname = onlyOne and "OneChain" or "TwoChains",
									rotation = door.sprite.rotation
								},
								data = {
									OWIsDoorChainLock = true
								}
							}, true)
						end
						return true
					end
					return false
				end
				grid.doorFunctions.TryRemoveChainLock = function(grid)
					if grid.DoorState == "Chained" then
						local chain = grid.DoorChainReference:GetEntity()
						if grid.DoorChainReference:Exists() then
							chain:Remove()
						end
						grid.DoorChainReference = nil
						
						grid.DoorState = "Closed"
						grid.PreviousState = grid.State
						grid.State = DoorState.STATE_CLOSED
						grid.CollisionClass = GridCollisionClass.COLLISION_WALL
						local room = game:GetRoom()
						if room:IsClear() then
							grid:Open()
						end
					end
				end
				grid.doorFunctions.IsChained = function(grid)
					return grid.DoorState == "Chained"
				end
				grid.doorFunctions.SpawnDust = function(grid)
					-- WIP
				end
				grid.doorFunctions.CanBlowOpen = function(grid)
					return (grid.DoorState == "Closed" or grid.DoorState == "Hidden" or grid.DoorState == "Planks") and grid.CanBlowUp
				end
				grid.doorFunctions.TryBlowOpen = function(grid)
					for _,callback in ipairs(oworld.PreBlowUpDoorCallbacks) do
						if not callback.Param or callback.Param == -1 or callback.Param == grid.DoorType then
							callback.Fn(nil, grid)
						end
					end
					if grid:CanBlowOpen() then
						
						local not_busted = false
						if grid.DoorState == "Hidden" then
							local door = grid.DoorReference:GetEntity()
							if grid.DoorReference:IsLoaded() then
								door:GetSprite():Play("Opened", true)
							elseif grid.DoorReference:Exists() then
								door.sprite.animname = "Opened"
							end
							not_busted = true
							grid.CanBlowUp = false
							grid.DoorState = "Open"
							grid.PreviousState = grid.State
							grid.State = DoorState.STATE_OPEN
							grid.CollisionClass = GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER
							
						elseif grid.DoorState == "Planks" then
							local planks = grid.DoorPlanksReference:GetEntity()
							if grid.DoorPlanksReference:IsLoaded() then
								local sprite = planks:GetSprite()
								if sprite:IsFinished("Idle") then
									sprite:Play("Damaged", true)
									not_busted = true
								elseif sprite:IsFinished("Damaged") then
									grid:TryRemovePlanks()
								end
								
							elseif grid.DoorPlanksReference:Exists() then
								if planks.sprite.animname == "Idle" or planks.sprite.animname == "Appear" then
									planks.sprite.animname = "Damaged"
									not_busted = true
								elseif planks.sprite.animname == "Damaged" then
									grid:TryRemovePlanks()
								end
							end
							
						else
							grid.State = DoorState.STATE_HALF_CRACKED
							local door = grid.DoorReference:GetEntity()
							if grid.DoorReference:IsLoaded() then
								door:GetSprite():Play("Break", true)
							elseif grid.DoorReference:Exists() then
								door.sprite.animname = "BrokenOpen"
							end
						end
						
						if not not_busted then
							grid.DoorState = "Busted"
							grid.Busted = true
							grid.PreviousState = grid.State
							grid.CollisionClass = GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER
						end
						
						for _,callback in ipairs(oworld.PostBlowUpDoorCallbacks) do
							if not callback.Param or callback.Param == -1 or callback.Param == grid.DoorType then
								callback.Fn(nil, grid)
							end
						end
						
						return true
					end
					return false
				end
				grid.doorFunctions.TryAddPlanks = function(grid, alreadyCracked)
					if grid.DoorState == "Open" then
						grid:Close()
					end
					
					if grid.DoorState == "Closed" then
						grid.DoorState = "Planks"
						local door = grid.DoorReference:GetEntity()
						if grid.DoorReference:IsLoaded() then
							local planks = Isaac.Spawn(EntityType.ENTITY_EFFECT, oworld.CheapEffectVariant, 0, door.Position, oworld.utils.VEC_ZERO, nil)
							planks:GetSprite():Load("gfx/grid/door_18_crackeddoor.anm2", true)
							if alreadyCracked then
								planks:GetSprite():Play("Damaged", true)
							else
								planks:GetSprite():Play("Appear", true)
							end
							planks:GetSprite().Rotation = door:GetSprite().Rotation
							planks:GetData().OWIsDoorPlanks = true
							grid.DoorPlanksReference = oworld.EntityRef(planks)
							
						elseif grid.DoorReference:Exists() then
							local tile = grid:GetTile()
							grid.DoorPlanksReference = oworld.AddGridEntityData(tile.X, tile.Y, {
								id = EntityType.ENTITY_EFFECT,
								variant = oworld.CheapEffectVariant,
								sprite = {
									anm2 = "gfx/grid/door_18_crackeddoor.anm2",
									animname = alreadyCracked and "Damaged" or "Idle",
									rotation = door.sprite.rotation
								},
								data = {
									OWIsDoorPlanks = true
								}
							}, true)
						end
						return true
					end
					return false
				end
				grid.doorFunctions.TryRemovePlanks = function(grid)
					if grid.DoorState == "Planks" then
						local planks = grid.DoorPlanksReference:GetEntity()
						if grid.DoorPlanksReference:Exists() then
							planks:Remove()
						end
						grid.DoorPlanksReference = nil
						
						grid.DoorState = "Closed"
						grid.PreviousState = grid.State
						grid.State = DoorState.STATE_CLOSED
						grid.CollisionClass = GridCollisionClass.COLLISION_WALL
						local room = game:GetRoom()
						if room:IsClear() then
							grid:Open()
						end
					end
				end
				grid.doorFunctions.HasPlanks = function(grid)
					return grid.DoorState == "Planks"
				end
				grid.doorFunctions.CanUnlock = function(grid, Force)
					if grid.DoorState == "Chained" then
						local chain = grid.DoorChainReference:GetEntity()
						if grid.DoorChainReference:IsLoaded() then
							if not chain:GetSprite():IsFinished("TwoChains") and not chain:GetSprite():IsFinished("OneChain") then
								return false
							end
						elseif grid.DoorChainReference:Exists() then
							if chain.sprite.animname ~= "TwoChains" and chain.sprite.animname ~= "OneChain" then
								return false
							end
						end
					end
					return (grid.DoorState == "Locked" or grid.DoorState == "Chained") and (room:IsClear() or Force)
				end
				grid.doorFunctions.TryUnlock = function(grid, Force, KeyAnimation, GoldenKey)
					if grid:CanUnlock(Force) then
						if grid.DoorState == "Locked" then
							grid.DoorState = "Open"
							grid.PreviousState = grid.State
							grid.State = DoorState.STATE_OPEN
							grid.CollisionClass = GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER
							local door = grid.DoorReference:GetEntity()
							if grid.DoorReference:IsLoaded() then
								if KeyAnimation and GoldenKey then
									door:GetSprite():Play("GoldenKeyOpen", true)
								elseif KeyAnimation then
									door:GetSprite():Play("KeyOpen", true)
								else
									door:GetSprite():Play(grid.OpenLockedAnimation, true)
								end
								if not sfx:IsPlaying(SoundEffect.SOUND_UNLOCK00) then
									sfx:Play(SoundEffect.SOUND_UNLOCK00, 1, 0, false, 1)
								end
							elseif grid.DoorReference:Exists() then
								door.Sprite.animname = grid.OpenAnimation
							end
							
						elseif grid.DoorState == "Chained" then
							local chain = grid.DoorChainReference:GetEntity()
							if grid.DoorChainReference:IsLoaded() then
								if chain:GetSprite():IsFinished("TwoChains") then
									chain:GetSprite():Play((GoldenKey and "Golden" or "") .. "KeyOpenChain1", true)
								elseif chain:GetSprite():IsFinished("OneChain") then
									chain:GetSprite():Play((GoldenKey and "Golden" or "") .. "KeyOpenChain2", true)
								end
							elseif grid.DoorChainReference:Exists() then
								if chain.sprite.animname == "TwoChains" then
									chain.sprite.animname = "OneChain"
								elseif chain.sprite.animname == "OneChain" then
									grid:TryRemoveChainLock()
								end
							end
						end
						return true
					end
					return false
				end
				grid.doorFunctions.IsOpen = function(grid)
					return grid.DoorState == "Open"
				end
				grid.doorFunctions.IsKeyFamiliarTarget = function()
					-- WIP
				end
				grid.doorFunctions.SetLocked = function(grid, Locked)
					if Locked and grid.DoorState ~= "Locked" then
						local door = grid.DoorReference:GetEntity()
						if grid.DoorReference:IsLoaded() then
							if grid.DoorState == "Open" or grid.DoorState == "Busted" then
								door:GetSprite():Play("KeyClose", true)
							else
								door:GetSprite():Play(grid.LockedAnimation, true)
							end
						elseif grid.DoorReference:Exists() then
							door.sprite.animname = grid.LockedAnimation
						end
						grid.DoorState = "Locked"
						grid.PreviousState = grid.State
						grid.State = DoorState.STATE_CLOSED
						grid.CollisionClass = GridCollisionClass.COLLISION_WALL
						
					elseif not Locked and grid.DoorState == "Locked" then
						local unlocked = grid:TryUnlock(false)
						if not unlocked then
							grid.DoorState = "Closed"
							grid.PreviousState = grid.State
							grid.State = DoorState.STATE_CLOSED
							grid.CollisionClass = GridCollisionClass.COLLISION_WALL
							local door = grid.DoorReference:GetEntity()
							if grid.DoorReference:IsLoaded() then
								if grid.DoorType == RoomType.ROOM_SECRET then
									door:GetSprite():Play("Close", true)
								else
									door:GetSprite():Play("Closed", true)
								end
							elseif grid.DoorReference:Exists() then
								if grid.DoorType == RoomType.ROOM_SECRET then
									door.sprite.animname = "Close"
								else
									door.sprite.animname = "Closed"
								end
							end
						end
					end
				end
				grid.doorFunctions.IsLocked = function(grid)
					return grid.DoorState == "Locked"
				end
				grid.doorFunctions.IsBusted = function(grid)
					return grid.DoorState == "Busted"
				end
				grid.doorFunctions.IsRoomType = function(grid, Type)
					return grid.CurrentRoomType == Type
				end
				grid.doorFunctions.IsTargetRoomAcrade = function(grid)
					return grid.TargetRoomType == RoomType.ROOM_ARCADE
				end
				grid.doorFunctions.GetSpriteOffset = function(grid)
					local door = grid.DoorReference:GetEntity()
					if grid.DoorReference:IsLoaded() then
						return door.Position - oworld.GetPositionByWorldTile(grid:GetTile()) + door:GetSprite().Offset
					elseif grid.DoorReference:Exists() then
						if not door.worldpos then return oworld.utils.VEC_ZERO end
						return door.worldpos - oworld.GetWorldPosByWorldTile(grid:GetTile()) + (door.sprite and door.sprite.offset or oworld.utils.VEC_ZERO)
					end
					return oworld.utils.VEC_ZERO
				end
				
				grid.CurrentRoomIndex = room.RoomId
				for i=1, 2 do
					local door = grid.DoorStructure.Doors[i]
					if door.Tile.X ~= tile.X or door.Tile.Y ~= tile.Y then
						grid.TargetRoomIndex = oworld.GetRoomByTile(tile).RoomId
					end
				end
				
				grid.CurrentRoomType = grid.CurrentRoomType or RoomType.ROOM_DEFAULT
				grid.TargetRoomType = grid.TargetRoomType or RoomType.ROOM_DEFAULT
				grid.Direction = grid.Direction or Direction.NO_DIRECTION
				grid.Slot = grid.Slot or DoorSlot.NO_DOOR_SLOT
				grid.ExtraSprite = Sprite() -- not supported
				grid.ExtraVisible = false -- not supported
				grid.Busted = false
				grid.PreviousVariant = grid.Desc.Variant
				grid.OpenAnimation = grid.OpenAnimation or "Open"
				grid.CloseAnimation = grid.CloseAnimation or "Close"
				grid.LockedAnimation = grid.LockedAnimation or "KeyClosed"
				grid.OpenLockedAnimation = grid.OpenLockedAnimation or "KeyOpenNoKey"
				grid.ApplyFuncOnThisDoorOnly = false
				grid.CanBlowUp = true
				grid.HasCoinLock = false
				
				local isFunctionForBothDoors = {SetRoomTypes = true, Open = true, Close = true, TryBlowOpen = true}
				grid = setmetatable(grid, {
					__newindex = function(g, k, v)
						if k == "State" then grid.Desc.State = v
						elseif k == "VarData" then grid.Desc.VarData = v
						else rawset(g, k, v) end
					end,
					__index = function(g, k)
						if k == "State" then return grid.Desc.State
						elseif k == "VarData" then return grid.Desc.VarData
						elseif k == "Position" then return oworld.GetPositionByWorldTile(tile) 
						elseif isFunctionForBothDoors[k] then 
							local tile = g:GetTile()
							local grid2
							for i=1, 2 do
								local doorstruc = g.DoorStructure.Doors[i]
								if tile.X ~= doorstruc.Tile.X or tile.Y ~= doorstruc.Tile.Y then
									grid2 = oworld.GetGridEntity(doorstruc.Tile)
									break
								end
							end
							return function(grid, a, b, c, d, e, f)
								if not grid.ApplyFuncOnThisDoorOnly then
									grid2.doorFunctions[k](grid2, a, b, c, d, e, f)
								end
								grid.ApplyFuncOnThisDoorOnly = false
								return grid.doorFunctions[k](grid, a, b, c, d, e, f)
							end
						elseif grid.doorFunctions[k] then
							return grid.doorFunctions[k]
						else
							return rawget(g, k) 
						end
					end
				})
				
				local level = game:GetLevel()
				if grid.TargetRoomType == RoomType.ROOM_SHOP or grid.TargetRoomType == RoomType.ROOM_TREASURE and oworld.LevelStage ~= LevelStage.STAGE1_1 
				or grid.TargetRoomType == RoomType.ROOM_LIBRARY then
					grid:SetLocked(true)
				end
				if grid.TargetRoomType == RoomType.ROOM_DICE then
					grid:TryAddChainLock()
				end
				if grid.TargetRoomType == RoomType.ROOM_ISAACS or grid.TargetRoomType == RoomType.ROOM_BARREN then
					grid:TryAddPlanks()
				end
				if grid.TargetRoomType == RoomType.ROOM_ARCADE then
					grid.HasCoinLock = true
				end
				if grid.DoorType == RoomType.ROOM_BOSS or grid.DoorType == RoomType.ROOM_CHALLENGE then
					grid.CanBlowUp = false
				end
				if grid.DoorType == RoomType.ROOM_SECRET or grid.DoorType == RoomType.ROOM_SUPERSECRET then
					grid.DoorState = "Hidden"
					grid.State = DoorState.STATE_CLOSED
					grid.PreviousState = grid.State
					grid.CollisionClass = GridCollisionClass.COLLISION_WALL
				end
			end
		}
	},
	[GridEntityType.GRID_TRAPDOOR] = {
		[0] = {
			anm2 = "door_11_trapdoor",
			animation = "Closed",
			collisionclass = GridCollisionClass.COLLISION_NONE,
			OnInit = function(grid)
				grid.TrapdoorState = "Closed"
			end,
			OnUpdate = function(grid)
				if grid.OnEnter then
					if grid.TrapdoorState == "Closed" then
						local playerTooClose = false
						for i=0, game:GetNumPlayers() - 1 do
							local player = Isaac.GetPlayer(i)
							
							if (player.Position - grid.Position):LengthSquared() <= 30 ^ 2 then
								playerTooClose = true
								break
							end
						end
						
						if not playerTooClose and (not grid.OpenConditions or grid.OpenConditions()) then
							grid.TrapdoorState = "Open"
							grid.Sprite:Play("Open Animation", true)
						end
					end
						
					if grid.TrapdoorState == "Open" then
						if grid.Sprite:IsFinished("Open Animation") then
							grid.Sprite:SetFrame("Opened", 0)
						end
						
						for i=0, game:GetNumPlayers() - 1 do
							local player = Isaac.GetPlayer(i)
							
							if (player.Position - grid.Position):LengthSquared() <= 20 ^ 2 then
								grid.TrapdoorState = "Entering"
								grid.EnteringPlayer = player
								grid.EnteringPlayer:AnimateTrapdoor()
								grid.Sprite:Play("Player Exit", true)
								break
							end
						end
					end
					
					if grid.TrapdoorState == "Entering" then
						if not grid.EnteringPlayer then
							grid.TrapdoorState = "Open"
							grid.Sprite:SetFrame("Opened", 0)
							return
							
						else
							grid.EnteringPlayer.Position = grid.EnteringPlayer.Position + (grid.Position - grid.EnteringPlayer.Position)*0.25
						
							if grid.EnteringPlayer:IsExtraAnimationFinished() then
								grid.OnEnter()
							end
						end
					end
				end
			end
		}
	},
	[GridEntityType.GRID_FIREPLACE] = {
		[0] = {
			collisionclass = GridCollisionClass.COLLISION_OBJECT,
			nocollision = true,
			destructable = true,
			crushable = true,
			OnInit = function(grid)
				local tile = grid:GetTile()
				grid.FireReference = oworld.AddEntityData(tile.X, tile.Y, oworld.World, {
					id = EntityType.ENTITY_FIREPLACE,
					variant = grid:GetVariant(),
					data = {GridEntity = grid}
				}, true)
			end,
			OnRemove = function(grid)
				local fire = grid.FireReference:GetEntity()
				if grid.FireReference:Exists() then
					fire:Remove()
				end
			end,
			OnDamage = function(grid, dmg)
				local fire = grid.FireReference:GetEntity()
				if grid.FireReference:IsLoaded() then
					fire:TakeDamage(dmg, 0, EntityRef(fire), 0)
				elseif grid.FireReference:Exists() then
					fire.hitpoints = fire.hitpoints - dmg
				end
			end
		}
	}
}

oworld.PetrifiedPoopSpawned = false
oworld:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, function(_, continue)
	if not continue then
		oworld.PetrifiedPoopSpawned = false
	end
end)

oworld:AddCallback(ModCallbacks.MC_ENTITY_TAKE_DMG, function(_, ent, dmg, flags, src, invuln)
	if ent.Variant == oworld.CustomPoopVariant then
		local data = ent:GetData()
		if data.PoopIsDestroyed then
			return false
		end
		if dmg >= ent.HitPoints then
			data.PoopIsDestroyed = true
			ent:GetSprite():Play("State5", true)
			ent.EntityCollisionClass = EntityCollisionClass.ENTCOLL_NONE
			ent.DepthOffset = -40
			sfx:Play(SoundEffect.SOUND_PLOP , 0.7, 0, false, 1)
			local eff = Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.POOP_EXPLOSION, 0, ent.Position, oworld.utils.VEC_ZERO, ent)
			local grid = data.GridEntity
			if grid then
				grid.CollisionClass = GridCollisionClass.COLLISION_NONE
				grid.State = 2
				grid.Desc.State = 2
				if grid and not grid.NoReward then
					local reward = oworld.SpawnPoopReward(grid.rng, oworld.GetPositionByWorldTile(grid:GetTile()), ent, not oworld.PetrifiedPoopSpawned)
					if reward and reward.Variant == PickupVariant.PICKUP_TRINKET and reward.SubType == TrinketType.TRINKET_PETRIFIED_POOP then
						oworld.PetrifiedPoopSpawned = true
					end
				end
			end
			return false
		else
			local sprite = ent:GetSprite()
			local poop_state = math.floor((ent.MaxHitPoints - ent.HitPoints + dmg) / ent.MaxHitPoints * 4) + 1
			if not sprite:IsPlaying("State" .. tostring(poop_state)) and not sprite:IsFinished("State" .. tostring(poop_state)) then
				sprite:Play("State" .. tostring(poop_state))
			end
		end
	end
end, oworld.CustomPoopId)

oworld:AddCallback(ModCallbacks.MC_PRE_NPC_COLLISION, function(_, ent, collider, low)
	if ent.Variant == oworld.CustomPoopVariant then
		if not oworld.CanCollideWithGrid(collider:GetData().FakeGridCollisionClass, GridCollisionClass.COLLISION_SOLID) then
			return true
		end
	end
end, oworld.CustomPoopId)

oworld:AddCallback(ModCallbacks.MC_PRE_LOAD_ENTITY, function(_, edata)
	if edata.variant == oworld.CheapEffectVariant then
		if edata.data.OWIsDoorEffect then
			if edata.sprite.animname == edata.data.GridEntity.OpenAnimation or edata.sprite.animname == "KeyOpen" or edata.sprite.animname == edata.data.GridEntity.OpenLockedAnimation or edata.sprite.animname == "GoldenKeyOpen" then
				edata.sprite.animname = "Opened"
			elseif edata.sprite.animname == edata.data.GridEntity.CloseAnimation and edata.data.GridEntity.DoorType ~= RoomType.ROOM_SECRET then
				edata.sprite.animname = "Closed"
			elseif edata.sprite.animname == "Break" then
				edata.sprite.animname = "BrokenOpen"
			elseif edata.sprite.animname == "KeyClose" then
				edata.sprite.animname = edata.data.GridEntity.LockedAnimation
			end
		end
		if edata.data.OWIsDoorChainLock then
			if edata.sprite.animname == "Appear" then
				edata.sprite.animname = "TwoChains"
			elseif edata.sprite.animname == "KeyOpenChain1" or edata.sprite.animname == "GoldenKeyOpenChain1" then
				edata.sprite.animname = "OneChain"
			elseif edata.sprite.animname == "KeyOpenChain2" or edata.sprite.animname == "GoldenKeyOpenChain2" then
				edata.sprite.animname = "NoChains"
			end
		end
		if edata.data.OWIsDoorPlanks then
			if edata.sprite.animname == "Appear" then
				edata.sprite.animname = "Idle"
			end
		end
	end
end, EntityType.ENTITY_EFFECT)

function oworld.SpawnTintedRockReward(rng, pos)
	local arr = {}
	if rng:RandomInt(25) == 0 then 
		table.insert(arr, Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COLLECTIBLE, CollectibleType.COLLECTIBLE_SMALL_ROCK, pos, oworld.utils.VEC_ZERO, nil))
	elseif rng:RandomInt(6) == 0 then
		table.insert(arr, Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_LOCKEDCHEST, 0, pos, oworld.utils.VEC_ZERO, nil))
	else
		for i=1, rng:RandomInt(2) + 1 do
			local vel = oworld.utils.RandomVector(rng:RandomFloat()+4)
			if rng:RandomInt(10) == 0 then
				table.insert(arr, Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_KEY, 0, pos, vel, nil))
			elseif rng:RandomInt(5) == 0 then
				table.insert(arr, Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_BOMB, 0, pos, vel, nil))
			else
				table.insert(arr, Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, HeartSubType.HEART_SOUL, pos, vel, nil))
			end
		end
	end
	return arr
end

function oworld.SpawnPoopReward(rng, pos, spawnerEnt, includePetrifiedPoop)
	local playerHasPetrifiedPoop = false
	for i=0, game:GetNumPlayers() - 1 do
		local player = Isaac.GetPlayer(i)
		if player:HasTrinket(TrinketType.TRINKET_PETRIFIED_POOP) then
			playerHasPetrifiedPoop = true
			break
		end
	end
	local r = rng:RandomFloat()
	if includePetrifiedPoop and r <= 0.005 then
		return Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_TRINKET, TrinketType.TRINKET_PETRIFIED_POOP, pos, oworld.utils.VEC_ZERO, spawnerEnt)
	elseif r <= 0.03 or playerHasPetrifiedPoop and r <= 0.045 then
		return Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_HEART, 0, pos, oworld.utils.VEC_ZERO, spawnerEnt)
	elseif r <= 0.1 or playerHasPetrifiedPoop and r <= 0.15 then
		return Isaac.Spawn(EntityType.ENTITY_PICKUP, PickupVariant.PICKUP_COIN, 0, pos, oworld.utils.VEC_ZERO, spawnerEnt)
	end
end

Isaac.DebugString("Loaded Open World gridimplementations")