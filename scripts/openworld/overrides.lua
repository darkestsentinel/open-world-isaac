local oworld = require "scripts.openworld"

require "scripts.apioverride"

-- Game functions override --

oworld.OldGame = Game
local oldgame = oworld.OldGame()

local gameattributes = {
	BossRushParTime=true, 
	BlueWombParTime=true, 
	ScreenShakeOffset=true, 
	Challenge=true, 
	Difficulty=true, 
	TimeCounter=true
}

oworld.LastDevilRoomStage = LevelStage.STAGE_NULL

function Game()
	return setmetatable({
		GetRoom = function()
			return oworld.GetCurrentRoom and oworld.GetCurrentRoom() or oldgame:GetRoom(), oworld.GetCurrentRoom and not not oworld.GetCurrentRoom()
		end,
		GetLastDevilRoomStage = function()
			return oworld.LastDevilRoomStage
		end,
		SetLastDevilRoomStage = function(levelStage)
			oworld.LastDevilRoomStage = levelStage
		end,
	}, {
		__index = function(t, k)
			return rawget(t, k) or gameattributes[k] and oldgame[k] or function(...)
				local args = {...}
				table.remove(args, 1)
				return oldgame[k](oldgame, table.unpack(args))
			end
		end
	})
end

local game = Game()

-- RoomEntities rewrite/optimization --

if not oworld.OldGetRoomEntities then
	oworld.OldGetRoomEntities = Isaac.GetRoomEntities
	-- optimization by putting Isaac.GetRoomEntities() in a var, which gets updated every frame
	function oworld:UpdateRoomEntities()
		oworld.roomEntities = oworld.OldGetRoomEntities()
	end
end

oworld:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, oworld.UpdateRoomEntities)
oworld:AddCallback(ModCallbacks.MC_POST_RENDER, oworld.UpdateRoomEntities)

function Isaac.GetRoomEntities() -- new GetRoomEntities for entities in an open world room
	if not oworld.LastActiveRoom then
		return oworld.roomEntities
	end
	local room = game:GetRoom()
	local roomentities = {}
	for _,ent in ipairs(oworld.roomEntities) do
		if room:IsPositionInRoom(ent.Position, 0) then
			table.insert(roomentities, ent)
		end
	end
	return roomentities
end

-- Sprite hijack --

oworld.EntityClasses = {Entity, EntityBomb, EntityEffect, EntityNPC, EntityPickup, EntityPlayer}

-- hijacks the Entity:GetSprite() function to keep track of entity sprites
oworld.SpriteAttribute = {Offset=true,Scale=true,Rotation=true,Color=true,FlipX=true,FlipY=true,PlaybackSpeed=true}
for _,entityClass in ipairs(oworld.EntityClasses) do
	local OldGetSpriteFunc = APIOverride.GetCurrentClassFunction(entityClass, "GetSprite")
	APIOverride.OverrideClassFunction(entityClass, "GetSprite", function(ent)
		local data = ent:GetData()
		return setmetatable({}, {
			__index = function(_, k)
				if oworld.SpriteAttribute[k] then
					return OldGetSpriteFunc(ent)[k]
				else
					return function(_, a, b, c)
						if data.oworldStoredSprite then
							if k == "Load" then 
								data.oworldStoredSprite.anm2 = a
							elseif k == "Play" then 
								data.oworldStoredSprite.animname = a
								data.oworldStoredSprite.on = "playing"
							elseif k == "SetFrame" then
								data.oworldStoredSprite.animname = a
								data.oworldStoredSprite.on = "frame"
							elseif k == "ReplaceSpritesheet" then data.oworldStoredSprite.replacedpngs[a] = b
							elseif k == "SetAnimation" then data.oworldStoredSprite.animname = a
							elseif k == "PlayOverlay" then 
								data.oworldStoredSprite.overlayanimname = a
								data.oworldStoredSprite.overlayon = "playing"
							elseif k == "SetOverlayAnimation" then data.oworldStoredSprite.overlayanimname = a
							elseif k == "SetOverlayRenderPriority" then data.oworldStoredSprite.overlayrenderpriority = a
							elseif k == "SetOverlayFrame" then 
								data.oworldStoredSprite.overlayanimname = a
								data.oworldStoredSprite.overlayon = "frame"
							elseif k == "RemoveOverlay" then data.oworldStoredSprite.overlayon = nil end
						end
						
						return OldGetSpriteFunc(ent)[k](OldGetSpriteFunc(ent), a, b, c)
					end	
				end				
			end,
			__newindex = function(_, k, v)
				OldGetSpriteFunc(ent)[k] = v
			end
		})
	end)
end

-- Level methods override --

APIOverride.OverrideClassFunction(Level, "GetCurrentRoom", function()
	return game:GetRoom()
end)

APIOverride.OverrideClassFunction(Level, "GetPreviousRoomIndex", function()
	return OWORLD.PrevRoomIndex
end)

APIOverride.OverrideClassFunction(Level, "GetCurrentRoomIndex", function()
	return OWORLD.CurrentRoomIndex
end)

APIOverride.OverrideClassFunction(Level, "GetStartingRoomIndex", function()
	for _,room in pairs(OWORLD.Rooms) do
		if room.StartingRoom then
			return room.GridIndex
		end
	end
end)

APIOverride.OverrideClassFunction(Level, "GetRooms", function()
	return {}
end)

-- RoomDescriptor Override --

--[[function OWORLD.GetRoomDescriptor(room)
	local roomdescriptordata = {
		StageId = room.StageId,
		Type = room.Type,
		Difficulty = room.Difficulty,
		Subtype = room.Subtype,
		Variant = room.Variant,
		Name = room.Name,
		Shape = room.RoomShapeId,
		Height = room.BottomRightTile.Y - room.TopLeftTile.Y,
		Width = room.BottomRightTile.X - room.TopLeftTile.X,
		InitialWeight = room.Weight,
		Weight = room.Weight,
		Doors = room.Doors
	}
	local roomdescriptor = {
		GridIndex = room.GridIndex,
		SafeGridIndex = room.SafeGridIndex,
		ListIndex = room.RoomId,
		Data = roomdescriptordata,
		OverrideData = roomdescriptordata,
		AllowedDoors = room.AllowedDoorSlot,
		DisplayFlags = room.DisplayFlags,
		VisitedCount = room.VisitedCount,
		Clear = room.Clear,
		ClearCount = room.ClearCount,
		DecorationSeed = room.Seed,
		SpawnSeed = room.Seed,
		AwardSeed = room.Seed,
		
		-- WIP
		PressurePlatesTriggered = false,
		SacrificeDone = false,
		ChallengeDone = false,
		SupriseMiniboss = false,
		HasWater = false,
		NoReward = false,
		PoopCount = 0,
		PitsCount = 0,
		ShopItemidx = 0,
		ShopItemDiscountIdx = 0,
		DeliriumDistance = 9999
	}
	return roomdescriptor
end

APIOverride.OverrideClassFunction(Level, "GetRoomCount", function()
	local n = 0
	for _,room in pairs(OWORLD.Rooms) do
		n = n + 1
	end
	return n
end)

APIOverride.OverrideClassFunction(Level, "GetRoomByIdx", function(level, roomid)
	return OWORLD.GetRoomDescriptor(OWORLD.Rooms[roomid])
end)

APIOverride.OverrideClassFunction(Level, "GetRooms", function()
	local rooms = {}
	local i = 1
	for _,room in pairs(OWORLD.Rooms) do
		rooms[i] = OWORLD.GetRoomDescriptor(room)
		i = i+1
	end
	function rooms:Get(id)
		return rooms[id + 1]
	end
	return rooms
end)]]

Isaac.DebugString("Loaded Open World overrides")