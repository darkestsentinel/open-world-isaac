local oworld

if include then
	oworld = include("scripts.openworld")
	include("scripts.openworld.utilities")
	include("scripts.openworld.maps")
	include("scripts.openworld.overrides")
	
	include("scripts.openworld.core")
	include("scripts.openworld.gridimplementations")
	include("scripts.openworld.gridentities")
	include("scripts.openworld.pathfinding")
	include("scripts.openworld.rooms")
	
	include("scripts.openworld.rewrittenenemies")
	
	include("scripts.openworld.menu")
	
else
	oworld = require "scripts.openworld"
	require "scripts.openworld.utilities"
	require "scripts.openworld.maps"
	require "scripts.openworld.overrides"
	
	require "scripts.openworld.core"
	require "scripts.openworld.gridimplementations"
	require "scripts.openworld.gridentities"
	require "scripts.openworld.pathfinding"
	require "scripts.openworld.rooms"
	
	require "scripts.openworld.rewrittenenemies"
	
	require "scripts.openworld.menu"
end

function oworld.FloorsInit()
	if REPENTANCE then
		require "scripts.minimapapi.rep.init"
		
	else
		require "scripts.minimapapi.init"
	end
	
	if include then
		include("scripts.openworld.floors.floors")
		
	else
		require "scripts.openworld.floors.floors"
	end
end

oworld.Loaded = true

Isaac.DebugString("Open World Isaac is succesfully loaded!")

return oworld
