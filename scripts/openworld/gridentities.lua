local oworld = require "scripts.openworld"

local game = Game()

oworld.NumCustomGridTypes = 0

function oworld.AddNewGridEntityType(GridData)
	oworld.NumCustomGridTypes = oworld.NumCustomGridTypes + 1
	
	local maxVanillaId = REPENTANCE and GridEntityType.GRID_ROCK_GOLD or GridEntityType.GRID_ROCK_SS
	local id = maxVanillaId + oworld.NumCustomGridTypes
	
	oworld.FakeGridTypes[id] = griddata
	
	return id
end

if not oworld.FakeGridEntities then
	oworld.FakeGridEntities = {}
	oworld.GridHandlerEntities = {}
end

oworld:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	local numgridhandlingents = #oworld.GridHandlerEntities
	local numremovedgridhandlingents = 0
	local cpos = oworld.WorldPosToPosition(oworld.WorldPlayerPos)
	for i=1, numgridhandlingents do
		local ent = oworld.GridHandlerEntities[i-numremovedgridhandlingents]
		if not ent:Exists() or not ent:GetData().GridSpriteHandlerActive then
			ent:Remove()
			table.remove(oworld.GridHandlerEntities, i-numremovedgridhandlingents)
			numremovedgridhandlingents = numremovedgridhandlingents+1
		end
		if not ent:GetData().GridSpriteHandlerActive then
			ent.Position = cpos
		end
		ent.Velocity = oworld.utils.VEC_ZERO
	end
end)

oworld:AddCallback(ModCallbacks.MC_POST_EFFECT_UPDATE, function(_, eff)
	-- makes bombs destroy grids
	if eff.Variant == EffectVariant.BOMB_EXPLOSION then
		if not eff:GetData().ExplosionInit then
			eff:GetData().ExplosionInit = true
			local room = game:GetRoom()
			local explosionradius = 100*eff.Scale
			local maxdistanceintiles = math.ceil(explosionradius/40)
			local explosiontile = oworld.GetWorldTileByPosition(eff.Position)
			for x=explosiontile.X-maxdistanceintiles, explosiontile.X+maxdistanceintiles do
				for y=explosiontile.Y-maxdistanceintiles, explosiontile.Y+maxdistanceintiles do
					local grid = oworld.GetGridEntity(Vector(x,y))
					if grid then
						local tilepos = grid.Position
						if (tilepos-eff.Position):LengthSquared() <= explosionradius^2 then
							if room:CheckLine(eff.Position, tilepos, 2, 0, false, true) then
								oworld.DestroyGridEntity(oworld.GetGridEntity(Vector(x,y)))
							end
							if grid.Desc.Type == GridEntityType.GRID_DOOR then
								grid:TryBlowOpen(true)
							end
						end
					end
				end
			end
		end
	end
end)

function Isaac.GridSpawn(gridType, variant, position, forced)
	return oworld.GridSpawn(gridType, variant, oworld.GetWorldTileByPosition(position), forced, false)
end

function oworld.GridSpawn(gridType, variant, tile, forced, skipOnSpawn)
	local prevgrid = oworld.GetGridEntity(tile)
	if forced and prevgrid then
		if prevgrid.Entity then
			prevgrid.Entity:Remove()
		end
		prevgrid = nil
		oworld.FakeGridEntities[tile.X][tile.Y] = nil
	end
	if not prevgrid then
		local rng = oworld.GetRNG()
		local gridEntity = {}
		gridEntity = {
			IsGridEntity = true,
			rng = rng,
			Visible = true,
			Desc = {
				Type = gridType,
				Variant = variant,
				State = 0,
				SpawnCount = 1,
				SpawnSeed = rng:GetSeed(),
				VarData = 0,
				Initialized = false,
				Loaded = false,
				VariableSeed = rng:GetSeed(),
				Sprite = {
					anm2 = nil,
					animation = nil,
					on = nil,
					overlayanimation = nil,
					overlayon = nil,
					overlayvariation = nil,
					variation = nil,
					offset = oworld.utils.VEC_ZERO,
					scale = Vector(1,1),
					rotation = 0,
					color = Color(1,1,1,1,0,0,0),
					flipx = false,
					flipy = false,
					playbackspeed = 1,
					replacedpngs = {}
				}
			},
			RNG = rng,
			GetRNG = function()
				return gridEntity.RNG
			end,
			GetTile = function()
				return tile
			end,
			SetType = function(_, gridenttype)
				gridEntity.Desc.Type = gridenttype
			end,
			SetVariant = function(_, gridentvariant)
				gridEntity.PreviousVariant = gridEntity.Desc.Variant
				gridEntity.Desc.Variant = gridentvariant
			end,
			Init = function()
				oworld.GridInit(gridEntity)
			end,
			PostInit = function()
				oworld.GridInit(gridEntity)
			end,
			Update = function() end,
			Render = function(_, offset)
				gridEntity.Sprite:Render(Isaac.WorldToScreen(gridEntity.Position)+offset, oworld.utils.VEC_ZERO, oworld.utils.VEC_ZERO)
			end,
			Hurt = function(_, dmg)
			
			end,
			Destroy = function()
				oworld.DestroyGridEntity(gridEntity)
			end,
			GetType = function()
				return gridEntity.Desc.Type
			end,
			GetVariant = function()
				return gridEntity.Desc.Variant
			end,
			GetGridIndex = function()
				return game:GetRoom():GetGridIndex(gridEntity.Position)
			end,
			GetSaveState = function()
				return gridEntity.Desc
			end,
			ToDoor = function()
				if gridEntity.Desc.Type == GridEntityType.GRID_DOOR then
					return gridEntity
				end
			end,
			ToPit = function()
				if gridEntity.Desc.Type == GridEntityType.GRID_PIT then
					return gridEntity
				end
			end,
			ToPoop = function()
				if gridEntity.Desc.Type == GridEntityType.GRID_POOP then
					return gridEntity
				end
			end,
			ToRock = function()
				if gridEntity.Desc.Type == GridEntityType.GRID_ROCK or gridEntity.Desc.Type == GridEntityType.GRID_ROCKB or gridEntity.Desc.Type == GridEntityType.GRID_ROCKT or gridEntity.Desc.Type == GridEntityType.GRID_ROCK_BOMB or gridEntity.Desc.Type == GridEntityType.GRID_ROCK_ALT then
					return gridEntity
				end
			end,
			ToPressurePlate = function()
				if gridEntity.Desc.Type == GridEntityType.GRID_PRESSURE_PLATE then
					return gridEntity
				end
			end,
			ToSpikes = function()
				if gridEntity.Desc.Type == GridEntityType.GRID_SPIKES or gridEntity.Desc.Type == GridEntityType.GRID_SPIKES_ONOFF then
					return gridEntity
				end
			end,
			ToTNT = function()
				if gridEntity.Desc.Type == GridEntityType.GRID_TNT then
					return gridEntity
				end
			end
		}
		
		gridEntity = setmetatable(gridEntity, { -- sets pointers to gridEntity.Desc for the State and VarData attributes
			__newindex = function(g, k, v)
				if k == "State" then gridEntity.Desc.State = v
				elseif k == "VarData" then gridEntity.Desc.VarData = v
				else rawset(g, k, v) end
			end,
			__index = function(g, k)
				if k == "State" then return gridEntity.Desc.State
				elseif k == "VarData" then return gridEntity.Desc.VarData
				elseif k == "Position" then return oworld.GetPositionByWorldTile(tile)
				elseif k == "CollisionClass" then return rawget(g, "CollisionClass") or oworld.GetGridData(g).collisionclass or 0 -- in case CollisionClass isn't setup yet
				else return rawget(g, k) end
			end
		})
		
		gridEntity.Sprite = setmetatable({
			IsFinished = function(_, anm)
				if gridEntity.Desc.Sprite.animation == anm and gridEntity.Desc.Sprite.on == "frame" then
					return true
				end
				return false
			end,
			Play = function(_, anm)
				gridEntity.Desc.Sprite.animation = anm
				gridEntity.Desc.Sprite.on = "playing"
			end,
			SetFrame = function(_, anm, frame)
				gridEntity.Desc.Sprite.animation = anm
				gridEntity.Desc.Sprite.on = "frame"
				gridEntity.Desc.Sprite.variation = frame
			end,
			Load = function(_, anm2)
				gridEntity.Desc.Sprite.anm2 = anm2
			end,
			ReplaceSpritesheet = function(_, layerid, png)
				gridEntity.Desc.Sprite.replacedpngs[layerid] = png
			end,
			IsLoaded = function()
				return false
			end,
			GetFilename = function()
				return gridEntity.Desc.Sprite.anm2
			end,
			SetAnimation = function(_, anm)
				gridEntity.Desc.Sprite.animation = anm
			end,
			GetFrame = function()
				return gridEntity.Desc.Sprite.variation or 0
			end,
			IsPlaying = function(_, anm)
				if gridEntity.Desc.Sprite.animation == anm and gridEntity.Desc.Sprite.on == "playing" then
					return true
				end
				return false
			end,
			PlayOverlay = function(_, anm)
				gridEntity.Desc.Sprite.overlayanimation = anm
				gridEntity.Desc.Sprite.overlayon = "playing"
			end,
			SetOverlayAnimation = function(_, anm)
				gridEntity.Desc.Sprite.overlayanimation = anm
			end,
			SetOverlayFrame = function(_, anm, frame)
				gridEntity.Desc.Sprite.overlayanimation = anm
				gridEntity.Desc.Sprite.overlayon = "frame"
				gridEntity.Desc.Sprite.overlayvariation = frame
			end,
			GetOverlayFrame = function()
				return gridEntity.Desc.Sprite.overlayvariation or 0
			end,
			RemoveOverlay = function()
				gridEntity.Desc.Sprite.overlayanimation = nil
				gridEntity.Desc.Sprite.overlayon = nil
				gridEntity.Desc.Sprite.overlayvariation = nil
			end,
			IsOverlayPlaying = function()
				if gridEntity.Desc.Sprite.overlayanimation == anm and gridEntity.Desc.Sprite.overlayon == "playing" then
					return true
				end
				return false
			end,
			IsOverlayFinished = function()
				if gridEntity.Desc.Sprite.overlayanimation == anm and gridEntity.Desc.Sprite.overlayon == "frame" then
					return true
				end
				return false
			end,
			GetLayerCount = function() 
				return 1 
			end
			
		}, {
			__newindex = function(t, k, v)
				gridEntity.Desc.Sprite[string.lower(k)] = v
				gridEntity.Desc.Sprite["changed_"..string.lower(k)] = true
			end,
			__index = function(t, k)
				if gridEntity.Desc.RealSprite then
					return gridEntity.Desc.RealSprite[k]
				else
					local spriteattributes = {Offset=true, Scale=true, Rotation=true, Color=true, FlipX=true, FlipY=true, PlaybackSpeed=true}
					if spriteattributes[k] then
						return gridEntity.Desc.Sprite[string.lower(k)]
					else
						return rawget(t, k) or function() return end
					end
				end
			end
		})
		
		function gridEntity:GetSprite()
			return gridEntity.Sprite
		end
		
		local griddata = oworld.GetGridData(gridEntity)
		if skipOnSpawn and griddata.OnSpawn then
			griddata.OnSpawn(gridEntity)
		end
		local ctile = oworld.GetWorldTileByWorldPos(oworld.WorldPlayerPos)
		if math.abs(ctile.X-tile.X) <= oworld.TilesOnScreenRendered.X and math.abs(ctile.Y-tile.Y) <= oworld.TilesOnScreenRendered.Y then
			oworld.LoadGridEntity(gridEntity)
		end
		oworld.FakeGridEntities[tile.X][tile.Y] = gridEntity
		return gridEntity
	else
		return nil
	end
end

oworld:AddCallback(ModCallbacks.MC_POST_UPDATE, function() -- grid update
	if not oworld.InModeMenu then
		local ctile = oworld.GetWorldTileByWorldPos(oworld.WorldPlayerPos)
		local startx, endx = ctile.X-oworld.TilesOnScreenActive.X-1, ctile.X+oworld.TilesOnScreenActive.X+1
		local starty, endy = ctile.Y-oworld.TilesOnScreenActive.Y-1, ctile.Y+oworld.TilesOnScreenActive.Y+1
		for x=startx, endx do
			for y=starty, endy do
				local grid = oworld.GetGridEntity(Vector(x,y))
				if grid and grid.Desc.Initialized then
					local griddata = oworld.GetGridData(grid)
					if griddata.OnUpdate then
						griddata.OnUpdate(grid)
					end
				end
			end
		end
	end
end)

oworld:AddCallback(ModCallbacks.MC_POST_GENERATE_WORLD, function() -- grid renderers init
	local ctile = oworld.GetWorldTileByWorldPos(oworld.WorldPlayerPos)
	oworld.GridRenderer = Isaac.Spawn(EntityType.ENTITY_EFFECT, oworld.CheapEffectVariant, 0, oworld.WorldPlayerPos+Vector(0,-500), oworld.utils.VEC_ZERO, nil)
	oworld.GridRenderer:GetData().IsGridRenderer = true
end)

oworld:AddCallback(ModCallbacks.MC_POST_EFFECT_RENDER, function(_, eff)
	local data = eff:GetData()
	--eff.DepthOffset = oworld.WorldPosToPosition(oworld.WorldPlayerPos).Y-eff.Position.Y-200
	if data.IsGridRenderer then
		local ctile = oworld.GetWorldTileByWorldPos(oworld.WorldPlayerPos)
		for x=-oworld.TilesOnScreenRendered.X, oworld.TilesOnScreenRendered.X do
			for y=-oworld.TilesOnScreenRendered.Y, oworld.TilesOnScreenRendered.Y do
				local tile = ctile+Vector(x,y)
				local grid = oworld.GetGridEntity(tile)
				if grid then
					if grid.Visible then
						grid.Sprite:Render(Isaac.WorldToScreen(grid.Position), oworld.utils.VEC_ZERO, oworld.utils.VEC_ZERO)
					end
					grid.Sprite:Update()
				end
			end
		end
	end
end, oworld.CheapEffectVariant)

function oworld.GridInit(grid, notresetsprite)
	local griddata = oworld.GetGridData(grid)
	
	if not notresetsprite then
		grid.Sprite = Sprite()
	end
	
	local griddata = oworld.GetGridData(grid)
	if grid.Desc.Sprite.anm2 or griddata.anm2 then
		grid.Sprite:Load("gfx/grid/"..(grid.Desc.Sprite.anm2 or griddata.anm2)..".anm2", true)
		if grid.Desc.Sprite.on == "playing" or griddata.animated and grid.Desc.Sprite.on ~= "frame" then
			grid.Sprite:Play(grid.Desc.Sprite.animation or griddata.animation, true)
		else
			grid.Sprite:SetFrame(grid.Desc.Sprite.animation or griddata.animation, grid.Desc.Sprite.variation or griddata.variations and grid.rng:RandomInt(griddata.variations) or 0)
		end
		
		if grid.Desc.Sprite.overlayon or griddata.overlayanimation then
			if grid.Desc.Sprite.overlayon == "playing" or griddata.overlayanimated and grid.Desc.Sprite.overlayon ~= "frame" then
				grid.Sprite:Play(grid.Desc.Sprite.overlayanimation or griddata.overlayanimation, true)
			else
				grid.Sprite:SetFrame(grid.Desc.Sprite.overlayanimation or griddata.overlayanimation, grid.Desc.Sprite.overlayvariation or griddata.overlayvariations and grid.rng:RandomInt(griddata.overlayvariations) or 0)
			end
		end
	end
	
	if grid.Desc.Sprite.replacedpngs then
		for i,png in pairs(grid.Desc.Sprite.replacedpngs) do
			grid.Sprite:ReplaceSpritesheet(i, png)
		end
		grid.Sprite:LoadGraphics()
	elseif griddata.replacedpngs then
		for i,png in pairs(griddata.replacedpngs) do
			grid.Sprite:ReplaceSpritesheet(i, png)
		end
		grid.Sprite:LoadGraphics()
	end
	
	local spriteattributes = {"Offset", "Scale", "Rotation", "Color", "FlipX", "FlipY", "PlaybackSpeed"}
	for _,attr in ipairs(spriteattributes) do
		if grid.Desc.Sprite["changed_"..string.lower(attr)] then
			grid.Sprite[attr] = grid.Desc.Sprite[string.lower(attr)]
		elseif griddata[string.lower(attr)] then
			grid.Sprite[attr] = griddata[string.lower(attr)]
		end
	end
	
	grid.CollisionClass = grid.CollisionClass or griddata.collisionclass
	if grid.Destructable == nil then
		grid.Destructable = griddata.destructable
	end
	if grid.Crushable == nil then
		grid.Crushable = griddata.crushable
	end
	if grid.NoCollision == nil then
		grid.NoCollision = griddata.nocollision
	end
	
	if griddata.OnInit then
		local bool = griddata.OnInit(grid)
	end
	
	grid.Desc.Initialized = true
end

function oworld.GetGridEntity(tile)
	if not oworld.FakeGridEntities or not oworld.FakeGridEntities[math.floor(tile.X)] then return nil end
	if tile.X > 0 and tile.Y > 0 and tile.X <= oworld.WorldWidth and tile.Y <= oworld.WorldHeight then
		return oworld.FakeGridEntities[math.floor(tile.X)][math.floor(tile.Y)]
	end
end

function oworld.GetGridData(grid)
	if not oworld.FakeGridTypes[grid.Desc.Type] then
		Isaac.ConsoleOutput("[ERROR] Tried to call 'GetGridData' on a grid with unknown Type")
		return nil
	end
	if oworld.FakeGridTypes[grid.Desc.Type][grid.Desc.Variant] then
		return oworld.FakeGridTypes[grid.Desc.Type][grid.Desc.Variant]
	else
		return oworld.FakeGridTypes[grid.Desc.Type][0]
	end
end

function oworld.DamageGridEntity(grid, damage)
	local griddata = oworld.GetGridData(grid)
	if griddata.OnDamage then
		griddata.OnDamage(grid, damage)
	end
end

function oworld.DestroyGridEntity(grid)
	if grid and grid.Destructable then
		local griddata = oworld.GetGridData(grid)
		if griddata.OnDestroy then
			griddata.OnDestroy(grid)
		end
		local tile = grid:GetTile()
		oworld.FakeGridEntities[tile.X][tile.Y] = nil
	end
end

function oworld.RemoveGridEntity(grid)
	if grid then
		local griddata = oworld.GetGridData(grid)
		if griddata.OnRemove then
			griddata.OnRemove(grid)
		end
		local tile = grid:GetTile()
		oworld.FakeGridEntities[tile.X][tile.Y] = nil
	end
end

-- checks if a EntityGridCollisionClass should collide with a GridCollisionClass
function oworld.CanCollideWithGrid(ecolclass, gcolclass, isplayer)
	if gcolclass == GridCollisionClass.COLLISION_PIT then
		return ecolclass == EntityGridCollisionClass.GRIDCOLL_GROUND
		
	elseif gcolclass == GridCollisionClass.COLLISION_SOLID or gcolclass == GridCollisionClass.COLLISION_OBJECT then
		return ecolclass and ecolclass >= EntityGridCollisionClass.GRIDCOLL_BULLET
	
	elseif gcolclass == GridCollisionClass.COLLISION_WALL then
		return ecolclass ~= EntityGridCollisionClass.GRIDCOLL_NONE
	
	elseif gcolclass == GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER then
		return ecolclass ~= EntityGridCollisionClass.GRIDCOLL_NONE and not isplayer
	end
	
	return false
end

Isaac.DebugString("Loaded Open World gridentities")