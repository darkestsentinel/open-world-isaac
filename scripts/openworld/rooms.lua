local oworld = require "scripts.openworld"

local game = Game()

-- rendering the floor
if not oworld.Floors then
	oworld.Floors = {}
	oworld.TotalFloorIds = 0
	oworld.FloorRenderedOnFrame = -1
	oworld.WorldFloorSprite = "gfx/backdrop/openworld_black_backdrop.png"
	oworld.WorldFloorEnts = {}
end

oworld:AddCallback(ModCallbacks.MC_PRE_GENERATE_WORLD, function()
	oworld.WorldFloorEnt = Isaac.Spawn(EntityType.ENTITY_EFFECT, oworld.CheapEffectVariant, 0, oworld.utils.VEC_ZERO, oworld.utils.VEC_ZERO, nil)
	oworld.WorldFloorEnt.Visible = false
	local sprite = oworld.WorldFloorEnt:GetSprite()
	sprite:Load("gfx/backdrop/openworld_backdrop.anm2", true)
	sprite:SetFrame("Floor", 0)
	sprite:ReplaceSpritesheet(0, oworld.WorldFloorSprite)
	sprite:LoadGraphics()
end)

function oworld.SetWorldFloorSprite(floorsprite)
	oworld.WorldFloorSprite = floorsprite
	oworld.WorldFloorEnt:GetSprite():ReplaceSpritesheet(0, oworld.WorldFloorSprite)
	oworld.WorldFloorEnt:GetSprite():LoadGraphics()
	oworld.WorldFloorEnt:GetData().OWorldStaticEntity = true
	oworld.WorldFloorEnt.Visible = false
	oworld.WorldFloorEnt:AddEntityFlags(EntityFlag.FLAG_NO_SPRITE_UPDATE)
end

function oworld.GetWorldFloorSprite()
	return oworld.WorldFloorSprite
end

function oworld.RenderFloors()
	if not oworld.RealRoom then return end
	if oworld.WorldFloorEnt and oworld.WorldFloorEnt:Exists() then
		oworld.WorldFloorEnt.Visible = true
		oworld.WorldFloorEnt.Position = oworld.utils.VEC_ZERO
		oworld.WorldFloorEnt:AddEntityFlags(EntityFlag.FLAG_RENDER_FLOOR | EntityFlag.FLAG_RENDER_WALL | EntityFlag.FLAG_NO_REMOVE_ON_TEX_RENDER)
	end
	
	for _,Floor in pairs(oworld.Floors) do
		oworld.RenderFloor(Floor)
	end
end

function oworld.RenderFloor(Floor)
	if Floor.Visible
	and math.abs(Floor.WorldPos.X-oworld.WorldPlayerPos.X+Floor.Size.X*20) <= oworld.TilesOnScreenRendered.X*40+Floor.Size.X*20
	and math.abs(Floor.WorldPos.Y-oworld.WorldPlayerPos.Y+Floor.Size.Y*20) <= oworld.TilesOnScreenRendered.Y*40+Floor.Size.Y*20 then
		if not oworld.WorldFloorEnts[Floor.Id] then
			oworld.WorldFloorEnts[Floor.Id] = Isaac.Spawn(EntityType.ENTITY_EFFECT, oworld.CheapEffectVariant, 0, oworld.RealRoom:GetCenterPos(), oworld.utils.VEC_ZERO, nil)
			oworld.WorldFloorEnts[Floor.Id]:GetSprite():Load("gfx/grid/grid_floor.anm2", true)
			oworld.WorldFloorEnts[Floor.Id]:GetSprite():SetFrame("floor", 0)
			oworld.WorldFloorEnts[Floor.Id]:GetData().OWorldStaticEntity = true
			oworld.WorldFloorEnts[Floor.Id]:AddEntityFlags(EntityFlag.FLAG_NO_SPRITE_UPDATE)
		end
		oworld.WorldFloorEnts[Floor.Id].Position = oworld.WorldPosToPosition(Floor.WorldPos)+Vector(-20,-1020)
		oworld.WorldFloorEnts[Floor.Id].Visible = true
		local sprite = oworld.WorldFloorEnts[Floor.Id]:GetSprite()
		sprite.Scale = Floor.Size
		sprite.Offset = Isaac.WorldToScreenDistance(Vector(0,1000))
		if Floor.Png then
			sprite:ReplaceSpritesheet(0, Floor.Png)
			sprite:LoadGraphics()
		end
		oworld.WorldFloorEnts[Floor.Id]:AddEntityFlags(EntityFlag.FLAG_RENDER_FLOOR | EntityFlag.FLAG_NO_REMOVE_ON_TEX_RENDER)
	end
end

oworld.OldAddCallback(oworld, ModCallbacks.MC_POST_EFFECT_UPDATE, function()
	if oworld.FloorRenderedOnFrame ~= game:GetFrameCount() and game:GetFrameCount()%4 == 1 then
		oworld.FloorRenderedOnFrame = game:GetFrameCount()
		oworld.RenderFloors()
	end
end, -1)

oworld:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	if oworld.WorldFloorEnt and oworld.WorldFloorEnt:Exists() then
		oworld.WorldFloorEnt.Visible = false
	end
	for _,floorent in pairs(oworld.WorldFloorEnts) do
		floorent.Visible = false
	end
end)

--[[
A roomshape can be put together by pasting square rooms together with a table
In that table you put in {Tile = Vector WorldTile, Size = Vector Size} for a square room
 - WorldTile: the top-left corner of that square room
 - Size: the size in tiles of that square room

Example of an L roomshape: {
	{Tile = Vector(5,5), Size = Vector(13, 7)},
	{Tile = Vector(5,12), Size = Vector(26, 7)}
}
]]

if not oworld.Rooms then
	oworld.Rooms = {}
	oworld.TileInRoom = {}
	oworld.TotalRoomIds = 0
	oworld.Overlays = {}
	oworld.TotalOverlayIds = 0
end

function oworld.GetRooms()
	return oworld.Rooms
end

-- room is optional, it'll check for every room if no room is specified
function oworld.IsTileInRoom(tile, room, includewalls)
	if room then
		if not room.TileInRoom[tile.X] then room.TileInRoom[tile.X] = {} end
		return room.TileInRoom[tile.X][tile.Y] == "room" or includewalls and room.TileInRoom[tile.X][tile.Y] == "wall"
	end
	return oworld.TileInRoom[tile.X][tile.Y]  == "room" or includewalls and oworld.TileInRoom[tile.X][tile.Y] == "wall"
end

function oworld.AddRoom(roomshape, backdroppng, variations, floorsprite, roomshapeid, Type)
	local rng = oworld.GetRNG()
	local seed = rng:GetSeed()
	local level = game:GetLevel()
	local room = {
		Shape = roomshape, 
		BackdropPng = oworld.SPECIAL_BACKDROP[Type] or backdroppng, 
		Variations = variations,
		RoomShapeId = roomshapeid,
		Type = Type or RoomType.ROOM_DEFAULT,
		Variant = 0,
		Subtype = 0,
		Seed = seed,
		StageId = level:GetAbsoluteStage(),
		Difficulty = 1,
		Name = "",
		Weight = 1,
		DisplayFlags = 5,
		VisitedCount = 0,
		OverlayIds = {},
		rng = rng
	}
	local tileinroom = {} -- get all tiles inside room
	variations = variations or {1}
	
	oworld.TotalRoomIds = oworld.TotalRoomIds+1
	room.RoomId = oworld.TotalRoomIds
	
	room.FloorIds = {}
	room.TopLeftTile = Vector(oworld.WorldWidth, oworld.WorldHeight)
	room.BottomRightTile = oworld.utils.VEC_ZERO
	for _,square in ipairs(roomshape) do
		oworld.TotalFloorIds = oworld.TotalFloorIds+1
		oworld.Floors[oworld.TotalFloorIds] = {Id=oworld.TotalFloorIds, WorldPos=oworld.GetWorldPosByWorldTile(square.Tile)+Vector(-40,-40), Size=square.Size+Vector(2,2), Png=oworld.SPECIAL_FLOOR[Type] or floorsprite, Visible = true}
		table.insert(room.FloorIds, oworld.TotalFloorIds)
		
		if room.TopLeftTile.X > square.Tile.X then
			room.TopLeftTile = Vector(square.Tile.X, room.TopLeftTile.Y)
		end
		if room.TopLeftTile.Y > square.Tile.Y then
			room.TopLeftTile = Vector(room.TopLeftTile.X, square.Tile.Y)
		end
		if room.BottomRightTile.X < square.Tile.X+square.Size.X then
			room.BottomRightTile = Vector(square.Tile.X+square.Size.X, room.BottomRightTile.Y)
		end
		if room.BottomRightTile.Y < square.Tile.Y+square.Size.Y then
			room.BottomRightTile = Vector(room.BottomRightTile.X, square.Tile.Y+square.Size.Y)
		end
		
		for x=square.Tile.X, square.Tile.X+square.Size.X-1 do
			tileinroom[x] = tileinroom[x] or {}
			for y=square.Tile.Y, square.Tile.Y+square.Size.Y-1 do
				tileinroom[x][y] = "room"
				oworld.TileInRoom[x][y] = "room"
			end
		end
	end
	
	local walls = {} -- spawn walls at the outer tiles of that room
	for _,square in ipairs(roomshape) do
		for x=square.Tile.X-1, square.Tile.X+square.Size.X do
			tileinroom[x] = tileinroom[x] or {}
			for y=square.Tile.Y-1, square.Tile.Y+square.Size.Y do
				if not tileinroom[x][y] then
					tileinroom[x][y] = "wall"
					oworld.TileInRoom[x][y] = "wall"
					local wall = oworld.GridSpawn(GridEntityType.GRID_WALL, 0, Vector(x,y), true)
					table.insert(walls, wall)
				end
			end
		end
	end
	room.TileInRoom = tileinroom
	
	-- setup the walls
	local directions = {Vector(1,0), Vector(0,1), Vector(-1,0), Vector(0,-1)}
	for _,wall in ipairs(walls) do
		if not wall.Desc.UpdatedWall then
			local tile = wall:GetTile()
			local x,y = tile.X,tile.Y
			
			tileinroom[x+1] = tileinroom[x+1] or {}
			tileinroom[x-1] = tileinroom[x-1] or {}
			local r,d,l,u = tileinroom[x+1][y], tileinroom[x][y+1], tileinroom[x-1][y], tileinroom[x][y-1]
			
			if not u and not l or not r and not u or not r and not d or not d and not l then
				local directions = {Vector(1,0), Vector(0,1), Vector(-1,0), Vector(0,-1)}
				for diri,dir in ipairs(directions) do
					if diri==1 and not l or diri==2 and not u or diri==3 and not r or diri==4 and not d then
						local checktile = tile
						local i = 0
						while true do
							i = i+1
							checktile = checktile+dir
							tileinroom[checktile.X+dir.X*math.max(i,2)] = tileinroom[checktile.X+dir.X*math.max(i,2)] or {}
							local grid = oworld.GetGridEntity(checktile)
							if grid and tileinroom[checktile.X+dir.X*math.max(i,2)][checktile.Y+dir.Y*math.max(i,2)] 
							and tileinroom[checktile.X+dir.X*math.max(i,2)][checktile.Y+dir.Y*math.max(i,2)] ~= "room" then
								local frame
								if diri==1 or diri==3 then
									if i<=4 then 
										frame = 1+i
										grid.Desc.UpdatedWall = true
									else 
										frame = 6+((i-2)%3) 
									end
									
									if tileinroom[grid:GetTile().X][grid:GetTile().Y-1] == "room" then
										grid.Sprite.FlipY = true
									end
									if diri==3 then 
										grid.Sprite.FlipX = true 
									end
								else
									if i==1 then 
										frame = 9
										grid.Desc.UpdatedWall = true
									else 
										frame = 10+((i-2)%3)
									end
									
									if tileinroom[grid:GetTile().X-1][grid:GetTile().Y] == "room" then
										grid.Sprite.FlipX = true
									end
									if diri==4 then 
										grid.Sprite.FlipY = true 
									end
								end
								grid.Sprite:SetFrame("wall"..variations[grid.rng:RandomInt(#variations) + 1], frame)
								if room.BackdropPng then
									grid.Sprite:ReplaceSpritesheet(0, room.BackdropPng)
									grid.Sprite:LoadGraphics()
								end
							else
								break
							end
						end
					end
				end
				if not r and not u then wall.Sprite.Rotation = 90
				elseif not r and not d then wall.Sprite.Rotation = 180
				elseif not d and not l then wall.Sprite.Rotation = 270 end
				wall.Sprite:SetFrame("wall"..variations[wall.rng:RandomInt(#variations) + 1], 0)
				if room.BackdropPng then
					wall.Sprite:ReplaceSpritesheet(0, room.BackdropPng)
					wall.Sprite:LoadGraphics()
				end
				
			elseif r=="room" and d=="room" or d=="room" and l=="room" or l=="room" and u=="room" or r=="room" and u=="room" then 
				local directions = {Vector(1,0), Vector(0,1), Vector(-1,0), Vector(0,-1)}
				for diri,dir in ipairs(directions) do
					if diri==1 and l=="room" or diri==2 and u=="room" or diri==3 and r=="room" or diri==4 and d=="room" then
						local checktile = tile
						local i = 0
						while true do
							i = i+1
							checktile = checktile+dir
							tileinroom[checktile.X+dir.X*math.max(i,2)] = tileinroom[checktile.X+dir.X*math.max(i,2)] or {}
							local grid = oworld.GetGridEntity(checktile)
							if i~=1 and grid and tileinroom[checktile.X+dir.X*math.max(i,2)][checktile.Y+dir.Y*math.max(i,2)] 
							and tileinroom[checktile.X+dir.X*math.max(i,2)][checktile.Y+dir.Y*math.max(i,2)] ~= "room" then
								local frame
								if diri==1 or diri==3 then
									frame = 6+((i-1)%3)
									if tileinroom[grid:GetTile().X][grid:GetTile().Y-1] == "room" then
										grid.Sprite.FlipY = true
									end
									if diri==3 then 
										grid.Sprite.FlipX = true 
									end
								else
									frame = 10+((i-1)%3)
									if tileinroom[grid:GetTile().X-1][grid:GetTile().Y] == "room" then
										grid.Sprite.FlipX = true
									end
									if diri==4 then 
										grid.Sprite.FlipY = true 
									end
								end
								grid.Sprite:SetFrame("wall"..variations[grid.rng:RandomInt(#variations) + 1], frame)
								if room.BackdropPng then
									grid.Sprite:ReplaceSpritesheet(0, room.BackdropPng)
									grid.Sprite:LoadGraphics()
								end
								grid.Desc.UpdatedWall = true
							elseif i~=1 then
								break
							end
						end
					end
				end
				if r=="room" and u=="room" then wall.Sprite.Rotation = 270
				elseif l=="room" and u=="room" then wall.Sprite.Rotation = 180
				elseif d=="room" and l=="room" then wall.Sprite.Rotation = 90 end
				wall.Sprite:SetFrame("wall"..variations[wall.rng:RandomInt(#variations) + 1], 1)
				if room.BackdropPng then
					wall.Sprite:ReplaceSpritesheet(0, room.BackdropPng)
					wall.Sprite:LoadGraphics()
				end
			end
		end
	end
	
	room.Hide = function(room)
		for y = room.TopLeftTile.Y - 1, room.BottomRightTile.Y + 1 do -- hiding grid ents/unloaded ents
			for x = room.TopLeftTile.X - 1, room.BottomRightTile.X + 1 do
				
				if room.TileInRoom[x] and room.TileInRoom[x][y] then
					local worldtile = Vector(x, y)
					
					local grid = oworld.GetGridEntity(worldtile)
					if grid then
						grid.Visible = false
					end
					
					local entityData = oworld.GetEntityData(x, y)
					for _,edata in ipairs(entityData) do
						edata.visible = false
					end
				end
			end
		end
		
		for _,floorid in ipairs(room.FloorIds) do
			oworld.Floors[floorid].Visible = false
		end
		
		for _,overlayid in ipairs(room.OverlayIds) do
			local overlay = oworld.Overlays[overlayid]
			if overlay.Loaded then
				overlay:Unload()
			end
			
			overlay.Visible = false
		end
		
		local centerPos = room:GetCenterPos()
		if oworld.WorldPosToPosition(oworld.WorldPlayerPos):DistanceSquared(centerPos) <= oworld.GetPositionByWorldTile(room.TopLeftTile):DistanceSquared(centerPos)*16 + oworld.TilesOnScreenActive.X^2 + oworld.TilesOnScreenActive.Y^2 then
			local entities = oworld.OldGetRoomEntities()
			
			for _,ent in ipairs(entities) do
				local worldtile = oworld.GetWorldTileByPosition(ent.Position)
				
				if room.TileInRoom[worldtile.X] and room.TileInRoom[worldtile.X][worldtile.Y] then
					ent.Visible = false
				end
			end
		end
		
		room.Hidden = true
	end
	
	room.Reveal = function(room)
		for y = room.TopLeftTile.Y - 1, room.BottomRightTile.Y + 1 do -- hiding grid ents/unloaded ents
			for x = room.TopLeftTile.X - 1, room.BottomRightTile.X + 1 do
				
				if room.TileInRoom[x] and room.TileInRoom[x][y] then
					local worldtile = Vector(x, y)
					
					local grid = oworld.GetGridEntity(worldtile)
					if grid then
						grid.Visible = true
					end
					
					local entityData = oworld.GetEntityData(x, y)
					for _,edata in ipairs(entityData) do
						edata.visible = true
					end
				end
			end
		end
		
		for _,floorid in ipairs(room.FloorIds) do
			oworld.Floors[floorid].Visible = true
			oworld.RenderFloor(oworld.Floors[floorid])
		end
		
		for _,overlayid in ipairs(room.OverlayIds) do
			local overlay = oworld.Overlays[overlayid]
			
			local cpos = oworld.WorldPlayerPos
			if math.abs(overlay.WorldPos.X-cpos.X+overlay.Size.X/2) < oworld.TilesOnScreenRendered.X*40+40+overlay.Size.X
			and math.abs(overlay.WorldPos.Y-cpos.Y+overlay.Size.Y/2) < oworld.TilesOnScreenRendered.Y*40+40+overlay.Size.Y then
				overlay:Load()
			end
			
			overlay.Visible = true
		end
		
		local centerPos = room:GetCenterPos()
		if oworld.WorldPosToPosition(oworld.WorldPlayerPos):DistanceSquared(centerPos) <= oworld.GetPositionByWorldTile(room.TopLeftTile):DistanceSquared(centerPos)*16 + oworld.TilesOnScreenActive.X^2 + oworld.TilesOnScreenActive.Y^2 then
			local entities = oworld.OldGetRoomEntities()
			
			for _,ent in ipairs(entities) do
				local worldtile = oworld.GetWorldTileByPosition(ent.Position)
				
				if room.TileInRoom[worldtile.X] and room.TileInRoom[worldtile.X][worldtile.Y] then
					ent.Visible = true
				end
			end
		end
		
		if (room:GetType() == RoomType.ROOM_SECRET or room:GetType() == RoomType.ROOM_SUPERSECRET) then
			oworld.AddRoomToMap(room)
		end
		
		room.Hidden = false
	end
	
	room.IsHidden = function(room)
		return room.Hidden
	end
	
	room.GetBackdropType = function(room)
		local backdropname = ""
		for k,v in pairs(oworld.BACKDROP) do
			if v == room.BackdropPng then
				backdropname = k
				break
			end
		end
		if backdropname ~= "" then
			for i,v in ipairs(oworld.BackdropTypeToName) do
				if v == backdropname then
					return i
				end
			end
		end
		return -1
	end
	
	room.GetBossID = function()
		return 0
	end
	
	room.Update = function() return oworld.RealRoom:Update() end
	room.Render = function() return oworld.RealRoom:Render() end
	room.IsInitialized = function() return true end
	
	room.GetGridCollision = function(room, index)
		local grid = room:GetGridEntity(index) or {}
		return grid.CollisionClass or GridCollisionClass.GRIDCOLL_NONE
	end
	room.GetGridCollisionAtPos = function(room, pos)
		local grid = room:GetGridEntity(room:GetGridIndex(pos)) or {}
		return grid.CollisionClass or GridCollisionClass.GRIDCOLL_NONE
	end
	
	room.GetType = function(room) return room.Type end
	
	room.GetDecorationSeed = function(room) return room.Seed end
	room.GetSpawnSeed = function(room) return room.Seed end
	room.GetAwardSeed = function(room) return room.Seed end
	
	room.GetRoomShape = function(room) return room.RoomShapeId end
	
	room.GetRoomConfigStage = function(room) return nil end
	
	room.GetGridPath = function(room, index) 
		local grid = room:GetGridEntity(index)
		if grid then
			if grid.GridPath then
				return grid.GridPath
				
			elseif grid.Desc.Type == GridEntityType.GRID_FIREPLACE then
				return 950
				
			elseif grid.CollisionClass ~= GridCollisionClass.COLLISION_NONE then
				return 1000
			end
		end
		
		return 0
	end
	room.GetGridPathFromPos = function(room, pos) 
		return room:GetGridPath(room:GetGridIndex(pos)) 
	end
	room.SetGridPath = function(room, gridPath) 
		local grid = room:GetGridEntity(index)
		
		grid.GridPath = gridPath
	end
	
	room.DamageGrid = function(room, index, damage)
		local grid = room:GetGridEntity(index)
		oworld.DamageGridEntity(grid, damage)
	end
	room.DestroyGrid = function(room, index, immediate)
		local grid = room:GetGridEntity(index)
		oworld.DestroyGridEntity(grid)
	end
	room.RemoveGrid = function(room, index)
		local grid = room:GetGridEntity(index)
		oworld.RemoveGridEntity(grid)
	end
	
	room.CheckLine = function(room, pos1, pos2, mode, gridpaththreshold, ignorewalls, ignorecrushable)
		return oworld.CheckLine(pos1, pos2, mode, gridpaththreshold, ignorewalls, ignorecrushable)
	end
	
	room.RoomGridOffset = oworld.utils.VEC_ZERO
	if room.RoomShapeId == RoomShape.ROOMSHAPE_IH or room.RoomShapeId == RoomShape.ROOMSHAPE_IIH then
		room.RoomGridOffset = Vector(0,2)
	elseif room.RoomShapeId == RoomShape.ROOMSHAPE_IV or room.RoomShapeId == RoomShape.ROOMSHAPE_IIV then
		room.RoomGridOffset = Vector(4,0)
	end
	
	room.GetGridWidth = function(room)
		return (room.BottomRightTile.X+room.RoomGridOffset.X)-(room.TopLeftTile.X-room.RoomGridOffset.X)+3
	end
	room.GetGridHeight = function(room)
		return (room.BottomRightTile.Y+room.RoomGridOffset.Y)-(room.TopLeftTile.Y-room.RoomGridOffset.Y)+3
	end
	room.GetGridSize = function(room)
		return room:GetGridWidth()*room:GetGridHeight()
	end
	
	room.GetGridIndexByWorldTile = function(room, tile)
		return (tile.X-(room.TopLeftTile.X-room.RoomGridOffset.X)+1) + (tile.Y-(room.TopLeftTile.Y-room.RoomGridOffset.Y)+1)*(room:GetGridWidth()-1)
	end
	room.GetGridIndex = function(room, pos)
		return room:GetGridIndexByWorldTile(oworld.GetWorldTileByPosition(pos))
	end
	room.GetGridPosition = function(room, index)
		return oworld.GetPositionByWorldTile(room:GetWorldTile(index))
	end
	room.GetWorldTile = function(room, index)
		local width = room:GetGridWidth()
		return Vector(index%(width-1), math.floor(index/(width-1)))+(room.TopLeftTile-room.RoomGridOffset)-Vector(1,1)
	end
	
	room.GetGridEntity = function(room, index)
		return oworld.GetGridEntity(room:GetWorldTile(index))
	end
	room.GetGridEntityFromPos = function(room, pos)
		return oworld.GetGridEntity(oworld.GetWorldTileByPosition(pos))
	end
	
	room.Clear = false
	room.IsClear = function(room)
		return room.Clear
	end
	room.ClearCount = 0
	room.SetClear = function(room, clear)
		
		if clear then
			for _,callback in ipairs(oworld.PreClearRoomCallbacks) do
				local ret = callback.Fn(nil, room)
				
				if ret == true then
					return
				end
			end
		end
		
		if clear and not room:IsClear() then
			room.ClearCount = room.ClearCount + 1
			
			if room.ClearCount == 1 and room.FrameCount > 1 and room:GetType() == RoomType.ROOM_DEFAULT then
				room:SpawnClearAward()
			end
		end
		if clear then
			for slot=DoorSlot.LEFT0, DoorSlot.DOWN1 do
				local door = room:GetDoor(slot)
				if door and door.Open then
					if room:GetType() == RoomType.ROOM_MINIBOSS then
						door:TryRemoveBar()
					end
					door:Open()
				end
			end
			
			for _,callback in ipairs(oworld.PostClearRoomCallbacks) do
				callback.Fn(nil, room)
			end
			
		elseif not clear then
			for slot=DoorSlot.LEFT0, DoorSlot.DOWN1 do
				local door = room:GetDoor(slot)
				if door and door.Close then
					if room:GetType() == RoomType.ROOM_MINIBOSS then
						door:Bar()
					else
						door:Close()
					end
				end
			end
		end
		room.Clear = clear
	end
	
	room.GetClampedGridIndex = function(room, pos)
		return room:GetGridIndex(room:GetClampedPosition(pos, 0))
	end
	
	room.GetClampedPosition = function(room, pos, margin)
		local topleftpos, bottomrightpos = room:GetTopLeftPos(), room:GetBottomRightPos()
		return Vector(math.max(math.min(pos.X, bottomrightpos.X+margin), topleftpos.X-margin), math.max(math.min(pos.Y, bottomrightpos.Y+margin), topleftpos.Y-margin))
	end
	
	room.IsPositionInRoom = function(room, pos, margin)
		if margin ~= 0 then
			local centertile = oworld.GetWorldTileByPosition(pos)
			local maxtilesaway = math.floor((margin+20)/40)
			local closestvalidtile, mindistsquared
			
			for x=-maxtilesaway, maxtilesaway do
				for y=-maxtilesaway, maxtilesaway do
					local tile = Vector(x+centertile.X, y+centertile.Y)
					if oworld.IsTileInRoom(tile, room) and margin < 0 or not oworld.IsTileInRoom(tile, room) and margin > 0 then
						local tilepos = oworld.GetPositionByWorldTile(tile)
						local distsquared = (pos-tilepos):LengthSquared()
						
						if not closestvalidtile or distsquared < mindistsquared then
							closestvalidtile = tile
							mindistsquared = distsquared
						end
					end
				end
			end
			
			if closestvalidtile then
				return mindistsquared <= margin^2
			else
				return false
			end
		else
			return oworld.IsTileInRoom(oworld.GetWorldTileByPosition(pos), room)
		end
	end
	
	room.GetTopLeftPos = function(room)
		return oworld.GetPositionByWorldTile(room.TopLeftTile)+Vector(-20,-20)
	end
	room.GetBottomRightPos = function(room)
		return oworld.GetPositionByWorldTile(room.BottomRightTile)+Vector(20,20)
	end
	room.GetCenterPos = function(room)
		if room.CenterWorldTile then
			return oworld.GetPositionByWorldTile(room.CenterWorldTile)
		else
			return (room:GetTopLeftPos()+room:GetBottomRightPos())/2
		end
	end
	
	room.GetRandomPosition = function(room, margin)
		local topleftpos, bottomrightpos = room:GetTopLeftPos(), room:GetBottomRightPos()
		local rPos
		while true do 
			rPos = Vector(math.random(math.floor(topleftpos.X), math.floor(bottomrightpos.X)), math.random(math.floor(topleftpos.Y), math.floor(bottomrightpos.Y)))
			
			if room:IsPositionInRoom(rPos, margin) then
				break
			end
		end
		
		return rPos
	end
	
	room.FrameCount = 0
	room.GetFrameCount = function(room)
		return room.FrameCount
	end
	
	room.FirstVisit = true
	room.IsFirstVisit = function(room)
		return room.FirstVisit
	end
	
	room.GetRenderSurfaceTopLeft = function(room)
		return --[[room:GetTopLeftPos()+Vector(-80,-80)]] oworld.RealRoom:GetRenderSurfaceTopLeft()
	end
	room.GetRenderScrollOffset = function(room) -- wip
		return oworld.RealRoom:GetRenderScrollOffset()
	end
	
	room.WorldToScreenPosition = function(room, vec)
		return Isaac.WorldToScreen(vec)
	end
	
	room.SpawnGridEntity = function(room, gridindex, gridtype, variant)
		oworld.GridSpawn(gridtype, variant, room:GetWorldTile(gridindex), true)
	end
	
	room.GetAliveBossesCount = function()
		local ents = Isaac.GetRoomEntities()
		local numBosses = 0
		for _,ent in ipairs(ents) do
			if ent:IsBoss() then
				numBosses = numBosses + 1
			end
		end
		return numBosses
	end
	
	room.GetAliveEnemiesCount = function()
		local ents = Isaac.GetRoomEntities()
		local numEnemies = 0
		for _,ent in ipairs(ents) do
			if ent:IsEnemy() then
				numEnemies = numEnemies + 1
			end
		end
		return numEnemies
	end
	
	-- Logic taken from https://gist.github.com/bladecoding/e2822580734366633347ac98dfe4a8d9
	-- Thanks Blade!
	room.SpawnClearAward = function(room)
		local awardSeed = room:GetAwardSeed()
		local player = game:GetPlayer(0)
		local difficulty = game.Difficulty

		local rng = RNG()
		rng:SetSeed(awardSeed, 35) --5, 9, 7
		
		local worldpos = oworld.PositionToWorldPos(room:GetCenterPos())
		local nearCenter = oworld.GetPositionByWorldTile(oworld.FindClosestFreeWorldTile(worldpos, EntityGridCollisionClass.GRIDCOLL_GROUND, false, 5))
		
		for _,fn in ipairs(oworld.PreSpawnCleanAward) do
			local ret = fn(rng, rng, nearCenter)
			
			if ret == true then
				return
			end
		end

		local pickupPercent = rng:RandomFloat()
		if (player:HasCollectible(CollectibleType.COLLECTIBLE_LUCKY_FOOT)) then
			pickupPercent = (pickupPercent * 0.9) + 0.1
		end

		local luck = math.max(math.min(player.Luck, 10), 0) --Clamp to 0-10

		--Max luck increases the pickupPercent range from 0-1 to 0-2
		--That means the more luck you have, the more likely you are to get chests.
		pickupPercent = rng:RandomFloat() * luck * 0.1 + pickupPercent

		if (player:HasTrinket(TrinketType.TRINKET_LUCKY_TOE)) then
			if (player:HasCollectible(CollectibleType.COLLECTIBLE_LUCKY_FOOT) and luck > 0) then
				pickupPercent = (pickupPercent * 0.98) + 0.02
			else
				pickupPercent = (pickupPercent * 0.9) + 0.1
			end
		end

		local pickupAward = CollectibleType.COLLECTIBLE_NULL
		local pickupCount = 1

		if (pickupPercent > 0.22) then
			if (pickupPercent < 0.3) then
				if (rng:RandomInt(3) == 0) then
					pickupAward = PickupVariant.PICKUP_TAROTCARD
				elseif (rng:RandomInt(2) == 0) then
					pickupAward = PickupVariant.PICKUP_TRINKET
				else
					pickupAward = PickupVariant.PICKUP_PILL
				end
			elseif (pickupPercent < 0.45) then
				pickupAward = PickupVariant.PICKUP_COIN
			elseif (pickupPercent < 0.5 and player:HasTrinket(TrinketType.TRINKET_RIB_OF_GREED)) then
				pickupAward = PickupVariant.PICKUP_COIN
			elseif (pickupPercent < 0.6 and (not player:HasTrinket(TrinketType.TRINKET_DAEMONS_TAIL) or rng:RandomInt(5) == 0)) then
				pickupAward = PickupVariant.PICKUP_HEART
			elseif (pickupPercent < 0.8) then
				pickupAward = PickupVariant.PICKUP_KEY
			elseif (pickupPercent < 0.95) then
				pickupAward = PickupVariant.PICKUP_BOMB
			else
				pickupAward = PickupVariant.PICKUP_CHEST
			end
			
			if (rng:RandomInt(20) == 0 or (rng:RandomInt(15) == 0 and player:HasTrinket(TrinketType.TRINKET_WATCH_BATTERY))) then
				pickupAward = PickupVariant.PICKUP_LIL_BATTERY
			end
			
			if (rng:RandomInt(50) == 0) then
				pickupAward = PickupVariant.PICKUP_GRAB_BAG
			end
			
			if (player:HasTrinket(TrinketType.TRINKET_ACE_SPADES) and rng:RandomInt(10) == 0) then
				pickupAward = PickupVariant.PICKUP_TAROTCARD
			elseif (player:HasTrinket(TrinketType.TRINKET_SAFETY_CAP) and rng:RandomInt(10) == 0) then
				pickupAward = PickupVariant.PICKUP_PILL
			elseif (player:HasTrinket(TrinketType.TRINKET_MATCH_STICK) and rng:RandomInt(10) == 0) then
				pickupAward = PickupVariant.PICKUP_BOMB
			elseif (player:HasTrinket(TrinketType.TRINKET_CHILDS_HEART) and rng:RandomInt(10) == 0 and (not player:HasTrinket(TrinketType.TRINKET_DAEMONS_TAIL) or rng:RandomInt(5) == 0)) then
				pickupAward = PickupVariant.PICKUP_HEART
			elseif (player:HasTrinket(TrinketType.TRINKET_RUSTED_KEY) and rng:RandomInt(10) == 0) then
				pickupAward = PickupVariant.PICKUP_KEY
			end
			
				
			if (player:HasCollectible(CollectibleType.COLLECTIBLE_SMELTER) and rng:RandomInt(50) == 0) then
				pickupAward = PickupVariant.PICKUP_TRINKET
			end
		end


		if (player:HasCollectible(CollectibleType.COLLECTIBLE_GUPPYS_TAIL)) then
			if (rng:RandomInt(3) ~= 0) then
				if (rng:RandomInt(3) == 0) then
					pickupAward = PickupVariant.PICKUP_NULL
				end
			else
				if (rng:RandomInt(2) ~= 0) then
					pickupAward = PickupVariant.PICKUP_LOCKEDCHEST
				else
					pickupAward = PickupVariant.PICKUP_CHEST
				end
			end
		end

		if (player:HasCollectible(CollectibleType.COLLECTIBLE_CONTRACT_FROM_BELOW) and pickupAward ~= PickupVariant.PICKUP_TRINKET) then
			pickupCount = player:GetCollectibleNum(COLLECTIBLE_CONTRACT_FROM_BELOW) + 1
			--The chance of getting nothing goes down with each contract exponentially
			local nothingChance = math.pow(0.666, pickupCount - 1)
			if (nothingChance * 0.5 > rng:NextFloat()) then
				pickupCount = 0
			end
		end

		if (difficulty == 1 and pickupAward == PickupVariant.PICKUP_HEART) then
			if rng:RandomInt(100) >= 35 then
				pickupAward = PickupVariant.PICKUP_NULL
			end
		end

		if player:HasCollectible(CollectibleType.COLLECTIBLE_BROKEN_MODEM) and rng:RandomInt(4) == 0 and pickupCount >= 1 and
				(pickupAward == PickupVariant.PICKUP_COIN or pickupAward == PickupVariant.PICKUP_HEART or pickupAward == PickupVariant.PICKUP_KEY or pickupAward == PickupVariant.PICKUP_GRAB_BAG or pickupAward == PickupVariant.PICKUP_BOMB) then
			pickupCount = pickupCount + 1
		end
		
		local spawnedAwards = {}
		if (pickupCount > 0 and pickupAward ~= PickupVariant.PICKUP_NULL) then
			for i=1, pickupCount do
				local ent = Isaac.Spawn(EntityType.ENTITY_PICKUP, pickupAward, 0, nearCenter, oworld.utils.VEC_ZERO, nil)
				
				table.insert(spawnedAwards, ent)
			end
		end
		
		return spawnedAwards
	end
	
	room.GetDevilRoomChance = function(room)
		return oworld.GetDevilRoomChance()
	end
	
	-- OVERLAYS --
	
	room.AddOverlay = function(room, overlaypng)
		oworld.TotalOverlayIds = oworld.TotalOverlayIds+1
		oworld.Overlays[oworld.TotalOverlayIds] = {
			Png = overlaypng,
			Loaded = false,
			Entity = nil,
			WorldPos = oworld.GetWorldPosByWorldTile(room.TopLeftTile)+Vector(-100,-100)+room.OverlayOffset,
			RoomId = room.RoomId,
			Size = oworld.GetWorldPosByWorldTile(room.BottomRightTile-room.TopLeftTile+Vector(4,4)),
			Visible = true
		}
		
		oworld.Overlays[oworld.TotalOverlayIds].Load = function(overlay)
			if not overlay.Entity then
				overlay.Entity = Isaac.Spawn(EntityType.ENTITY_EFFECT, oworld.CheapEffectVariant, 0, oworld.utils.VEC_ZERO, oworld.utils.VEC_ZERO, nil)
				overlay.Entity:GetData().IsOWOverlay = true
				overlay.Entity:GetData().LinkedRoomId = overlay.RoomId
				overlay.Entity:GetData().GridSpriteHandler = true
				overlay.Entity:GetSprite():Load("gfx/backdrop/openworld_room_overlay.anm2", true)
				if overlay.Png and overlay.Png ~= "gfx/grid/shading.png" then
					overlay.Entity:GetSprite():ReplaceSpritesheet(0, overlay.Png)
					overlay.Entity:GetSprite():LoadGraphics()
				end
				overlay.Entity:GetSprite():Play("Idle", true)
			end
			overlay.Loaded = true
		end
		
		oworld.Overlays[oworld.TotalOverlayIds].Unload = function(overlay)
			if overlay.Entity then
				overlay.Entity:Remove()
				overlay.Entity = nil
			end
			overlay.Loaded = false
		end
		
		local overlay = oworld.Overlays[oworld.TotalOverlayIds]
		
		local cpos = oworld.WorldPlayerPos
		if math.abs(overlay.WorldPos.X-cpos.X+overlay.Size.X/2) < oworld.TilesOnScreenRendered.X*40+40+overlay.Size.X
		and math.abs(overlay.WorldPos.Y-cpos.Y+overlay.Size.Y/2) < oworld.TilesOnScreenRendered.Y*40+40+overlay.Size.Y then
			overlay:Load()
		end
		
		table.insert(room.OverlayIds, oworld.TotalOverlayIds)
		
		return oworld.TotalOverlayIds
	end
	
	room.RemoveOverlay = function(room, overlayId)
		for i,id in ipairs(room.OverlayIds) do
			if overlayId == id then
				table.remove(room.OverlayIds, i)
			end
		end
		
		oworld.Overlays[i] = nil
	end
	
	room.OverlayOffset = oworld.utils.VEC_ZERO
	if room.RoomShapeId == RoomShape.ROOMSHAPE_IH or room.RoomShapeId == RoomShape.ROOMSHAPE_IIH then
		room.OverlayOffset = Vector(0,-80)
	elseif room.RoomShapeId == RoomShape.ROOMSHAPE_IV or room.RoomShapeId == RoomShape.ROOMSHAPE_IIV then
		room.OverlayOffset = Vector(-160,0)
	end
	
	-- DOORS --
	
	room.Doors = {}
	room.AddDoor = function(room, doorstructure)
		if doorstructure then
			for i,doorstruc in ipairs(doorstructure.Doors) do
				if i == 2 then
					room = oworld.GetRoomByTile(doorstruc.Tile)
				end
				local grid = oworld.GetGridEntity(doorstruc.Tile)
				grid:SetType(GridEntityType.GRID_DOOR)
				grid.DoorStructure = doorstructure
				grid.CurrentRoomType = doorstruc.CurrentRoomType
				grid.TargetRoomType = doorstruc.TargetRoomType
				grid.Direction = (3 - doorstruc.Rotation/90 - 1)
				if grid.Direction == -1 then grid.Direction = 3 end
				for slot=DoorSlot.LEFT0, DoorSlot.DOWN1 do
					local slot_tile = room:GetDoorSlotWorldTile(slot)
					if slot_tile and doorstruc.Tile.X == slot_tile.X and doorstruc.Tile.Y == slot_tile.Y then
						grid.Slot = slot
						break
					end
				end
				
				anim = "Closed"
				grid.CollisionClass = GridCollisionClass.COLLISION_WALL
				grid.DoorState = "Closed"
				grid.State = DoorState.STATE_CLOSED
				grid.PreviousState = DoorState.STATE_CLOSED
				
				local dooranm2
				if doorstruc.CurrentRoomType == RoomType.ROOM_SECRET or doorstruc.TargetRoomType == RoomType.ROOM_SECRET then
					dooranm2 = oworld.DOOR[RoomType.ROOM_SECRET]
					grid.DoorType = RoomType.ROOM_SECRET
					
				elseif doorstruc.TargetRoomType == RoomType.ROOM_DEVIL then
					dooranm2 = oworld.DOOR[RoomType.ROOM_DEVIL]
					grid.DoorType = RoomType.ROOM_DEVIL
					
				elseif doorstruc.TargetRoomType == RoomType.ROOM_ANGEL then
					dooranm2 = oworld.DOOR[RoomType.ROOM_ANGEL]
					grid.DoorType = RoomType.ROOM_ANGEL
					
				else
					dooranm2 = oworld.DOOR[doorstruc.CurrentRoomType] or oworld.DOOR[doorstruc.TargetRoomType] or oworld.DOOR[-1]
					if oworld.DOOR[doorstruc.CurrentRoomType] then
						grid.DoorType = doorstruc.CurrentRoomType
					elseif oworld.DOOR[doorstruc.TargetRoomType] then
						grid.DoorType = doorstruc.TargetRoomType
					else
						grid.DoorType = RoomType.ROOM_DEFAULT
					end
				end
				local pos
				if grid.DoorType == RoomType.ROOM_SECRET or grid.DoorType == RoomType.ROOM_SUPERSECRET then
					pos = oworld.GetPositionByWorldTile(doorstruc.Tile)
				else
					pos = oworld.GetPositionByWorldTile(doorstruc.Tile)+Vector(0,20):Rotated(doorstruc.Rotation)
				end
				
				if oworld.IsWorldTileInLoadingDistance(oworld.PositionToWorldPos(pos)) then
					local door = Isaac.Spawn(EntityType.ENTITY_EFFECT, oworld.CheapEffectVariant, 0, pos, oworld.utils.VEC_ZERO, nil)
					door:GetSprite():Load(dooranm2, true)
					door:GetSprite():Play(anim, true)
					door:GetSprite().Rotation = doorstruc.Rotation
					door:GetData().RoomId = room.RoomId
					door:GetData().OWIsDoorEffect = true
					door:GetData().GridEntity = grid
					door.DepthOffset = -100
					grid.DoorReference = oworld.EntityRef(door)
				else
					grid.DoorReference = oworld.AddEntityData(doorstruc.Tile.X, doorstruc.Tile.Y, oworld.World, {
						id = EntityType.ENTITY_EFFECT,
						variant = oworld.CheapEffectVariant,
						worldpos = oworld.PositionToWorldPos(pos),
						depthoffset = -100,
						sprite = {
							anm2 = dooranm2,
							animname = anim,
							on = "playing",
							rotation = doorstruc.Rotation
						},
						data = {
							RoomId = room.RoomId,
							OWIsDoorEffect = true,
							GridEntity = grid
						}
					}, true)
				end
				
				local griddata = oworld.GetGridData(grid)
				griddata.OnSpawn(grid)
				
				table.insert(room.Doors, {
					Structure = doorstructure, 
					EntityRef = grid.DoorReference, 
					Tile = doorstruc.Tile, 
					Rotation = doorstruc.Rotation
				})
			end
			for i,tile in ipairs(doorstructure.WalledOffTiles) do
				oworld.RemoveGridEntity(oworld.GetGridEntity(tile))
				if doorstructure.Doors[1].Rotation == 0 or doorstructure.Doors[1].Rotation == 180 then
					oworld.GridSpawn(GridEntityType.GRID_WALL, 0, tile+Vector(1,0), false)
					oworld.GridSpawn(GridEntityType.GRID_WALL, 0, tile+Vector(-1,0), false)
				elseif doorstructure.Doors[1].Rotation == 90 or doorstructure.Doors[1].Rotation == 270 then
					oworld.GridSpawn(GridEntityType.GRID_WALL, 0, tile+Vector(0,1), false)
					oworld.GridSpawn(GridEntityType.GRID_WALL, 0, tile+Vector(0,-1), false)
				end
			end
		end
	end
	
	room.GetDoor = function(room, slot)
		local tile = room:GetDoorSlotWorldTile(slot)
		if tile then
			local grid = oworld.GetGridEntity(tile)
			if grid and grid.Desc.Type == GridEntityType.GRID_DOOR then
				return grid
			end
		end
	end
	
	room.DoorSlotWorldTile = {}
	room.UpdateDoorSlotWorldTiles = function(room)
		local typicaldoorslottiles = {
			[DoorSlot.LEFT0] = Vector(0,4),
			[DoorSlot.UP0] = Vector(7,0),
			[DoorSlot.RIGHT0] = Vector(32,4),
			[DoorSlot.DOWN0] = Vector(7,20),
			[DoorSlot.LEFT1] = Vector(0,15),
			[DoorSlot.UP1] = Vector(24,0),
			[DoorSlot.RIGHT1] = Vector(32,15),
			[DoorSlot.DOWN1] = Vector(24,20)
		}
		for slot=DoorSlot.LEFT0, DoorSlot.DOWN1 do
			local doortile = typicaldoorslottiles[slot]
			local stile = room.RoomTile and oworld.GetTopLeftWorldTileByRoomTile(room.RoomTile) - Vector(1,1) or room:GetWorldTile(0)
			while doortile.X >= 0 and doortile.X <= 32 and doortile.Y >= 0 and doortile.Y <= 20 do
				local tile = doortile+stile
				if room.TileInRoom[tile.X] and room.TileInRoom[tile.X][tile.Y] == "wall" then
					local wall = oworld.GetGridEntity(tile)
					if wall and wall.Sprite:GetFrame() >= 2 and wall.Sprite:GetFrame() <= 12 then
						room.DoorSlotWorldTile[slot] = tile
						wall.Slot = slot
						break
					end
				end
				
				doortile = doortile+Vector(0,1):Rotated(((slot-1)*90)%360)
			end
		end
	end

	room:UpdateDoorSlotWorldTiles()
	
	room.GetDoorSlotWorldTile = function(room, slot)
		return room.DoorSlotWorldTile[slot]
	end
	
	room.GetDoorSlotPosition = function(room, slot)
		local tile = room:GetDoorSlotWorldTile()
		return tile and oworld.GetPositionByWorldTile(tile)
	end
	
	room.AllowedDoorSlot = {[DoorSlot.LEFT0]=true, true, true, true, true, true, true, true}
	room.AllowDoorSlot = function(room, slot, allow)
		room.AllowedDoorSlot[slot] = allow
	end
	room.IsDoorSlotAllowed = function(room, slot)
		if room.AllowedDoorSlot[slot] then
			local tile = room:GetDoorSlotWorldTile(slot)
			return not not tile and oworld.GetGridEntity(tile).Desc.Type == GridEntityType.GRID_WALL
		else
			return false
		end
	end
	
	if room.RoomShapeId == RoomShape.ROOMSHAPE_IH or room.RoomShapeId == RoomShape.ROOMSHAPE_IIH then
		room.AllowedDoorSlot[DoorSlot.UP0] = false
		room.AllowedDoorSlot[DoorSlot.UP1] = false
		room.AllowedDoorSlot[DoorSlot.DOWN0] = false
		room.AllowedDoorSlot[DoorSlot.DOWN1] = false
		
	elseif room.RoomShapeId == RoomShape.ROOMSHAPE_IV or room.RoomShapeId == RoomShape.ROOMSHAPE_IIV then
		room.AllowedDoorSlot[DoorSlot.LEFT0] = false
		room.AllowedDoorSlot[DoorSlot.LEFT1] = false
		room.AllowedDoorSlot[DoorSlot.RIGHT0] = false
		room.AllowedDoorSlot[DoorSlot.RIGHT1] = false
	end
	
	room.RemoveDoor = function(room, slot)
		local door = room:GetDoor(slot)
		if door then
			oworld.RemoveGridEntity(door)
		end
	end
	
	room.ShouldKeepDoorsClosed = false
	room.KeepDoorsClosed = function(room)
		room.ShouldKeepDoorsClosed = true
	end
	
	-- LAYOUT --
	room.Layout = {TYPE=1, VARIANT=0, SUBTYPE=0, NAME="New Room", DIFFICULTY=1, WEIGHT=1, WIDTH=room.BottomRightTile.X-room.TopLeftTile.X+1, HEIGHT=room.BottomRightTile.Y-room.TopLeftTile.Y+1, SHAPE=room.RoomShapeId}
	
	for slot,tile in pairs(room.DoorSlotWorldTile) do
		table.insert(room.Layout, {ISDOOR=true, GRIDX=tile.X-room.TopLeftTile.X, GRIDY=tile.Y-room.TopLeftTile.Y, SLOT=slot, EXISTS=true})
	end
	
	room.SetLayout = function(room, layout)
		room.Layout = layout
	end
	room.GetLayout = function(room)
		return room.Layout
	end
	
	room.SpawnLayout = function(room, noentities, nogridentities)
		if not room.Layout then
			Isaac.ConsoleOutput("[ERROR] No layout found for room " .. tostring(room.RoomId) .. "!")
			return
		end
		room.Difficulty = room.Layout.DIFFICULTY
		room.Type = room.Layout.TYPE
		room.Variant = room.Layout.VARIANT
		room.Subtype = room.Layout.SUBTYPE
		room.Name = room.Layout.NAME
		room.Weight = room.Layout.WEIGHT
		for _,entsdata in ipairs(room.Layout) do
			if entsdata.ISDOOR then
				if room.DoorSlotWorldTile[entsdata.SLOT] then
					room:AllowDoorSlot(entsdata.SLOT, entsdata.EXISTS)
				end
			else
				local totalweight = 0
				for _,entdata in ipairs(entsdata) do
					totalweight = totalweight+entdata.WEIGHT
				end
				
				local r = room.rng:RandomFloat()*totalweight
				local cumulativeweight = 0
				for _,entdata in ipairs(entsdata) do
					cumulativeweight = cumulativeweight+entdata.WEIGHT
					if r <= cumulativeweight then
						if entdata.TYPE <= 0 then
							break
						end
						
						local x = entsdata.GRIDX
						local y = entsdata.GRIDY
						if room:GetRoomShape() == RoomShape.ROOMSHAPE_IV or room:GetRoomShape() == RoomShape.ROOMSHAPE_IIV then
							x = x-4
						end
						if room:GetRoomShape() == RoomShape.ROOMSHAPE_IH or room:GetRoomShape() == RoomShape.ROOMSHAPE_IIH then
							y = y-2
						end
						
						if entdata.TYPE < 1000 then
							oworld.AddEntityData(room.TopLeftTile.X+x, room.TopLeftTile.Y+y, oworld.World, {id=entdata.TYPE == 999 and 1000 or entdata.TYPE, variant=entdata.VARIANT, subtype=entdata.SUBTYPE, visible = not room:IsHidden()})
						else
							local gridtype = oworld.CorrectedGridTypes[entdata.TYPE] and type(oworld.CorrectedGridTypes[entdata.TYPE]) ~= "number" and oworld.CorrectedGridTypes[entdata.TYPE].Type or oworld.CorrectedGridTypes[entdata.TYPE] or entdata.TYPE
							local gridvariant = oworld.CorrectedGridTypes[entdata.TYPE] and type(oworld.CorrectedGridTypes[entdata.TYPE]) ~= "number" and oworld.CorrectedGridTypes[entdata.TYPE].Variant or 0
							if oworld.FakeGridTypes[gridtype] and oworld.FakeGridTypes[gridtype][gridvariant] then
								local grid = oworld.AddGridEntityData(room.TopLeftTile.X+x, room.TopLeftTile.Y+y, oworld.World, gridtype, gridvariant)
								grid.Visible = not room:IsHidden()
							end
						end
						break
					end
				end
			end
		end
	end
	
	room.ChooseLayoutFromLuaRooms = function(room, luarooms, ignoretype)
		local validluarooms = {}
		local totalweight = 0
		for _,luaroom in ipairs(luarooms) do
			if luaroom.SHAPE == room.RoomShapeId and (ignoretype or luaroom.TYPE == room:GetType()) then
				local correctdoors = true
				for _,entsdata in ipairs(luaroom) do
					if entsdata.ISDOOR then
						if not entsdata.EXISTS then
							if room:GetDoor(entsdata.SLOT) then
								correctdoors = false
								break
							end
						end
					else
						break
					end
				end
				if correctdoors then
					totalweight = totalweight+luaroom.TYPE
					table.insert(validluarooms, luaroom)
				end
			end
		end
		
		local r = room.rng:RandomFloat()*totalweight
		local cumulativeweight = 0
		for _,luaroom in ipairs(validluarooms) do
			cumulativeweight = cumulativeweight+luaroom.TYPE
			if r <= cumulativeweight then
				return luaroom
			end
		end
		return nil
	end
	
	setmetatable(room, {
		__newindex = function(t, k, v)
			rawset(room, k, v)
		end,
		__index = function(t, k)
			return rawget(room, k)
		end
	})
	oworld.Rooms[oworld.TotalRoomIds] = room
	
	return room
end

oworld.LastActiveRoom = oworld.LastActiveRoom or nil

oworld.GetRoomByTile = function(tile)
	for i=1, oworld.TotalRoomIds do
		local room = oworld.Rooms[i]
		if room and oworld.IsTileInRoom(tile, room, true) then
			return room
		end
	end
end

oworld.GetCurrentRoom = function()
	return oworld.LastActiveRoom
end

oworld.UpdateGetRoom = function()
	local playertile = oworld.GetWorldTileByWorldPos(oworld.WorldPlayerPos)
	for i=1, oworld.TotalRoomIds do
		local room = oworld.Rooms[i]
		if room and room ~= oworld.LastActiveRoom and oworld.IsTileInRoom(playertile, room) then
			if oworld.LastActiveRoom then
				oworld.LastActiveRoom.FirstVisit = false
				oworld.LastActiveRoom.FrameCount = 0
			end
			room.VisitedCount = room.VisitedCount + 1
			oworld.LastActiveRoom = room
			for _,fn in ipairs(oworld.PostNewRoomCallbacks) do
				fn(room, room)
			end
			if not room:IsClear() then
				local room_is_clear = true
				for _,e in ipairs(Isaac.GetRoomEntities()) do
					e:ClearEntityFlags(EntityFlag.FLAG_CONFUSION)
					if room_is_clear and e:CanShutDoors() then
						room_is_clear = false
					end
				end
				room:SetClear(room_is_clear)
			end
			return room
		end
	end
	return oworld.LastActiveRoom
end

oworld:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	oworld.UpdateGetRoom()
end)

oworld.OldGetCurrentRoomIndex = APIOverride.GetCurrentClassFunction(Level, "GetCurrentRoomIndex")
APIOverride.OverrideClassFunction(Level, "GetCurrentRoomIndex", function(level)
	if not oworld.LastActiveRoom then
		return oworld.OldGetCurrentRoomIndex(level)
	else
		return game:GetRoom().RoomId
	end
end)

oworld:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	if oworld.InModeMenu then return end
	
	-- update roomframecount
	local room = oworld.GetCurrentRoom()
	if room then
		room.FrameCount = room.FrameCount+1
	end
	
	-- keeping track of the overlays
	for i=1, oworld.TotalOverlayIds do
		local overlay = oworld.Overlays[i]
		if overlay and overlay.Visible then
			local cpos = oworld.WorldPlayerPos
			if overlay.Loaded then
				if math.abs(overlay.WorldPos.X-cpos.X+overlay.Size.X/2) >= oworld.TilesOnScreenRendered.X*40+40+overlay.Size.X
				or math.abs(overlay.WorldPos.Y-cpos.Y+overlay.Size.Y/2) >= oworld.TilesOnScreenRendered.Y*40+40+overlay.Size.Y then
					overlay:Unload()
				end
			else
				if math.abs(overlay.WorldPos.X-cpos.X+overlay.Size.X/2) < oworld.TilesOnScreenRendered.X*40+40+overlay.Size.X
				and math.abs(overlay.WorldPos.Y-cpos.Y+overlay.Size.Y/2) < oworld.TilesOnScreenRendered.Y*40+40+overlay.Size.Y then
					overlay:Load()
				end
			end
		end
		if overlay.Entity then
			overlay.Entity.DepthOffset = oworld.WorldPosToPosition(oworld.WorldPlayerPos).Y-overlay.Entity.Position.Y-300
			local room = oworld.Rooms[overlay.RoomId]
			overlay.WorldPos = oworld.GetWorldPosByWorldTile(room.TopLeftTile)+Vector(-100,-100)+room.OverlayOffset
			overlay.Entity:GetSprite().Offset = Isaac.WorldToScreenDistance(overlay.WorldPos-oworld.WorldPlayerPos)
			overlay.Entity.Position = oworld.WorldPosToPosition(oworld.WorldPlayerPos)
		end
	end
	
	-- keeping track of movement between rooms
	local player = Isaac.GetPlayer(0)
	local playertile = oworld.GetWorldTileByPosition(player.Position)
	local room = oworld.GetCurrentRoom()
	if room then
		local inroomtransition = false
		for _,door in ipairs(room.Doors) do
			local tile = door.Tile
			if tile.X == playertile.X and tile.Y == playertile.Y then
				local tilepos = oworld.GetPositionByWorldTile(tile)
				if door.Rotation == 0 and player.Position.Y < tilepos.Y
				or door.Rotation == 90 and player.Position.X > tilepos.X
				or door.Rotation == 180 and player.Position.Y > tilepos.Y
				or door.Rotation == 270 and player.Position.Y < tilepos.Y then
					inroomtransition = true
				end
			else
				for _,tile in ipairs(door.Structure.WalledOffTiles) do
					if tile.X == playertile.X and tile.Y == playertile.Y then
						inroomtransition = true
						break
					end
				end
			end
			if inroomtransition then
				break
			end
		end
		if inroomtransition then
			player:GetData().InRoomTransition = true
			player.Visible = false
			player:SetShootingCooldown(2)
		elseif player:GetData().InRoomTransition then
			player:GetData().InRoomTransition = false
			player.Visible = true
		end
	end
end)

--[[ 
DOORSTRUCTURE {
	Doors        -- Table with 2 {(Integer) Rotation, (Vector WorldTile) Tile} Tables, specifies where the 2 doors are and where they are facing
	WalledOffTiles -- Table with all WorldTiles that act as pathway and will be blocked off by invisible walls on the sides
}
]]

function oworld.GenerateDoorStructure(wall, maxtiledistance, originroomtype, targetroomtype)
	if wall and wall.Desc.Type == GridEntityType.GRID_WALL then
		local doorstructure = {Doors = {}, WalledOffTiles = {}}
		local tile = wall:GetTile()
		local wallframe = wall.Sprite:GetFrame()
		local doorsettedup = false
		if wallframe >= 2 and wallframe <= 8 then
			local y = tile.Y
			if wall.Sprite.FlipY then -- door down
				table.insert(doorstructure.Doors, {Rotation=180, Tile=Vector(tile.X, tile.Y)})
				while y >= 1 and y <= oworld.WorldHeight and (not maxtiledistance or y <= tile.Y+maxtiledistance) do
					y = y+1
					local grid = oworld.GetGridEntity(Vector(tile.X, y))
					if grid and grid.Desc.Type == GridEntityType.GRID_WALL then
						local frame = grid.Sprite:GetFrame()
						if frame >= 2 and frame <= 8 and not grid.Sprite.FlipY then
							table.insert(doorstructure.Doors, {Rotation=0, Tile=Vector(tile.X, y)})
							doorsettedup = true
							break
						end
					else
						table.insert(doorstructure.WalledOffTiles, Vector(tile.X, y))
					end
				end
			else -- door up
				table.insert(doorstructure.Doors, {Rotation=0, Tile=Vector(tile.X, tile.Y)})
				while y >= 1 and y <= oworld.WorldHeight and (not maxtiledistance or y >= tile.Y-maxtiledistance) do
					y = y-1
					local grid = oworld.GetGridEntity(Vector(tile.X, y))
					if grid and grid.Desc.Type == GridEntityType.GRID_WALL then
						local frame = grid.Sprite:GetFrame()
						if frame >= 2 and frame <= 8 and grid.Sprite.FlipY then
							table.insert(doorstructure.Doors, {Rotation=180, Tile=Vector(tile.X, y)})
							doorsettedup = true
							break
						end
					else
						table.insert(doorstructure.WalledOffTiles, Vector(tile.X, y))
					end
				end
			end
			
		elseif wallframe >= 9 and wallframe <= 12 then
			local x = tile.X
			if wall.Sprite.FlipX then -- door right
				table.insert(doorstructure.Doors, {Rotation=90, Tile=Vector(tile.X, tile.Y)})
				while x >= 1 and x <= oworld.WorldWidth and (not maxtiledistance or x <= tile.X+maxtiledistance) do
					x = x+1
					local grid = oworld.GetGridEntity(Vector(x, tile.Y))
					if grid and grid.Desc.Type == GridEntityType.GRID_WALL then
						local frame = grid.Sprite:GetFrame()
						if frame >= 9 and frame <= 12 and not grid.Sprite.FlipX then
							table.insert(doorstructure.Doors, {Rotation=270, Tile=Vector(x, tile.Y)})
							doorsettedup = true
							break
						end
					else
						table.insert(doorstructure.WalledOffTiles, Vector(x, tile.Y))
					end
				end
			else -- door left
				table.insert(doorstructure.Doors, {Rotation=270, Tile=Vector(tile.X, tile.Y)})
				while x >= 1 and x <= oworld.WorldWidth and (not maxtiledistance or x >= tile.X-maxtiledistance) do
					x = x-1
					local grid = oworld.GetGridEntity(Vector(x, tile.Y))
					if grid and grid.Desc.Type == GridEntityType.GRID_WALL then
						local frame = grid.Sprite:GetFrame()
						if frame >= 9 and frame <= 12 and grid.Sprite.FlipX then
							table.insert(doorstructure.Doors, {Rotation=90, Tile=Vector(x, tile.Y)})
							doorsettedup = true
							break
						end
					else
						table.insert(doorstructure.WalledOffTiles, Vector(x, tile.Y))
					end
				end
			end
		end
		
		if doorsettedup and doorstructure.Doors[2] then
			local room1 = oworld.GetRoomByTile(doorstructure.Doors[1].Tile)
			local room2 = oworld.GetRoomByTile(doorstructure.Doors[2].Tile)
			
			doorstructure.Doors[1].CurrentRoomType = originroomtype or room1 and room1:GetType() or RoomType.ROOM_DEFAULT
			doorstructure.Doors[2].TargetRoomType = originroomtype or room1 and room1:GetType() or RoomType.ROOM_DEFAULT
			
			doorstructure.Doors[1].TargetRoomType = targetroomtype or room2 and room2:GetType() or RoomType.ROOM_DEFAULT
			doorstructure.Doors[2].CurrentRoomType = targetroomtype or room2 and room2:GetType() or RoomType.ROOM_DEFAULT
			
			return doorstructure
		end
	end
	return nil
end

Isaac.DebugString("Loaded Open World rooms")