local oworld = require "scripts.openworld"

local game = Game()

-- workaround to getting the npc that uses Pathfinder
oworld.CurrentActiveNPC = nil

oworld:AddCallback(ModCallbacks.MC_PRE_NPC_UPDATE, function(_, npc)
	oworld.CurrentActiveNPC = npc
end)
oworld:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, npc)
	oworld.CurrentActiveNPC = npc
end)

oworld:AddCallback(ModCallbacks.MC_POST_UPDATE, function()
	oworld.CurrentActiveNPC = nil
end)

if not oworld.CurrentPathfinderRoomFrame then
	oworld.PathfinderHelper = nil
	oworld.CurrentPathfinderRoomFrame = -1
	oworld.CurrentPathfinderTile = oworld.utils.VEC_ZERO
	oworld.CurrentPathfinderEntityCollisionClass = EntityGridCollisionClass.GRIDCOLL_GROUND
end

-- returns a 2 dimensional array showing for every tile how many tiles it is away of the targettile, going up a number for every tile
-- tiles with a movement blocking grid, tiles that are outside of the loading area or tiles that have no path to the player in the loading area are nil
function oworld.GetPathfinderHelper(targettile, gridecollisionclass, forceupdate)
	gridecollisionclass = gridecollisionclass or EntityGridCollisionClass.GRIDCOLL_GROUND
	local roomframecount = game:GetRoom():GetFrameCount()
	if forceupdate or not oworld.PathfinderHelper or roomframecount ~= oworld.CurrentPathfinderRoomFrame or oworld.CurrentPathfinderTile.X ~= targettile.X or oworld.CurrentPathfinderTile.Y ~= targettile.Y or oworld.CurrentPathfinderEntityCollisionClass ~= gridecollisionclass then
		oworld.CurrentPathfinderRoomFrame = roomframecount
		oworld.CurrentPathfinderTile = targettile
		oworld.CurrentPathfinderEntityCollisionClass = gridecollisionclass
		oworld.PathfinderHelper = {}
		for x=1, oworld.WorldWidth do
			oworld.PathfinderHelper[x] = {}
		end
		
		local aligningtilesoffset = {Vector(1,0), Vector(-1,0), Vector(0,1), Vector(0,-1)}
		local outerpathfindergrids = {targettile}
		oworld.PathfinderHelper[targettile.X][targettile.Y] = 0
		local distance = 0
		while #outerpathfindergrids ~= 0 do
			distance = distance+1
			local newlyaddedgrids = {}
			for _,outertile in ipairs(outerpathfindergrids) do
				for _,alignedtileoffset in ipairs(aligningtilesoffset) do
					local tile = outertile+alignedtileoffset
					if not oworld.PathfinderHelper[tile.X][tile.Y]
					and (not oworld.FakeGridEntities[tile.X][tile.Y] or oworld.FakeGridEntities[tile.X][tile.Y]
					and not oworld.CanCollideWithGrid(gridecollisionclass, oworld.FakeGridEntities[tile.X][tile.Y].CollisionClass)
					and not oworld.GetGridData(oworld.FakeGridEntities[tile.X][tile.Y]).pathfinderavoid)
					and tile.X >= targettile.X-oworld.TilesOnScreenActive.X
					and tile.X <= targettile.X+oworld.TilesOnScreenActive.X
					and tile.Y >= targettile.Y-oworld.TilesOnScreenActive.Y
					and tile.Y <= targettile.Y+oworld.TilesOnScreenActive.Y then
						oworld.PathfinderHelper[tile.X][tile.Y] = distance
						table.insert(newlyaddedgrids, tile)
					end
				end
			end
			outerpathfindergrids = newlyaddedgrids
		end
	end
	return oworld.PathfinderHelper
end

-- rewritten FindGridPath function
APIOverride.OverrideClassFunction(PathFinder, "FindGridPath", function(pathfinder, Pos, Speed, PathMarker, UseDirectPath, ReturnVelocity)
	Speed = Speed*6
	local pathfinderhelper, npc = oworld.GetPathfinderHelper(oworld.GetWorldTileByPosition(Pos)), oworld.CurrentActiveNPC
	local npctile = oworld.GetWorldTileByPosition(npc.Position)
	local aligningtilesoffset = {Vector(1,0), Vector(-1,0), Vector(0,1), Vector(0,-1)}
	if pathfinderhelper[npctile.X][npctile.Y] then
		--[[local room = game:GetRoom()
		local closestinsighttilestoplayer = {}
		local closestdistancetoplayer = pathfinderhelper[npctile.X][npctile.Y]
		local cangostraighttotile = function(npc, pos)
			local dirvec = pos-npc.Position
			local offset = Vector(dirvec.X*-1, dirvec.Y):Resized(npc.Size+5)
			return room:CheckLine(npc.Position+offset, pos+offset, 0) and room:CheckLine(npc.Position-offset, pos-offset, 0)
		end
		for x=1, oworld.WorldWidth do
			for y=1, oworld.WorldHeight do
				if pathfinderhelper[x][y] then
					local tilepos = oworld.GetPositionByWorldTile(Vector(x,y))
					if closestdistancetoplayer == pathfinderhelper[x][y] and cangostraighttotile(npc, tilepos) then
						table.insert(closestinsighttilestoplayer, tilepos)
					elseif closestdistancetoplayer and closestdistancetoplayer > pathfinderhelper[x][y] and cangostraighttotile(npc, tilepos) then
						closestinsighttilestoplayer = {tilepos}
						closestdistancetoplayer = pathfinderhelper[x][y]
					end
				end
			end
		end
		local closestinsighttiletoplayer
		local closestdistancetonpc
		for _,tilepos in ipairs(closestinsighttilestoplayer) do
			local distance = (tilepos-npc.Position):LengthSquared()
			if not closestinsighttiletoplayer or distance < closestdistancetonpc then
				closestinsighttiletoplayer = tilepos
				closestdistancetonpc = distance
			end
		end
		if closestinsighttiletoplayer then
			local dirvec = closestinsighttiletoplayer-npc.Position
			local offset = Vector(dirvec.Y, dirvec.X*-1):Resized(npc.Size+10)
			Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.TEAR_POOF_B, 0, npc.Position+offset, oworld.utils.VEC_ZERO, nil)
			Isaac.Spawn(EntityType.ENTITY_EFFECT, EffectVariant.TEAR_POOF_B, 0, npc.Position-offset, oworld.utils.VEC_ZERO, nil)
			npc.Velocity = (closestinsighttiletoplayer-npc.Position):Resized(Speed)
			return
		end]]
		local tilestowardsplayer = {}
		for _,alignedtileoffset in ipairs(aligningtilesoffset) do
			local tile = npctile+alignedtileoffset
			if pathfinderhelper[tile.X][tile.Y] and pathfinderhelper[npctile.X][npctile.Y] > pathfinderhelper[tile.X][tile.Y] then
				table.insert(tilestowardsplayer, tile)
			end
		end
		
		if #tilestowardsplayer ~= 0 then
			
			local closesttiletowardsplayer, closestdistance
			for _,tile in ipairs(tilestowardsplayer) do
				local distance = (oworld.GetPositionByWorldTile(tile)-npc.Position):LengthSquared()
				if not closestdistance or distance < closestdistance then
					closestdistance = distance
					closesttiletowardsplayer = tile
				end
			end
			
			if closesttiletowardsplayer then
				local vel = (oworld.GetPositionByWorldTile(closesttiletowardsplayer)-npc.Position):Resized(Speed)
				local npctilepos = oworld.GetPositionByWorldTile(npctile)
				if math.abs(vel.Y) > math.abs(vel.X) then
					if math.abs(npc.Position.X-npctilepos.X) > math.max(0, 20-npc.Size) then
						vel = Vector(math.min(math.max(npctilepos.X-npc.Position.X, -Speed), Speed), 0)
					end
				else
					if math.abs(npc.Position.Y-npctilepos.Y) > math.max(0, 20-npc.Size) then
						vel = Vector(0, math.min(math.max(npctilepos.Y-npc.Position.Y, -Speed), Speed))
					end
				end
				if ReturnVelocity then
					return vel
				else
					npc.Velocity = vel
					return
				end
			end
		end
	end
	if ReturnVelocity then
		return oworld.utils.VEC_ZERO
	else
		npc.Velocity = oworld.utils.VEC_ZERO
	end
end)

function oworld.CheckLine(Pos1, Pos2, Mode, GridPathThreshold, IgnoreWalls, IgnoreCrushable)
	Pos1,Pos2 = oworld.PositionToWorldPos(Pos1), oworld.PositionToWorldPos(Pos2)
	local posdiff,startpoint
	if Pos1.X < Pos2.X then 
		posdiff = Pos2-Pos1
		startpoint = Pos1
	else 
		posdiff = Pos1-Pos2
		startpoint = Pos2
	end
	local checkpoint = startpoint
	if posdiff.Y == 0 then posdiff = Vector(posdiff.X, 0.00001) end
	local coef = posdiff.Y/posdiff.X
	local collideswithgrid = false
	while not collideswithgrid do
		local nextcheckpoint = Vector(math.min(math.floor(checkpoint.X/40+1)*40, posdiff.X+startpoint.X), 0)
		nextcheckpoint = Vector(nextcheckpoint.X, ((nextcheckpoint.X-startpoint.X)*coef)+startpoint.Y)
		local numcheckpointsonrow = math.ceil(math.abs(nextcheckpoint.Y-checkpoint.Y)/40)+1
		for i=0, numcheckpointsonrow-1 do
			local grid
			if i == numcheckpointsonrow-1 then
				grid = oworld.GetGridEntity(oworld.GetWorldTileByWorldPos(Vector(nextcheckpoint.X, nextcheckpoint.Y)))
			else
				grid = oworld.GetGridEntity(oworld.GetWorldTileByWorldPos(Vector(nextcheckpoint.X, checkpoint.Y+(i*40*oworld.utils.Sign(posdiff.Y)))))
			end
			if grid then
				if not IgnoreWalls or grid.CollisionClass ~= GridCollisionClass.COLLISION_WALL and grid.CollisionClass ~= GridCollisionClass.COLLISION_WALL_EXCEPT_PLAYER then
					if not IgnoreCrushable or not grid.crushable then
						if Mode == 3 and grid.CollisionClass ~= GridCollisionClass.COLLISION_NONE and grid.CollisionClass ~= GridCollisionClass.COLLISION_PIT then
							collideswithgrid = true 
							break
						elseif Mode == 2 and not grid.Destructable and grid.CollisionClass ~= GridCollisionClass.COLLISION_NONE then
							collideswithgrid = true
							break
						elseif Mode < 2 and (grid.CollisionClass ~= GridCollisionClass.COLLISION_NONE or oworld.GetGridData(grid).pathfinderavoid) then
							collideswithgrid = true
							break
						end
					end
				end
			end
		end
		if nextcheckpoint.X == posdiff.X+startpoint.X then
			break
		end
		checkpoint = nextcheckpoint
	end
	return not collideswithgrid
end