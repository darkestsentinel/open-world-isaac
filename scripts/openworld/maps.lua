local oworld = require "scripts.openworld"

--[[ GameMode Structure
	{
		name                 -- String, the name of the mode in the menu
		noworldboundary      -- Boolean, when true doesn't make use of oworld.World, but will have an infinite world
		generateworld        -- Function, activated just after the world has been created and just before tiles get loaded
		options = { -- extra options after choosing the mode
			{
				name             -- String, the name of the option in the menu
				var              -- String, the variable in oworld to assign the value to
				defaultvalue     -- Number/Boolean/Table, the start value, determines the type of value
				lowestvalue      -- Number, only used if the defaultvalue is a number, the value can't go any lower than this value
				highestvalue     -- Number, only used if the defaultvalue is a number, the value can't go any higher than this value
				step             -- Number, only used if the defaultvalue is a number, the amount the value increases or decreases every step, 1 by default
			}
		}
	}
]]

--[[ EntityData Stucture
	{
		id              -- Integer (the only non-optional value)
		variant         -- Integer
		subtype         -- Integer
		velocity        -- Vector
		worldpos        -- Vector
		data            -- Table
		hitpoints       -- Number
		maxhitpoints    -- Number
		entityflags     -- Integer
		spawnertype     -- Integer
		spawnervariant  -- Integer
		spawnerentity   -- Entity
		mass            -- Number
		gridcolclass    -- GridCollisionClass Enum
		entcolclass     -- EntityCollisionClass Enum
		coldmg          -- Number
		parent          -- Entity
		child           -- Entity
		target          -- Entity
		depthoffset     -- Number
		visible         -- Boolean
		
		state           -- Integer
		
		-- sprite
		sprite = {
			anm2                       -- String
			animname                   -- String
			on                         -- String, either "playing" or "frame"
			replacedpngs               -- Table
			overlayanimname            -- String
			overlayon                  -- String, either "playing" or "frame"
			overlayrenderpriority      -- Boolean
			offset                     -- Vector
			scale                      -- Vector
			rotation                   -- Number
			flipx                      -- Boolean
			flipy                      -- Boolean
			playbackspeed              -- Number
		}
		
		-- pickup
		charge          -- Integer
		price           -- Integer
		touched         -- Boolean
		shopitemid      -- Integer
		theresoptions   -- Boolean
		wait            -- Integer
		autoupdateprice -- Boolean
	}
]]

oworld.BackdropTypeToName = {
	[1] = "BASEMENT",
	[2] = "CELLAR",
	[3] = "BURNING_BASEMENT",
	[4] = "CAVES",
	[5] = "CATACOMBS",
	[6] = "DROWNED_CAVES",
	[7] = "DEPTHS",
	[8] = "NECROPOLIS",
	[9] = "DANK_DEPTHS",
	[10] = "WOMB",
	[11] = "UTERO",
	[12] = "SCARRED_WOMB",
	[13] = "BLUE_WOMB",
	[14] = "SHEOL",
	[15] = "CATHEDRAL",
	[16] = "DARK_ROOM",
	[17] = "CHEST",
	[18] = "DARK_ROOM",
	[19] = "LIBRARY",
	[20] = "SHOP",
	[21] = "ISAACS_ROOM",
	[22] = "BARREN_ROOM",
	[23] = "SECRET_ROOM",
	[24] = "DICE_ROOM",
	[25] = "ARCADE",
	[26] = "DARK_ROOM",
	[27] = "BLUE_SECRET",
	[28] = "SHOP"
}

oworld.BACKDROP = {
	LIBRARY = "gfx/backdrop/0a_library.png",
	SHOP = "gfx/backdrop/0b_shop.png",
	ISAACS_ROOM = "gfx/backdrop/0c_isaacsroom.png",
	BARREN_ROOM = "gfx/backdrop/0d_barrenroom.png",
	ARCADE = "gfx/backdrop/0e_arcade.png",
	DICE_ROOM = "gfx/backdrop/0e_diceroom.png",
	SECRET_ROOM = "gfx/backdrop/0f_secretroom.png",
	BASEMENT = "gfx/backdrop/01_basement.png",
	CELLAR = "gfx/backdrop/02_cellar.png",
	CAVES = "gfx/backdrop/03_caves.png",
	CATACOMBS = "gfx/backdrop/04_catacombs.png",
	DEPTHS = "gfx/backdrop/05_depths.png",
	NECROPOLIS = "gfx/backdrop/06_necropolis.png",
	WOMB = "gfx/backdrop/07_the womb.png",
	UTERO = "gfx/backdrop/08_utero.png",
	SHEOL = "gfx/backdrop/09_sheol.png",
	CATHEDRAL = "gfx/backdrop/10_cathedral.png",
	CHEST = "gfx/backdrop/11_chest.png",
	DARK_ROOM = "gfx/backdrop/12_darkroom.png",
	BURNING_BASEMENT = "gfx/backdrop/13_the burning basement.png",
	DROWNED_CAVES = "gfx/backdrop/14_the drowned caves.png",
	DANK_DEPTHS = "gfx/backdrop/1_the dank depths.png",
	SCARRED_WOMB = "gfx/backdrop/16_the scarred womb.png",
	BLUE_SECRET = "gfx/backdrop/17_blue secret.png",
	BLUE_WOMB = "gfx/backdrop/18_blue womb.png"
}

oworld.SPECIAL_BACKDROP = {
	[RoomType.ROOM_SHOP] = oworld.BACKDROP.SHOP,
	[RoomType.ROOM_SECRET] = oworld.BACKDROP.SECRET_ROOM,
	[RoomType.ROOM_SUPERSECRET] = oworld.BACKDROP.SECRET_ROOM,
	[RoomType.ROOM_ARCADE] = oworld.BACKDROP.ARCADE,
	[RoomType.ROOM_CURSE] = oworld.BACKDROP.DEPTHS,
	[RoomType.ROOM_CHALLENGE] = oworld.BACKDROP.DEPTHS,
	[RoomType.ROOM_LIBRARY] = oworld.BACKDROP.LIBRARY,
	[RoomType.ROOM_SACRIFICE] = oworld.BACKDROP.DEPTHS,
	[RoomType.ROOM_DEVIL] = oworld.BACKDROP.SHEOL,
	[RoomType.ROOM_ANGEL] = oworld.BACKDROP.CATHEDRAL,
	[RoomType.ROOM_BOSSRUSH] = oworld.BACKDROP.DEPTHS,
	[RoomType.ROOM_ISAACS] = oworld.BACKDROP.ISAACS_ROOM,
	[RoomType.ROOM_BARREN] = oworld.BACKDROP.BARREN_ROOM,
	[RoomType.ROOM_CHEST] = oworld.BACKDROP.CHEST,
	[RoomType.ROOM_DICE] = oworld.BACKDROP.DICE_ROOM,
	[RoomType.ROOM_BLACK_MARKET] = oworld.BACKDROP.DEPTHS
}

oworld.FLOOR = {
	LIBRARY = "gfx/grid/floor_library.png",
	SHOP = "gfx/grid/floor_shop.png",
	ARCADE = "gfx/grid/floor_arcade.png",
	DICE_ROOM = "gfx/grid/floor_dice.png",
	SECRET_ROOM = "gfx/grid/floor_secret.png",
	BASEMENT = "gfx/grid/floor_basement.png",
	CAVES = "gfx/grid/floor_caves.png",
	CATACOMBS = "gfx/grid/floor_catacombs.png",
	DEPTHS = "gfx/grid/floor_depths.png",
	BLUE_WOMB = "gfx/grid/floor_bluewomb.png",
	CATHEDRAL = "gfx/grid/floor_cathedral.png",
	NECROPOLIS = "gfx/grid/floor_necropolis.png",
	SHEOL = "gfx/grid/floor_sheol.png",
	WOMB = "gfx/grid/floor_womb.png"
}

oworld.SPECIAL_FLOOR = {
	[RoomType.ROOM_SHOP] = oworld.FLOOR.SHOP,
	[RoomType.ROOM_SECRET] = oworld.FLOOR.SECRET_ROOM,
	[RoomType.ROOM_SUPERSECRET] = oworld.FLOOR.SECRET_ROOM,
	[RoomType.ROOM_ARCADE] = oworld.FLOOR.ARCADE,
	[RoomType.ROOM_CURSE] = oworld.FLOOR.DEPTHS,
	[RoomType.ROOM_CHALLENGE] = oworld.FLOOR.DEPTHS,
	[RoomType.ROOM_LIBRARY] = oworld.FLOOR.LIBRARY,
	[RoomType.ROOM_SACRIFICE] = oworld.FLOOR.DEPTHS,
	[RoomType.ROOM_DEVIL] = oworld.FLOOR.DEPTHS,
	[RoomType.ROOM_ANGEL] = oworld.FLOOR.CATHEDRAL,
	[RoomType.ROOM_BOSSRUSH] = oworld.FLOOR.DEPTHS,
	[RoomType.ROOM_ISAACS] = oworld.FLOOR.LIBRARY,
	[RoomType.ROOM_BARREN] = oworld.FLOOR.LIBRARY,
	[RoomType.ROOM_CHEST] = oworld.FLOOR.SHOP,
	[RoomType.ROOM_DICE] = oworld.FLOOR.DICE_ROOM,
	[RoomType.ROOM_BLACK_MARKET] = oworld.FLOOR.DEPTHS
}

oworld.ROOM_OVERLAY = {
	SHADING_1x1 = "gfx/grid/shading.png",
	SHADING_1x2 = "gfx/grid/shading_1x2.png",
	SHADING_2x1 = "gfx/grid/shading_2x1.png",
	SHADING_2x2 = "gfx/grid/shading_2x2.png",
	SHADING_IH = "gfx/grid/shading_ih.png",
	SHADING_IIH = "gfx/grid/shading_iih.png",
	SHADING_IV = "gfx/grid/shading_iv.png",
	SHADING_IIV = "gfx/grid/shading_iiv.png",
	SHADING_LBL = "gfx/grid/shading_lbl.png",
	SHADING_LBR = "gfx/grid/shading_lbr.png",
	SHADING_LTL = "gfx/grid/shading_ltl.png",
	SHADING_LTR = "gfx/grid/shading_ltr.png"
}

oworld.DOOR = {
	[-1] = "gfx/grid/door_01_normaldoor.anm2",
	[RoomType.ROOM_TREASURE] = "gfx/grid/door_02_treasureroomdoor.anm2",
	[RoomType.ROOM_CHALLENGE] = "gfx/grid/door_03_ambushroomdoor.anm2",
	[RoomType.ROOM_SACRIFICE] = "gfx/grid/door_04_selfsacrificeroomdoor.anm2",
	[RoomType.ROOM_ARCADE] = "gfx/grid/door_05_arcaderoomdoor.anm2",
	[RoomType.ROOM_DEVIL] = "gfx/grid/door_07_devilroomdoor.anm2",
	[RoomType.ROOM_ANGEL] = "gfx/grid/door_07_holyroomdoor.anm2",
	[RoomType.ROOM_SECRET] = "gfx/grid/door_08_holeinwall.anm2",
	[RoomType.ROOM_SUPERSECRET] = "gfx/grid/door_08_holeinwall.anm2",
	[RoomType.ROOM_BOSS] = "gfx/grid/door_10_bossroomdoor.anm2"
}

oworld.PoopVariant = { -- taken from stage api 1.5
	Normal = 0,
	Red = 1,
	Eternal = 2,
	Golden = 3,
	Rainbow = 4,
	Black = 5,
	White = 6
}

oworld.CorrectedGridTypes = { -- taken from stage api 1.5
	[1000] = GridEntityType.GRID_ROCK,
	[1001] = GridEntityType.GRID_ROCK_BOMB,
	[1002] = GridEntityType.GRID_ROCK_ALT,
	[1300] = GridEntityType.GRID_TNT,
	[1498] = {Type = GridEntityType.GRID_POOP, Variant = oworld.PoopVariant.White},
	[1497] = {Type = GridEntityType.GRID_POOP, Variant = oworld.PoopVariant.Black},
	[1496] = {Type = GridEntityType.GRID_POOP, Variant = oworld.PoopVariant.Golden},
	[1495] = {Type = GridEntityType.GRID_POOP, Variant = oworld.PoopVariant.Eternal},
	[1494] = {Type = GridEntityType.GRID_POOP, Variant = oworld.PoopVariant.Rainbow},
	[1490] = {Type = GridEntityType.GRID_POOP, Variant = oworld.PoopVariant.Red},
	[1500] = GridEntityType.GRID_POOP,
	[1900] = GridEntityType.GRID_ROCKB,
	[1930] = GridEntityType.GRID_SPIKES,
	[1931] = GridEntityType.GRID_SPIKES_ONOFF,
	[1940] = GridEntityType.GRID_SPIDERWEB,
	[3000] = GridEntityType.GRID_PIT,
	[4000] = GridEntityType.GRID_LOCK,
	[4500] = GridEntityType.GRID_PRESSURE_PLATE,
	[5000] = GridEntityType.GRID_STATUE,
	[5001] = {Type = GridEntityType.GRID_STATUE, Variant = 1},
	[9000] = GridEntityType.GRID_TRAPDOOR,
	[9100] = GridEntityType.GRID_STAIRS,
	[10000] = GridEntityType.GRID_GRAVITY
}

Isaac.DebugString("Loaded Open World maps")